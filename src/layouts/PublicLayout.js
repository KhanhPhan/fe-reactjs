import React from 'react'
import PropTypes from 'prop-types'
import { Card, Container } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Logo from 'assets/images/logo/logo-with-text.png'
import { publicLayoutStyle } from './style'

const PublicLayout = ({ classes, children }) => (
  <div className={classes.container}>
    <Container maxWidth="sm" className={classes.content}>
      <Card className={classes.card}>
        <div className={classes.logo}><img src={Logo} alt="background" /></div>
        {children}
      </Card>
    </Container>
  </div>
)

PublicLayout.propTypes = {
  classes: PropTypes.shape().isRequired,
  children: PropTypes.node.isRequired,
}

export default withStyles(publicLayoutStyle)(PublicLayout)
