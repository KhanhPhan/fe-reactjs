export const adminLayoutStyle = (theme) => ({
  container: {
    position: 'relative',
    display: 'flex',
    height: '100%',
    paddingLeft: theme.spacing(7),
    flex: 1,
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    background: theme.color.backgroundContent,
    '&>div:nth-child(2)': {
      overflowY: 'scroll',
      flex: 1,
    },
  },
})

export const publicLayoutStyle = (theme) => ({
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    background: theme.color.backgroundContent,
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    maxWidth: 500,
  },
  logo: {
    paddingBottom: theme.spacing(1),
    textAlign: 'center',
  },
  card: {
    width: '100%',
    padding: theme.spacing(3),
    borderRadius: theme.spacing(1.5),
    boxShadow: '0px 2px 8px rgba(0, 0, 0, 0.15) !important',
    '&>div': {
      '&:first-child': {
        marginBottom: theme.spacing(3),
      },
    },

    '& .buttonContainer': {
      marginBottom: theme.spacing(1.5),
      '&>button, a': {
        marginRight: theme.spacing(1),
      },
    },
    '& .inactive': {
      background: 'none',
    },
    '& .infoIcon': {
      marginTop: 7,
      marginRight: -theme.spacing(3),
      marginLeft: theme.spacing(1),
    },
    '&>form': {
      '&>div': {
        '&.space-between': {
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        },
      },
      '&>h1': {
        marginBottom: theme.spacing(1),
      },
      '&>h4': {
        marginBottom: theme.spacing(1),
      },
      '&>p': {
        marginBottom: theme.spacing(2),
      },
      '&>button': {
        width: '100%',
        margin: theme.spacing(1, 0, 3, 0),
      },
    },
  },
})
