const style = (theme) => ({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    flexDirection: 'column',
    background: theme.color.white,
    color: theme.color.black,
    padding: theme.spacing(1),
    boxShadow: 'inset -1px 0px 0px #E2E2EA !important',
    height: `calc(100% - ${theme.spacing(2)}px)`,
    zIndex: theme.zIndex.drawer,
    flex: '0 0 auto',
    width: theme.spacing(5),
    overflowY: 'auto',
    '&::-webkit-scrollbar': {
      display: 'none',
    },
    '& .navbar-title': {
      opacity: 0,
    },
    '& .navbar-dot': {
      opacity: 0,
    },
    '& .child-nav': {
      height: '0px !important',
      '&.with-icon': {
        height: '60px !important',
      },
    },

    '&:hover': {
      width: 256,
      '& .navbar-title': {
        opacity: 1,
        marginLeft: theme.spacing(1),
      },
      '& .navbar-dot': {
        opacity: 1,
        marginLeft: theme.spacing(-2),
      },
      '& .child-nav': {
        height: 'auto !important',
        '&.with-icon': {
          height: 'auto !important',
        },
      },
    },
  },
  modal: {
    '&> div': {
      background: 'transparent',
    },
  },
  nav: {
    height: '100%',
  },
})

export default style
