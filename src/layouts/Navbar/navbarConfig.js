import {
  Category, Home, People, Person, Store,
} from '@material-ui/icons'

const navbarConfig = [
  {
    title: 'Dashboard',
    href: '/dashboard',
    icon: Home,
  },
  {
    title: 'Merchant Management',
    href: '/merchants',
    icon: Store,
  },
  {
    title: 'User Management',
    href: '/users',
    icon: Person,
    hiddenHrefs: ['/roles'],
    children: [
      {
        title: 'Users',
        href: '/users',
      },
      {
        title: 'Roles',
        href: '/roles',
      },
    ],
  },
  {
    title: 'Member Management',
    href: '/members',
    icon: People,
  },
  {
    title: 'Category',
    href: '/categories',
    icon: Category,
  },
  {
    title: 'Campaign Management',
    href: '/campain',
    icon: Home,
  },
]

export default navbarConfig
