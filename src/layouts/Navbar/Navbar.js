import React from 'react'
import PropTypes from 'prop-types'
import { Paper, withStyles } from '@material-ui/core'
import Navigation from 'components/common/Navigation'
import navbarConfig from './navbarConfig'
import style from './style'

const NavbarContent = ({
  isDisplayFull, className, pages,
}) => (
  <nav className={className}>
    <Navigation pages={pages} data={{}} isDisplayFull={isDisplayFull} />
  </nav>
)

NavbarContent.defaultProps = {
  className: '',
}

NavbarContent.propTypes = {
  isDisplayFull: PropTypes.bool.isRequired,
  className: PropTypes.string,
  pages: PropTypes.arrayOf(PropTypes.shape()).isRequired,
}

const Navbar = ({ classes }) => (
  <Paper className={classes.root} square>
    <NavbarContent isDisplayFull pages={navbarConfig} className={classes.nav} />
  </Paper>
)

Navbar.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(Navbar)
