import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { matchPath, useLocation } from 'react-router-dom'
import { AdminRouter } from 'routers'
import PublicLayout from './PublicLayout'
import AdminLayout from './AdminLayout'

const Layout = ({ children }) => {
  const { pathname } = useLocation()
  const isAdminLayout = useMemo(
    () => AdminRouter.find((item) => matchPath(pathname, {
      path: item.path,
      exact: item.exact,
    })),
    [pathname],
  )

  if (isAdminLayout) {
    return <AdminLayout>{children}</AdminLayout>
  }
  return <PublicLayout>{children}</PublicLayout>
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
