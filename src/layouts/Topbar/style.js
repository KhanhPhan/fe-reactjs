const style = (theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottom: `1px solid ${theme.color.disableGray1}`,
    background: '#58D443',
    color: theme.color.black,
  },
  button: {
    padding: 0,
    minWidth: 0,
    marginRight: theme.spacing(1),
    borderRadius: 20,
    color: 'inherit !important',
    '&> span': {
      padding: 0,
    },
    '&:last-child': {
      marginRight: 0,
    },
    [theme.breakpoints.down('xs')]: {
      marginRight: 0,
    },
  },

  avatar: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderRadius: 20,
    cursor: 'pointer',
  },
  paper: {
    boxShadow: '0px 2px 10px #D6DEF2',
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    marginTop: 2,
  },
  list: {
    '& li': {
      padding: 0,

      '& a, span:first-child': {
        display: 'block',
        width: '100%',
        padding: '6px 16px',
      },
    },
  },
  userAvatar: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderRadius: 30,
    color: 'white',
    background: theme.color.secondaryBlue2,
    marginRight: theme.spacing(1),
  },
  userName: {
    color: 'inherit',
    fontWeight: 500,
    fontSize: 14,
    width: 100,
    wordWrap: 'break-word',
    marginRight: theme.spacing(1),
  },
  userRole: {
    fontWeight: 300,
    fontSize: 13,
  },
  userMenuItem: {
    marginTop: 8,
    marginBottom: 4,
    minWidth: 120,
    '& a': {
      color: 'inherit',
    },
  },
})

export default style
