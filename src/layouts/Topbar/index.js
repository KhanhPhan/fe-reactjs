import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  Avatar, IconButton, withStyles,
} from '@material-ui/core'
import { Menu as MenuIcon, EmailOutlined as EmailIcon } from '@material-ui/icons'
import { useLocation } from 'react-router-dom'
import User from 'assets/images/logo.png'
import style from './style'

const Topbar = ({ classes }) => {
  const location = useLocation()

  useEffect(() => {
  }, [location.pathname])

  return (
    <div className={classes.container}>
      <div style={{ width: '100%', color: '#fff' }}>
        <IconButton style={{ textAlign: 'left', color: '#fff' }}>
          <MenuIcon />
        </IconButton>
        <span style={{ width: '1px', border: 'solid 0.5px #fff', margin: '15px 5px 0 0' }} />
        <IconButton
          className={classes.button}
          aria-controls="simple-menu"
          aria-haspopup="true"
          variant="string"
        >
          <Avatar src={User} className={classes.avatar} />
        </IconButton>
        <span className={classes.userName} style={{ textAlign: 'center' }}>
          Yourmoney
        </span>
        <IconButton style={{ float: 'right', color: '#fff' }}>
          <EmailIcon />
        </IconButton>
      </div>
    </div>
  )
}

Topbar.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(Topbar)
