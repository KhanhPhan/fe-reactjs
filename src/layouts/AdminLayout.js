import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { adminLayoutStyle } from './style'
import Topbar from './Topbar'

const AdminLayout = ({ classes, children }) => (
  // <div className={classes.container}>
  <div className={classes.content}>
    <Topbar />
    <div>{children}</div>
  </div>
  // </div>
)

AdminLayout.propTypes = {
  classes: PropTypes.shape().isRequired,
  children: PropTypes.node.isRequired,
}

export default withStyles(adminLayoutStyle)(AdminLayout)
