import { transformTypeOptions } from 'utils/common'

const DEFAULT_LOGIN_PARAMS = {
  email: '',
  password: '',
}

const DEFAULT_PHONE_LOGIN_PARAMS = {
  phone: '',
  code: '',
}

const DEFAULT_RESET_PASSWORD_PARAMS = {
  email: '',
}

const DEFAULT_REGISTER_PARAMS = {
  fullName: '',
  email: '',
  phone: '',
  idCardNumber: '',
  city: '',
  district: '',
  password: '',
  confirmPassword: '',
  address: '',
  merchantInfo: {
    name: '',
    email: '',
    phone: '',
    tax: '',
    city: '',
    district: '',
    address: '',
  },
}

const REGISTER_STEP = {
  OWNER: 'owner-info',
  MERCHANT: 'merchant-info',
}

const REGISTER_STEP_OPTIONS = transformTypeOptions(REGISTER_STEP)

const DEFAULT_VERIFY_PARAMS = {
  code: '',
}

const LOGIN_TYPE = {
  OWNER: 'Owner',
  STAFF: 'Staff',
}

export {
  DEFAULT_LOGIN_PARAMS,
  DEFAULT_REGISTER_PARAMS,
  DEFAULT_RESET_PASSWORD_PARAMS,
  DEFAULT_PHONE_LOGIN_PARAMS,
  REGISTER_STEP,
  REGISTER_STEP_OPTIONS,
  DEFAULT_VERIFY_PARAMS,
  LOGIN_TYPE,
}
