import { transformTypeOptions } from 'utils/common'

// GENERAL SETTINGS

export const DEFAULT_PAGE_SIZE = 25

export const PAGE_SIZE_LIST = [10, 25, 50, 75, 100]

export const PAGE_SIZE_OPTIONS_RAW = PAGE_SIZE_LIST.map((x) => ({
  value: x,
  label: x,
}))

export const PAGE_SIZE_OPTIONS = PAGE_SIZE_LIST.map((x) => (
  { value: x, label: `${x}${x === DEFAULT_PAGE_SIZE ? ' (Default) ' : ''}` }
))

export const DEFAULT_FILTER_VIEW = 'open'

export const FILTER_VIEW_LIST = ['open', 'collapsed']

export const FILTER_VIEW_OPTIONS = transformTypeOptions(FILTER_VIEW_LIST)

export const DEFAULT_CALENDAR_VIEW = 'dayGridMonth'

export const CALENDAR_VIEW_OPTIONS = [{
  value: 'dayGridMonth',
  label: 'Monthly View',
}, {
  value: 'dayGridWeek',
  label: 'Weekly View',
}]

export const DEFAULT_PRINTING_SIZE = 'label'

export const PRINTING_SIZE_OPTIONS = [
  {
    label: 'Label (6" x 4")',
    value: 'label',
  },
  {
    label: 'Letter',
    value: 'letter',
  },
]

export const TIMEZONE_SETTING = {
  AUTO_DETECT: 'auto',
  MANUALLY: 'manual',
}

export const DEFAULT_TIMEZONE_SETTING = TIMEZONE_SETTING.AUTO_DETECT

export const DEFAULT_MANUALLY_TIMEZONE = '-8'

export const MANUALLY_TIMEZONE_OPTIONS = [
  {
    value: '+7',
    subLabel: 'UTC +7',
    label: 'Indochina Time',
    zone: 'Asia/Ho_Chi_Minh',
    country: 'Vietnam',
  },
  {
    value: '-3.5',
    subLabel: 'UTC -3.5',
    label: 'Newfoundland Standard Time',
    zone: 'Canada/Newfoundland',
    country: 'US & Canada',
  },
  {
    value: '-4', subLabel: 'UTC -4', label: 'Atlantic Standard Time', zone: 'Canada/Atlantic',
  },
  {
    value: '-5', subLabel: 'UTC -5', label: 'Eastern Standard Time', zone: 'Canada/Eastern',
  },
  {
    value: '-6', subLabel: 'UTC -6', label: 'Central Standard Time', zone: 'Canada/Central',
  },
  {
    value: '-7', subLabel: 'UTC -7', label: 'Mountain Standard Time', zone: 'Canada/Mountain',
  },
  {
    value: '-8', subLabel: 'UTC -8', label: 'Pacific Standard Time', zone: 'Canada/Pacific',
  },
  {
    value: '-9', subLabel: 'UTC -9', label: 'Alaska Standard Time', zone: 'US/Alaska',
  },
  {
    value: '-10', subLabel: 'UTC -10', label: 'Hawaii-Aleutian Standard Time', zone: 'US/Aleutian',
  },
]

export const GENERAL_SETTING_TAB = {
  DISPLAY: 'display',
  FULFILLMENT: 'fulfillment',
}

export const DURATION_OPTIONS = [
  { label: '1 min', value: 1 },
  { label: '3 min', value: 3 },
  { label: '5 min', value: 5 },
]

export const REPEAT_OPTIONS = [
  { label: 'Never', value: -1 },
  { label: 'Every 5 min', value: 5 },
  { label: 'Every 15 min', value: 15 },
  { label: 'Every 30 min', value: 30 },
  { label: 'Every hour', value: 60 },
]

export const GENERAL_SETTING_OPTIONS = transformTypeOptions(GENERAL_SETTING_TAB)
