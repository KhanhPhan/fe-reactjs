import { PROVINCE_LIST } from './province'
import { DISTRICT_LIST } from './district'

const PROVINCE_OPTIONS = PROVINCE_LIST.map((x) => ({
  value: x.name,
  label: x.name,
  code: x.code,
}))

const DISTRICT_OPTIONS = DISTRICT_LIST.map((x) => ({
  value: x.name,
  label: x.name,
  parentCode: x.parent_code,
}))

export {
  PROVINCE_OPTIONS,
  DISTRICT_OPTIONS,
}
