export const PERMISSION_NAME = {
  OWNER: 'Owner',
  MANAGER_USER: 'Manager',
}

export const DEFAULT_ROLE_FORM = {
  title: '',
  permissions: [],
}
