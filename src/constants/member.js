import { transformTypeOptions } from 'utils/common'

export const MEMBER_TABS = {
  INFO: 'member-information',
  TRANSACTION: 'transaction-history',
}

export const MEMBER_OPTIONS = transformTypeOptions(MEMBER_TABS)
