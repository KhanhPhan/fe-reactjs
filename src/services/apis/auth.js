import { convertRegisterParams } from 'utils/auth'
import endpoints from './endpoints'
import request from './request'

const login = async (data) => request.post(endpoints.auth.login, data)

const register = async (data) => request.post(endpoints.auth.register, convertRegisterParams(data))

const vefifyCode = async (data) => request.post(endpoints.auth.verify_onwner, data)

const resendCode = async (data) => request.post(endpoints.auth.resend_code, data)

const changePassword = async (data) => request.post(endpoints.password.change, data)

const getProfile = async (data) => request.get(endpoints.profile.info, data)

const updateProfile = async (data) => request.post(
  endpoints.profile.update,
  convertRegisterParams(data),
)
const authApi = {
  login,
  register,
  getProfile,
  updateProfile,
  vefifyCode,
  resendCode,
  changePassword,
}

export default authApi
