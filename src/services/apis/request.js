import axios from 'axios'
import { getURIFromObject } from 'utils/common'
import { get, set, KEYS } from 'utils/localStorage'
import endpoints, { getBaseURL } from './endpoints'

let subscribers = []
let refreshCalled = false

const subscribeTokenRefresh = (cb) => {
  subscribers.push(cb)
}

const onRefreshed = (token) => {
  subscribers.map((cb) => cb(token))
}

const axiosRequest = axios.create({
  baseURL: getBaseURL(),
  timeout: 60 * 1000,
})

axiosRequest.interceptors.request.use(
  async (req) => {
    const token = get(KEYS.TOKEN) || ''
    req.headers.Authorization = `Bearer ${token}`
    req.headers.lang = 'en'
    return req
  },
  async (error) => error,
)

axiosRequest.interceptors.response.use(
  async (response) => {
    if (response.data) {
      return response.data
    }
    return response
  },
  async ({
    response,
    message,
    config: { originalRequest },
    response: { status },
  }) => {
    if (response) {
      if (
        status === 401
        && !RegExp(`${endpoints.auth.refresh_token}$`).test(originalRequest.url)
        && get(KEYS.REFRESH_TOKEN)
      ) {
        if (!refreshCalled) {
          refreshCalled = true
          axiosRequest.post(
            endpoints.auth.refresh_token,
            {
              refresh: get(KEYS.REFRESH_TOKEN),
              uuid: get(KEYS.UUID),
            },
          ).then((res) => {
            refreshCalled = false
            const { data: { access } } = res
            onRefreshed(access)
            set(KEYS.TOKEN, access)
            subscribers = []
          }).catch(() => {
            window.location.replace('/login')
          })
        } else {
          const recallRequests = new Promise((resolve, reject) => {
            subscribeTokenRefresh((token) => {
              if (token) {
                originalRequest.headers.Authorization = `Bearer ${token}`
                resolve(axiosRequest(originalRequest))
              } else {
                const rejectedError = { response: { status: 401 } }
                reject(rejectedError)
              }
            })
          })
          return recallRequests
        }
      }
    }
    if (response?.data?.msg) throw response?.data?.msg
    throw message
  },
)

const request = ({
  get: (url, data) => axiosRequest
    .get(`${url}${getURIFromObject(data)}`),
  post: (url, data) => axiosRequest.post(url, data),
  put: (url, data) => axiosRequest.put(url, data),
  patch: (url, data) => axiosRequest.patch(url, data),
  delete: (url, data) => axiosRequest.delete(url, data),
})

export default request
