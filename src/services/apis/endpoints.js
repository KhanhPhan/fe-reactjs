export const getBaseURL = () => process.env.REACT_APP_ENDPOINTS

const endpoints = {
  auth: {
    login: '/operators/account/login',
    register: '/operators/account/sign-up',
    refresh_token: '/operators/account/new-access-token',
    verify_onwner: '/operators/account/verify-owner',
    resend_code: '/operators/account/resend-verify-owner',
  },
  profile: {
    info: '/operators/account/get-one-by-token',
    update: '/operators/account/update-by-token',
    uploadAvatar: '/operators/account/upload-s3',
  },
  password: {
    change: '/operators/account/change-operators-password',
    reset: '/operators/account/reset-merchant-password',
  },
  role: {
    list: '/operators/role/search',
    detail: '/operators/role/get-one',
    create: '/operators/role/create',
    update: '/operators/role/update',
  },
  permission: {
    list: '/operators/role/get-permission',
  },
  member: {
    list: '/operators/member/search',
    get: '/operators/member/get-one',
  },
  merchant: {
    list: '/operators/merchant/search',
    get: '/operators/merchant/get-one',
    update: '/operators/merchant/update',
    create: '/operators/merchant/create',
  },
  user: {
    list: '/operators/account/search',
    detail: '/operators/account/get-one',
    update: '/operators/account/update-profile',
  },
  category: {
    list: '/operators/category/search',
    detail: '/operators/category/get-one',
    update: '/operators/category/update',
    create: '/operators/category/create',
    delete: '/operators/category/delete',
  },
}

export default endpoints
