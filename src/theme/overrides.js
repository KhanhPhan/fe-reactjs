export default {
  MuiCard: {
    root: {
      boxShadow: 'none !important',
    },
  },
  MuiTableCell: {
    root: {
      fontSize: 14,
    },
  },
}
