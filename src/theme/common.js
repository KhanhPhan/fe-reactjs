// Double check when changing the bellow numbers

export const COLORS = {
  primaryBlue: '#0062FF',
  secondaryBlue1: '#0046B8',
  secondaryBlue2: '#B4BFD9',
  secondaryBlue3: '#EDF1FB',
  secondaryBlue4: '#4551C6',
  secondaryBlue5: '#6874CF',
  secondaryBlue6: '#8B97D8',
  secondaryBlue7: '#ADB9E1',
  secondaryBlue8: '#2D36BD',
  secondaryBlue9: '#FAFCFF',
  blueGradientStart: '#2933C5',
  blueGradientEnd: '#B6C2E3',
  white: '#FFFFFF',
  black: '#171725',
  dark: '#171F46',
  backgroundBlue: '#F8F9FF',
  backgroundGreen: '#00B79C',

  textGray: '#2E2E2E',
  textGray1: '#414141',
  textGray2: '#7f8597cf',
  textGray3: '#3B4757',
  textGray4: '#696974',
  backgroundGray: '#FAFAFB',
  backgroundGray2: '#F1F1F5',
  disableGray: '#C7C7C7',
  disableGray1: '#EAEAEA',
  disableGray2: '#F4F4F4',
  background: '#FDFDFF',
  backgroundContent: '#F5F5F7',
  darkGray: '#414141',
  grayBorder: '#E0E0E0',
  grayBorder2: '#E4E4E4',
  midGray: '#74798C',
  backgroundIcon: '#C3C3C3',

  primaryOrange: '#FA8C73',
  lightOrange: '#FA8C73',
  violet900: '#160E4D',
  violet600: '#BFC3E0',
  violet300: '#E9E7F6',
  lightViolet: '#F8F8FC',
  cantecOrange: '#FF7B5D',
  breadstackBlue: '#2834C5',
  red: '#F64066',
  lightRed: '#FFE6EC',
  green: '#73D6BA',
  lightGreen: '#EAFFF9',
  yellow: '#FFC149',
  lightYellow: '#FFF8ED',
  gray900: '#797979',
  gray600: '#C9C9C9',
  gray300: '#F8F8F8',

  midPurple: '#E9E7F6',
  lightPurple: '#F8F8FC',
  darkPurple: '#160E4D',
}

export const VIEWPORT_HEIGHT = {
  md: 768,
}

export const vh = {
  up: (key) => `@media screen and (min-height: ${
    typeof key === 'number' ? key : VIEWPORT_HEIGHT[key]
  }px)`,
  down: (key) => `@media screen and (max-height: ${
    typeof key === 'number' ? key : VIEWPORT_HEIGHT[key] - 1
  }px)`,
}
