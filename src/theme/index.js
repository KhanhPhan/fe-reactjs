import { createTheme } from '@material-ui/core'
import palette from './palette'
import typography from './typography'
import overrides from './overrides'
import { COLORS } from './common'

const theme = createTheme({
  palette,
  typography,
  overrides,
  spacing: 10,
  color: COLORS,
})

export const colors = COLORS

export default theme
