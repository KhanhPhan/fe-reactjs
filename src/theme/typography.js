import { COLORS } from './common'

export default {
  fontFamily: [
    'DM Sans',
  ].join(','),
  h1: {
    color: COLORS.black,
    fontWeight: 'bold',
    fontSize: 32,
  },
  h2: {
    color: COLORS.black,
    fontWeight: 'bold',
    fontSize: 22,
  },
  h3: {
    color: COLORS.primaryBlue,
    fontWeight: 500,
    fontSize: 18,
  },
  h4: {
    fontWeight: 500,
    fontSize: 16,
  },
  subtitle1: {
    fontWeight: 'bold',
  },
  subtitle2: {
    fontWeight: 500,
  },
  body1: {
    fontSize: 14,
    color: COLORS.textGray4,
  },
  body2: {
    fontSize: 12,
  },
}
