import { useRef } from 'react'

const useFirstLoadingDetection = (loadingFactors) => {
  const loadingCount = useRef(0)

  if (loadingCount.current <= 1 && loadingFactors.some((l) => l)) {
    loadingCount.current = 1
    return true
  }

  if (loadingCount.current === 1 && !loadingFactors.some((l) => l)) {
    loadingCount.current = 2
  }

  return false
}

export default useFirstLoadingDetection
