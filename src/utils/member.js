import moment from 'moment'
import { getGender } from './common'

export const convertMemberList = (data) => data

export const convertMemberDetail = ({
  gender, dob, ...data
}) => ({
  ...data,
  gender: getGender(gender),
  dob: moment(dob).format('DD/MM/YYYY'),
})
