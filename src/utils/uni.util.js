/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable consistent-return */
/* eslint-disable max-len */
/* eslint-disable no-useless-catch */
/* eslint-disable radix */
/* eslint-disable no-shadow */
/* eslint-disable no-return-await */
/* eslint-disable no-use-before-define */
const bip39 = require('bip39')
const crypto = require('crypto')
const { hdkey } = require('ethereumjs-wallet')
const ExactMath = require('exact-math')
const Unichain = require('@uniworld/unichain-js')

const host = process.env.MAINNET_UNICHAIN_HOST
const relayHost = process.env.MAINNET_UNICHAIN_RELAY_HOST
const unichain = new Unichain({
  fullHost: 'https://node.unichain.world',
  solidityNode: 'https://relay-node.unichain.world',
  privateKey: process.env.REACT_APP_UWN_ACCCOUNT_PRIVATE_KEY,
})

function generateMnemonic() {
  const randomBytes = crypto.randomBytes(32)
  const mnemonic = bip39.entropyToMnemonic(randomBytes.toString('hex'))
  return mnemonic
}

function generateAccount() {
  const mnemonic = generateMnemonic()
  const privateKey = generatePrivKey(mnemonic).toString('hex')
  const address = unichain.address.fromPrivateKey(privateKey)
  return {
    privateKey,
    address,
  }
}

function generateSeed(mnemonic) {
  return bip39.mnemonicToSeedSync(mnemonic)
}

function generatePrivKey(mnemonic) {
  const seed = generateSeed(mnemonic)
  return hdkey.fromMasterSeed(seed).derivePath('m/44\'/60\'/0\'/0/0').getWallet().getPrivateKey()
}

async function getBlock(blockNumber) {
  return await unichain.unx.getBlockByNumber(blockNumber)
}

async function getLatestBlockNumber() {
  const current = await unichain.unx.getCurrentBlock()
  return current
}

function fromHex(hexAddress) {
  return unichain.address.fromHex(hexAddress)
}

function toHex(hexAddress) {
  return unichain.address.toHex(hexAddress)
}

const getContract = async (address) => await unichain.contract().at(address)

const balanceUnwOf = async (address) => {
  try {
    const results = await unichain.unx.getAccount(address)
    console.log(results)
    // unw lock
    let total = 0
    if (results.frozen) {
      const now = Date.now()
      results.frozen.forEach((item) => {
        if (item.expire_time > now) {
          total = ExactMath.add(total, item.frozen_balance)
        }
      })
    }
    // unw vote
    let votes = 0
    if (results.votes) {
      results.votes.forEach((item) => {
        votes = ExactMath.add(votes, item.vote_count)
      })
    }
    console.log(`balance of ${address} = `, {
      status: true,
      balance: ExactMath.div(results.balance ? results.balance : 0, 10 ** 6),
      assets: results.asset ? results.asset : [],
      lock: total,
      vote_count: votes,
    })

    return {
      status: true,
      balance: ExactMath.div(results.balance ? results.balance : 0, 10 ** 6),
      assets: results.asset ? results.asset : [],
      lock: total,
      vote_count: votes,
    }
  } catch (error) {
    console.log(error)
    return {
      status: false,
      balance: 0,
      assets: [],
      lock: 0,
      vote_count: 0,
    }
  }
}

async function createTokenContract(privateKey, tokenAddress) {
  const host = process.env[`${process.env.MODE}_UNICHAIN_HOST`]
  const relayHost = process.env[`${process.env.MODE}_UNICHAIN_RELAY_HOST`]
  const unichain = new Unichain({
    fullHost: host,
    solidityNode: relayHost,
    privateKey,
  })

  return await unichain.contract().at(tokenAddress)
}

async function transferToken(privateKey, tokenAddress, to, amount) {
  const tokenContract = await createTokenContract(privateKey, tokenAddress)
  console.log('amount = ', parseInt(amount * 10 ** 6))
  const result = await tokenContract.transfer(to, parseInt(amount * 10 ** 6)).send({})
  return result
}

const balanceTokenOf = async (tokenAddress, address) => {
  try {
    const tokenContract = await getContract(tokenAddress)
    const balance = await tokenContract.balanceOf(address).call()
    console.log('balanceTokenOf = ', ExactMath.div(balance.toString(), 10 ** 6))
    return ExactMath.div(balance.toString(), 10 ** 6)
  } catch (error) {
    throw error
  }
}
const toUNW = (mUNW) => mUNW * 10 ** 6
const transferUnw = async (privateKey, fromAddress, toAddress, amount) => {
  try {
    const unichain = new Unichain({
      fullHost: host,
      solidityNode: relayHost,
    })
    amount = parseInt(toUNW(amount)) // Check amount
    const data = {
      to_address: unichain.address.toHex(toAddress),
      owner_address: unichain.address.toHex(fromAddress),
      amount,
    }
    const unsingedTx = await unichain.currentProviders().fullNode.request('wallet/createtransaction', data, 'post')
    const signedTx = await unichain.unx.signTransaction(unsingedTx, privateKey, 0)
    const res = await unichain.unx.sendRawTransaction(signedTx)
    return res
  } catch (error) {
    console.log(error)
  }
}

const getAccount = async (address) => {
  console.log(address)
  console.log(Unichain.address.toHex(address))
  return await unichain.unx.getAccount(address)
}

const getBalance = async (address) => await unichain.unx.getBalance(address)
// balanceUnwOf("UYYyJdbxw9qefcXQUXA9H8fno7buL6x2wE")

export const walletUtils = {
  transferUnw,
  generateAccount,
  getBlock,
  getLatestBlockNumber,
  fromHex,
  toHex,
  transferToken,
  balanceUnwOf,
  balanceTokenOf,
  generateMnemonic,
  generatePrivKey,
  getAccount,
  getBalance,
}
