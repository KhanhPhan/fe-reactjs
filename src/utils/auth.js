export const convertRegisterParams = ({ email, phone, password }) => ({
  email,
  password,
  phone: `+${phone}`,
})
