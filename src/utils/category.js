export const convertCategoryList = (list) => list

export const convertCategoryDetail = (data) => data

export const convertCategoryParams = ({
  _id, name, description, icon,
}) => ({
  name,
  description,
  icon,
  ...(_id ? { id: _id } : {}),
})
