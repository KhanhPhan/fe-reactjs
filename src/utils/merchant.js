export const convertMerchantList = (data) => data
export const convertMerchant = (data) => data
export const convertMerchantInfoParams = ({
  phone, dob,
  fullName,
  gender,
  avatar,
}) => ({
  phone,
  dob: dob || '',
  fullname: fullName,
  gender,
  avatar: avatar || '',
})
export const convertCreateMerchantInfoParams = ({
  name,
  email,
  shortDescription,
  address,
  phone,
  tax,
  description,
  avatar,
}) => ({
  name,
  email,
  shortDescription,
  address,
  phone: `+${phone}`,
  tax,
  description,
  avatar,
})

export const convertUpdateMerchantInfoParams = ({
  name,
  email,
  shortDescription,
  address,
  phone,
  tax,
  description,
  id,
  avatar,
}) => ({
  name,
  email,
  shortDescription,
  address,
  phone: `+${phone}`,
  tax,
  description,
  id,
  avatar,
})
