export const convertProfileDetail = (data) => data

export const convertProfileParams = ({
  accountName, fullName, email, phone, avatar,
}) => ({
  accountName, fullName, email, phone: `+${phone}`, avatar,
})

export const convertUpdatedProfile = (data) => data
