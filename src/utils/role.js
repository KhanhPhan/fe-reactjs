import { PERMISSION_NAME } from 'constants/role'

export const convertPermissions = (data) => (data || []).map((value) => ({
  value,
  label: PERMISSION_NAME[value],
}))

export const convertRoleList = ({ data, ...props }) => ({
  ...props,
  data: data.map(({ permissions, ...itemProps }) => ({
    ...itemProps,
    permissions: permissions.map((x) => PERMISSION_NAME[x]).join(', '),
  })),
})

export const convertPermissionList = (data) => Object.values(data?.data || {})
  .map((value) => ({ value, label: PERMISSION_NAME[value] }))

export const convertRoleParams = ({ permissions, ...data }) => ({
  ...data,
  permissions: permissions.map(({ value }) => value),
})

export const convertRoleDetail = ({ permissions, ...data }) => ({
  ...data,
  permissions: convertPermissions(permissions),
})
