import { object, string, ref } from 'yup'

const LoginSchema = object().shape({
  email: string().trim().email('Invalid email')
    .required('Email is required'),
  password: string().trim()
    .min(8, 'Pasword must be more than 8 characters long')
    .required('Password is required'),
})

const PhoneLoginSchema = object().shape({
})

const ResetPasswordSchema = object().shape({
  email: string().trim().email('Invalid email')
    .required('Email is required'),
})

const RegisterOwnerSchema = object().shape({
  fullName: string().trim()
    .required('Name is required'),
  email: string().trim().email('Invalid email')
    .required('Email is required'),
  phone: string()
    .required('Phone Number is required'),
  idCardNumber: string().trim()
    .required('Id card number required'),
  password: string().trim()
    .min(8, 'Pasword must be more than 8 characters long')
    .required('Password is required'),
  confirmPassword: string()
    .oneOf([ref('password')], 'Passwords must match')
    .required('Confirm Password is required.'),
})

const RegisterMerchantSchema = object().shape({
  merchantInfo: object().shape({
    name: string().trim()
      .required('Name is required'),
    email: string().trim().email('Invalid email')
      .required('Email is required'),
    phone: string()
      .required('Phone Number is required'),
    tax: string().trim()
      .required('Tax ID number is required'),
    city: string().trim()
      .required('City is required'),
    district: string().trim()
      .required('District is required'),
    address: string().trim()
      .required('Address is required.'),
  }),
})

const VerifyEmailSchema = object().shape({
})

export {
  LoginSchema,
  ResetPasswordSchema,
  PhoneLoginSchema,
  RegisterOwnerSchema,
  RegisterMerchantSchema,
  VerifyEmailSchema,
}
