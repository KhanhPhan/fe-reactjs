import { object, string } from 'yup'

const CategorySchema = object().shape({
  name: string().trim()
    .required('Name is required'),
  description: string()
    .required('Description is required'),
  icon: string().trim()
    .required('Icon is required'),
})

export {
  CategorySchema,
}
