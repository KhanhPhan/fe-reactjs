import { object, string, array } from 'yup'

const RoleSchema = object().shape({
  title: string().trim()
    .required('Title is required'),
  permissions: array().min(1, 'Select atleast 1 permission'),
})

export {
  RoleSchema,
}
