import { object, string } from 'yup'

const UserSchema = object().shape({
  fullName: string().trim()
    .required('Name is required'),
  email: string().trim().email('Invalid email')
    .required('Email is required'),
})

export {
  UserSchema,
}
