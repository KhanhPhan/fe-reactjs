import { object, string } from 'yup'

const UpdateMerchantInfoSchema = object().shape({
  name: string().trim()
    .required('Full name is required'),
  phone: string().trim()
    .required('Phone is required'),
})

export {
  UpdateMerchantInfoSchema,
}
