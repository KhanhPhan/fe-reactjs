import React from 'react'
import {
  withStyles,
  FormControl,
  Input,
  InputAdornment,
} from '@material-ui/core'
import { Search } from '@material-ui/icons'
import PropTypes from 'prop-types'
import style from './style'

const Category = ({ classes }) => (
  <div className={classes.title}>
    <FormControl style={{ width: '100%' }}>
      <Input
        id="input-with-icon-adornment"
        startAdornment={(
          <InputAdornment position="start">
            <Search />
          </InputAdornment>
        )}
      />
    </FormControl>
  </div>
)

Category.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(Category)
