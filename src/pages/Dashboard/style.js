const style = (theme) => ({
  container: {
    borderBottom: `1px solid ${theme.color.disableGray1}`,
    background: '#58D443',
    color: theme.color.black,
    padding: theme.spacing(1, 2),

    [theme.breakpoints.down('md')]: {
      padding: '14px 16px',
    },
  },
  title: {
    borderLeft: 'solid 1px green',
    paddingLeft: '10px',
    fontSize: '24px',
    marginBottom: '10px',
  },
})

export default style
