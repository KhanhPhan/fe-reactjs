import React from 'react'
import Bg from 'assets/images/bg.png'
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types'
import style from './style'

const Dashboard = ({ classes }) => (
  <div style={{ fontFamily: 'Roboto' }}>
    <div>
      <img src={Bg} alt="Ảnh bìa" />
    </div>
    <div className={classes.container} style={{ background: '#fff' }}>
      <div className={classes.title}>
        Vể Yourmoney
      </div>
      <div>
        Với tuyên ngôn &quot;
        <b>Quản lý tài chính cá nhân một cách hiệu quả hơn</b>
        &quot;,
        Yourmoney luôn nỗ lực mang đến các giải pháp quản lý tài chính hiệu quả nhất
        đến người sử dụng.
        Lấy chất lượng dịch vụ và đổi mới sáng tạo làm trụ cột,
        Yourmoney chú trọng các sản phẩm hiện đại,
        mang bản sắc riêng hướng đến vị thế
        <b> ứng dụng quản lý tài chính cá nhân số 1 Việt Nam</b>
        .
      </div>
    </div>
  </div>
)

Dashboard.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(Dashboard)
