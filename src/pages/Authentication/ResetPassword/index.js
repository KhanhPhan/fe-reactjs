import ResetPasswordForm from './ResetPasswordForm'
import ResetPasswordSuccess from './ResetPasswordSuccess'

export {
  ResetPasswordForm,
  ResetPasswordSuccess,
}
