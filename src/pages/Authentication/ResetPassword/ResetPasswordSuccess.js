import React from 'react'
import { Typography } from '@material-ui/core'

const ResetPasswordSuccess = () => (
  <>
    <Typography variant="h1">Password Reset Email Sent!</Typography>
    <br />
    <div>Please check your inbox for a reset email from us.</div>
    <br />
    <div>
      *If you have not received the email, please check the spam box. Or you can choose to&nbsp;
      <a href="/#">resent the email</a>
      .
    </div>
  </>
)

export default ResetPasswordSuccess
