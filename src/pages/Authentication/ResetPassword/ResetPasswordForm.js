import React from 'react'
import { Box, Typography } from '@material-ui/core'
import { Form, Formik } from 'formik'
import { Link } from 'react-router-dom'
import Button from 'components/common/Button'
import { FastField } from 'components/common/Formik'
import { DEFAULT_RESET_PASSWORD_PARAMS } from 'constants/auth'
import { ResetPasswordSchema } from 'utils/schema/auth'

const ResetPasswordForm = () => (
  <Formik
    initialValues={DEFAULT_RESET_PASSWORD_PARAMS}
    onSubmit={() => {}}
    validationSchema={ResetPasswordSchema}
  >
    {
      () => (
        <Form>
          <Typography variant="h1">Reset Password</Typography>
          <Typography variant="body1">
            Enter your registered email below.
            We’ll send you a password recover instructions to your email.
          </Typography>
          <FastField.TextInput name="email" placeholder="Email" />
          <Button type="submit">Next</Button>
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            Go back to
            &nbsp;
            <Link to="/login">Sign in</Link>
          </Box>
        </Form>
      )
    }
  </Formik>
)

export default ResetPasswordForm
