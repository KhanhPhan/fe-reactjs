import React from 'react'
import {
  Typography,
} from '@material-ui/core'
import { Form, Formik } from 'formik'
import { Link, useHistory } from 'react-router-dom'
import Button from 'components/common/Button'
import { Field } from 'components/common/Formik'
import { PhoneLoginSchema } from 'utils/schema/auth'
import { useAuth } from 'redux/hooks/auth'
import { DEFAULT_PHONE_LOGIN_PARAMS } from 'constants/auth'

const LoginWithPhone = () => {
  const {
    actions, data, loading, error,
  } = useAuth()
  const history = useHistory()

  const onSubmit = () => {
    // actions.login(values, {
    //   onSuccess: () => history.push('/'),
    // })
    // LOGIN SUCCESS:
    history.push('/')
  }

  const onSendCode = (phone) => {
    actions.loginWithPhone({ phone })
  }

  return (
    <Formik
      initialValues={DEFAULT_PHONE_LOGIN_PARAMS}
      onSubmit={onSubmit}
      validationSchema={PhoneLoginSchema}
      enableReinitialize
    >
      {
        ({ values }) => (
          <Form>
            <Typography variant="h1">Sign in</Typography>
            <Typography variant="body1">Log in with your Email or Phone number</Typography>
            <div className="buttonContainer">
              <Link
                to="/login"
              >
                <Button
                  color="secondary"
                  variant="contained"
                  className="phoneButton"
                >
                  Email
                </Button>
              </Link>
              <Button
                color="secondary"
                variant="contained"
              >
                Phone
              </Button>
            </div>
            <Field.PhoneNumber
              name="phone"
              buttonLabel={data.phone ? 'Resend' : 'Get Code'}
              buttonLoading={loading}
              onButtonClick={() => onSendCode(values.phone)}
            />
            {
              data.phone && (
                <>
                  <br />
                  <div>Enter your passcode:</div>
                  <Field.CodeInput
                    name="code"
                    error={!!error}
                    helperText={error}
                  />
                  <Button type="submit" loading={loading}>Login</Button>
                </>
              )
            }
          </Form>
        )
      }
    </Formik>
  )
}

LoginWithPhone.propTypes = {
}

export default LoginWithPhone
