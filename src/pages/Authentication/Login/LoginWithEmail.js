import React from 'react'
import {
  Box,
} from '@material-ui/core'
import { Form, Formik } from 'formik'
import { Link, useHistory } from 'react-router-dom'
import Button from 'components/common/Button'
import { Field } from 'components/common/Formik'
import { LoginSchema } from 'utils/schema/auth'
import { useAuth } from 'redux/hooks/auth'
import Tooltip, { TooltipPasswordContent } from 'components/common/Tooltip'
import InfoIcon from 'assets/images/icons/info.svg'
import { DEFAULT_LOGIN_PARAMS } from 'constants/auth'

const LoginWithEmail = () => {
  const { actions, loading, error } = useAuth()
  const history = useHistory()

  const onFail = () => {}

  const onSubmit = (values) => {
    actions.login(values, {
      onSuccess: () => history.push('/'),
      onFail,
    })
  }

  return (
    <Formik
      initialValues={DEFAULT_LOGIN_PARAMS}
      onSubmit={onSubmit}
      validationSchema={LoginSchema}
      enableReinitialize
    >
      {
        () => (
          <Form>
            <Field.TextInput
              name="email"
              legend="Email"
              placeholder="Email"
              error={!!error}
            />
            <Field.TextInput
              name="password"
              placeholder="Password"
              legend={(
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                  <span>Password</span>
                  <Link to="/reset-password">Forgot Password?</Link>
                </Box>
              )}
              type="password"
              error={!!error}
              helperText={error}
              renderRightItem={(
                <Tooltip title={<TooltipPasswordContent />}>
                  <img
                    width={20}
                    height={20}
                    src={InfoIcon}
                    alt="info"
                    className="infoIcon"
                  />
                </Tooltip>
              )}
            />
            <Button type="submit" loading={loading}>Login</Button>
          </Form>
        )
      }
    </Formik>
  )
}

LoginWithEmail.propTypes = {
}

export default LoginWithEmail
