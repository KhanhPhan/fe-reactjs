import React from 'react'
import { Typography } from '@material-ui/core'

const RegisterSuccess = () => (
  <>
    <Typography variant="h1">Thank you for signing up!</Typography>
    <br />
    <div>Please check your inbox for a verification email from us.</div>
    <br />
    <div>
      *If you have not received the email, please check the spam box. Or you can choose to&nbsp;
      <a href="/#">resent the email</a>
      .
    </div>
  </>
)

export default RegisterSuccess
