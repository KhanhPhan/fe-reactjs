import React from 'react'
import { Typography } from '@material-ui/core'
import Tooltip, { TooltipPasswordContent } from 'components/common/Tooltip'
import InfoIcon from 'assets/images/icons/info.svg'
import { useAuth } from 'redux/hooks/auth'
import { FastField } from 'components/common/Formik'

const OwnerInfo = () => {
  const { error } = useAuth()

  return (
    <>
      <Typography variant="h4">Contact Info</Typography>
      <FastField.TextInput
        name="fullName"
        placeholder="Owner Name"
      />
      <FastField.TextInput
        name="email"
        placeholder="Owner Email"
      />
      <FastField.PhoneNumber
        name="phone"
        placeholder="Owner Phone Number"
      />
      <Typography variant="h4">Legal Representative Info</Typography>
      <FastField.TextInput
        name="idCardNumber"
        placeholder="ID Card Number"
      />
      <Typography variant="h4">Password</Typography>
      <FastField.TextInput
        name="password"
        placeholder="Password"
        type="password"
        renderRightItem={(
          <Tooltip title={<TooltipPasswordContent />}>
            <img
              width={20}
              height={20}
              src={InfoIcon}
              alt="info"
              className="infoIcon"
            />
          </Tooltip>
        )}
      />
      <FastField.TextInput
        name="confirmPassword"
        placeholder="Confirm Password"
        type="password"
        error={error}
        helperText={error}
      />
    </>
  )
}

OwnerInfo.propTypes = {
}

export default OwnerInfo
