import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { Box, Typography } from '@material-ui/core'
import { Form, Formik } from 'formik'
import { Link, useHistory } from 'react-router-dom'
import Button from 'components/common/Button'
import {
  DEFAULT_REGISTER_PARAMS,
  REGISTER_STEP,
  REGISTER_STEP_OPTIONS,
} from 'constants/auth'
import { RegisterMerchantSchema, RegisterOwnerSchema } from 'utils/schema/auth'
import { useAuth } from 'redux/hooks/auth'
import Tabs from 'components/common/Tabs'
import OwnerInfo from './OwnerInfo'
import MerchantInfo from './MerchantInfo'

const Register = ({ step }) => {
  const { actions, loading } = useAuth()
  const history = useHistory()

  const onSubmit = (values, { setTouched }) => {
    setTouched({})
    if (step === REGISTER_STEP.OWNER) {
      history.push(`/register/${REGISTER_STEP.MERCHANT}`)
    } else {
      actions.register(values, {
        onSuccess: () => history.push(`/verify-email?email=${values.email}`),
      })
    }
  }

  const currentSchema = useMemo(() => {
    switch (step) {
      case REGISTER_STEP.MERCHANT:
        return RegisterMerchantSchema
      case REGISTER_STEP.OWNER:
      default:
        return RegisterOwnerSchema
    }
  }, [step])

  const currentForm = useMemo(() => {
    switch (step) {
      case REGISTER_STEP.MERCHANT:
        return <MerchantInfo />
      case REGISTER_STEP.OWNER:
      default:
        return <OwnerInfo />
    }
  }, [step])

  return (
    <Formik
      initialValues={DEFAULT_REGISTER_PARAMS}
      onSubmit={onSubmit}
      validationSchema={currentSchema}
      enableReinitialize
    >
      {
        () => (
          <Form>
            <Typography variant="h1">Sign up</Typography>
            <Tabs
              tabs={REGISTER_STEP_OPTIONS}
              value={step}
              withNumber
              fullWidth
              style={{ marginBottom: 16 }}
            />
            {currentForm}
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                marginBottom: '16px !important',
              }}
            >
              I agree to the
              &nbsp;
              <Link to="/terms-of-use">Terms of use</Link>
              &nbsp;
              and
              &nbsp;
              <Link to="/privacy-policy">Privacy policy</Link>
            </Box>
            <Button
              loading={loading}
              type="submit"
            >
              {step === REGISTER_STEP.OWNER ? 'Next' : 'Sign Up'}
            </Button>
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
              Go back to
              &nbsp;
              <Link to="/login">Sign in</Link>
            </Box>
          </Form>
        )
      }
    </Formik>
  )
}

Register.defaultProps = {
  step: REGISTER_STEP.OWNER,
}

Register.propTypes = {
  step: PropTypes.string,
}

export default Register
