import React, { useEffect, useMemo } from 'react'
import { Typography } from '@material-ui/core'
import { useFormikContext } from 'formik'
import { useHistory } from 'react-router-dom'
import { useAuth } from 'redux/hooks/auth'
import { FastField, Field } from 'components/common/Formik'
import { DISTRICT_OPTIONS, PROVINCE_OPTIONS } from 'constants/location'

const MerchantInfo = () => {
  const { error } = useAuth()
  const { values } = useFormikContext()
  const history = useHistory()

  useEffect(() => {
    if (!values.fullName) {
      history.push('/register')
    }
  }, [values.fullName])

  const provinceOptions = useMemo(
    () => DISTRICT_OPTIONS.filter((x) => !values.merchantInfo.city
    || x.parentCode === PROVINCE_OPTIONS.find((y) => y.value === values.merchantInfo.city).code),
    [values.merchantInfo.city],
  )

  return (
    <>
      <Typography variant="h4">General Info</Typography>
      <FastField.TextInput
        name="merchantInfo.name"
        placeholder="Merchant Name *"
      />
      <FastField.TextInput
        name="merchantInfo.email"
        placeholder="Merchant Email"
      />
      <FastField.PhoneNumber
        name="merchantInfo.phone"
        placeholder="Owner Phone Number"
      />
      <Typography variant="h4">Legal Representative Info</Typography>
      <FastField.TextInput
        name="merchantInfo.tax"
        placeholder="Tax Identification Number"
      />
      <Typography variant="h4">Merchant Address</Typography>
      <Field.Dropdown
        name="merchantInfo.city"
        placeholder="City *"
        autoComplete="new-password"
        options={PROVINCE_OPTIONS}
      />
      <Field.Dropdown
        name="merchantInfo.district"
        placeholder="District *"
        autoComplete="new-password"
        options={provinceOptions}
      />
      <Field.TextInput
        name="merchantInfo.address"
        placeholder="Street name, Building, House no. *"
        error={!!error}
        helperText={error}
      />
    </>
  )
}

MerchantInfo.propTypes = {
}

export default MerchantInfo
