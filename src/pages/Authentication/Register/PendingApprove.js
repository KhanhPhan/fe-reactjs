import React from 'react'
import PropTypes from 'prop-types'
import { Container, Typography, withStyles } from '@material-ui/core'
import { Link } from 'react-router-dom'
import PendingApproveImage from 'assets/images/icons/pending_approve.png'
import Button from 'components/common/Button'
import style from './style'

const PendingApprove = ({ classes }) => (
  <Container maxWidth="xs" className={classes.container}>
    <img src={PendingApproveImage} alt="pending" />
    <Typography variant="h2">
      Your Account Is Now
      <br />
      Being Processed
    </Typography>
    <div>
      Please wait for our approval or you can contact your
      <br />
      merchant for further information.
    </div>
    <Link to="/login">
      <Button
        variant="contained"
      >
        Back to Home
      </Button>
    </Link>
  </Container>
)

PendingApprove.propTypes = {
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(PendingApprove)
