import React from 'react'
import {
  Typography,
} from '@material-ui/core'
import { Form, Formik } from 'formik'
import { useHistory } from 'react-router-dom'
import Button from 'components/common/Button'
import { Field } from 'components/common/Formik'
import { VerifyEmailSchema } from 'utils/schema/auth'
import { useAuth } from 'redux/hooks/auth'
import { DEFAULT_VERIFY_PARAMS } from 'constants/auth'
import { get, KEYS } from 'utils/localStorage'

const VerifyEmail = () => {
  const {
    actions, loading, error,
  } = useAuth()

  const history = useHistory()
  const onSubmit = (values) => {
    actions.verifyCode({ numberVerify: values.code, uuid: get(KEYS.UUID) }, {
      onSuccess: () => history.push('/login'),
    })
  }

  const resendCode = () => {
    const { search } = history.location
    const params = new URLSearchParams(search)
    const email = params.get('email')
    actions.resendCode({ email }, {
      onSuccess: () => history.push('/login'),
    })
  }

  return (
    <Formik
      initialValues={DEFAULT_VERIFY_PARAMS}
      onSubmit={onSubmit}
      validationSchema={VerifyEmailSchema}
      enableReinitialize
    >
      {
        () => (
          <Form>
            <Typography variant="h1">Verify Email</Typography>
            <Typography variant="body1">
              Please enter verification code that was sent to your email:
            </Typography>
            <Field.CodeInput
              name="code"
              error={!!error}
              helperText={error}
            />
            <br />
            <div className="buttonContainer">
              <Button type="submit" loading={loading}>Verify</Button>
              <Button
                type="button"
                loading={loading}
                onClick={() => { resendCode() }}
              >
                Resend
              </Button>
            </div>

          </Form>
        )
      }
    </Formik>
  )
}

VerifyEmail.propTypes = {
}

export default VerifyEmail
