import RegisterForm from './RegisterForm'
import RegisterPhoneForm from './RegisterPhoneForm'
import RegisterSuccess from './RegisterSuccess'
import VerifyEmail from './VerifyEmail'
import PendingApprove from './PendingApprove'

export {
  RegisterForm,
  RegisterPhoneForm,
  RegisterSuccess,
  VerifyEmail,
  PendingApprove,
}
