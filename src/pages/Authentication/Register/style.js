const style = (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& img': {
      maxWidth: 140,
      marginBottom: theme.spacing(2),
    },
    '& h2': {
      textAlign: 'center',
      marginBottom: theme.spacing(2),
    },
    '& div': {
      marginBottom: theme.spacing(4),
      textAlign: 'center',
    },
    '& button, & a': {
      width: '100%',
    },
  },
})

export default style
