import React, { useEffect } from 'react'
import { Typography } from '@material-ui/core'
import { useHistory } from 'react-router-dom'

const Logout = () => {
  const history = useHistory()

  useEffect(() => {
    setTimeout(() => {
      history.push('/login')
    }, 2000)
  }, [])

  return (
    <Typography
      variant="h2"
      style={{ textAlign: 'center' }}
    >
      Loging out
      <span className="loader__dot">.</span>
      <span className="loader__dot">.</span>
      <span className="loader__dot">.</span>
    </Typography>

  )
}
Logout.propTypes = {
}

export default Logout
