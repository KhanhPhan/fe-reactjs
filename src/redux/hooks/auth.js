import { useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { authActions } from '../actions/auth'

export const useAuth = () => {
  const { loading, data, error } = useSelector((state) => state.auth)

  const dispatch = useDispatch()

  const actions = useMemo(() => bindActionCreators(authActions, dispatch), [dispatch])

  return {
    actions, loading, data, error,
  }
}
