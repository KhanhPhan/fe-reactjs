import { combineReducers } from '@reduxjs/toolkit'
import auth from './auth'
import profile from '../queries/profile'
import role from '../queries/role'
import member from '../queries/member'
import merchant from '../queries/merchant'
import user from '../queries/user'
import category from '../queries/category'

const combinedReducers = combineReducers({
  auth,
  profile,
  [profile.reducerPath]: profile.reducer,
  [role.reducerPath]: role.reducer,
  [member.reducerPath]: member.reducer,
  [merchant.reducerPath]: merchant.reducer,
  [user.reducerPath]: user.reducer,
  [category.reducerPath]: category.reducer,
})

export default (state, action) => {
  if (action.type === 'auth/logout') {
    state = {}
  }
  return combinedReducers(state, action)
}
