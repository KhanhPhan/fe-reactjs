import { createSlice } from '@reduxjs/toolkit'
import { set, KEYS } from 'utils/localStorage'

const initialState = {
  loading: false,
  data: {},
  error: null,
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    resetState: (state) => {
      state.loading = false
      state.data = {}
      state.error = null
    },
    login: (state) => {
      state.loading = true
      state.error = null
    },
    loginOk: (state, action) => {
      state.loading = false
      state.data = action.payload
      state.error = null
      set(KEYS.TOKEN, action.payload?.data?.token?.accessToken)
      set(KEYS.REFRESH_TOKEN, action.payload?.data?.refreshToken)
      set(KEYS.UUID, action.payload?.data?.uuid)
    },
    loginErr: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    sendCode: (state) => {
      state.loading = true
      state.error = null
    },
    sendCodeOk: (state, action) => {
      state.loading = false
      state.data = action.payload
    },
    sendCodeErr: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    resendCode: (state) => {
      state.loading = true
      state.error = null
    },
    resendCodeOk: (state, action) => {
      state.loading = false
      state.data = action.payload
    },
    resendCodeErr: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    register: (state) => {
      state.loading = true
    },
    registerOk: (state, action) => {
      state.loading = false
      set(KEYS.UUID, action.payload?.data?.uuid)
    },
    registerErr: (state, action) => {
      state.loading = false
      state.error = action.payload || 'Unable to Register'
    },
    changePassword: (state) => {
      state.loading = true
      state.error.changePassword = null
    },
    changePasswordOk: (state) => {
      state.loading = false
      state.error.changePassword = null
    },
    changePasswordErr: (state, action) => {
      state.loading = false
      state.error.changePassword = action.payload || 'Unable to change password'
    },
  },
})

export const authReduxActions = authSlice.actions

export default authSlice.reducer
