import authApi from 'services/apis/auth'
import { authReduxActions } from '../reducers/auth'
import { apiAction } from '../common/builder'

const login = (data, callback) => apiAction(
  authApi.login(data),
  [authReduxActions.login, authReduxActions.loginOk, authReduxActions.loginErr],
  callback,
  {},
)

const loginWithPhone = (data) => (dispatch) => {
  dispatch(authReduxActions.sendCodeOk(data))
}

export const authActions = {
  login,
  loginWithPhone,
}
