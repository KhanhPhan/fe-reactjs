import authApi from 'services/apis/auth'
import { authReduxActions } from '../reducers/auth'
import { apiAction } from '../common/builder'

const login = (data, callback) => apiAction(
  authApi.login(data),
  [authReduxActions.login, authReduxActions.loginOk, authReduxActions.loginErr],
  callback,
  {},
)

const register = (data, callback) => apiAction(
  authApi.register(data),
  [authReduxActions.register, authReduxActions.registerOk, authReduxActions.registerErr],
  callback,
  {},
)

const loginWithPhone = (data) => (dispatch) => {
  dispatch(authReduxActions.sendCodeOk(data))
}

const verifyCode = (data, callback) => apiAction(
  authApi.vefifyCode(data),
  [authReduxActions.sendCode, authReduxActions.sendCodeOk, authReduxActions.sendCodeErr],
  callback,
  {},
)

const resendCode = (data) => apiAction(
  authApi.resendCode(data),
  [authReduxActions.resendCode, authReduxActions.resendCodeOk, authReduxActions.resendCodeErr],
  callback,
  {},
)

const changePassword = (data, callback) => apiAction(
  authApi.changePassword(data),
  [authReduxActions.changePassword,
    authReduxActions.changePasswordOk,
    authReduxActions.changePasswordErr],
  callback,
  {},
)

const resetState = () => (dispatch) => {
  dispatch(authReduxActions.resetState())
}

export const authActions = {
  login,
  register,
  loginWithPhone,
  verifyCode,
  resendCode,
  changePassword,
  resetState,
}
