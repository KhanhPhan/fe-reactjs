import { createApi } from '@reduxjs/toolkit/query/react'
import endpoints from 'services/apis/endpoints'
import {
  convertMerchantList,
  convertCreateMerchantInfoParams,
  convertUpdateMerchantInfoParams,
} from 'utils/merchant'
import { baseQueryWithAuth } from '.'

const merchantApi = createApi({
  reducerPath: 'merchantApi',
  baseQuery: baseQueryWithAuth,
  endpoints: (builder) => ({
    getMerchants: builder.query({
      query: ({ keyword = '' }) => `${endpoints.merchant.list}?keyword=${keyword}`,
      transformResponse: (response) => convertMerchantList(response),
    }),
    getMerchantById: builder.query({
      query: ({ id = '' }) => `${endpoints.merchant.get}?_id=${id}`,
      transformResponse: (response) => response,
    }),
    updateMerchantInfo: builder.mutation({
      query(data) {
        return {
          url: `${endpoints.merchant.update}`,
          method: 'POST',
          body: convertUpdateMerchantInfoParams(data),
        }
      },
      transformResponse: (response) => response,
    }),
    createMerchantInfo: builder.mutation({
      query(data) {
        return {
          url: `${endpoints.merchant.create}`,
          method: 'POST',
          body: convertCreateMerchantInfoParams(data),
        }
      },
      transformResponse: (response) => response,
    }),
  }),
})

export const {
  useGetMerchantsQuery,
  useGetMerchantByIdQuery,
  useUpdateMerchantInfoMutation,
  useCreateMerchantInfoMutation,
} = merchantApi

export default merchantApi
