import { createApi } from '@reduxjs/toolkit/query/react'
import endpoints from 'services/apis/endpoints'
import { convertUserDetail, convertUserList, convertUserParams } from 'utils/user'
import { baseQueryWithAuth } from '.'

const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: baseQueryWithAuth,
  endpoints: (builder) => ({
    getUsers: builder.query({
      query: () => endpoints.user.list,
      transformResponse: (response) => convertUserList(response),
    }),
    getUser: builder.query({
      query: (id = '') => `${endpoints.user.detail}?_id=${id}`,
      transformResponse: (response) => convertUserDetail(response),
    }),
    updateUser: builder.mutation({
      query(data) {
        return {
          url: endpoints.user.update,
          method: 'POST',
          body: convertUserParams(data),
        }
      },
      transformResponse: (response) => response,
    }),
  }),
})

export const {
  useGetUsersQuery,
  useGetUserQuery,
  useUpdateUserMutation,
} = userApi

export default userApi
