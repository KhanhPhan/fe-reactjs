/* eslint-disable max-len */
import { createApi } from '@reduxjs/toolkit/query/react'
import endpoints from 'services/apis/endpoints'
import {
  convertPermissionList, convertRoleDetail, convertRoleList, convertRoleParams,
} from 'utils/role'
import { baseQueryWithAuth } from '.'

const roleApi = createApi({
  reducerPath: 'roleApi',
  baseQuery: baseQueryWithAuth,
  endpoints: (builder) => ({
    getPermissions: builder.query({
      query: () => endpoints.permission.list,
      transformResponse: (response) => convertPermissionList(response),
    }),
    getRoles: builder.query({
      query: () => endpoints.role.list,
      transformResponse: (response) => convertRoleList(response),
    }),
    getRole: builder.query({
      query: (id = '') => `${endpoints.role.detail}?_id=${id}`,
      transformResponse: (response) => convertRoleDetail(response),
    }),
    createRole: builder.mutation({
      query(data) {
        return {
          url: endpoints.role.create,
          method: 'POST',
          body: convertRoleParams(data),
        }
      },
      transformResponse: (response) => response,
    }),
    updateRole: builder.mutation({
      query(data) {
        return {
          url: endpoints.role.update,
          method: 'POST',
          body: convertRoleParams(data),
        }
      },
      transformResponse: (response) => response,
    }),
  }),

})

export const {
  useGetRolesQuery,
  useGetRoleQuery,
  useGetPermissionsQuery,
  useCreateRoleMutation,
  useUpdateRoleMutation,
} = roleApi

export default roleApi
