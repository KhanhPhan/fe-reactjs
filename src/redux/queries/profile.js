import { createApi } from '@reduxjs/toolkit/query/react'
import endpoints from 'services/apis/endpoints'
import { convertProfileDetail, convertUpdatedProfile, convertProfileParams } from 'utils/profile'
import { baseQueryWithAuth } from '.'

const profileApi = createApi({
  reducerPath: 'profileApi',
  baseQuery: baseQueryWithAuth,
  endpoints: (builder) => ({
    getProfile: builder.query({
      query: () => endpoints.profile.info,
      transformResponse: (response) => convertProfileDetail(response),
    }),
    updateProfile: builder.mutation({
      query(data) {
        return {
          url: endpoints.profile.update,
          method: 'POST',
          body: convertProfileParams(data),
        }
      },
      transformResponse: (response) => convertUpdatedProfile(response),
    }),
    uploadAvatar: builder.mutation({
      query(data) {
        return {
          url: endpoints.profile.uploadAvatar,
          method: 'POST',
          body: data,
        }
      },
      transformResponse: (response) => response,
    }),
  }),
})

export const {
  useGetProfileQuery,
  useUpdateProfileMutation,
  useUploadAvatarMutation,
} = profileApi

export default profileApi
