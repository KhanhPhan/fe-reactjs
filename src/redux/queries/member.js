import { createApi } from '@reduxjs/toolkit/query/react'
import endpoints from 'services/apis/endpoints'
import { convertMemberList, convertMemberDetail } from 'utils/member'
import { baseQueryWithAuth } from '.'

const memberApi = createApi({
  reducerPath: 'memberApi',
  baseQuery: baseQueryWithAuth,
  endpoints: (builder) => ({
    getMembers: builder.query({
      query: ({ keyword }) => `${endpoints.member.list}?keysearch=${keyword}`,
      transformResponse: (response) => convertMemberList(response),
    }),
    getMember: builder.query({
      query: (id = '') => `${endpoints.member.get}?_id=${id}`,
      transformResponse: (response) => convertMemberDetail(response),
    }),
  }),
})

export const {
  useGetMembersQuery,
  useGetMemberQuery,
} = memberApi

export default memberApi
