import { createApi } from '@reduxjs/toolkit/query/react'
import endpoints from 'services/apis/endpoints'
import {
  convertCategoryDetail,
  convertCategoryList,
  convertCategoryParams,
} from 'utils/category'
import { baseQueryWithAuth } from '.'

const categoryApi = createApi({
  reducerPath: 'categoryApi',
  baseQuery: baseQueryWithAuth,
  endpoints: (builder) => ({
    getCategories: builder.query({
      query: () => endpoints.category.list,
      transformResponse: (response) => convertCategoryList(response),
    }),
    getCategory: builder.query({
      query: (id = '') => `${endpoints.category.detail}?id=${id}`,
      transformResponse: (response) => convertCategoryDetail(response),
    }),
    createCategory: builder.mutation({
      query(data) {
        return {
          url: endpoints.category.create,
          method: 'POST',
          body: convertCategoryParams(data),
        }
      },
      transformResponse: (response) => response,
    }),
    updateCategory: builder.mutation({
      query(data) {
        return {
          url: endpoints.category.update,
          method: 'POST',
          body: convertCategoryParams(data),
        }
      },
      transformResponse: (response) => response,
    }),
  }),

})

export const {
  useGetCategoriesQuery,
  useGetCategoryQuery,
  useCreateCategoryMutation,
  useUpdateCategoryMutation,
} = categoryApi

export default categoryApi
