import {
  fetchBaseQuery,
} from '@reduxjs/toolkit/dist/query/react'
import endpoints, { getBaseURL } from 'services/apis/endpoints'
import { get, KEYS, set } from 'utils/localStorage'

const baseQuery = fetchBaseQuery({
  baseUrl: getBaseURL(),
  prepareHeaders: (headers) => {
    if (get(KEYS.TOKEN)) {
      headers.set('Authorization', `Bearer ${get(KEYS.TOKEN) || ''}`)
      headers.set('lang', 'en')
    }
    return headers
  },
})

export const baseQueryWithAuth = async (
  args,
  api,
  extraOptions,
) => {
  let result = await baseQuery(args, api, extraOptions)
  if (result.error && result.error.status === 401) {
    const refreshResult = await baseQuery({
      url: endpoints.auth.refresh_token,
      method: 'POST',
      body: {
        refreshToken: get(KEYS.REFRESH_TOKEN),
        uuid: get(KEYS.UUID),
      },
    }, api, extraOptions)
    const { data } = refreshResult
    if (data) {
      set(KEYS.TOKEN, data?.data?.token?.accessToken)
      set(KEYS.REFRESH_TOKEN, data?.data?.refreshToken)
      result = await baseQuery(args, api, extraOptions)
    } else {
      api.dispatch({ type: 'auth/logout' })
      window.location.href = '/login'
    }
  }
  return result?.data || result
}
