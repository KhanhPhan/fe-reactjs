export const apiAction = (
  apiRequest,
  [get, success, error],
  callback,
  xProps,
) => (dispatch) => {
  dispatch(get(xProps))
  return apiRequest.then((response) => {
    if (callback?.onSuccess) callback.onSuccess(response)
    dispatch(success(Object.keys(xProps).length === 0 ? response : { ...response, ...xProps }))
  }).catch((err) => {
    if (callback?.onFail) callback.onFail(err)
    dispatch(error(err || ''))
  })
}
