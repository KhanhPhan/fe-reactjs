import Login, { LoginWithPhone } from 'pages/Authentication/Login'
import Logout from 'pages/Authentication/Logout'
import {
  PendingApprove, RegisterForm, RegisterPhoneForm, RegisterSuccess, VerifyEmail,
} from 'pages/Authentication/Register'
import { ResetPasswordForm, ResetPasswordSuccess } from 'pages/Authentication/ResetPassword'

const PublicRouter = [
  {
    path: '/login',
    component: Login,
    exact: true,
  },
  {
    path: '/logout',
    component: Logout,
    exact: true,
  },
  {
    path: '/login-with-phone',
    component: LoginWithPhone,
    exact: true,
  },
  {
    path: '/reset-password',
    component: ResetPasswordForm,
    exact: true,
  },
  {
    path: '/reset-password-success',
    component: ResetPasswordSuccess,
    exact: true,
  },
  {
    path: '/register/:step?',
    component: RegisterForm,
    exact: true,
  },
  {
    path: '/register-with-phone',
    component: RegisterPhoneForm,
    exact: true,
  },
  {
    path: '/register-success',
    component: RegisterSuccess,
    exact: true,
  },
  {
    path: '/verify-email',
    component: VerifyEmail,
    exact: true,
  },
  {
    path: '/pending-approve',
    component: PendingApprove,
    exact: true,
  },
]

export default PublicRouter
