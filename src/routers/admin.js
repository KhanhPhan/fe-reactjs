import Dashboard from 'pages/Dashboard'
import Category from 'pages/Category'

const AdminRouter = [
  {
    path: '/',
    component: Dashboard,
    exact: true,
  },
  {
    path: '/dashboard',
    component: Dashboard,
    exact: true,
  },
  {
    path: '/home',
    component: Category,
    exact: true,
  },
]

export default AdminRouter
