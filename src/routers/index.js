import AdminRouter from './admin'
import PublicRouter from './public'

const Routers = [...PublicRouter, ...AdminRouter]

export { PublicRouter, AdminRouter }

export default Routers
