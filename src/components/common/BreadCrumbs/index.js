/* eslint-disable react/no-array-index-key */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Breadcrumbs as MBread, useMediaQuery, useTheme, withStyles,
} from '@material-ui/core'
import { NavigateNext } from '@material-ui/icons'
import { Link } from 'react-router-dom'
import { colors } from 'theme'
import { TextBlockToolTip } from 'components/common/TextBlock'

const StyledBreadcrumbs = withStyles((theme) => ({
  root: {
  },
  separator: {
    padding: theme.spacing(0, 0.5),
    margin: 0,
    '& .MuiSvgIcon-root': {
      fontSize: 16,
      position: 'relative',
      top: 1,
    },
    nameBr: {
      background: 'red',
    },
  },
}))(MBread)

const BreadCrumbs = ({ list, tooltip }) => {
  const theme = useTheme()
  const isTablet = useMediaQuery(theme.breakpoints.down(768))
  const renderLinkBR = () => (
    list.map((x, index) => (
      x.to ? (
        <Link key={index} to={x.to}>
          <span
            style={{
              maxWidth: '500px',
              color: x.color || colors.textGray2,
              fontSize: '1.25rem',
              fontWeight: 400,
              display: 'flex',
              alignItems: 'center',
              height: 25,
            }}
          >
            {x.name}
          </span>
        </Link>
      ) : (
        <TextBlockToolTip
          maxLine={1}
          style={{
            color: x.color || colors.black,
            fontSize: '1.25rem',
            fontWeight: 500,
            display: 'flex',
            alignItems: 'center',
            height: 25,
          }}
          title={tooltip ? <span>{x.name}</span> : ''}
          key={index}
        >
          <span
            data-tooltip
            style={{
              maxWidth: isTablet ? '200px' : '400px',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              WebkitBoxOrient: 'vertical',
              display: 'block',
              whiteSpace: 'nowrap',
            }}
          >
            {x.name}
          </span>

        </TextBlockToolTip>

      )
    ))
  )
  return (

    <StyledBreadcrumbs classes={{ }} separator={<NavigateNext />} aria-label="breadcrumb">
      {
          renderLinkBR()
        }
    </StyledBreadcrumbs>

  )
}

BreadCrumbs.defaultProps = {
  list: [],
  tooltip: false,
}

BreadCrumbs.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape()),
  tooltip: PropTypes.bool,
}

export default BreadCrumbs
