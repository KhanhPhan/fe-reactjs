import React, {
  useRef, useCallback, useMemo, useState,
} from 'react'
import { withStyles } from '@material-ui/core/styles'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import PropTypes from 'prop-types'
import GalleryDialog from 'components/common/Gallery'
import useGalleryDialog from 'utils/hooks/useGalleryDialog'
import { MEDIA_FILE_TYPE } from 'constants/media'
import style from './style.js'
import './CustomVideo'

const TextEditor = ({
  classes,
  value,
  onChange,
  removeMedia,
  isProductMode,
  containerToolbar,
  noUploadVideo,
}) => {
  const editorRef = useRef()
  const [activeGallery, setActiveGallery] = useState(false)
  const [images, setImages] = useState([])
  const [galleryProps, galleryDispatch] = useGalleryDialog()
  const [mediaSelectType, setMediaSelectType] = useState([MEDIA_FILE_TYPE.IMAGE])

  const onHandleVideo = useCallback(() => {
    setActiveGallery(true)
    setImages([])
    setMediaSelectType([MEDIA_FILE_TYPE.VIDEO])
    galleryDispatch.openGallery()
  }, [setActiveGallery, galleryDispatch, setImages])

  const onHandleImage = useCallback(() => {
    setActiveGallery(true)
    setImages([])
    setMediaSelectType([MEDIA_FILE_TYPE.IMAGE])
    galleryDispatch.openGallery()
  }, [setActiveGallery, galleryDispatch, setImages])

  const insertMedia = useCallback((files) => {
    galleryDispatch.closeGallery()
    setImages(files)
    if (!editorRef.current) return
    if (!files || files.length === 0) return
    const filesLength = files.length
    for (let i = 0; i < filesLength; i += 1) {
      const file = files[i]
      const fileType = file && file.contentType && file.contentType.split('/')[0]
      const selectionIndex = editorRef.current.getEditor().getSelection(true)?.index || 0
      if (fileType === 'image') {
        if (file && file.src) {
          editorRef.current.getEditor().insertEmbed(selectionIndex, 'image', file.src)
          editorRef.current.getEditor().setSelection(selectionIndex + 1)
        }
      } else if (fileType === 'video') {
        if (file && file.src) {
          editorRef.current.getEditor().insertEmbed(selectionIndex, 'video', file.src, 'user')
          editorRef.current.getEditor().format('width', 300)
          editorRef.current.getEditor().format('height', 300)
          editorRef.current.getEditor().setSelection(selectionIndex + 1)
        }
      }
    }
    editorRef.current.focus()
  }, [galleryDispatch, setImages])

  const modules = useMemo(() => ({
    toolbar: {
      container: containerToolbar.length ? containerToolbar : [
        [{ size: [] }],
        [{ font: [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [
          { list: 'ordered' },
          { list: 'bullet' },
          { indent: '-1' },
          { indent: '+1' },
        ],
        ['link', 'image', 'video'],
        ['clean'],
      ],
      handlers: {
        image: onHandleImage,
        video: onHandleVideo,
      },
    },
    clipboard: { matchVisual: false },
  }), [onHandleImage, onHandleVideo])

  return (
    <>
      <ReactQuill
        ref={editorRef}
        className={classes.quillContainer}
        theme="snow"
        value={value}
        onChange={onChange}
        modules={modules}
        formats={ReactQuill.formats}
        bounds=".container"
      />
      {activeGallery && (
        <GalleryDialog
          images={images}
          {...galleryProps}
          multiple
          handleSave={insertMedia}
          showTypes={mediaSelectType}
          selectTypes={mediaSelectType}
          noUploadVideo={noUploadVideo}
          isProductMode={isProductMode}
          removeMedia={removeMedia}
          acceptTypes={mediaSelectType.indexOf('image') !== -1
            ? '.jpg,.png,.jpeg'
            : mediaSelectType.indexOf('video') !== -1 ? '.mp4,.mov' : '.jpg,.png,.jpeg,.mp4,.mov'}
          enabledEditMetadataIds={['alternative_text', 'title']}
        />
      )}
    </>
  )
}

TextEditor.defaultProps = {
  value: '',
  // onHandleVideo: () => {},
  onChange: () => { },
  props: {},
  removeMedia: false,
  isProductMode: true,
  containerToolbar: [],
  noUploadVideo: false,
}

TextEditor.propTypes = {
  classes: PropTypes.shape().isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  // onHandleVideo: PropTypes.func,
  props: PropTypes.shape(),
  isProductMode: PropTypes.bool,
  removeMedia: PropTypes.bool,
  containerToolbar: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.shape(), PropTypes.arrayOf(PropTypes.shape())]),
  ),
  noUploadVideo: PropTypes.bool,
}

export default withStyles(style)(TextEditor)
