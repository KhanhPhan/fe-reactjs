import { Quill } from 'react-quill'

const Video = Quill.import('formats/video')
const Link = Quill.import('formats/link')

class CustomVideo extends Video {
  static create(value) {
    const video = super.create(value)
    video.setAttribute('controls', true)
    video.setAttribute('type', 'video/*')
    video.setAttribute('style', 'height: 200px;')
    video.setAttribute('src', this.sanitize(value))
    return video
  }

  static sanitize(url) {
    return Link.sanitize(url)
  }
}
CustomVideo.blotName = 'video'
CustomVideo.className = 'ql-video'
CustomVideo.tagName = 'video'

Quill.register('formats/video', CustomVideo)
export default CustomVideo
