const style = {
  quillContainer: {
    '& .ql-toolbar': {
      borderRadius: '5px 5px 0 0',
      borderColor: theme.color.secondaryBlue1,
      border: '0.25px solid',
      paddingTop: 6,
      paddingBottom: 6,
    },
    '& .ql-container': {
      borderRadius: '0 0 5px 5px',
      backgroundColor: theme.color.secondaryBlue3,
      borderColor: theme.color.secondaryBlue1,
      border: '0.25px solid',
    },
    '& .ql-editor': {
      minHeight: 120,
      fontSize: 14,
    },
  },
}

export default style
