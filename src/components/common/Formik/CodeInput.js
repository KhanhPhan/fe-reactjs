import React from 'react'
import PropTypes from 'prop-types'
import { getIn } from 'formik'
import CodeInputStyled from 'components/common/CodeInput'

const CodeInput = ({
  field, form, error, helperText, ...props
}) => (
  <CodeInputStyled
    {...props}
    {...form}
    {...field}
    error={!!error || (!!getIn(form.touched, field.name) && !!getIn(form.errors, field.name))}
    helperText={helperText || (getIn(form.touched, field.name) && getIn(form.errors, field.name))}
    name={field.name}
  />
)

CodeInput.defaultProps = {
  error: false,
  helperText: '',
}

CodeInput.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  form: PropTypes.shape({
    touched: PropTypes.shape(),
    errors: PropTypes.shape(),
  }).isRequired,
}
export default CodeInput
