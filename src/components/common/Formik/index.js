import FieldWrapper from 'utils/formik/FieldWrapper'
import FastFieldWrapper from 'utils/formik/FastFieldWrapper'

import TextInput from './TextInput'
import CheckBox from './CheckBox'
import Dropdown from './Dropdown'
import DayPicker from './DayPicker'
import PhoneNumber from './PhoneNumber'
import CodeInput from './CodeInput'
import SingleAsyncSelect from './SingleAsyncSelect'
import Input from './Input'

export const Field = {
  Input: FieldWrapper(Input),
  TextInput: FieldWrapper(TextInput),
  CheckBox: FieldWrapper(CheckBox),
  Dropdown: FieldWrapper(Dropdown),
  DayPicker: FieldWrapper(DayPicker),
  PhoneNumber: FieldWrapper(PhoneNumber),
  CodeInput: FieldWrapper(CodeInput),
  SingleAsyncSelect: FastFieldWrapper(SingleAsyncSelect),
}

export const FastField = {
  TextInput: FastFieldWrapper(TextInput),
  DayPicker: FastFieldWrapper(DayPicker),
  PhoneNumber: FieldWrapper(PhoneNumber),
  SingleAsyncSelect: FastFieldWrapper(SingleAsyncSelect),
}

export default {
  Field,
  FastField,
}
