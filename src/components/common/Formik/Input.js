import React from 'react'
import PropTypes from 'prop-types'
import { getIn } from 'formik'
import TextInput from 'components/common/TextInput'

const Input = ({
  field, form, inputProps, error, helperText, ...props
}) => (
  <TextInput
    error={!!error || (!!getIn(form.touched, field.name) && !!getIn(form.errors, field.name))}
    helperText={helperText || (getIn(form.touched, field.name) && getIn(form.errors, field.name))}
    inputProps={{
      ...field,
      ...inputProps,
      value: field.value ?? '',
    }}
    {...props}
  />
)

Input.defaultProps = {
  inputProps: {},
  error: '',
  helperText: '',
}

Input.propTypes = {
  inputProps: PropTypes.shape(),
  form: PropTypes.shape().isRequired,
  field: PropTypes.shape().isRequired,
  error: PropTypes.string,
  helperText: PropTypes.string,
}

export default Input
