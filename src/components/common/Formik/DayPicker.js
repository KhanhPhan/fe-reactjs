import React from 'react'
import PropTypes from 'prop-types'
import DayPickerRaw from 'components/common/DayPicker'

const DayPicker = ({
  field,
  form,
  disabled = false,
}) => (
  <DayPickerRaw
    {...props}
    disabled={disabled}
    value={field.value}
    setValue={(v) => form.setFieldValue(field.name, v)}
  />
)

DayPicker.defaultProps = {
  disabled: false,
}

DayPicker.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
  form: PropTypes.shape({
    touched: PropTypes.shape(),
    errors: PropTypes.shape(),
    values: PropTypes.shape(),
    setFieldValue: PropTypes.func,
  }).isRequired,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]).isRequired,
  disabled: PropTypes.bool,
}

export default DayPicker
