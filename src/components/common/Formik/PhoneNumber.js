import React from 'react'
import PropTypes from 'prop-types'
import { getIn } from 'formik'
import PhoneNumberStyled from 'components/common/PhoneNumber'

const PhoneNumber = ({
  field, form, error, inputProps, helperText, ...props
}) => (
  <PhoneNumberStyled
    {...form}
    {...props}
    inputProps={{
      ...field,
      ...inputProps,
      value: field.value ?? '',
    }}
    error={!!error || (!!getIn(form.touched, field.name) && !!getIn(form.errors, field.name))}
    helperText={helperText || (getIn(form.touched, field.name) && getIn(form.errors, field.name))}
    name={field.name}
  />
)

PhoneNumber.defaultProps = {
  inputProps: {},
  error: false,
  helperText: '',
}

PhoneNumber.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  form: PropTypes.shape({
    touched: PropTypes.shape(),
    errors: PropTypes.shape(),
  }).isRequired,
  inputProps: PropTypes.shape(),
}
export default PhoneNumber
