import React from 'react'
import { getIn } from 'formik'
import PropTypes from 'prop-types'
import TextInput from 'components/common/TextInput'

const Input = ({
  field, form, inputProps, error, helperText, ...props
}) => (
  <TextInput
    id={field.name}
    error={!!error || (!!getIn(form.touched, field.name) && !!getIn(form.errors, field.name))}
    helperText={helperText || (getIn(form.touched, field.name) && getIn(form.errors, field.name))}
    inputProps={{
      ...field,
      ...inputProps,
      value: field.value ?? '',
    }}
    {...props}
  />
)

Input.defaultProps = {
  error: false,
  helperText: '',
  inputProps: {},
}

Input.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
  form: PropTypes.shape({
    touched: PropTypes.shape(),
    errors: PropTypes.shape(),
  }).isRequired,
  inputProps: PropTypes.shape(),
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  helperText: PropTypes.string,
}

export default Input
