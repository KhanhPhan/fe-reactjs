import React from 'react'
import PropTypes from 'prop-types'
import { Single } from 'components/common/AsyncSelect'

const SingleAsyncSelect = ({
  field, form, forwardValues, revertValues, options, ...props
}) => (
  <Single
    name={field.name}
    {...field}
    values={forwardValues(field.value)}
    onChange={(v) => form.setFieldValue(field.name, revertValues(v))}
    options={options}
    {...props}
  />
)

SingleAsyncSelect.defaultProps = {
  forwardValues: (v) => v,
  revertValues: (v) => v,
  options: [],
}

SingleAsyncSelect.propTypes = {
  forwardValues: PropTypes.func,
  revertValues: PropTypes.func,
  form: PropTypes.shape().isRequired,
  field: PropTypes.shape().isRequired,
  options: PropTypes.shape(),
}

export default SingleAsyncSelect
