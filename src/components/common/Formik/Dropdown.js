/* eslint-disable react/require-default-props */
/* eslint-disable no-unused-vars */
import React from 'react'
import { getIn } from 'formik'
import PropTypes from 'prop-types'
import Dropdown from 'components/common/Dropdown'

const FormDropdown = ({
  field, form, inputProps, onChange, error, helperText, options, ...props
}) => (
  <Dropdown
    value={getIn(form.values, field.name)}
    options={options}
    inputProps={{
      error: !!error || (getIn(form.touched, field.name) && getIn(form.errors, field.name)),
      helperText: helperText || (getIn(form.touched, field.name) && getIn(form.errors, field.name)),
    }}
    {...props}
    onChange={(_, newValue) => {
      onChange(newValue)
      form.setFieldValue(field.name, newValue.value)
    }}
  />
)

FormDropdown.defaultProps = {
  onChange: () => {},
}

FormDropdown.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
  form: PropTypes.shape({
    touched: PropTypes.shape(),
    errors: PropTypes.shape(),
    values: PropTypes.shape(),
    setFieldValue: PropTypes.func,
  }).isRequired,
  inputProps: PropTypes.shape(),
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]).isRequired,
  helperText: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape()),
  onChange: PropTypes.func,
}

export default FormDropdown
