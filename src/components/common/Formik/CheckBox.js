import React from 'react'
import PropTypes from 'prop-types'
import CheckBoxStyled from 'components/common/Checkbox'

const CheckBox = ({ field, ...props }) => (
  <CheckBoxStyled
    {...props}
    id={field.name}
    checked={field.value}
  />
)

CheckBox.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
}
export default CheckBox
