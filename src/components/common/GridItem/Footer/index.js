import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'

const style = (theme) => ({
  divider: {
    width: `calc(100% + ${theme.spacing(4)}px)`,
    margin: theme.spacing(3, -3),
    marginTop: 'auto',
    height: 1,
    background: theme.color.disableGray1,
  },
})

const Footer = ({ classes, children, className }) => (
  <>
    <div className={clsx(classes.divider, className)} />
    <div className={classes.footer}>{children}</div>
  </>
)

Footer.defaultProps = {
  className: '',
}

Footer.propTypes = {
  classes: PropTypes.shape().isRequired,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
}

export default withStyles(style)(Footer)
