import React, { useState, useRef, useEffect } from 'react'
import Datetime from 'react-datetime'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import TextInput from 'components/common/TextInput'
import Dropdown from 'components/common/Dropdown'
import Button from 'components/common/Button'
import 'react-datetime/css/react-datetime.css'

const options = [{ value: 'AM', label: 'AM' }, { value: 'PM', label: 'PM' }]

const themeStyle = (theme) => ({
  dateTimeContainer: {
    '& .rdtPicker': {
      zIndex: '10 !important',
      marginTop: 5,
    },
    '&.center': {
      '& .rdtPicker': {
        left: '-5%',
      },
    },
  },
  pickupInput: {
    marginTop: 10,

    '& .MuiOutlinedInput-input': {
      color: `${theme.color.primaryBlue} !important`,
      height: 38,
      padding: 0,
      fontSize: 14,
      paddingLeft: 10,
    },
  },
  input: {
    marginTop: 10,
    maxWidth: 38,
    marginLeft: 15,
    '& .MuiOutlinedInput-input': {
      color: `${theme.color.primaryBlue} !important`,
      height: 38,
      padding: 0,
      fontSize: 12,
      paddingLeft: 10,
    },
    '& .MuiInputBase-input': {
      paddingRight: '5px !important',
    },
  },
  dropDown: {
    marginTop: 10,
    marginLeft: 15,
    paddingTop: 16,
    '& .MuiOutlinedInput-root': {
      color: `${theme.color.primaryBlue} !important`,
      padding: 0,
      fontSize: 12,
      paddingLeft: 10,
    },
  },
  container: {
    display: 'inline-flex',
    justifyContent: 'space-between',
  },
  cssSpan: {
    display: 'inline-flex',
    alignItems: 'center',
    paddingTop: 20,
    paddingLeft: 10,
  },
  bottomContainer: {
    paddingTop: 20,
  },
  cssButton: {
    width: '100%',
  },
  disabledTimePicker: {
    pointerEvents: 'none',
  },

})

const convertTime = (value) => {
  if (!value) {
    return { hour: '', minutes: '', typeTime: { value: 'AM', label: 'AM' } }
  }
  const arrayTime = value.split(':')
  const hour = arrayTime[0]
  const arrayMinutes = arrayTime[1].split(' ')
  return {
    hour,
    minutes: arrayMinutes[0],
    typeTime: {
      value: arrayMinutes[1].trim(),
      label: arrayMinutes[1].trim(),
    },
  }
}

const TimePicker = ({
  classes, disabled, center,
  value: time, onChange: setTime,
  ...props
}) => {
  const [hour, setHour] = useState('')
  const [minutes, setMinutes] = useState('')
  const [typeTime, setTypeTime] = useState({ value: 'AM', label: 'AM' })
  const [openPickup, setOpenPickup] = useState(false)
  const timePickerRef = useRef(null)
  useEffect(() => {
    const defaultTime = convertTime(time)
    setHour(defaultTime.hour)
    setMinutes(defaultTime.minutes)
    setTypeTime(defaultTime.typeTime)
  }, [time])

  useEffect(() => {
    function onOutsideClick(e) {
      if (
        timePickerRef && timePickerRef.current && !timePickerRef.current.contains(e.target)
        && (typeof e.target.className?.includes === 'function'
        && !e.target.className.includes('MuiDialog'))
        && (e.target.className
        && !e.target.className.includes('MuiButton'))
        && (e.target.className && !e.target.className.includes('MuiAutocomplete'))
      ) {
        setOpenPickup(false)
      }
    }

    window.addEventListener('mousedown', onOutsideClick)
    return () => {
      window.removeEventListener('mousedown', onOutsideClick)
    }
  }, [timePickerRef])

  const handleChangeHours = (e) => {
    const value = e.target.value.length > 0
      ? Math.max(0, parseInt(e.target.value, 10)).toString().slice(0, 2)
      : ''
    if (value >= 0 && value <= 12) {
      setHour(value)
    } else {
      setHour('')
    }
  }

  const handleChangeMinutes = (e) => {
    const value = e.target.value.length > 0
      ? Math.max(0, parseInt(e.target.value, 10)).toString().slice(0, 2)
      : ''
    if (value >= 0 && value <= 60) {
      setMinutes(value)
    } else {
      setMinutes('')
    }
  }
  const handleSelectTime = (value) => {
    setTypeTime(value)
  }
  const handleChangeTime = () => {
    const defaultTime = convertTime(time)
    const timeHour = `${
      hour !== '' ? (`0${hour}`).slice(-2) : defaultTime.hour
    }:${
      minutes !== '' ? (`0${minutes}`).slice(-2) : defaultTime.minutes
    } ${typeTime.label}`
    setTime(timeHour)
    setOpenPickup(false)
  }
  const renderView = () => (
    <div className="wrapper">
      <div className={classes.container}>
        <TextInput
          className={classes.input}
          value={hour}
          onChange={handleChangeHours}
          disabled={disabled}
        />
        <span className={classes.cssSpan}>:</span>
        <TextInput
          className={classes.input}
          value={minutes}
          onChange={handleChangeMinutes}
          disabled={disabled}
        />
        <Dropdown
          className={classes.dropDown}
          options={options}
          onChange={(_, value) => handleSelectTime(value)}
          disabled={disabled}
          value={typeTime.value}
        />
      </div>
      <div className={classes.bottomContainer}>
        <Button
          className={classes.cssButton}
          label="Apply"
          onClick={() => handleChangeTime()}
          disabled={disabled}
        />
      </div>
    </div>
  )

  return (
    <div
      id="time-picker"
      ref={timePickerRef}
      className={clsx({
        [classes.disabledTimePicker]: disabled,
      })}
    >
      <Datetime
        {...props}
        className={clsx(classes.dateTimeContainer, { center })}
        dateFormat={false}
        value={time}
        renderView={(mode, renderDefault) => renderView(mode, renderDefault)}
        open={openPickup}
        renderInput={(xProps) => (
          <TextInput
            {...xProps}
            className={classes.pickupInput}
            value={time}
            onClick={() => setOpenPickup(!openPickup)}
            InputProps={{
              inputProps: { readOnly: true },
            }}
            disabled={disabled}
          />
        )}
      />
    </div>
  )
}

TimePicker.defaultProps = {
  startTime: '',
  disabled: false,
  center: false,
  value: '',
  onChange: () => {},
}
TimePicker.propTypes = {
  classes: PropTypes.shape().isRequired,
  startTime: PropTypes.string,
  disabled: PropTypes.bool,
  center: PropTypes.bool,
  value: PropTypes.string,
  onChange: PropTypes.func,
}

export default withStyles(themeStyle)(TimePicker)
