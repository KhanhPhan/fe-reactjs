const d = {
  icon: 'warn',
  title: 'Warning',
  okText: 'OK',
  closeOnClick: true,
}
const getErrorMessage = (error) => {
  if (typeof error === 'string') {
    try {
      const parsedErr = JSON.parse(error)
      if (typeof parsedErr === 'string') return error
      if (parsedErr.message) return parsedErr.message
    } catch {
      return error
    }
  }
  if (error?.message) return error.message
  return 'Something went wrong!'
}

// try to search messages below to avoid adding duplicated ones
const dialogParams = {
  error: (e) => ({
    ...d,
    title: 'Error',
    description: getErrorMessage(e),
  }),
}

export default dialogParams
