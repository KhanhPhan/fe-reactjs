import { ThemeProvider } from '@material-ui/core/styles'
import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom'
import theme from 'theme'
import CustomDialog from './CustomDialog'

const DialogContent = (props) => {
  const [open, setOpen] = useState(true)

  useEffect(() => {
    setOpen(true)
  }, [props])

  const close = () => {
    setOpen(false)
  }

  return (
    <ThemeProvider
      theme={theme}
    >
      <CustomDialog open={open} onClose={close} {...props} />
    </ThemeProvider>
  )
}

const showDialog = (props) => {
  ReactDOM.render(<DialogContent {...props} />, document.getElementById('errorMessage'))
}

export {
  showDialog,
}
