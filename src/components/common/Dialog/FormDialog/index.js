import React, { useLayoutEffect } from 'react'
import { withStyles } from '@material-ui/core/styles'
import MuiDialog from '@material-ui/core/Dialog'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogContent from '@material-ui/core/DialogContent'
import MuiDialogActions from '@material-ui/core/DialogActions'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import Typography from '@material-ui/core/Typography'
import clsx from 'clsx'
import PropTypes from 'prop-types'

const Dialog = withStyles((theme) => ({
  paper: {
    'body.visual-viewport-height-small &': {
      display: 'block',
    },
  },
  paperScrollPaper: {
    'body.visual-viewport-height-small &': {
      maxHeight: `calc(100% - ${theme.spacing(2)}px)`,
    },
  },
}))((props) => <MuiDialog {...props} />)

const DialogTitle = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1, 2),
  },
  closeButton: {
    color: theme.color.darkGray,
    marginTop: -6,
  },
  header: {
    padding: theme.spacing(2, 2),
  },
  titleText: {
    fontSize: 16,
    color: theme.color.primaryBlue,
    marginRight: theme.spacing(3),
    fontWeight: 500,
  },
  headerContent: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  description: {
    color: theme.color.midGray,
    fontSize: 14,
  },
}))((props) => {
  const {
    children, extraHeader, classes, onClose, disableXClose, ...other
  } = props
  return (
    <MuiDialogTitle disableTypography className={`${classes.root} ${classes.header}`} {...other}>
      <div className={classes.headerContent}>
        <Typography variant="h6" className={classes.titleText}>{children}</Typography>
        {onClose && !disableXClose ? (
          <IconButton
            aria-label="close"
            size="small"
            className={classes.closeButton}
            onClick={onClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        ) : null}
      </div>
      {extraHeader ? (<div className={classes.description}>{extraHeader}</div>) : null}
    </MuiDialogTitle>
  )
})

const DialogContent = withStyles(() => ({
  root: {
    padding: 0,
    borderTopWidth: 0,
    '&.displayBorderTop': {
      borderTopWidth: 1,
    },
  },
}))(MuiDialogContent)

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1, 2),
    justifyContent: 'unset',
    minHeight: 61,
  },
}))(MuiDialogActions)

const CustomizedDialogs = ({
  open,
  onClose,
  title,
  extraHeader,
  children,
  footer,
  disableXClose,
  contentClassName,
  displayBorderTop,
  ...props
}) => {
  const handleClose = () => {
    onClose()
  }

  useLayoutEffect(() => {
    // @TODO: visualViewport has not supported Firefox & IE yet.
    // Refer: https://developer.mozilla.org/en-US/docs/Web/API/VisualViewport

    const handleResize = () => {
      if (!window.visualViewport) return

      if (window.visualViewport.height < 400) {
        document.body.classList.add('visual-viewport-height-small')
      } else {
        document.body.classList.remove('visual-viewport-height-small')
      }
    }

    handleResize()
    window.visualViewport?.addEventListener('resize', handleResize)

    return () => window.visualViewport?.removeEventListener('resize', handleResize)
  }, [])

  return (
    <Dialog
      open={open}
      maxWidth="sm"
      fullWidth
      disableBackdropClick
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      {...props}
    >
      {title && (
      <DialogTitle
        id="customized-dialog-title"
        onClose={handleClose}
        disableXClose={disableXClose}
        extraHeader={extraHeader}
      >
        {title}
      </DialogTitle>
      )}
      <DialogContent
        className={clsx(contentClassName, {
          displayBorderTop,
        })}
        id="form-dialog-content"
        dividers={!!title}
      >
        {children}
      </DialogContent>
      {footer() && (
      <DialogActions>
        {footer()}
      </DialogActions>
      )}

    </Dialog>
  )
}

CustomizedDialogs.defaultProps = {
  extraHeader: '',
  onClose: () => {},
  disableXClose: false,
  footer: () => {},
  title: null,
  children: '',
  contentClassName: '',
  displayBorderTop: false,
}

CustomizedDialogs.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  disableXClose: PropTypes.bool,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  extraHeader: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  footer: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node,
  ]),
  contentClassName: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  displayBorderTop: PropTypes.bool,
}

export default CustomizedDialogs
