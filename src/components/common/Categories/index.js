/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight'
import clsx from 'clsx'
import CheckBox from 'components/common/Checkbox'
import { convertCategoryList } from 'utils/common'

const themeStyle = (theme) => ({
  categoriesContainer: {
    marginTop: 24,
    padding: 4,
    borderRadius: 5,
    backgroundColor: '#EDF1FB',
    border: '1px solid #B4BFD9',

  },
  categories: {
    minHeight: 60,
    maxHeight: 200,
    overflowY: 'auto',
    overflowX: 'hidden',
    padding: 6,
    scrollbarWidth: 'thin',
    '&::-webkit-scrollbar': {
      width: 5,
      height: 5,
    },
    '&::-webkit-scrollbar-track': {
      borderRadius: 10,
      backgroundColor: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      borderRadius: 10,
      backgroundColor: theme.color.secondaryBlue2,
    },
  },
})

const Categories = ({
  classes,
  allCategories,
  onCheck,
  className,
  values,
}) => {
  const [nodes, setNodes] = useState([])

  useEffect(() => {
    setNodes(convertCategoryList(allCategories))
  }, [allCategories])
  const renderCategoryList = (categories, step, selectedItems) => {
    const elements = []
    categories.forEach((item, index) => {
      const checked = (selectedItems || []).findIndex((v) => v.id === item.id) >= 0
      if (!item.children || !item.children.length) {
        elements.push(
          <div style={{ display: 'flex' }} key={`__${index}${item.id}`}>
            {
              step
                ? (
                  <>
                    <span style={{ marginLeft: (step - 1) * 25 }} />
                    <SubdirectoryArrowRightIcon
                      style={{
                        fontSize: '14px',
                        marginTop: 10,
                        marginLeft: 5,
                      }}
                    />
                  </>
                )
                : null
            }
            <CheckBox
              key={item.id}
              label={item.name ? item.name.replace(/&amp;/g, '&') : ''}
              whiteColor
              checked={checked}
              onChange={(e) => onCheck(item.id, e.target.checked)}
            />
          </div>,
        )
      } else {
        elements.push(
          <div style={{ display: 'flex' }} key={`__${index}${item.id}`}>
            {
              step
                ? (
                  <>
                    <span style={{ marginLeft: (step - 1) * 25 }} />
                    <SubdirectoryArrowRightIcon
                      style={{
                        fontSize: '1rem',
                        marginTop: 10,
                        marginLeft: 5,
                      }}
                    />
                  </>
                )
                : null
            }
            <CheckBox
              key={item.id}
              label={item.name ? item.name.replace(/&amp;/g, '&') : ''}
              checked={checked}
              whiteColor
              onChange={(e) => onCheck(item.id, e.target.checked)}
            />
          </div>,
        )
        elements.push(renderCategoryList(item.children, step + 1, selectedItems))
      }
    })
    return elements
  }

  return (
    <div id="checkboxes" className={clsx(classes.categoriesContainer, className)}>
      <div className={classes.categories}>
        {renderCategoryList(nodes, 0, values)}
      </div>
    </div>
  )
}

Categories.defaultProps = {
  allCategories: [],
  onCheck: () => {},
  className: '',
  values: [],
}

Categories.propTypes = {
  classes: PropTypes.shape().isRequired,
  allCategories: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.shape()),
  ]),
  onCheck: PropTypes.func,
  className: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.shape()),
}

export default withStyles(themeStyle)(Categories)
