import React from 'react'
import PropTypes from 'prop-types'
import {
  withStyles, Stepper as ThemeStepper, Step, StepLabel, makeStyles,
} from '@material-ui/core'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

import clsx from 'clsx'
import StepConnector from './StepConnector'
import BoardStepper from './BoardStepper'
import TextStepper from './TextStepper'

const style = (theme) => ({
  root: {
    padding: theme.spacing(1, 0),
    zIndex: 1,
  },
  label: {
    fontSize: 14,
    color: theme.color.textGray2,
    fontWeight: 400,
    '&$labelActive': {
      color: theme.color.primaryBlue,
      fontWeight: 400,
    },
    '&$labelCompleted': {
      color: theme.color.primaryBlue,
      fontWeight: 400,
    },
    '&$alternativeLabel': {
      marginTop: 10,
    },
    '&.firstStep': {
      textAlign: 'left',
      marginLeft: '50%',
    },
    '&.lastStep': {
      textAlign: 'right',
      marginRight: '50%',
    },
  },
  alternativeLabel: {},
  labelActive: {},
  labelCompleted: {},
})

const useStepIconStyles = makeStyles({
  root: {
    zIndex: 1,
    color: theme.color.secondaryBlue2,
  },
  active: {
    color: theme.color.primaryBlue,
  },
  completed: {
    color: theme.color.primaryBlue,
  },
})

const StepIcon = ({
  active,
  completed,
}) => {
  const classes = useStepIconStyles()
  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {completed ? <CheckCircleIcon fontSize="inherit" /> : (
        <FiberManualRecordIcon
          fontSize="inherit"
        />
      )}
    </div>
  )
}

StepIcon.propTypes = {
  active: PropTypes.bool.isRequired,
  completed: PropTypes.bool.isRequired,
}

const Stepper = ({ classes, activeStep, steps }) => (
  <ThemeStepper
    alternativeLabel
    className={classes.root}
    activeStep={activeStep}
    connector={<StepConnector />}
  >
    {steps.map((label, idx) => (
      <Step key={label}>
        <StepLabel
          StepIconComponent={(props) => (
            <StepIcon
              {...props}
            />
          )}
          classes={{
            label: clsx(classes.label, {
              firstStep: idx === 0,
              lastStep: idx === steps.length - 1,
            }),
            alternativeLabel: classes.alternativeLabel,
            active: classes.labelActive,
            completed: classes.labelCompleted,
            firstStep: classes.firstStep,
          }}
        >
          {label}
        </StepLabel>
      </Step>
    ))}
  </ThemeStepper>
)

Stepper.propTypes = {
  classes: PropTypes.shape().isRequired,
  activeStep: PropTypes.number,
  steps: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ])),
}

Stepper.Onboard = BoardStepper
Stepper.Text = TextStepper

Stepper.defaultProps = {
  steps: [],
  activeStep: 0,
}

export default withStyles(style)(Stepper)
