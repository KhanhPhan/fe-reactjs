import React from 'react'
import PropTypes from 'prop-types'
import {
  withStyles, Stepper as ThemeStepper, Step, StepLabel,
} from '@material-ui/core'

import clsx from 'clsx'
import StepConnector from './StepConnector'
import StepIcon from './StepIcon'

const style = (theme) => ({
  root: {
    padding: theme.spacing(1, 0),
    zIndex: 1,
    marginLeft: 'calc(-12.5% - 24px)',
    marginRight: 'calc(-12.5% - 24px)',
    backgroundColor: 'transparent',
  },
  label: {
    fontSize: 14,
    color: theme.color.textGray2,
    fontWeight: 400,
    '&$labelActive': {
      color: theme.color.primaryBlue,
      fontWeight: 400,
    },
    '&$labelCompleted': {
      color: theme.color.primaryBlue,
      fontWeight: 400,
    },
    '&$alternativeLabel': {
      marginTop: 10,
    },
    '&.firstStep': {
      textAlign: 'left',
      marginLeft: 'calc(50% - 5px)',
    },
    '&.lastStep': {
      textAlign: 'right',
      marginRight: 'calc(50% - 5px)',
    },
    '&.fail': {
      color: theme.color.rubyRed1,
    },
  },
  labelSub: {
    color: '#74798C',
    fontSize: 12,
  },
  alternativeLabel: {},
  labelActive: {},
  labelCompleted: {},
})

const OrderStepper = ({ classes, data, activeStep }) => {
  const { state, steps, desc } = data

  const mappedState = (state || []).map((item) => {
    if (item === 'completed' || item === 'active') return 'active'
    if (item === 'failWarning' || item === 'warning') return 'fail'
    return ''
  })

  return (
    <ThemeStepper
      alternativeLabel
      className={classes.root}
      activeStep={activeStep + 1}
      connector={<StepConnector state={mappedState} />}
    >
      {steps.map((label, idx) => (
        <Step
          key={label}
          active={mappedState[idx] === 'active'}
          completed={mappedState[idx] === 'active'}
          fail={mappedState[idx] === 'fail'}
        >
          <StepLabel
            StepIconComponent={() => (
              <StepIcon
                completed={mappedState[idx] === 'active'}
                active={mappedState[idx] === 'active'}
                fail={mappedState[idx] === 'fail'}
              />
            )}
            classes={{
              label: clsx(classes.label, {
                firstStep: idx === 0,
                lastStep: idx === steps.length - 1,
                fail: mappedState[idx] === 'fail',
              }),
              alternativeLabel: classes.alternativeLabel,
              active: classes.labelActive,
              completed: classes.labelCompleted,
              firstStep: classes.firstStep,
            }}
          >
            {label}
            {(desc[idx] && desc[idx].title)
              && <div className={classes.labelSub}>{desc[idx].title}</div>}
            {(desc[idx] && desc[idx].note)
              && <div className={classes.labelSub}>{desc[idx].note}</div>}
          </StepLabel>
        </Step>
      ))}
    </ThemeStepper>
  )
}

OrderStepper.propTypes = {
  classes: PropTypes.shape().isRequired,
  data: PropTypes.shape(),
  activeStep: PropTypes.number,
}

OrderStepper.defaultProps = {
  data: {},
  activeStep: 0,
}

export default withStyles(style)(OrderStepper)
