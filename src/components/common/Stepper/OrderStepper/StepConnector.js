import React from 'react'
import { withStyles } from '@material-ui/core'
import StepConnector from '@material-ui/core/StepConnector'
import clsx from 'clsx'
import PropTypes from 'prop-types'

const style = (theme) => ({
  alternativeLabel: {
    marginTop: -5,
    left: 'calc(-50%)',
    right: 'calc(50%)',
  },
  active: {
    '& $line': {
      backgroundColor: theme.color.primaryBlue,
    },
  },
  completed: {
    '& $line': {
      backgroundColor: theme.color.primaryBlue,
    },
  },
  fail: {
    '& $line': {
      backgroundColor: theme.color.rubyRed1,
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 1,
  },
})

const Connector = ({
  classes, state, index, ...props
}) => {
  const active = state[index - 1] === 'active'
    && (state[index] === 'active' || state[index] === 'fail')
  const fail = state[index - 1] === 'fail' && state[index] === 'fail'
  return (
    <StepConnector
      {...props}
      active={active || fail}
      index={index}
      classes={{
        ...classes,
        active: clsx(classes.active, {
          [classes.fail]: fail,
        }, {
          line: active,
        }),
      }}
    />
  )
}

Connector.defaultProps = {
  state: [],
  index: 1,
}

Connector.propTypes = {
  classes: PropTypes.shape().isRequired,
  state: PropTypes.arrayOf(PropTypes.string),
  index: PropTypes.number,
}

export default withStyles(style)(Connector)
