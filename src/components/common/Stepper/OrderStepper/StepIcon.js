import React from 'react'
import PropTypes from 'prop-types'
import {
  withStyles,
} from '@material-ui/core'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import clsx from 'clsx'

const style = (theme) => ({
  root: {
    zIndex: 1,
    color: theme.color.secondaryBlue2,
  },
  active: {
    color: theme.color.primaryBlue,
  },
  fail: {
    color: theme.color.rubyRed1,
  },
  completed: {
    color: theme.color.primaryBlue,
  },
})

const StepIcon = ({
  active,
  fail,
  classes,
  completed,
}) => (
  <div
    className={clsx(classes.root, {
      [classes.active]: active,
      [classes.fail]: fail,
      [classes.completed]: completed,
    })}
  >
    <FiberManualRecordIcon
      fontSize="inherit"
    />
  </div>
)

StepIcon.propTypes = {
  classes: PropTypes.shape().isRequired,
  active: PropTypes.bool,
  fail: PropTypes.bool,
  completed: PropTypes.bool,
}

StepIcon.defaultProps = {
  fail: false,
  active: false,
  completed: false,
}

export default withStyles(style)(StepIcon)
