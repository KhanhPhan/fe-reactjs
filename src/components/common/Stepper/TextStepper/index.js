import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'

const style = (theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  item: {
    marginRight: theme.spacing(2),
    color: theme.color.secondaryBlue2,
    display: 'flex',
    alignItems: 'center',
    '&> div': {
      borderRadius: theme.spacing(0.5),
      background: theme.color.secondaryBlue2,
      width: theme.spacing(1),
      height: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    '&.active': {
      color: theme.color.darkGray,
      '&> div': {
        background: theme.color.primaryBlue,
      },
    },
    '&:last-child': {
      marginRight: 0,
    },
  },
})

const TextStepper = ({
  classes, className, classNameItem, steps, active,
}) => (
  <div className={clsx(classes.container, className)}>
    {
      steps.map((x) => (
        <div
          key={x.value}
          className={clsx(classes.item, classNameItem, {
            active: active === x.value,
          })}
        >
          <div />
          {x.label}
        </div>
      ))
    }
  </div>
)

TextStepper.defaultProps = {
  active: null,
  className: '',
  classNameItem: '',
}

TextStepper.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  classNameItem: PropTypes.string,
  steps: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  active: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
}

export default withStyles(style)(TextStepper)
