import React from 'react'
import { withStyles } from '@material-ui/core'
import StepConnector from '@material-ui/core/StepConnector'

const style = (theme) => ({
  alternativeLabel: {
    marginTop: -5,
    left: 'calc(-50%)',
    right: 'calc(50%)',
  },
  active: {
    '& $line': {
      borderColor: theme.color.primaryBlue,
    },
  },
  completed: {
    '& $line': {
      borderColor: theme.color.primaryBlue,
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 1,
  },
})

const Connector = (props) => <StepConnector {...props} />

export default withStyles(style)(Connector)
