import React from 'react'
import PropTypes from 'prop-types'
import { Typography, withStyles } from '@material-ui/core'
import clsx from 'clsx'

// TODO: improve the arrow
const stepperStyle = (theme) => ({
  stepContainer: {
    display: 'flex',
  },
  step: {
    flex: 1,
    minWidth: 150,
    padding: theme.spacing(0, 1),
    height: 36,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: theme.color.secondaryBlue3,
    '& .MuiTypography-root': {
      transform: 'translateX(4px)',
      userSelect: 'none',
    },

    '&:first-child': {
      borderRadius: '20px 0 0 20px',
      '& .MuiTypography-root': {
      },
    },

    '&:last-child': {
      borderRadius: '0 20px 20px 0',
      paddingRight: theme.spacing(2),
    },

    '&:not($active)': {
      color: theme.color.darkGray,
    },

    '&:not(:last-child)::after': {
      content: '""',
      display: 'inline-block',
      position: 'absolute',
      top: 7,
      right: -11,
      width: 20,
      height: 20,
      transform: 'rotate(-58.8deg) skewX(-22deg) scaleY(0.9)',
      backgroundColor: 'inherit',
      borderTopColor: 'transparent',
      borderLeftColor: 'transparent',
      zIndex: '1',
    },

    '&::after': {
      border: `1px solid ${theme.color.secondaryBlue2}`,
    },
  },
  preActive: {
    '&::after': {
      borderRightColor: theme.color.primaryBlue,
      borderBottomColor: theme.color.primaryBlue,
    },
  },
  active: {
    color: theme.color.white,
    backgroundColor: theme.color.primaryBlue,

    borderTopColor: theme.color.primaryBlue,
    borderBottomColor: theme.color.primaryBlue,
    '&::after': {
      borderRightColor: theme.color.primaryBlue,
      borderBottomColor: theme.color.primaryBlue,
    },
  },

  allowClick: {
    cursor: 'pointer',
  },
})

const FulfillmentStepper = ({
  classes,
  active,
  steps,
  onChangeStep,
  style,
  className,
  classNameStep,
}) => {
  const checkActive = (index) => active === steps[index]?.value
  return (
    <div className={clsx(classes.stepContainer, className)} style={style}>
      {steps.map((x, index) => (
        <div
          role="presentation"
          className={clsx(
            classes.step,
            classNameStep,
            {
              [classes.preActive]: checkActive(index + 1),
              [classes.active]: checkActive(index),
              [classes.allowClick]: onChangeStep,
            },
          )}
          key={x.value}
          onClick={() => onChangeStep && onChangeStep(x.value)}
        >
          <Typography variant="subtitle1">
            {x.label}
            {' '}
            {x.moreInfo ? `(${x.moreInfo})` : ''}
          </Typography>
        </div>
      ))}
    </div>
  )
}

FulfillmentStepper.defaultProps = {
  className: '',
  classNameStep: '',
  style: {},
  active: 0,
  steps: [],
  onChangeStep: null, // (newStepValue) => {}
}

FulfillmentStepper.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  classNameStep: PropTypes.string,
  style: PropTypes.shape(),
  steps: PropTypes.arrayOf(PropTypes.shape()),
  active: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  onChangeStep: PropTypes.func,
}

export default withStyles(stepperStyle)(FulfillmentStepper)
