import React from 'react'
import PropTypes from 'prop-types'
import { Typography, withStyles } from '@material-ui/core'
import clsx from 'clsx'

const stepperStyle = (theme) => ({
  stepContainer: {
    display: 'flex',
    paddingRight: theme.spacing(3),
  },
  step: {
    minWidth: 150,
    padding: theme.spacing(0, 1),
    height: 36,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    border: `1px solid ${theme.color.secondaryBlue2}`,
    borderLeft: 'none',
    borderRight: 'none',
    '& .MuiTypography-root': {
      fontSize: 14,
      transform: 'translateX(16px)',
    },

    '&:first-child': {
      borderRadius: '20px 0 0 20px',
      borderLeft: `1px solid ${theme.color.secondaryBlue2}`,
      '&$active': {
        borderLeftColor: theme.color.primaryBlue,
      },
      '& .MuiTypography-root': {
        transform: 'translateX(6px)',
      },
    },

    '&:not($active)': {
      color: theme.color.secondaryBlue2,
    },

    '&::after': {
      content: '""',
      display: 'inline-block',
      position: 'absolute',
      top: 1,
      right: -17,
      width: 31,
      height: 31,
      transform: 'rotate(-34.5deg) skewX(20deg) scaleY(0.95)',
      borderBottomRightRadius: 5,
      border: `1px solid ${theme.color.secondaryBlue2}`,
      borderTopColor: 'transparent',
      borderLeftColor: 'transparent',
    },
  },
  preActive: {
    '&::after': {
      borderRightColor: theme.color.primaryBlue,
      borderBottomColor: theme.color.primaryBlue,
    },
  },
  active: {
    borderTopColor: theme.color.primaryBlue,
    borderBottomColor: theme.color.primaryBlue,
    '&::after': {
      borderRightColor: theme.color.primaryBlue,
      borderBottomColor: theme.color.primaryBlue,
    },

  },
})

const BoardStepper = ({
  classes,
  active,
  steps,
  style,
  className,
  classNameStep,
}) => {
  const checkActive = (index) => active === steps[index]?.value
  return (
    <div className={clsx(classes.stepContainer, className)} style={style}>
      {steps.map((x, index) => (
        <div
          className={clsx(
            classes.step,
            classNameStep,
            {
              [classes.preActive]: checkActive(index + 1),
              [classes.active]: checkActive(index),
            },
          )}
          key={x.value}
        >
          <Typography variant="h6">
            {x.label}
          </Typography>
        </div>
      ))}
    </div>
  )
}

BoardStepper.defaultProps = {
  className: '',
  classNameStep: '',
  style: {},
  active: 0,
  steps: [],
}

BoardStepper.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  classNameStep: PropTypes.string,
  style: PropTypes.shape(),
  steps: PropTypes.arrayOf(PropTypes.shape()),
  active: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
}

export default withStyles(stepperStyle)(BoardStepper)
