import React from 'react'
import PropTypes from 'prop-types'
import {
  withStyles,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core'
import CheckIcon from '@material-ui/icons/Check'
import clsx from 'clsx'

const style = (theme) => ({
  formControlLabel: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    marginLeft: -8,
    '& .MuiFormControlLabel-label': {
      color: theme.color.black,
      fontSize: 14,
    },
  },
  checkedIcon: {
    width: 20,
    height: 20,
    border: `1px solid ${theme.color.secondaryBlue8}`,
    borderRadius: 3,
    background: theme.color.secondaryBlue3,
  },
  uncheckedIcon: {
    width: 20,
    height: 20,
    border: `1px solid ${theme.color.secondaryBlue2}`,
    borderRadius: 3,
    background: theme.color.secondaryBlue3,
    color: 'transparent',
    '&.error': {
      border: `1px solid ${theme.color.rubyRed1}`,
    },
    '&.whiteColor': {
      background: 'white',
    },
  },
  disabledBorder: {
    border: `1px solid ${theme.color.secondaryBlue2}`,
  },
})

const CheckBoxStyled = withStyles((theme) => ({
  root: {
    color: theme.color.secondaryBlue2,
    padding: 8,
  },
  checked: {},
}))((props) => <Checkbox {...props} />)

const CheckBox = ({
  classes,
  label,
  disabled,
  color,
  error,
  children,
  className,
  whiteColor,
  ...props
}) => (
  <FormControlLabel
    className={clsx(classes.formControlLabel, className)}
    label={label || children}
    control={(
      <CheckBoxStyled
        color={color}
        disabled={disabled}
        checkedIcon={(
          <CheckIcon
            className={clsx(classes.checkedIcon, { [classes.disabledBorder]: disabled })}
          />
        )}
        icon={<CheckIcon className={clsx(classes.uncheckedIcon, { error }, { whiteColor })} />}
        role="presentation"
        onClick={(e) => {
          e.stopPropagation()
        }}
        {...props}
      />
    )}
    {
      ...(disabled ? { onClick: (e) => e.preventDefault() } : {})
    }
  />
)

CheckBox.defaultProps = {
  color: 'primary',
  disabled: false,
  className: '',
  label: '',
  children: '',
  error: false,
  whiteColor: false,
}

CheckBox.propTypes = {
  classes: PropTypes.shape().isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape(),
  ]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape(),
  ]),
  color: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  whiteColor: PropTypes.bool,
}

export default withStyles(style)(CheckBox)
