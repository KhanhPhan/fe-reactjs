import React, { useEffect, useState } from 'react'
import { useFieldValue } from 'utils/formik/hooks'
import SearchBox from 'components/common/AsyncSelect/SearchBox'
import countryApi from 'services/apis/country'

const StateSearch = () => {
  const allCountries = countryApi.getList()
  const [state, setState] = useFieldValue('state')
  const [country] = useFieldValue('country')
  const [selectedStates, setSelectedStates] = useState([])
  const allStates = (country && country.length > 0
    ? allCountries.filter((c) => country.includes(c.code))
    : allCountries)
    .map((c) => c.states.map((st) => ({ name: st.name, code: `${c.code}:${st.code}` })))
    .flat().map(({ name, code }) => ({ value: code, label: name }))

  useEffect(() => {
    if (state && state.length !== selectedStates.length) {
      setSelectedStates(allStates.filter((x) => state.find((y) => y === x.value)))
    }
  }, [state])

  const onStateSelect = (sltItems) => {
    setSelectedStates(sltItems)
    if (!sltItems) {
      setState([])
      return
    }
    const stateCodes = []
    sltItems.forEach((p) => {
      stateCodes.push(p.value)
    })
    setState(stateCodes)
  }

  return (
    <SearchBox
      value={selectedStates}
      options={allStates?.filter((x) => !selectedStates.find((y) => y.value === x.value))}
      handleFilterOption={
        (res) => res.filter((x) => !selectedStates.find((y) => y.value === x.value))
      }
      inputProps={{
        placeholder: 'Select a state/ prov.',
      }}
      multiple
      popupIcon
      disableClearable
      onSearchBoxChange={(object, value) => {
        onStateSelect(value)
      }}
    />
  )
}

StateSearch.defaultProps = {}

StateSearch.propTypes = {}

export default StateSearch
