import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  withStyles,
  TextField,
  makeStyles,
  FormLabel,
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { KeyboardArrowDown } from '@material-ui/icons'
import clsx from 'clsx'
import IconButton from '@material-ui/core/IconButton'
import Icon from 'components/common/Icon'
import { colors } from 'theme'
import Overlay from '../Overlay'

const style = (theme) => ({
  root: {
    flex: 1,
    width: '100%',
    height: theme.spacing(4),
    paddingBottom: theme.spacing(2),

    '& input': {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontSize: 14,
      background: theme.color.backgroundGray,
    },
    '& textarea': {
      fontSize: 14,
    },
    '& .MuiFormHelperText-root:first-letter': {
      textTransform: 'uppercase',
    },

    '& .MuiInputBase-root.Mui-disabled': {
      background: theme.color.disableGray2,
      pointerEvents: 'none',
    },

    '& .MuiFormHelperText-contained': {
      marginRight: 3,
      marginLeft: 3,
      marginTop: 1,
    },

    '& .MuiFormHelperText-root.Mui-error': {
      color: theme.color.rubyRed1,
    },
  },
  popperDisablePortal: {
    position: 'relative',
  },
  inputRoot: {
    height: 40,
  },
  option: {
    borderRadius: 5,
    '&:hover': {
      background: theme.color.secondaryBlue3,
      color: theme.color.darkGray,
    },
    fontSize: 14,
  },
  paper: {
    marginTop: theme.spacing(1),
    boxShadow: '0px 2px 10px #D6DEF2',
    borderRadius: 5,
  },

  textInputRoot: {
    background: theme.color.backgroundGray,
    paddingBottom: theme.spacing(2),

    '& input': {
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontSize: 14,
      background: 'none',
    },
    '& textarea': {
      fontSize: 14,
    },
    '& .MuiFormHelperText-root:first-letter': {
      textTransform: 'uppercase',
    },

    '& .MuiInputBase-root.Mui-disabled': {
      background: theme.color.disableGray2,
      pointerEvents: 'none',
    },
  },
})

const useTextFieldStyled = makeStyles({
  input: {
    '&.disabled': {
      color: colors.disableGray,
    },
    padding: '0 4px !important',
    fontSize: 14,
  },
  cssFocused: {
  },
  notchedOutline: {

  },
  option: {
    minWidth: 120,
  },
})

const useFormLabelStyles = makeStyles((theme) => ({
  formLabel: {
    textAlign: 'left',
    fontSize: 14,
    letterSpacing: 0,
    color: theme.color.black,
    marginBottom: theme.spacing(1),
    opacity: 1,
  },
}))

const useIconCloseStyles = makeStyles(() => ({
  clearRangePrice: {
    position: 'absolute',
    right: -2,
    bottom: -16,
    padding: 10,
  },
}))

const Dropdown = ({
  classes: allClasses,
  options,
  label,
  placeholder,
  value,
  inputProps,
  readOnly,
  disabled,
  className,
  legend,
  onClear,
  error,
  helperText,
  ...props
}) => {
  const { textInputRoot, ...classes } = allClasses
  const textFieldClasses = useTextFieldStyled()
  const formLabelClasses = useFormLabelStyles()
  const iconCloseClasses = useIconCloseStyles()
  const [overlay, setOverlay] = useState(false)

  return (
    <>
      {legend && (
        <FormLabel
          className={
            formLabelClasses.formLabel
          }
          component="legend"
        >
          {legend}
        </FormLabel>
      )}
      <Autocomplete
        className={clsx(
          classes.root,
          className,
        )}
        classes={{
          ...classes,
          popperDisablePortal: classes.popperDisablePortal,
        }}
        options={options}
        value={options.find((e) => e.value === value) || { value: '', label: '' }}
        disableClearable
        openOnFocus
        getOptionSelected={(option, val) => option.label === val.label}
        getOptionLabel={(option) => option.label || ''}
        popupIcon={onClear && value ? (
          <IconButton className={iconCloseClasses.clearRangePrice} size="small" onClick={onClear}>
            <Icon
              name="close"
              color={colors.primaryBlue}
              size={10}
            />
          </IconButton>
        ) : <KeyboardArrowDown color={disabled ? 'disabled' : 'primary'} />}
        renderInput={(params) => (
          <TextField
            className={textInputRoot}
            {...params}
            label={label}
            variant="outlined"
            size="small"
            placeholder={placeholder}
            error={error}
            helperText={helperText}
            InputProps={{
              ...params.InputProps,
              classes: {
                input: clsx(textFieldClasses.input, disabled && 'disabled'),
                focused: textFieldClasses.cssFocused,
                notchedOutline: clsx(textFieldClasses.notchedOutline, disabled && 'disabled'),
              },
              inputProps: {
                readOnly,
                ...params.inputProps,
                autoComplete: 'new-password',
              },
            }}
            {...inputProps}
          />
        )}
        disabled={disabled}
        onOpen={() => {
          setOverlay(true)
          document.body.style.overflow = 'hidden'
        }}
        onClose={() => {
          setOverlay(false)
          document.body.style.overflow = ''
        }}
        {...props}
      />
      {overlay && <Overlay />}
    </>
  )
}

Dropdown.defaultProps = {
  label: '',
  options: [],
  placeholder: '',
  inputProps: {},
  value: '',
  readOnly: false,
  disabled: false,
  className: '',
  legend: '',
  onClear: null,
  error: false,
  helperText: '',
}

Dropdown.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape()),
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  readOnly: PropTypes.bool,
  inputProps: PropTypes.oneOfType([PropTypes.object, PropTypes.shape()]),
  disabled: PropTypes.bool,
  legend: PropTypes.string,
  onClear: PropTypes.func,
  error: PropTypes.bool,
  helperText: PropTypes.string,
}

export default withStyles(style)(Dropdown)
