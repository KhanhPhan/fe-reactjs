/* eslint-disable no-async-promise-executor */
/* eslint-disable no-underscore-dangle */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles, Typography } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import Button from 'components/common/Button'
import style from './style'

const DropFiles = (props) => {
  const {
    classes,
    uploadMultiFile,
    destination,
    acceptTypes,
    titleUpload,
    didFinishUploadFiles,
  } = props
  const [addFileError, setAddFileError] = useState('')

  const onFileLoad = (files) => {
    const isTypeAccept = Array
      .from(files).some((e) => acceptTypes && acceptTypes.indexOf(e.type.split('/')[1]) === -1)
    const isImageFiles = Array.from(files).some((e) => e.type.split('/')[0] === 'image')
    const isVideoFiles = Array.from(files).some((e) => e.type.split('/')[0] === 'video')
    const isImageFileType = Array
      .from(files).some(
        (e) => e.type.split('/')[1] === 'jpg'
        || e.type.split('/')[1] === 'png'
        || e.type.split('/')[1] === 'jpeg',
      )
    const fileLarger10MB = Array.from(files).find((e) => (e.size / 1024 / 1024) > 10)

    if (isTypeAccept) {
      setAddFileError('Sorry, this file type is not supported.')
    } else if (isImageFiles && !isTypeAccept && !isImageFileType) {
      setAddFileError('For image formats, we only support JPG, JPEG and PNG for now')
    } else if (isVideoFiles && !isTypeAccept) {
      setAddFileError('For video formats, we only support MP4 and MOV for now')
    } else if (fileLarger10MB) {
      setAddFileError('Your file is bigger than 10MB')
    } else {
      didFinishUploadFiles(files)
    }
  }

  const onDrop = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (!uploadMultiFile && (e.dataTransfer.files || []).length > 1) {
      setAddFileError('You can upload at most 1 file')
    } else {
      onFileLoad(e.dataTransfer.files)
    }
  }
  const onDragOver = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }

  return (
    <Card
      className={classes.uploadContainer}
      onDrop={onDrop}
      onDragOver={onDragOver}
    >
      {
        destination && (
        <Typography variant="subtitle1" className={classes.title}>
          Upload Destination
        </Typography>
        )
      }

      { titleUpload.length
        && (
        <div className={classes.uploadTitle}>
          {`${titleUpload} > Customer Documents`}
        </div>
        )}

      <div className={classes.uploadContent}>
        <>
          {!addFileError ? (
            <>
              <div className={classes.subTitle}>Drop files here to upload</div>
              <div className={classes.smallText}>or</div>
              <label
                className={classes.button}
                htmlFor="file-upload"
              >
                {`Select ${uploadMultiFile ? 'Files' : 'File'}`}
                <input
                  className={classes.input}
                  id="file-upload"
                  type="file"
                  name="myFile"
                  accept={acceptTypes}
                  multiple={uploadMultiFile}
                  onChange={(e) => onFileLoad(e.target.files)}
                />
              </label>
              <div className={classes.smallText}>Maximum size: 10 MB</div>
            </>
          ) : (
            <div className={classes.errorOverlay}>
              <img className={classes.errorImageIcon} alt="warning" src="/icons/warning.svg" />
              <h3 className={classes.errorTitle}>ERROR</h3>
              <p className={classes.errorDescription}>{addFileError}</p>
              <Button
                variant="contained"
                rounded
                className={classes.errorOkButton}
                onClick={() => setAddFileError('')}
              >
                Ok
              </Button>
            </div>
          )}
        </>
      </div>
    </Card>
  )
}
DropFiles.propTypes = {
  classes: PropTypes.shape().isRequired,
  uploadMultiFile: PropTypes.bool,
  acceptTypes: PropTypes.string,
  titleUpload: PropTypes.string,
  didFinishUploadFiles: PropTypes.func,
  destination: PropTypes.bool,
}

DropFiles.defaultProps = {
  uploadMultiFile: false,
  destination: false,
  acceptTypes: '',
  titleUpload: '',
  didFinishUploadFiles: () => {},
}

export default withStyles(style)(DropFiles)
