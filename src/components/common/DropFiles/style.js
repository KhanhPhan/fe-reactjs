const style = (theme) => ({
  uploadContainer: {
    background: '#F8F9FF',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: '100%',
  },
  uploadTitle: {
    border: '1px solid #B4BFD9',
    background: 'white',
    padding: 10,
    fontWeight: '500',
    fontSize: 14,
    width: '95%',
    marginTop: 5,
    borderRadius: 3,
    color: '#373737',
  },
  button: {
    background: theme.color.primaryBlue,
    color: 'white',
    padding: '10px 20px',
    borderRadius: 25,
    fontSize: 14,
    cursor: 'pointer',
  },
  input: {
    display: 'none',
  },
  uploadContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  subTitle: {
    fontSize: 14,
    fontWeight: 500,
  },
  smallText: {
    fontSize: 14,
    color: theme.color.textGray2,
    margin: '12px 0',
  },
  errorText: {
    fontSize: 14,
    margin: '14px 0 ',
  },

  errorOverlay: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  errorImageIcon: {
    height: 88,
    textAlign: 'center',
    margin: '0 auto 10px auto',
    pointerEvents: 'none',
  },
  errorTitle: {
    textAlign: 'center',
    fontSize: 28,
  },
  errorDescription: {
    marginBottom: 20,
  },
  cancelButton: {
    marginTop: 20,
  },
  loading: {
    marginTop: 20,
    marginBottom: 10,
  },
  title: {
    width: '96.5%',
    textAlign: 'left',
    marginTop: 20,
  },
})

export default style
