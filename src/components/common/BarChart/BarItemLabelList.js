import React from 'react'
import PropTypes from 'prop-types'

import BarItemLabel from './BarItemLabel'

const BarItemLabelList = ({ items, loadTime, renderBarItemLabel }) => (
  <>
    { items.map(({
      x, y, direction, width, height, value, color, maxWidth, totalOrder,
    }) => (
      <BarItemLabel
        key={x}
        x={x}
        y={y}
        width={width}
        height={height}
        direction={direction}
        value={value}
        color={color}
        maxWidth={maxWidth}
        loadTime={loadTime}
        renderBarItemLabel={renderBarItemLabel}
        totalOrder={totalOrder}
      />
    ))}
  </>
)

BarItemLabelList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    direction: PropTypes.oneOf(['up', 'down']).isRequired,
    totalOrder: PropTypes.number.isRequired,
  })).isRequired,
  loadTime: PropTypes.number.isRequired,
  // eslint-disable-next-line
  renderBarItemLabel: PropTypes.func
}

export default BarItemLabelList
