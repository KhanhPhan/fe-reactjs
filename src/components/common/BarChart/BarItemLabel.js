import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'

import useAnimationBar from './useAnimationBar'
import { abbreviateNumber } from './utils'

const BarItemLabel = ({
  x, y, width, height, direction, value, color, loadTime, renderBarItemLabel, totalOrder,
}) => {
  const ref = useRef()
  const defaultCurveForLargeValue = 10
  const defaultCurveForSmallValue = 5

  const [labelHeight, setLabelHeight] = useState(15)

  const barHeightScale = useAnimationBar({
    loadTime, height, defaultCurveForLargeValue, defaultCurveForSmallValue,
  })

  useEffect(() => setLabelHeight(ref.current?.clientHeight || 15), [])

  const top = direction === 'up'
    ? y + height - barHeightScale - (height > 10
      ? defaultCurveForLargeValue
      : defaultCurveForSmallValue) - labelHeight - 5
    // `5`: add a little space to make the text appears more clear
    // why `10`?
    // add a little space to make the text more clear
    // feel free to play around with this value
    : y - height + barHeightScale + (height > 10
      ? defaultCurveForLargeValue : defaultCurveForSmallValue) + labelHeight - 5 // same above

  return (
    <span
      style={{
        position: 'absolute',
        top,
        left: x + width / 2,
        fontSize: 12,
        transform: 'translateX(-50%)',
        color,
      }}
      ref={ref}
    >
      {(renderBarItemLabel && renderBarItemLabel({ totalOrder })) || abbreviateNumber(value)}
    </span>
  )
}

BarItemLabel.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  direction: PropTypes.oneOf(['up', 'down']).isRequired,
  color: PropTypes.string.isRequired,
  loadTime: PropTypes.number.isRequired,
  // eslint-disable-next-line
  renderBarItemLabel: PropTypes.func,
  totalOrder: PropTypes.number.isRequired,
}

export default BarItemLabel
