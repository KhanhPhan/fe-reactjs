/* eslint-disable no-shadow */
import React from 'react'
import PropTypes from 'prop-types'

import { hexToRgb, withStyles } from '@material-ui/core'
import { formatNumberWithSign } from 'utils/common'
import { colors } from 'theme'
import BarItem from './BarItem'

const color = `rgba(${hexToRgb(colors.darkGray).replace('rgb(', '').replace(')', '')}, 0.8)`
const style = () => ({
  root: {
    '& .tooltip': {
      visibility: 'hidden',
      background: color,
      color: theme.color.white,
      textAlign: 'center',
      borderRadius: '10px',
      padding: '2px 0',
      zIndex: 1,
      height: '32px',
      display: 'grid',
      '&::after': {
        content: '""',
        position: 'absolute',
        left: '50%',
        top: '50%',
        marginLeft: '-8px',
        borderLeft: '6px solid transparent',
        borderRight: '6px solid transparent',
        borderTop: `6px solid ${color}`,
      },
    },
    '&:hover': {
      '& .tooltip': {
        visibility: 'visible',
      },
    },
  },
})

const BarItemList = ({ items, loadTime, classes }) => (
  <g>
    { items.map(({
      x, y, width, height, direction, value, color, totalOrder,
    }) => (
      <g key={x} className={classes.root} width={width} height={height}>
        <BarItem
          x={x}
          y={y}
          width={width}
          height={height}
          direction={direction}
          value={value}
          color={color}
          loadTime={loadTime}
        />
        <foreignObject width="74" height="71" x={x - 30} y={y - 60}>
          {
            totalOrder > 0 && value > 0
            && (
              <div className="tooltip">
                <span>{totalOrder > 1 ? `${totalOrder} orders` : `${totalOrder} order`}</span>
                <span>{formatNumberWithSign(value)}</span>
              </div>
            )
          }
        </foreignObject>
      </g>
    )) }
  </g>
)

BarItemList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    direction: PropTypes.oneOf(['up', 'down']).isRequired,
    totalOrder: PropTypes.number.isRequired,
  })).isRequired,
  loadTime: PropTypes.number.isRequired,
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(BarItemList)
