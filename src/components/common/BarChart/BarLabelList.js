import React from 'react'
import PropTypes from 'prop-types'
import { colors } from 'theme'

const BarLabelList = ({ items }) => (
  <>
    {
      items.map(({
        x, y, width, name, maxWidth,
      }) => (
        <span
          key={`${name}-${x}`}
          title={name}
          style={{
            width: maxWidth,
            textAlign: 'center',
            position: 'absolute',
            top: y,
            left: (x + width / 2) - (maxWidth / 2),
            fontSize: 12,
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            color: colors.textGray2,
          }}
        >
          {name}
        </span>
      ))
    }
  </>
)

BarLabelList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
  })).isRequired,
}

export default BarLabelList
