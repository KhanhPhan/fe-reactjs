import React, { useMemo } from 'react'
import PropTypes from 'prop-types'

import { colors } from 'theme'
import { abbreviateNumber } from './utils'

const BarGridLine = ({ items }) => {
  const calcLines = useMemo(() => items.map((l) => ({
    ...l,
    count: abbreviateNumber(l.count),
  })), [items])

  return (
    <g>
      {calcLines.map(({
        x1, x2, y1, y2, count, baseLine,
      }) => (
        <g key={count}>
          <line
            x1={x1}
            x2={x2}
            y1={y1}
            y2={y2}
            stroke={baseLine ? colors.secondaryBlue2 : colors.secondaryBlue3}
          />
          <text dy=".32em" x="0" y={y1} fill={colors.textGray2}>{count}</text>
        </g>
      ))}
    </g>
  )
}

BarGridLine.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    x1: PropTypes.number.isRequired,
    x2: PropTypes.number.isRequired,
    y1: PropTypes.number.isRequired,
    y2: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired,
  })).isRequired,
}

export default BarGridLine
