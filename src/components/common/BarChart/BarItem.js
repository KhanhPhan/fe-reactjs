import React from 'react'
import PropTypes from 'prop-types'

import useAnimationBar from './useAnimationBar'

const BarItem = ({
  x, y, width, height, color, loadTime, direction,
}) => {
  const defaultCurveForLargeValue = 10
  const defaultCurveForSmallValue = 5

  const barHeightScale = useAnimationBar({
    loadTime, height, defaultCurveForLargeValue, defaultCurveForSmallValue,
  })

  if (height <= 5) {
    if (direction === 'up') {
      return (
        <path
          d={`
              M ${x} ${y + height}
              L ${x} ${y}
              L ${x + width} ${y}
              L ${x + width} ${y + height}
            `}
          style={{ fill: color }}
        />
      )
    }

    return (
      <path
        d={`
            M ${x} ${y - height}
            L ${x} ${y}
            L ${x + width} ${y}
            L ${x + width} ${y - height}
          `}
        style={{ fill: color }}
      />
    )
  }

  // Why `y + height - 5`?
  // It should be `y + height` for the correct data, right?
  // Yes. It should be if we use `<rect />`
  // But we use `path` and draw a curve, we need to add a little bit of pixel to cover the curve
  if (direction === 'up') {
    return (
      <path
        d={`
            M ${x} ${y + height} 
            L ${x} ${y + height - barHeightScale} 
            C ${x - 5 + width / 3} ${
          y + height - 5 - barHeightScale - (
            height > 10
              ? defaultCurveForLargeValue
              : defaultCurveForSmallValue)}, ${x + 5 + (2 * width) / 3} ${
          y + height - 5 - barHeightScale - (
            height > 10
              ? defaultCurveForLargeValue
              : defaultCurveForSmallValue)}, ${x + width} ${y + height - barHeightScale} 
            L ${x + width} ${y + height} 
            L ${x - width} ${y + height}
          `}
        style={{ fill: color }}
      />
    )
  }

  return (
    <path
      d={`
          M ${x} ${y - height} 
          L ${x} ${y - height + barHeightScale} 
          C ${x - 5 + width / 3} ${
        y - height + 5 + barHeightScale + (
          height > 10
            ? defaultCurveForLargeValue
            : defaultCurveForSmallValue
        )}, ${x + 5 + (2 * width) / 3} ${y - height + 5 + barHeightScale + (
        height > 10
          ? defaultCurveForLargeValue
          : defaultCurveForSmallValue)}, ${x + width} ${y - height + barHeightScale} 
          L ${x + width} ${y - height} 
          L ${x - width} ${y - height}
        `}
      style={{ fill: color }}
    />
  )
}

BarItem.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  loadTime: PropTypes.number.isRequired,
  direction: PropTypes.oneOf(['up', 'down']).isRequired,
}

export default BarItem
