import { interpolate } from 'd3-interpolate'
import { scaleSequential } from 'd3-scale'
import { colors } from 'theme'

// This function is inspired from `js-abbreviation-number` package
// with some modification to fit our needs
// since `js-abbreviation-number` doesn't handle scientific number
export const abbreviateNumber = (num, toFixed = 1) => {
  const symbols = ['', 'K', 'M', 'G', 'T', 'P']

  // handle negatives
  const sign = Math.sign(num) >= 0
  const absNum = Math.abs(num)

  // what tier? (determines SI symbol)
  const tier = (Math.log10(absNum) / 3) | 0 // eslint-disable-line no-bitwise

  // if zero, we don't need a suffix
  if (tier === 0) {
    return Number.isInteger(absNum) ? num.toString() : Number.parseFloat(num).toFixed(2)
  }

  // get suffix and determine scale
  const suffix = tier >= 6 ? `e+${tier * 3}` : symbols[tier]

  const scale = 10 ** (tier * 3)

  // scale the number
  const scaled = absNum / scale

  const rounded = scaled.toFixed(toFixed)

  // format number and add suffix
  return (!sign ? '-' : '') + rounded + suffix
}

export const colorRange = (
  domain, startColor = colors.blueGradientStart, endColor = colors.blueGradientEnd,
) => {
  const [min, max] = [Math.min(...domain), Math.max(...domain)]
  return scaleSequential().domain([max, min]).interpolator(interpolate(startColor, endColor))
}
