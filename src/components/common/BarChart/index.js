import React, {
  useMemo, useEffect, useRef, useState,
} from 'react'
import PropTypes from 'prop-types'
import { scaleLinear, scalePoint } from 'd3-scale'
import debounce from 'lodash/debounce'

import BarGridLine from './BarGridLine'
import BarItemList from './BarItemList'
import BarLabelList from './BarLabelList'
import BarItemLabelList from './BarItemLabelList'
import { colorRange } from './utils'

const defaultConfig = {
  margin: {
    top: 30, right: 0, bottom: 60, left: 30,
  },
  height: 250,
  barItemWidth: 10,
  loadTime: 1000,
}

const BarChart = ({
  data, sort, config, renderBarItemLabel,
}) => {
  const {
    margin, height, barItemWidth, barItemColor, loadTime,
  } = { ...defaultConfig, ...config }

  const barChartRef = useRef()
  const [width, setWidth] = useState(0)

  useEffect(() => {
    // init barChart width
    setWidth(barChartRef.current.clientWidth)

    const resize = debounce(() => {
      setWidth(barChartRef.current.clientWidth)
    }, 200)

    window.addEventListener('resize', resize)

    return () => window.removeEventListener('resize', resize)
  }, [])

  const [chartWidth, chartHeight, chartData, chartLines, chartLabels] = useMemo(() => {
    if (!width) return [0, height, [], [], []]

    let sortedData
    let bandNames
    let bandValues
    let bandTotalOrders
    // use this value coz the sort() mutates the original value
    // and we don't want to do that
    const derivedData = [...data]

    switch (true) {
      case sort === 'asc': {
        sortedData = derivedData.sort(
          (a, b) => (a.value > b.value ? 1 : -1),
        )
        bandNames = sortedData.map((d) => d.name)
        bandValues = sortedData.map((d) => d.value)
        bandTotalOrders = sortedData.map((d) => d.totalOrder)
        break
      }
      case sort === 'desc': {
        sortedData = derivedData.sort((a, b) => (a.value > b.value ? -1 : 1))
        bandNames = sortedData.map((d) => d.name)
        bandValues = sortedData.map((d) => d.value)
        bandTotalOrders = sortedData.map((d) => d.totalOrder)
        break
      }
      case typeof sort === 'function': {
        sortedData = derivedData.sort(sort)
        bandNames = sortedData.map((d) => d.name)
        bandValues = sortedData.map((d) => d.value)
        bandTotalOrders = sortedData.map((d) => d.totalOrder)
        break
      }
      default: {
        bandNames = derivedData.map((d) => d.name)
        bandValues = derivedData.map((d) => d.value)
        bandTotalOrders = derivedData.map((d) => d.totalOrder)
      }
    }

    // descending sort
    const sortedBandValues = derivedData.sort(
      (a, b) => (a.value > b.value ? -1 : 1),
    ).map((d) => d.value)

    const hasAllPositiveNum = sortedBandValues[sortedBandValues.length - 1] >= 0
    const hasAllNegativeNum = sortedBandValues[0] <= 0

    let yScale
    if (hasAllNegativeNum) {
      yScale = scaleLinear().domain(
        sortedBandValues.includes(0)
          ? [sortedBandValues[0], sortedBandValues[sortedBandValues.length - 1]]
          : [0, sortedBandValues[sortedBandValues.length - 1]],
      )
    }

    if (hasAllPositiveNum) {
      yScale = scaleLinear().domain(
        sortedBandValues.includes(0)
          ? [sortedBandValues[0], sortedBandValues[sortedBandValues.length - 1]]
          : [sortedBandValues[0], 0],
      )
    }

    // has both negative and positive values
    if (!hasAllPositiveNum && !hasAllNegativeNum) {
      yScale = scaleLinear()
        .domain([sortedBandValues[0], sortedBandValues[sortedBandValues.length - 1]])
    }

    let ticks = yScale.ticks(5).sort((a, b) => (a > b ? -1 : 1))

    const maxTick = ticks[0]
    // round the grid lines to above the maximum of datinum
    // round the grid lines to above the maximum of datinum again increase height show tooltip
    if (maxTick < yScale.domain()[0]) {
      ticks.unshift(maxTick + (ticks[0] - ticks[1]) * 2, maxTick + (ticks[0] - ticks[1]))
    }

    const minTick = ticks[ticks.length - 1]
    // round the grid lines to below the minimum of datinum
    // round the grid lines to below the minimum of datinum again increase height show tooltip
    if (minTick > yScale.domain()[1]) {
      ticks.push(minTick - (ticks[0] - ticks[1]), minTick - (ticks[0] - ticks[1]) * 2)
    }

    // Remove decimal number from ticks
    ticks = ticks.filter(Number.isInteger)

    const max = ticks[0]
    const min = ticks[ticks.length - 1]

    const yAxis = scaleLinear()
      .domain([max, min])
      .range([margin.top, height - margin.bottom])

    const xAxis = scalePoint()
      .domain(bandNames)
      .range([margin.left, width - margin.right])
      .padding(0.5)
      .round(true)

    const bandColors = colorRange(bandValues)

    const items = bandNames.map((b, i) => ({
      x: xAxis(b) - barItemWidth / 2,
      y: yAxis(bandValues[i]),
      width: barItemWidth,
      height: bandValues[i] <= 0
        ? yAxis(bandValues[i]) - yAxis(0) : yAxis(0) - yAxis(bandValues[i]),
      value: bandValues[i],
      color: barItemColor || bandColors(bandValues[i]),
      direction: bandValues[i] < 0 ? 'down' : 'up',
      totalOrder: bandTotalOrders[i],
    }))

    const lines = ticks.map((t, i) => ({
      x1: 50,
      y1: yAxis(t),
      x2: width,
      y2: yAxis(t),
      count: ticks[i],
      baseLine: t === 0,
    }))

    const labels = bandNames.map((b, i) => ({
      name: b,
      x: items[i].x,
      // why `12` & `20`
      // just to add more spaces between chart item and text.
      // feel free to play around with these values
      y: bandValues[i] >= 0 ? items[i].y + items[i].height + 12 : items[i].y - items[i].height - 20,
      width: items[i].width,
      // 10 = marginLeft + marginRight of a given item
      // to add a little bit spacing between items
      maxWidth: (width - margin.left - margin.right) / (bandNames.length) - 10,
    }))

    return [width, height, items, lines, labels]
  }, [data, width])

  return (
    <div style={{ width: '100%', position: 'relative' }} ref={barChartRef}>
      <svg width={chartWidth} height={chartHeight} fontSize="12px">
        <BarGridLine items={chartLines} />
        <BarItemList items={chartData} loadTime={loadTime} />
      </svg>
      <BarLabelList items={chartLabels} />
      <BarItemLabelList
        items={chartData}
        loadTime={loadTime}
        renderBarItemLabel={renderBarItemLabel}
      />
    </div>
  )
}

BarChart.defaultProps = {
  config: defaultConfig,
}

BarChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.number,
  })).isRequired,
  config: PropTypes.shape({
    height: PropTypes.number,
    margin: PropTypes.shape({
      top: PropTypes.number,
      right: PropTypes.number,
      bottom: PropTypes.number,
      left: PropTypes.number,
    }),
    barItemWidth: PropTypes.number,
    barItemColor: PropTypes.string,
    loadTime: PropTypes.number,
  }),
  // eslint-disable-next-line
  sort: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  // eslint-disable-next-line
  renderBarItemLabel: PropTypes.func,
}

export default BarChart
