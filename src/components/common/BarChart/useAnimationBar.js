import { useEffect, useState } from 'react'
import { scaleLinear } from 'd3-scale'

const useAnimationBar = ({
  loadTime, height, defaultCurveForLargeValue, defaultCurveForSmallValue,
}) => {
  const [barHeightScale, setBarHeightScale] = useState(0)

  useEffect(() => {
    let i = 0
    let maxHeightBeforeCurve = height

    if (height > 10) {
      maxHeightBeforeCurve = height - defaultCurveForLargeValue
    } else if (height > 5) {
      maxHeightBeforeCurve = height - defaultCurveForSmallValue
    }

    const heightScale = scaleLinear().domain([0, loadTime]).range([0, maxHeightBeforeCurve])

    const id = setInterval(() => {
      i += 1
      if (i * 50 > loadTime) {
        clearInterval(id)
        return
      }

      setBarHeightScale(heightScale(i * 50))
    }, 50)

    return () => clearInterval(id)
  }, [height, loadTime])

  return barHeightScale
}

export default useAnimationBar
