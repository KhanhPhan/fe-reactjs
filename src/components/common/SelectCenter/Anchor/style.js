const style = (theme) => ({
  root: {
    '& .MuiOutlinedInput-root': {
      '& fieldset.MuiOutlinedInput-notchedOutline': {
        borderColor: 'rgba(0, 0, 0, 0.23)',
        borderWidth: '1px',
      },
    },

    '& input.MuiInputBase-input': {
      paddingRight: 0,
    },
  },

  open: {
    '& .MuiOutlinedInput-root': {
      '& fieldset.MuiOutlinedInput-notchedOutline': {
        borderColor: theme.color.primaryBlue,
        borderWidth: '2px',
      },
    },
  },

  icon: {
    padding: 3,
    pointerEvents: 'none',
    marginLeft: theme.spacing(0.5),
  },

  iconClear: {
    padding: 10,
    pointerEvents: 'all',
    borderRadius: '50%',
  },

  counter: {
    color: theme.color.viridianGreen1,
    fontSize: 14,
    marginLeft: theme.spacing(0.5),
  },

  inputBaseRoot: {
    cursor: 'pointer',
  },

  inputBaseInput: {
    textAlign: 'left',
  },
})

export default style
