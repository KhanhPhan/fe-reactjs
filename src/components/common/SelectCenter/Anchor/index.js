import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import { isEmpty, last } from 'lodash'
import { withStyles } from '@material-ui/core'
import { KeyboardArrowDown, KeyboardArrowUp } from '@material-ui/icons'
import IconButton from '@material-ui/core/IconButton'

import { propTypes } from 'react-barcode'
import Icon from 'components/common/Icon'
import TextInput from 'components/common/TextInput'
import { clear } from 'utils/storage/localStorage'
import { colors } from 'theme'
import style from './style'

const Anchor = ({
  classes,
  placeholder,
  value,
  disabled,
  className,
  options,
  clearable,
  disableHoverEffect,
  onOpen,
  handleClear,
  isOpen,
  ...props
}) => {
  const onFocus = (e) => {
    e.preventDefault()
    e.target.blur()
    onOpen(e.target.offsetParent)
  }

  const renderValue = useCallback(() => {
    if (isEmpty(value)) {
      return ''
    }

    if (Array.isArray(value)) {
      return options.find((opt) => opt?.value === last(value))?.label || ''
    }

    return options.find((opt) => opt?.value === value)?.label || ''
  }, [options, value])

  const renderCounter = useCallback(() => {
    if (Array.isArray(value) && value.length > 1) {
      return <span className={classes.counter}>{`+${value.length - 1}`}</span>
    }

    return null
  }, [value])

  const renderIcon = useCallback(() => {
    if (isOpen) {
      return <KeyboardArrowUp color="primary" className={classes.icon} />
    }

    if (!isEmpty(value) && clearable) {
      return (
        <IconButton
          size="small"
          onClick={(e) => {
            e.stopPropagation()
            handleClear()
          }}
          className={clsx(classes.icon, classes.iconClear)}
        >
          <Icon
            name="close"
            color={
              disabled ? 'rgba(0, 0, 0, 0.26)' : colors.primaryBlue
            }
            size={10}
          />
        </IconButton>
      )
    }

    return (
      <KeyboardArrowDown
        color={disabled ? 'disabled' : 'primary'}
        className={classes.icon}
      />
    )
  }, [isOpen, value, clear])

  return (
    <div className={clsx(classes.root, className, {
      [classes.open]: isOpen,
      [classes.disableHoverEffect]: disableHoverEffect,
    })}
    >
      <TextInput
        checkRemoveCursor
        variant="outlined"
        size="small"
        placeholder={placeholder}
        value={renderValue()}
        disabled={disabled}
        endDotDotDot
        InputProps={{
          onFocus,
          endAdornment: (
            <>
              {renderCounter()}
              {renderIcon()}
            </>
          ),
          classes: {
            root: classes.inputBaseRoot,
            input: classes.inputBaseInput,
          },
          readOnly: true,
        }}
        {...props}
      />
    </div>
  )
}

Anchor.defaultProps = {
  value: '',
  className: '',
  placeholder: '',
  disabled: false,
  options: [],
  clearable: false,
  disableHoverEffect: false,
  onOpen: () => {},
  handleClear: () => {},
  isOpen: false,
}
Anchor.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.shape()),
  clearable: PropTypes.bool,
  disableHoverEffect: PropTypes.bool,
  onOpen: propTypes.func,
  handleClear: PropTypes.func,
  isOpen: PropTypes.bool,
}

export default React.memo(withStyles(style)(Anchor))
