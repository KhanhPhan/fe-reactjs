const style = (theme) => ({
  root: {
    visibility: 'hidden',
    overflow: 'hidden',
    height: 0,
  },
  popperDisablePortal: {
    position: 'relative',
  },
  paper: {
    boxShadow: 'none',
    margin: 0,
    fontSize: 14,

    '& .MuiAutocomplete-noOptions, .MuiAutocomplete-loading': {
      padding: theme.spacing(2, 1, 1),
    },

    '&.p-1': {
      '& .MuiAutocomplete-noOptions, .MuiAutocomplete-loading': {
        padding: theme.spacing(1),
      },
    },
    '& .MuiAutocomplete-option[aria-disabled="true"]': {
      opacity: 1,
    },
  },
  option: {
    padding: 0,
    background: 'transparent !important',
  },
  listbox: {
    maxHeight: '41vh',
    paddingBottom: 0,

    '&.pt-0': {
      paddingTop: 0,
    },
  },
})

export default style
