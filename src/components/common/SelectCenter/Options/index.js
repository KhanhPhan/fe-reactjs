import React, { useCallback, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import { withStyles } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Typography from '@material-ui/core/Typography'
import TextInput from 'components/common/TextInput'
import useDebounce from 'utils/hooks/useDebounce'
import Option from '../Option'
import style from './style'

const NO_OPTIONS_TEXT = 'No options'

const Options = ({
  classes,
  searchPlaceholder,
  value,
  options: rawOptions,
  asyncRequest,
  filterOptions,
  isAsync,
  multiple,
  searchable,
  onSelect,
  renderOption: CustomizedOption,
  autocompleteProps,
}) => {
  const [loading, setLoading] = useState(false)
  const [searchedOptions, setSearchedOptions] = useState([])
  const [noOptionsText, setNoOptionsText] = useState(NO_OPTIONS_TEXT)
  const [inputValue, setInputValue] = useState('')

  const options = (inputValue && isAsync) ? searchedOptions : rawOptions

  const getOptionsByKeyword = async (keyword) => {
    if (!keyword) {
      setSearchedOptions([])
      setLoading(false)
      return
    }

    if (noOptionsText !== NO_OPTIONS_TEXT) {
      setNoOptionsText(NO_OPTIONS_TEXT)
    }

    setLoading(true)

    try {
      const res = await asyncRequest(keyword)
      const newOptions = filterOptions(res)
      setSearchedOptions(newOptions)
    } catch (e) {
      setSearchedOptions([])
      setNoOptionsText('Something went wrong')
    } finally {
      setLoading(false)
    }
  }

  const debouncedInputValue = useDebounce(inputValue, 500)

  useEffect(() => {
    if (isAsync) {
      getOptionsByKeyword(debouncedInputValue)
    }
  }, [debouncedInputValue, isAsync])

  const renderOption = useCallback((item) => {
    const optProps = {
      key: item.value,
      data: item,
      multiple,
      selected: multiple ? (value || []).includes(item.value) : item.value === value,
      ...((isAsync && multiple)
        ? { onClick: () => onSelect(item) }
        : {}),
    }

    if (typeof CustomizedOption === 'function') {
      return (
        <CustomizedOption {...optProps} />
      )
    }

    return (
      <Option {...optProps} />
    )
  }, [value, multiple, isAsync])

  return (
    <>
      { searchable && (
        <TextInput
          autoFocus
          InputProps={{
            endAdornment: false,
            style: {
              height: 32,
              padding: 0,
            },
          }}
          placeholder={searchPlaceholder}
          value={inputValue}
          onChange={(e) => {
            setInputValue(e.target.value)

            if (isAsync && !loading) {
              setLoading(true)
            }
          }}
        />
      )}

      <Autocomplete
        className={classes.root}
        classes={{
          ...classes,
          root: classes.root,
          paper: clsx(classes.paper, { 'p-1': !searchable }),
          option: classes.option,
          popperDisablePortal: classes.popperDisablePortal,
          listbox: clsx(classes.listbox, 'customized-scrollbar', { 'pt-0': !searchable }),
        }}
        open
        disablePortal
        disableClearable
        disableCloseOnSelect
        blurOnSelect={false}
        clearOnBlur={false}
        multiple={multiple}
        loading={loading}
        options={options}
        renderInput={(params) => (<TextInput {...params} />)}
        inputValue={inputValue}
        getOptionLabel={(option) => option.label || ''}
        getOptionDisabled={(option) => option.disabled}
        renderOption={renderOption}
        noOptionsText={(
          <Typography variant="subtitle2">
            {noOptionsText}
          </Typography>
            )}
        {...(!(isAsync && multiple) ? {
          onChange: (e, selected) => {
            onSelect(selected)
          },
        } : {})}
        {...(multiple ? {
          renderTags: () => null,
          value: options.filter((x) => (value || []).includes(x.value)),
        } : {})}
        {...(isAsync ? { filterOptions: (o) => o } : {})}
        {...autocompleteProps}
      />
    </>
  )
}

Options.defaultProps = {
  value: '',
  onSelect: () => { },
  asyncRequest: null,
  searchPlaceholder: '',
  options: [],
  filterOptions: (e) => e,
  multiple: false,
  searchable: false,
  isAsync: false,
  renderOption: null,
  autocompleteProps: {},
}

Options.propTypes = {
  classes: PropTypes.shape().isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  searchPlaceholder: PropTypes.string,
  onSelect: PropTypes.func,
  asyncRequest: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape()),
  isAsync: PropTypes.bool,
  filterOptions: PropTypes.func,
  multiple: PropTypes.bool,
  searchable: PropTypes.bool,
  renderOption: PropTypes.func,
  autocompleteProps: PropTypes.shape(),
}

export default React.memo(withStyles(style)(Options))
