const style = (theme) => ({
  paper: {
    boxShadow: ' 0px 2px 10px #d6def2',
    boxSizing: 'border-box',
    borderRadius: 5,
    marginTop: 10,
    padding: theme.spacing(1),
    fontSize: 14,
  },

  paperPaddingLg: {
    padding: theme.spacing(2),
  },

})

export default style
