import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Button from 'components/common/Button'
import style from './style'

const Actions = ({
  classes,
  onApply,
  onCancel,
}) => (
  <div className={classes.root}>
    <Button
      onClick={onApply}
    >
      Apply
    </Button>
    <Button
      onClick={onCancel}
      color="secondary"
    >
      Cancel
    </Button>
  </div>
)

Actions.defaultProps = {
  onApply: () => {},
  onCancel: () => {},
}

Actions.propTypes = {
  classes: PropTypes.shape().isRequired,
  onApply: PropTypes.func,
  onCancel: PropTypes.func,
}

export default React.memo(withStyles(style)(Actions))
