const style = (theme) => ({
  root: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center',

    '& > button': {
      flex: 1,
      maxWidth: 100,

      '&:nth-child(2)': {
        marginLeft: theme.spacing(1),

      },
    },
  },
})

export default style
