import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import { withStyles, MenuItem, Typography } from '@material-ui/core'
import Checkbox from 'components/common/Checkbox'
import style from './style'

const Option = ({
  classes,
  onClick,
  data,
  selected,
  multiple,
}) => {
  if (data.isGroupLabel) {
    return (
      <Typography
        key={data.value}
        variant="subtitle2"
        className={classes.groupLabel}
      >
        {data.label}
      </Typography>
    )
  }

  return (
    <MenuItem
      classes={{
        root: clsx(classes.option, {
          [classes.optionWithCheck]: multiple,
        }),
      }}
      onClick={(e) => {
        e.preventDefault()
        onClick()
      }}
      selected={selected && !multiple}
      disabled={data.disabled}
      component="div"
    >
      {multiple && <Checkbox className={classes.checkbox} checked={selected} />}
      <div className={classes.labelContainer}>
        <div className={classes.label}>
          {data.label || ''}
        </div>
        { data.subLabel && <div className={classes.subLabel}>{data.subLabel}</div>}
      </div>
    </MenuItem>
  )
}

Option.defaultProps = {
  onClick: () => {},
  data: {},
  selected: false,
  multiple: false,
}

Option.propTypes = {
  classes: PropTypes.shape().isRequired,
  onClick: PropTypes.func,
  data: PropTypes.shape(),
  selected: PropTypes.bool,
  multiple: PropTypes.bool,
}

export default React.memo(withStyles(style)(Option))
