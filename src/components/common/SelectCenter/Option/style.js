const style = (theme) => ({
  option: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    padding: theme.spacing(1),
    justifyContent: 'flex-start',
    borderRadius: 4,
    overflow: 'hidden',

    '.MuiAutocomplete-option[aria-disabled="true"] &': {
      opacity: 0.38,
    },
  },
  optionWithCheck: {
    background: 'transparent',
    padding: theme.spacing(0.5, 1),
  },
  checkbox: {
    margin: theme.spacing(-0.5, 0, -0.5, -1),
    flex: '0 0 38px',
    pointerEvents: 'none',
  },
  labelContainer: {
    flex: 1,
    overflow: 'hidden',
    fontSize: 14,
    fontWeight: 400,
  },
  label: {
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  subLabel: {
    color: theme.color.textGray2,
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  groupLabel: {
    borderTop: `1px solid ${theme.color.disableGray1}`,
    paddingTop: theme.spacing(1),
    paddingBottom: 1,
    marginTop: 5,
    pointerEvents: 'none',

    '&:first-of-type': {
      border: 'none',
      paddingTop: 1,
    },
  },
})

export default style
