import React, {
  useState, useEffect, useCallback, useMemo,
} from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import {
  uniqBy, groupBy as _groupBy, concat, flatten,
} from 'lodash'
import { withStyles } from '@material-ui/core'
import Popover from '@material-ui/core/Popover'

import Anchor from './Anchor'
import Options from './Options'
import Actions from './Actions'
import style from './style'

const SelectCenter = ({
  classes,
  placeholder,
  searchPlaceholder,
  value: rawValue,
  disabled,
  className,
  options,
  onChange,
  onClear,
  onBlur,
  asyncRequest,
  clearable,
  disableHoverEffect,
  filterOptions,
  groupByKey,
  getGroupLabel,
  searchable,
  multiple,
  resetWhen,
  popoverProps,
  autocompleteProps,
  renderOption,
  renderAnchor: CustomizedAnchor,
  renderOptions: CustomizedOptions,
  renderActions: CustomizedActions,
  ...props
}) => {
  const [anchorEl, setAnchorEl] = useState(null)
  const [value, setValue] = useState('')
  const [persistedOptions, setPersistedOptions] = useState([])

  const isOpen = Boolean(anchorEl)
  const isAsync = typeof asyncRequest === 'function'

  const groupedOptions = useMemo(
    () => flatten(concat(Object.values(_groupBy(options || [], groupByKey)).map((g) => {
      if (g.length) {
        const groupValue = g[0][groupByKey]
        const groupLabel = (typeof getGroupLabel === 'function') ? getGroupLabel(groupValue) : ''

        return [
          ...(groupLabel ? [{
            label: (typeof getGroupLabel === 'function') ? getGroupLabel(groupValue) : '-',
            value: groupValue,
            isGroupLabel: true,
            disabled: true,
          }] : []),
          ...g,
        ]
      }
      return []
    }))),
  )

  const visibleOptions = (!!groupByKey && !isAsync) ? groupedOptions : options
  const onOpen = (el) => {
    if (!anchorEl) {
      setAnchorEl(el)
    }
  }

  const onClose = () => {
    setAnchorEl(null)
    onBlur()
  }

  const handleClear = () => {
    onClear()
    onChange(multiple ? [] : '')
  }

  const shouldReset = useCallback(() => {
    if (typeof resetWhen === 'function') {
      return resetWhen()
    }

    if (typeof resetWhen === 'boolean') {
      return resetWhen
    }

    return false
  }, [resetWhen])

  useEffect(() => {
    if (shouldReset()) {
      handleClear()

      if (isAsync) {
        setPersistedOptions([])
      }
    }
  }, [shouldReset])

  useEffect(() => {
    setValue(rawValue ?? (multiple ? [] : ''))
  }, [rawValue, multiple])

  const renderAnchor = () => {
    const anchorProps = {
      placeholder,
      className,
      disableHoverEffect,
      clearable,
      isOpen,
      value,
      options: uniqBy([...persistedOptions, ...options], 'value'),
      disabled,
      onOpen,
      handleClear,
      ...props,
    }

    if (typeof CustomizedAnchor === 'function') {
      return <CustomizedAnchor {...anchorProps} />
    }

    return <Anchor {...anchorProps} />
  }

  const renderActions = () => {
    const actionsProps = {
      onApply: () => {
        onChange(value)
        onClose()
      },
      onCancel: () => {
        setValue(rawValue)
        onClose()
      },
    }
    if (typeof CustomizedActions === 'function') {
      return <CustomizedActions {...actionsProps} />
    }

    if (multiple) {
      return (
        <Actions {...actionsProps} />
      )
    }

    return null
  }

  const renderOptions = () => {
    if (typeof CustomizedOptions === 'function') {
      return (
        <CustomizedOptions
          options={uniqBy([...persistedOptions, ...visibleOptions], 'value')}
          value={value}
          setValue={setValue}
          onChange={onChange}
          onClose={onClose}
          setPersistedOptions={
            (selected) => setPersistedOptions(uniqBy([...persistedOptions, selected], 'value'))
          } // for async case
        />
      )
    }

    return (
      <Options
        multiple={multiple}
        searchable={searchable || isAsync}
        options={uniqBy([...persistedOptions, ...visibleOptions], 'value')}
        value={value}
        onSelect={(selected) => {
          if (isAsync) {
            if (multiple) {
              setPersistedOptions(
                uniqBy([...persistedOptions, selected], 'value'),
              )
              const selectedValue = selected.value

              const idx = (value || []).findIndex(
                (v) => v === selectedValue,
              )

              if (idx === -1) {
                setValue([...value, selectedValue])
              } else {
                const newvalue = [...value]
                newvalue.splice(idx, 1)

                setValue(newvalue)
              }
            } else {
              setPersistedOptions(
                uniqBy([...persistedOptions, selected], 'value'),
              )
              onChange(selected.value)
              onClose()
            }

            return
          }

          if (multiple) {
            setValue(selected.map((s) => s.value))
          } else {
            onChange(selected.value)
            onClose()
          }
        }}
        isAsync={isAsync}
        asyncRequest={asyncRequest}
        searchPlaceholder={searchPlaceholder}
        filterOptions={filterOptions}
        autocompleteProps={autocompleteProps}
      />
    )
  }

  return (
    <>
      {renderAnchor()}

      <Popover
        open={isOpen}
        anchorEl={anchorEl}
        onClose={() => {
          if (multiple) setValue(rawValue)
          onClose()
        }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transitionDuration={0}
        {...popoverProps}
        classes={{
          ...(popoverProps?.classes || {}),
          paper: clsx(classes.paper, {
            [classes.paperPaddingLg]: multiple || searchable || isAsync,
            ...(popoverProps?.classes?.paper || {}),
          }),
        }}
        PaperProps={{
          ...(popoverProps?.PaperProps || {}),
          style: {
            width: anchorEl?.getBoundingClientRect().width || 'auto',
            ...(popoverProps?.PaperProps?.style || {}),
          },
        }}
      >
        {renderOptions()}
        {renderActions()}
      </Popover>
    </>
  )
}

SelectCenter.defaultProps = {
  value: '',
  className: '',
  placeholder: '',
  searchPlaceholder: '',
  disabled: false,
  onChange: () => {},
  onClear: () => {},
  onBlur: () => {},
  asyncRequest: null,
  options: [],
  filterOptions: (e) => e,
  disableHoverEffect: false,
  clearable: false,
  searchable: false,
  multiple: false,
  groupByKey: '',
  getGroupLabel: null,
  resetWhen: false,
  popoverProps: {},
  autocompleteProps: {},
  renderAnchor: null,
  renderOption: null,
  renderOptions: null,
  renderActions: null,
}

SelectCenter.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  options: PropTypes.arrayOf(PropTypes.shape()),
  placeholder: PropTypes.string,
  searchPlaceholder: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onClear: PropTypes.func,
  onBlur: PropTypes.func,
  asyncRequest: PropTypes.func,
  clearable: PropTypes.bool,
  filterOptions: PropTypes.func,
  disableHoverEffect: PropTypes.bool,
  searchable: PropTypes.bool,
  multiple: PropTypes.bool,
  groupByKey: PropTypes.string,
  getGroupLabel: PropTypes.func,
  resetWhen: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.func,
  ]),
  popoverProps: PropTypes.shape(),
  autocompleteProps: PropTypes.shape(),
  renderAnchor: PropTypes.func,
  renderOption: PropTypes.func,
  renderOptions: PropTypes.func,
  renderActions: PropTypes.func,
}

export default React.memo(withStyles(style)(SelectCenter))
