import React, { useState, useEffect } from 'react'
import { hexToRgb, makeStyles, withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import LinearProgress from '@material-ui/core/LinearProgress'
import { colors } from 'theme'

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
  },
  title: {
    color: theme.color.textGray2,
    fontSize: 12,
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 4,
  },
  label: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    marginRight: 24,
  },
  value: {

  },
})

const useStyles = ({ opacity }) => makeStyles({
  root: {
    height: 10,
    borderRadius: '0 10px 10px 0',
  },
  colorPrimary: {
    backgroundColor:
      `rgba(${hexToRgb(colors.primaryBlue).replace('rgb(', '').replace(')', '')}, 0.1)`,
  },
  bar: {
    borderRadius: '0 10px 10px 0',
    backgroundColor: colors.primaryBlue,
    opacity,
  },
})

const LinearProgressBar = ({
  label, values, percentage, classes, color, opacity, ...props
}) => {
  const [progress, setProgress] = useState(0)
  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) => (prevProgress >= percentage ? percentage : prevProgress + 10))
    }, 200)
    return () => {
      clearInterval(timer)
    }
  }, [])

  const linearStyle = useStyles({ opacity })()
  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <span className={classes.label}>{label}</span>
        <span>{values}</span>
      </div>
      <LinearProgress
        classes={{
          root: linearStyle.root,
          colorPrimary: linearStyle.colorPrimary,
          bar: linearStyle.bar,
        }}
        variant="determinate"
        value={progress}
        {...props}
      />
    </div>
  )
}

LinearProgressBar.defaultProps = {
  label: '',
  values: 10,
  percentage: 10,
  color: colors.primaryBlue,
  opacity: 0,
}

LinearProgressBar.propTypes = {
  classes: PropTypes.shape().isRequired,
  values: PropTypes.string,
  label: PropTypes.string,
  percentage: PropTypes.number,
  color: PropTypes.string,
  opacity: PropTypes.number,
}

export default withStyles(styles)(LinearProgressBar)
