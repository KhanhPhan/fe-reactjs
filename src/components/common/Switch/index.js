import React from 'react'
import Switch from '@material-ui/core/Switch'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import clsx from 'clsx'

const StyledSwitch = withStyles((theme) => ({
  root: {
    width: 80,
    height: 32,
    padding: 0,
  },
  rootNoText: {
    width: 60,
  },
  switchBase: {
    padding: 0,
    top: 3,
    left: 3,
    '&$checked': {
      transform: 'translateX(48px)',
      '& + $track': {
        backgroundColor: theme.color.viridianGreen1,
        opacity: 1,
        border: 'none',
        '&::after': {
          content: '"ON"',
          position: 'absolute',
          top: 8,
          left: 20,
          fontSize: 14,
          color: theme.color.white,
        },
      },
    },
    '& input': {
      left: '-200%',
      width: 130,
      height: 32,
      top: -3,
    },
  },

  switchBaseNoText: {
    '&$checked': {
      transform: 'translateX(28px)',
    },
  },

  thumb: {
    color: theme.color.white,
    width: 26,
    height: 26,
  },
  track: {
    borderRadius: 30,
    backgroundColor: theme.color.secondaryBlue2,
    opacity: 1,
    '&::after': {
      content: '"OFF"',
      position: 'absolute',
      fontSize: 14,
      top: 8,
      left: 36,
      color: theme.color.white,
    },
  },
  disabled: {
    '&$checked + $track': {
      opacity: '0.5 !important',
    },
  },
  checked: {},
  trackNoText: {
    '&::before, &::after': {
      display: 'none',
    },
  },
}))(({ classes, noText, ...props }) => (
  <Switch
    disableRipple
    classes={{
      root: clsx(classes.root, {
        [classes.rootNoText]: noText,
      }),
      switchBase: clsx(classes.switchBase, {
        [classes.switchBaseNoText]: noText,
      }),
      thumb: classes.thumb,
      track: clsx(classes.track, {
        [classes.trackNoText]: noText,
      }),
      checked: classes.checked,
      disabled: classes.disabled,
    }}
    {...props}
  />
))
const Switcher = ({
  checked, onChange, disabled, ...props
}) => (
  <StyledSwitch
    checked={checked}
    onChange={onChange} // e not e.target.checked
    disabled={disabled}
    {...props}
  />
)
Switcher.defaultProps = {
  checked: false,
  onChange: () => {},
  disabled: false,
  noText: false,
}
Switcher.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  noText: PropTypes.bool,
}
export default Switcher
