import React, { useEffect, useState, useRef } from 'react'
import debounce from 'lodash/debounce'
import Autocomplete from '@material-ui/lab/Autocomplete'
import {
  withStyles,
  TextField,
  Typography,
  Paper,
} from '@material-ui/core'

import PropTypes from 'prop-types'
import clsx from 'clsx'
import { KeyboardArrowDown } from '@material-ui/icons'
// import { FastField } from 'formik'
import SearchIcon from '@material-ui/icons/Search'
import InputAdornment from '@material-ui/core/InputAdornment'

const searchBoxStyle = () => ({
  wrapInputBoxSearch: {
    position: 'relative',
  },
  listBox: {
    fontSize: 14,
  },
  searchBoxContainer: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    '& .MuiAutocomplete-inputRoot': {
      paddingTop: '2px !important',
      paddingBottom: '2px !important',
    },
    '& .MuiAutocomplete-root': {
      flex: '1',
      width: '100%',
      '& .MuiInputBase-root .MuiChip-root': {
        margin: '0 2px 2px 2px',
      },
    },
    '& .MuiChip-label.MuiChip-labelSmall': {
      textOverflow: 'ellipsis',
      overflow: 'hidden',
      maxWidth: '237px',
      height: '1.2em',
      whiteSpace: 'nowrap',
    },
    '& button': {
      zIndex: 1,
    },
    '& svg.MuiSvgIcon-root': {
      marginRight: 7,
    },
  },
  root: {
    height: 19,
  },
  overlay: {
    position: 'fixed',
    width: '100vw',
    height: '100vh',
    top: 0,
    left: 0,
    zIndex: 100,
  },
  rootWrapTextfiled: {
    width: '100%',
    '& svg.MuiSvgIcon-root': {
      color: '#7f8597cf',
    },
  },
  endDotDotDot: {
    '& input': {
      display: 'inline-block',
      whiteSpace: 'nowrap',
      overflow: 'hidden !important',
      textOverflow: 'ellipsis',
      maxWidth: '80%',
    },
  },
})

const SearchBox = ({
  classes,
  inputProps,
  apiAction,
  className,
  value,
  onSearchBoxChange, // (e, valuesOnSearchBox) => {}
  handleFilterOption,
  rightComponent,
  isAdd,
  createInventory,
  multiple,
  options: defaultOptions,
  popupIcon,
  disableClearable,
  customShowInput,
  isShowCustomInput,
  autoComplete,
  endDotDotDot,
  disabledAllOption,
  searchIcon,
  ...props
}) => {
  const [loading, setLoading] = useState(false)
  const [options, setOptions] = useState(defaultOptions || [])
  const [noOptionsTxt, setNoOptionsTxt] = useState('No options')
  const [overlay, setOverlay] = useState(false)

  useEffect(() => {
    if (defaultOptions) {
      setOptions(defaultOptions)
    }
  }, [defaultOptions])

  const getOptionsFromRequest = async (keyword) => {
    setOptions([])
    setNoOptionsTxt('No options')
    if (keyword.trim() && (!value || keyword !== value.label)) {
      setLoading(true)
      try {
        const res = await apiAction(keyword)
        setLoading(false)
        const optionsData = handleFilterOption(res)
        setOptions(optionsData)
      } catch {
        setNoOptionsTxt('Something went wrong')
        setLoading(false)
        setOptions([])
      }
    }
  }

  const onInputChange = apiAction
    ? debounce((val) => getOptionsFromRequest(val), 500)
    : (keyword) => {
      const optionsData = handleFilterOption(
        (defaultOptions || [])
          .filter((x) => x.label.toLowerCase().indexOf(keyword.toLowerCase()) !== -1),
      )
      setOptions(optionsData)
    }

  const inputRef = useRef()

  const renderInput = (params) => (
    <div
      className={clsx(classes.wrapInputBoxSearch, { [classes.endDotDotDot]: endDotDotDot })}
      role="presentation"
      onClick={() => {
        inputRef.current.focus()
        inputRef.current.click()
      }}
    >
      <TextField
        ref={inputRef}
        variant="outlined"
        autoComplete="new-password"
        classes={{
          root: classes.rootWrapTextfiled,
        }}
        {...params}
        {...inputProps}
        InputProps={{
          ...params.InputProps,
          classes: {
            input: classes.root,
            ...params.InputProps.classes,
          },
          inputProps: {
            ...params.inputProps,
            autoComplete: autoComplete ? 'on' : 'off',
            role: 'presentation',
          },
          endAdornment: searchIcon ? (
            <InputAdornment position="end">
              <SearchIcon />
            </InputAdornment>
          ) : null,
        }}
      />
      {isShowCustomInput && customShowInput}
    </div>

  )

  const RenderNoResult = () => (
    <>
      {
      !isAdd || !createInventory ? (
        <Typography variant="subtitle2">
          {noOptionsTxt}
        </Typography>
      )
        : null
    }

    </>
  )

  const CustomPaper = (propsPaper) => (
    <Paper
      classes={{ root: classes.listBox }}
      elevation={8}
      {...propsPaper}
    />
  )

  return (
    <>
      <div className={classes.searchBoxContainer}>
        <Autocomplete
          multiple={multiple}
          renderOption={(option) => option.label || option.title || ''}
          PaperComponent={CustomPaper}
          className={clsx(className)}
          getOptionDisabled={(ops) => {
            if (disabledAllOption && (ops.label === `+ Create New SKU "${ops.sku}"`)) {
              return false
            }
            if (disabledAllOption) {
              return true
            }
            return ops.isDisabled
          }}
          openOnFocus
          loading={loading}
          disableClearable={disableClearable}
          renderInput={renderInput}
          options={options}
          value={value}
          getOptionLabel={(option) => option.label || ''}
          filterOptions={(opt, params) => {
            const { inputValue } = params
            const checkExist = opt
              .filter(
                (item) => (item.name || '').toLowerCase().trim()
                  === (inputValue || '').toLowerCase().trim(),
              )
            if (isAdd && inputValue && (!opt.length || !checkExist.length)) {
              opt.unshift({
                inputValue: params.inputValue,
                label: `Create "${params.inputValue}"`,
                isAdd: true,
              })
            }
            if (createInventory && params.inputValue.trim()) {
              opt.unshift({
                sku: params.inputValue,
                label: `+ Create New SKU "${params.inputValue}"`,
                value: params.inputValue,
                image: null,
                createInventory: true,
              })
            }
            return opt
          }}
          onInputChange={(_, val) => onInputChange(val)}
          onChange={onSearchBoxChange}
          popupIcon={popupIcon ? <KeyboardArrowDown color="primary" /> : false}
          loadingText={(
            <Typography variant="subtitle2">
              Loading...
            </Typography>
          )}
          noOptionsText={<RenderNoResult />}
          size="small"
          onOpen={() => {
            setOverlay(true)
            document.body.style.overflow = 'hidden'
          }}
          onClose={() => {
            setOverlay(false)
            document.body.style.overflow = ''
          }}
          {...props}
        />
        {rightComponent}
      </div>

      {overlay && <div className={classes.overlay} />}
    </>
  )
}

SearchBox.defaultProps = {
  inputProps: {},
  onSearchBoxChange: () => {},
  handleFilterOption: (e) => e,
  value: {},
  className: '',
  rightComponent: '',
  isAdd: false,
  multiple: false,
  options: null,
  apiAction: null,
  popupIcon: false,
  disableClearable: false,
  createInventory: false,
  customShowInput: <></>,
  isShowCustomInput: false,
  autoComplete: false,
  endDotDotDot: false,
  disabledAllOption: false,
  searchIcon: false,
}

const IValue = PropTypes.shape({
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  label: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
})

SearchBox.propTypes = {
  classes: PropTypes.shape().isRequired,
  inputProps: PropTypes.shape(),
  apiAction: PropTypes.func,
  onSearchBoxChange: PropTypes.func,
  handleFilterOption: PropTypes.func,
  value: PropTypes.oneOfType([
    IValue,
    PropTypes.arrayOf(IValue),
  ]),
  className: PropTypes.string,
  rightComponent: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  isAdd: PropTypes.bool,
  multiple: PropTypes.bool,
  popupIcon: PropTypes.bool,
  disableClearable: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.shape()),
  createInventory: PropTypes.bool,
  isShowCustomInput: PropTypes.bool,
  autoComplete: PropTypes.bool,
  customShowInput: PropTypes.node,
  endDotDotDot: PropTypes.bool,
  disabledAllOption: PropTypes.bool,
  searchIcon: PropTypes.bool,

}

export default withStyles(searchBoxStyle)(SearchBox)
