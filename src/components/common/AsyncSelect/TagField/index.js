/* eslint-disable react/no-array-index-key */
import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Chip from '@material-ui/core/Chip'
import PropTypes from 'prop-types'
import { Close } from '@material-ui/icons'

const TagItem = withStyles((theme) => ({
  root: {
    height: 32,
    fontSize: 14,
    borderRadius: 5,
    border: `1px solid ${theme.color.darkGray}`,
    margin: theme.spacing(0.5),
    maxWidth: '95%',
    '& span': {
      width: '100%',
    },
  },
}
))((props) => <Chip {...props} />)

const TagList = withStyles((theme) => ({
  tagContainer: {
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 5,
    border: `1px solid ${theme.color.secondaryBlue2}`,
    padding: theme.spacing(0.5),
  },
}))(({
  classes,
  tags, // [{ value: '', label: '' }]
  onChange, // (allTags: []) => {}
  flexibleHeight,
  style,
}) => {
  const onRemove = (id) => {
    const newTagList = [...tags]
    newTagList.splice(id, 1)
    onChange([...newTagList])
  }

  return (
    <div
      className={classes.tagContainer}
      style={{
        minHeight: flexibleHeight && 169,
        height: !flexibleHeight && 169,
        overflowY: 'auto',
        ...style,
      }}
    >
      {(tags).map((item, index) => (
        <TagItem
          key={index}
          label={item.label}
          onDelete={() => onRemove(index)}
          deleteIcon={(
            <Close />
          )}
        />
      ))}
    </div>
  )
})

TagList.defaultProps = {
  tags: [],
  onChange: () => {},
}

TagList.propTypes = {
  classes: PropTypes.shape(),
  tags: PropTypes.arrayOf(PropTypes.shape()),
  onChange: PropTypes.func,
  flexibleHeight: PropTypes.bool,
  props: PropTypes.shape(),
}

export default TagList
