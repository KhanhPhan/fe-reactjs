import React, { useState } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import Button from 'components/common/Button'

import SearchBox from '../SearchBox'
import TagField from '../TagField'

const style = (theme) => ({
  rightBtn: {
    marginLeft: theme.spacing(1),
    '@media screen and (max-width: 768px)': {
      maxWidth: '20%',
    },
  },
  defaultButtonAdd: {
    padding: 0,
    '@media screen and (max-width: 768px)': {
      minWidth: '100%',
      '& .MuiButton-label': {
        padding: '0 5px',
      },
    },
  },
})

const CustomMultiOptions = ({
  classes,
  values,
  onChange,
  tagFieldStyle,
  flexibleHeight,
  refuseDuplicate,
  ...props
}) => {
  const [newTags, setNewTags] = useState([])

  const onChooseTag = (vals) => { // multi select > vals: [{}]a
    setNewTags(vals)
  }

  const onAddingTags = () => {
    if (refuseDuplicate) {
      const selectedOfNewTags = []
      newTags.forEach((t) => {
        if (!selectedOfNewTags.some((s) => s.value === t.value)) {
          selectedOfNewTags.push(t)
        }
      })

      const concatedValues = [...values]
      selectedOfNewTags.forEach((t) => {
        if (!values.some((v) => v.value === t.value)) {
          concatedValues.push(t)
        }
      })

      onChange(concatedValues)
    } else {
      onChange([...values, ...newTags])
    }
    setNewTags([])
  }

  const onChangeTagField = (allTags) => {
    onChange(allTags)
  }

  return (
    <>
      <SearchBox
        multiple
        rightComponent={(
          <div className={classes.rightBtn}>
            <Button
              className={classes.defaultButtonAdd}
              color="secondary"
              label="+ Add"
              onClick={onAddingTags}
            />
          </div>
        )}
        {...props}
        value={newTags}
        onSearchBoxChange={(_, vals) => onChooseTag(vals)}
      />
      <TagField
        tags={values}
        onChange={onChangeTagField}
        flexibleHeight={flexibleHeight}
        style={{
          marginTop: 24,
          ...tagFieldStyle,
        }}
      />
    </>
  )
}

CustomMultiOptions.defaultProps = {
  onChange: () => {},
  values: [],
  tagFieldStyle: {},
  flexibleHeight: false,
  props: {},
  refuseDuplicate: true,
}

CustomMultiOptions.propTypes = {
  classes: PropTypes.shape().isRequired,
  values: PropTypes.arrayOf(PropTypes.shape()),
  onChange: PropTypes.func,
  tagFieldStyle: PropTypes.shape(),
  flexibleHeight: PropTypes.bool,
  props: PropTypes.shape(),
  refuseDuplicate: PropTypes.bool,
}

export default withStyles(style)(CustomMultiOptions)
