import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import clsx from 'clsx'

import SearchBox from '../SearchBox'
import TagField from '../TagField'

const style = (theme) => ({
  customSearchBox: {
    '& .MuiAutocomplete-endAdornment': {
      '& *': {
        display: 'none',
      },
      '&::after': {
        content: '""',
        display: 'inline-block',
        position: 'absolute',
        top: 2,
        right: -3,
        width: 24,
        height: 24,
        maskImage: 'url("/icons/search2.svg")',
        WebkitMaskImage: 'url("/icons/search2.svg")',
        WebkitMaskSize: 'cover',
        backgroundColor: theme.color.textGray2,
      },
    },
  },
  iconField: {
    color: theme.color.textGray2,
    width: 30,
  },
})

const CustomSingleOption = ({
  classes,
  values,
  onChange,
  tagFieldStyle,
  flexibleHeight,
  onCreateNew,
  options,
  ...props
}) => {
  const onChooseTag = async (val) => { // single select > val: {}
    if (!val) return
    if (val.isAdd) {
      onCreateNew(val)
      onChange([...values, { ...val, label: val.inputValue }])
    } else {
      onChange([...values, val])
    }
  }

  const onChangeTagField = (allTags) => {
    onChange(allTags)
  }
  return (
    <>
      <SearchBox
        className={
          clsx(classes.customSearchBox)
        }
        multiple={false}
        value={[]}
        onSearchBoxChange={(_, val) => onChooseTag(val)}
        options={options}
        {...props}
      />
      <TagField
        tags={values}
        onChange={onChangeTagField}
        flexibleHeight={flexibleHeight}
        style={{
          marginTop: 24,
          ...tagFieldStyle,
        }}
      />
    </>
  )
}

CustomSingleOption.defaultProps = {
  onChange: () => {},
  values: [],
  props: {},
  tagFieldStyle: {},
  flexibleHeight: false,
  onCreateNew: () => {},
  options: [],
}

CustomSingleOption.propTypes = {
  classes: PropTypes.shape().isRequired,
  values: PropTypes.arrayOf(PropTypes.shape()),
  props: PropTypes.shape(),
  onChange: PropTypes.func,
  onCreateNew: PropTypes.func,
  tagFieldStyle: PropTypes.shape(),
  options: PropTypes.shape(),
  flexibleHeight: PropTypes.bool,
}

export default withStyles(style)(CustomSingleOption)
