import React from 'react'

const Overlay = () => (
  <div style={{
    position: 'fixed',
    width: '100vw',
    height: '100vh',
    top: 0,
    left: 0,
  }}
  />
)

Overlay.defaultProps = {
}

Overlay.propTypes = {
}
export default Overlay
