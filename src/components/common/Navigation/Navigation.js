/* eslint-disable react/no-multi-comp */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Typography, withStyles } from '@material-ui/core'
import { matchPath, useLocation } from 'react-router-dom'
import clsx from 'clsx'
import { navigationStyle } from './style'
import NavigationList from './NavigationList'

const Navigation = ({
  classes, title, pages, isDisplayFull, data,
}) => {
  const { pathname } = useLocation()
  const [isCollapse, setIsCollapse] = useState(() => (isDisplayFull
    ? pages.map(
      (item) => !!matchPath(pathname, {
        path: item.href,
        exact: false,
      }) || !!item.hiddenHrefs?.some((h) => !!matchPath(pathname, { path: h, exact: false })),
    )
    : []))

  const handleToggle = (pageTitle) => {
    setIsCollapse(
      pages.map((item, index) => (item.title === pageTitle ? !isCollapse[index] : false)),
    )
  }

  return (
    <div className={clsx(classes.root)}>
      {title && <Typography variant="caption">{title}</Typography>}
      <NavigationList
        depth={0}
        pages={pages}
        pathname={pathname}
        data={data}
        isDisplayFull={isDisplayFull}
        isCollapse={isCollapse}
        setIsCollapse={handleToggle}
      />
    </div>
  )
}

Navigation.defaultProps = {
  title: '',
  data: {},
}

Navigation.propTypes = {
  classes: PropTypes.shape().isRequired,
  title: PropTypes.string,
  isDisplayFull: PropTypes.bool.isRequired,
  data: PropTypes.shape(),
  pages: PropTypes.arrayOf(PropTypes.shape()).isRequired,

}

export default withStyles(navigationStyle)(Navigation)
