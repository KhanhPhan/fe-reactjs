export const navigationStyle = () => ({
  root: {
    height: 'calc(100% - 16px)',
  },
})

export const navigationListStyle = () => ({
  root: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
})

export const navigationListItemStyle = (theme) => ({
  item: {
    display: 'block',
    paddingTop: 0,
    paddingBottom: 0,
    '&.hrTop': {
      marginTop: 'auto',
      paddingTop: theme.spacing(0.5),
      '&::before': {
        content: '',
        width: '70%',
        height: 2,
        marginLeft: '15%',
        background: theme.color.midPurple,
      },
    },
  },
  itemLeaf: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
  },
  button: {
    backgroundColor: 'transparent',
    color: theme.color.black,
    '&:focus': {
      backgroundColor: 'transparent',
    },
    padding: theme.spacing(1),
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    width: '100%',
    minWidth: 0,
    fontWeight: 400,
    minHeight: theme.spacing(5),
    marginBottom: 1,
    '&:hover': {
      background: 'rgba(0,0,0,.08)',
    },
  },
  title: {
    marginLeft: -5,
    '-webkit-transition': 'all 0.2s ease',
    '-moz-transition': 'all 0.2s ease',
    '-o-transition': 'all 0.2s ease',
    '-ms-transition': 'all 0.2s ease',
    transition: 'all 0.2s ease',
    whiteSpace: 'nowrap',
  },
  expandIcon: {
    marginLeft: 'auto',
    height: 16,
    width: 16,
  },
  label: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: 'auto',
    '&.withBadge': {
      marginLeft: 8,
    },
  },
  active: {
    backgroundColor: 'rgba(0, 98, 255, .15) !important',
    boxShadow: '0px 1px 2px #160E4D14',
  },
  activeLeaf: {},
  badge: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#FF4949',
    color: 'white',
    marginLeft: 'auto',
    fontSize: 13,
    fontWeight: 500,
    width: 20,
    height: 20,
    borderRadius: 10,
    paddingBottom: 3,
  },
  dot: {
    position: 'absolute',
    width: theme.spacing(1),
    height: theme.spacing(1),
    background: theme.color.black,
    borderRadius: theme.spacing(0.5),
    marginLeft: theme.spacing(-0.5),
    marginBottom: 1,
    '-webkit-transition': 'all 0.2s ease',
    '-moz-transition': 'all 0.2s ease',
    '-o-transition': 'all 0.2s ease',
    '-ms-transition': 'all 0.2s ease',
    transition: 'all 0.2s ease',
  },
  customIcon: {
    fontSize: theme.spacing(2),
  },
  icon: {
    fontSize: theme.spacing(3),
  },
})
