import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import {
  ListItem, Collapse, withStyles, Button,
} from '@material-ui/core'
import { ExpandMore, ExpandLess } from '@material-ui/icons'
import { matchPath } from 'react-router-dom'
import { navigationListItemStyle } from './style'
import CustomRouterLink from './CustomRouterLink'

const NavigationListItem = ({
  classes,
  title,
  href,
  depth,
  children,
  icon: ItemIcon,
  isCollapse,
  pathname,
  data,
  isDisplayFull,
  handleToggle,
  parentHref = '',
  stickBottom,
}) => {
  const style = {
    paddingLeft: depth > 0 && !ItemIcon ? 30 + 10 * depth : 10,
  }

  let active

  if (children) {
    active = !!matchPath(pathname, { path: parentHref, exact: false })

    return (
      <ListItem className={clsx(classes.item, { hrTop: stickBottom })} disableGutters>
        <CustomRouterLink
          activeClassName={classes.active}
          className={classes.button}
          onClick={handleToggle ? () => handleToggle(title) : null}
          style={style}
          active={active}
        >
          {ItemIcon && <ItemIcon className={classes.icon} />}
          {isDisplayFull && (
            <>
              <div className={clsx(classes.title, 'navbar-title')}>{title}</div>
              {data && data !== 0 ? <div className={classes.badge}>{data}</div> : ''}
              {isCollapse ? (
                <ExpandLess
                  className={clsx(classes.expandIcon, { withBadge: data && data !== 0 })}
                  color="inherit"
                />
              ) : (
                <ExpandMore
                  className={clsx(classes.expandIcon, { withBadge: data && data !== 0 })}
                  color="inherit"
                />
              )}
            </>
          )}
        </CustomRouterLink>
        <Collapse
          in={isCollapse}
          className={clsx({
            'child-nav': isCollapse,
            'with-icon': stickBottom,
          })}
        >
          {children}
        </Collapse>
      </ListItem>
    )
  }

  active = !!matchPath(pathname, { path: href, exact: true })

  return (
    <ListItem className={classes.itemLeaf} disableGutters>
      <Button
        activeClassName={!ItemIcon ? classes.activeLeaf : classes.active}
        className={clsx(classes.button, `depth-${depth}`)}
        component={CustomRouterLink}
        style={style}
        to={href}
        active={active}
      >
        {
          ItemIcon
            ? ItemIcon && <ItemIcon className={classes.icon} />
            : active && <div className={clsx(classes.dot, 'navbar-dot')} />
        }
        {isDisplayFull && (
          <>
            <div className={clsx(classes.title, 'navbar-title')}>{title}</div>
            {data && data !== 0 ? <div className={classes.badge}>{data}</div> : ''}
          </>
        )}
      </Button>
    </ListItem>
  )
}

NavigationListItem.defaultProps = {
  icon: '',
  data: '',
  handleToggle: () => {},
  stickBottom: false,
  parentHref: '',
  isCollapse: false,
  children: null,
}

NavigationListItem.propTypes = {
  classes: PropTypes.shape().isRequired,
  title: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  depth: PropTypes.number.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.shape(),
    PropTypes.string,
  ]),
  icon: PropTypes.string,
  isCollapse: PropTypes.bool,
  pathname: PropTypes.string.isRequired,
  data: PropTypes.string,
  isDisplayFull: PropTypes.bool.isRequired,
  handleToggle: PropTypes.func,
  parentHref: PropTypes.string,
  stickBottom: PropTypes.bool,
}

export default withStyles(navigationListItemStyle)(NavigationListItem)
