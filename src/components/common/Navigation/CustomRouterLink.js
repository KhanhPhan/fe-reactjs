import { Button } from '@material-ui/core'
import PropTypes from 'prop-types'
import React, { forwardRef } from 'react'
import { Link } from 'react-router-dom'

const CustomRouterLink = forwardRef(
  (
    {
      to, onClick, children, className, activeClassName, active, style,
    },
    ref,
  ) => (onClick ? (
    <Button
      ref={ref}
      style={{ ...style, flexGrow: 1 }}
      className={`${className} ${active ? activeClassName : ''}`}
      onClick={onClick}
    >
      {children}
    </Button>
  ) : (
    <Link
      to={to}
      ref={ref}
      style={{ ...style, flexGrow: 1 }}
      className={`${className} ${active ? activeClassName : ''}`}
    >
      {children}
    </Link>
  )),
)

CustomRouterLink.defaultProps = {
  to: '',
  style: {},
  onClick: null,
}

CustomRouterLink.propTypes = {
  to: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.shape(),
    PropTypes.string,
  ]).isRequired,
  className: PropTypes.string.isRequired,
  activeClassName: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
  style: PropTypes.shape(),
}

export default CustomRouterLink
