/* eslint-disable react/no-multi-comp */
import React from 'react'
import PropTypes from 'prop-types'
import { List, withStyles } from '@material-ui/core'
import { mapDataForNavbar } from 'utils/router'
import NavigationListItem from './NavigationListItem'
import { navigationListStyle } from './style'

const reduceChildRoutes = ({
  pathname,
  items,
  page,
  depth,
  data,
  isDisplayFull,
  isCollapse,
  setIsCollapse,
}) => {
  if (page.children) {
    items.push(
      <NavigationListItem
        depth={depth}
        href="#"
        icon={page.icon}
        key={page.title}
        isCollapse={isCollapse}
        handleToggle={setIsCollapse}
        title={page.title}
        pathname={pathname}
        data={data[mapDataForNavbar(page.href)]}
        isDisplayFull={isDisplayFull}
        parentHref={page.href}
        hiddenHrefs={page.hiddenHrefs}
        stickBottom={page.stickBottom}
      >
        <NavigationList
          depth={depth + 1}
          pages={page.children}
          pathname={pathname}
          data={data}
          isDisplayFull={isDisplayFull}
        />
      </NavigationListItem>,
    )
  } else {
    items.push(
      <NavigationListItem
        depth={depth}
        href={page.href}
        icon={page.icon}
        key={page.title}
        title={page.title}
        pathname={pathname}
        data={data[mapDataForNavbar(page.href)]}
        isDisplayFull={isDisplayFull}
        hiddenHrefs={page.hiddenHrefs}
        stickBottom={page.stickBottom}
      />,
    )
  }
  return items
}

const NavigationList = ({
  classes,
  pages,
  isDisplayFull,
  pathname,
  isCollapse = [],
  setIsCollapse,
  depth = 0,
  data,
}) => (
  <List className={classes?.root}>
    {pages.reduce(
      (items, page, index) => reduceChildRoutes({
        items,
        page,
        isDisplayFull,
        isCollapse: isDisplayFull && isCollapse ? isCollapse[index] : false,
        setIsCollapse,
        pathname,
        depth,
        data,
      }),
      [],
    )}
  </List>
)

NavigationList.defaultProps = {
  depth: 0,
  data: {},
  isCollapse: [],
  setIsCollapse: () => {},
  classes: {},
}

NavigationList.propTypes = {
  classes: PropTypes.shape(),
  pages: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  isDisplayFull: PropTypes.bool.isRequired,
  pathname: PropTypes.string.isRequired,
  isCollapse: PropTypes.arrayOf(PropTypes.bool),
  setIsCollapse: PropTypes.func,
  depth: PropTypes.number,
  data: PropTypes.shape(),
}

export default withStyles(navigationListStyle)(NavigationList)
