import React, { useCallback, useMemo } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { Link, matchPath, useParams } from 'react-router-dom'
import clsx from 'clsx'
import { useUser } from 'redux/hooks/user'
import { useAuth } from 'modules/auth/redux/hook'
import SystemRouter from 'router/SystemRouters'

const style = (theme) => ({
  link: {
    display: 'block',
    textDecoration: 'underline',
    color: `${theme.color.primaryBlue} !important`,
  },
})

const TextLink = ({
  classes,
  children,
  to,
  className,
  ...props
}) => {
  const { storeId } = useParams()
  const { user } = useUser()
  const { authState } = useAuth()
  const checkMatchPath = useCallback(
    (route) => !!matchPath(
      to.split('?')[0], { path: route.path, exact: route.exact, strict: route.strict },
    ), [to],
  )

  const findRoute = useCallback((routes) => {
    let foundRoute = null
    for (let i = 0; i < routes.length; i += 1) {
      const route = routes[i]
      if (route.children) {
        foundRoute = findPage(route.children)
        if (foundRoute) {
          break
        }
      }
      if (checkMatchPath(route)) {
        return route
      }
    }
    return foundRoute
  }, [checkMatchPath])

  const hasPermissionRole = useMemo(() => {
    const route = findRoute(SystemRouter)

    if (!route) {
      return true
    }

    if (route && !route.roles) {
      return true
    }
    if (route && (route.roles === true || route.roles(storeId, user.plugins)(authState.data))) {
      return true
    }
    return false
  }, [findRoute, user.plugins, authState.data])

  return (
    <>
      { hasPermissionRole && (
        <Link
          to={to}
          className={clsx(classes.link, className)}
          {...props}
        >
          {children}
        </Link>
      )}
      { !hasPermissionRole && children}
    </>
  )
}

TextLink.defaultProps = {
  className: '',
}

TextLink.propTypes = {
  classes: PropTypes.shape().isRequired,
  to: PropTypes.string.isRequired,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
}

export default withStyles(style)(TextLink)
