import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

const styles = (theme) => ({
  container: {
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 5,
    padding: theme.spacing(2),
    fontSize: 12,
    lineHeight: '14px',
    color: theme.color.midGray,
  },
  title: {
    fontSize: 14,
    lineHeight: '17px',
    fontWeight: 500,
    marginBottom: theme.spacing(1),
    color: 'black',
  },
})

const SubjectInfo = ({
  title, children, classes, className, titleClassName,
}) => (
  <div className={clsx(classes.container, className)}>
    <div className={clsx(classes.title, titleClassName)}>{title}</div>
    {children}
  </div>
)

SubjectInfo.propTypes = {
  title: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string,
  ]),
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  titleClassName: PropTypes.string,
}

SubjectInfo.defaultProps = {
  title: '',
  children: '',
  className: '',
  titleClassName: '',
}

export default withStyles(styles)(SubjectInfo)
