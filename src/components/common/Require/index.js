import React from 'react'
import { COLORS } from 'theme/common'

const Require = () => <span style={{ color: COLORS.red }}>*</span>

export default Require
