const BORDER_RADIUS = 6

const style = (theme) => ({
  root: {
    width: '100%',
  },
  container: {
    width: '100%',
    borderRadius: BORDER_RADIUS,
    marginTop: theme.spacing(2),
  },
  header: {
    padding: theme.spacing(1, 2),
    borderBottom: `1px solid ${theme.color.disableGray1}`,
    borderRadius: BORDER_RADIUS,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerExpand: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
  content: {
    background: theme.color.white,
    padding: theme.spacing(2),
  },
  headerRight: {
    display: 'flex',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    fontWeight: '500',
    flex: 1,
    marginRight: theme.spacing(2),
  },
  btnArrow: {
    padding: 0,
  },
  icArrow: {
    color: theme.color.primaryBlue,
  },
  icArrowOpen: {
    transform: 'rotate(-180deg)',
  },
  icArrowClosed: {
    transform: 'rotate(0)',
  },
  icSort: {
    width: '22px !important',
    height: '22px !important',
    margin: theme.spacing(0, 0.5),
  },
  btnSort: {
    padding: 0,
    margin: theme.spacing(0, 0.5),
  },
})

export default style
