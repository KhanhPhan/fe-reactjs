/* eslint-disable no-param-reassign */
import React, {
  useState, useRef, forwardRef, useImperativeHandle,
} from 'react'
import PropTypes from 'prop-types'
import { Card, withStyles } from '@material-ui/core'
import clsx from 'clsx'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import IconButton from '@material-ui/core/IconButton'
import style from './style'

function renderContent({
  classes, className, title, children, contentClassName,
  initExpand,
  idx, onMove, area, hideIcon,
}, ref) {
  const parent = useRef()
  const [expand, setExpand] = useState(initExpand)
  const selectedDragIndex = useRef(-1)
  const handleCollaspe = () => setExpand(!expand)

  useImperativeHandle(ref, () => ({
    isExpand: () => expand,
    setExpand,
  }), [expand, setExpand])

  const onDragStart = (e) => {
    e.dataTransfer.effectAllowed = 'move'
    selectedDragIndex.current = idx
    e.target.parentElement.dataset.throughDragIdx = -1
    e.target.parentElement.dataset.dragArea = area
  }

  const onDragEnd = (e) => {
    if (parent.current) {
      parent.current.setAttribute('draggable', false)
      if (e.target.parentElement.dataset.throughDragIdx >= 0
        && e.target.parentElement.dataset.throughDragIdx !== selectedDragIndex.current) {
        onMove(selectedDragIndex.current, e.target.parentElement.dataset.throughDragIdx)
        e.target.parentElement.dataset.throughDragIdx = -1
        selectedDragIndex.current = -1
      }
    }
  }

  return (
    <Card
      className={clsx(classes.container, className)}
      data-idx={idx}
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
      ref={parent}
      onDragOver={(e) => {
        e.preventDefault()
        if (e.target.parentElement.dataset.dragArea !== area) {
          return false
        }
        e.target.parentElement.dataset.throughDragIdx = idx
        return true
      }}
    >
      <div
        ref={ref}
        className={clsx(classes.header, {
          [classes.headerExpand]: expand,
        })}
      >
        <div className={classes.title}>
          {title}
        </div>
        {
          !hideIcon && (
            <div className={classes.headerRight}>
              <IconButton onClick={handleCollaspe} className={classes.btnArrow}>
                <ExpandMoreIcon className={clsx(classes.icArrow, {
                  [classes.icArrowOpen]: expand,
                  [classes.icArrowClosed]: !expand,
                })}
                />
              </IconButton>
            </div>
          )
        }
      </div>
      {expand && (
      <div className={clsx(classes.content, contentClassName)}>
        {children}
      </div>
      )}
    </Card>
  )
}

const CollapseForm = forwardRef(renderContent)

CollapseForm.defaultProps = {
  className: '',
  title: '',
  children: '',
  contentClassName: '',
  initExpand: true,
  idx: -1,
  onMove: () => {},
  area: '',
  hideIcon: false,
}

CollapseForm.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  contentClassName: PropTypes.string,
  initExpand: PropTypes.bool,
  idx: PropTypes.number,
  onMove: PropTypes.func,
  area: PropTypes.string,
  hideIcon: PropTypes.bool,
}
export default withStyles(style)(CollapseForm)
