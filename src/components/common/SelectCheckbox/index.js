/* eslint-disable react/no-array-index-key */
import React, { useMemo, useCallback } from 'react'
import PropTypes from 'prop-types'
import {
  withStyles, Typography, Grid,
} from '@material-ui/core'
import clsx from 'clsx'
import includes from 'lodash/includes'
import Checkbox from 'components/common/Checkbox'

const style = (theme) => ({
  container: {
    width: '100%',
  },
  root: {
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 5,
    border: `1px solid ${theme.color.secondaryBlue2}`,
    padding: theme.spacing(0.5),
    maxHeight: 100,
    overflowY: 'auto',
    overflowX: 'hidden',
    scrollbarWidth: 'thin',
    '&::-webkit-scrollbar': {
      width: 5,
      height: 5,
    },
    '&::-webkit-scrollbar-track': {
      borderRadius: 10,
      backgroundColor: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      borderRadius: 10,
      backgroundColor: theme.color.secondaryBlue2,
    },
  },
  legend: {
    marginBottom: 5,
  },
})

const SelectCheckbox = ({
  classes, className, values, options, onChange, legend, forwardoptions,
}) => {
  const list = useMemo(
    () => (options || []).map((item) => forwardoptions(item)), [options, forwardoptions],
  )
  const handleSingleCheck = useCallback((_, value) => {
    if (includes(values, value)) {
      onChange((values || []).filter((v) => v !== value))
    } else {
      onChange(values && Array.isArray(values) ? [...values, value] : [value])
    }
  }, [values, onChange])

  return (
    <div className={clsx(classes.container, className)}>
      {legend && <Typography variant="subtitle1" className={classes.legend}>{legend}</Typography>}
      <Grid id="wrap-checkbox-select" container spacing={1} className={classes.root}>
        {list.map((o, idx) => (
          <Grid key={`${o.value}${o.label}${idx}`} item xs={6}>
            <Checkbox
              label={o.label}
              checked={!!(values || []).find((element) => element === o.value)}
              onChange={(e) => handleSingleCheck(e, o.value)}
              name={o.label}
              whiteColor
            />
          </Grid>
        ))}
      </Grid>
    </div>
  )
}

SelectCheckbox.defaultProps = {
  className: '',
  values: [],
  options: [],
  legend: null,
  onChange: () => {},
  forwardoptions: (o) => o,
}

SelectCheckbox.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.any),
  options: PropTypes.arrayOf(PropTypes.shape()),
  legend: PropTypes.string,
  onChange: PropTypes.func,
  forwardoptions: PropTypes.func,
}
export default withStyles(style)(SelectCheckbox)
