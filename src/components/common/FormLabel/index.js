import { FormLabel, withStyles } from '@material-ui/core'

const FormLabelStyled = withStyles((theme) => ({
  root: {
    textAlign: 'left',
    fontSize: 14,
    letterSpacing: 0,
    color: theme.color.black,
    marginBottom: theme.spacing(1),
    opacity: 1,
    padding: '4px 0 0 0',
  },
}))(FormLabel)

export default FormLabelStyled
