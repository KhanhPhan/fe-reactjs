import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputBase from '@material-ui/core/InputBase'
import moment from 'moment'

const BootstrapInput = withStyles((theme) => ({
  root: {
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase)

const themeStyle = (theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  wrapMenu: {
    padding: 0,
    '& ul.MuiList-root.MuiMenu-list': {
      padding: 0,
      paddingRight: 0,
    },
    '&::-webkit-scrollbar': {
      width: 5,
      height: 5,
    },
    '&::-webkit-scrollbar-track': {
      borderRadius: 10,
      backgroundColor: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      borderRadius: 10,
      backgroundColor: '#b4bfd9',
    },
  },
  menuList: {
    maxHeight: '50vh',
    '& li': {
      font: 'normal normal normal 14px/17px Rubik',
      letterSpacing: '0px',
      color: '#292929',
      paddingBottom: 10,
      paddingTop: 10,
      padding: '0 39px 20px 25px',
      paddingLeft: 17,
      display: 'flex',
      justifyContent: 'center',
    },
  },
})
let loop = 0
let hour = 0
const arrSelectTimes = []
for (let i = 0; i < 96; i += 1) {
  const label = `${
    hour.toLocaleString().length === 1
      ? `0${hour}`
      : hour} : ${(loop * 15).toLocaleString().length === 1 ? `0${loop * 15}` : loop * 15}`
  arrSelectTimes.push({
    value: moment(label, 'HH:mm').format('hh:mm A'),
    label,
  })
  loop += 1
  if (loop === 4) {
    hour += 1
  }
  if (loop === 4) {
    loop = 0
  }
}

const TimePickerSelect = ({
  classes,
  handleChange,
  defaultValue,
  placeholder,
  disabled,
  value,
}) => {
  // const [value, setValue] = useState('Time')
  const onHandleChange = (e) => {
    // setValue(e.target.value)
    handleChange(e)
  }
  return (
    <div className={classes.root} id="time-picker-select">
      <FormControl className={classes.margin}>
        <Select
          labelId="demo-customized-select-label"
          id="demo-customized-select"
          renderValue={(val) => val}
          onChange={(e) => onHandleChange(e)}
          defaultValue={defaultValue}
          input={<BootstrapInput />}
          disabled={disabled}
          placeholder={placeholder}
          value={value}
          MenuProps={{
            classes: { paper: classes.wrapMenu, list: classes.menuList },
          }}
        >
          <MenuItem value="Time" disabled>{placeholder}</MenuItem>
          {
            arrSelectTimes.map((item) => <MenuItem value={item.value}>{item.label}</MenuItem>)
        }
        </Select>
      </FormControl>
    </div>
  )
}

TimePickerSelect.defaultProps = {
  handleChange: () => null,
  defaultValue: 'Time',
  placeholder: 'Time',
  disabled: false,
  value: null,
}
TimePickerSelect.propTypes = {
  classes: PropTypes.shape().isRequired,
  handleChange: PropTypes.func,
  defaultValue: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  value: PropTypes.string,
}

export default withStyles(themeStyle)(TimePickerSelect)
