/* eslint-disable react/no-array-index-key */
/* eslint-disable no-case-declarations */
/* eslint-disable no-use-before-define */
import React from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import Tag from './Tag'

const style = (theme) => ({
  inputRoot: {
    alignItems: 'flex-start',
  },
  root: {
    '& .MuiOutlinedInput-root.Mui-disabled': {
      background: theme.color.disableGray2,
    },
  },
})

const InputTag = ({
  classes,
  tags,
  onTagsChange,
  readOnly,
  disabledInput,
  unique,
  disabledTag,
  ...props
}) => {
  const [inputTag, setInputTag] = React.useState('')
  const onInputTagChange = (_, value) => {
    if (value === ',') return
    setInputTag(value)
  }

  const handleKeyDown = (event) => {
    if (!inputTag) return
    switch (event.key) {
      case 'Enter':
      case ',':
      case 'Tab':
        if (unique && tags.indexOf(inputTag.trim()) !== -1) {
          return
        }
        setInputTag('')
        const newTags = [...tags, inputTag]
        onTagsChange(newTags, inputTag)

        break
      default:
        break
    }
  }

  return (
    <Autocomplete
      {...props}
      classes={classes}
      multiple
      options={[]}
      value={tags}
      open={false}
      disabled={disabledInput}
      disableClearable
      forcePopupIcon={false}
      inputValue={inputTag}
      onInputChange={onInputTagChange}
      onKeyDown={handleKeyDown}
      renderTags={(value) => value.map((tag, index) => (
        <Tag
          key={`tag${index}`}
          label={tag}
          style={{
            marginRight: 5,
            marginTop: 5,
            boxShadow: '0 0 0 1px #C7C7C7',
            maxWidth: 'calc(100% - 5px)',
          }}
          onDelete={() => onTagsChange(value.filter((item) => item !== tag), tag)}
          disabled={disabledTag}
        />
      ))}
      renderInput={(params) => (
        <TextField
          {...params}
          variant="outlined"
          inputProps={{
            ...params?.inputProps,
            readOnly,
          }}
        />
      )}
    />
  )
}
InputTag.defaultProps = {
  tags: [],
  onTagsChange: () => { },
  readOnly: false,
  disabledInput: false,
  unique: false,
  disabledTag: false,
}

InputTag.propTypes = {
  classes: PropTypes.shape().isRequired,
  tags: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.array,
  ]),
  onTagsChange: PropTypes.func,
  readOnly: PropTypes.bool,
  disabledInput: PropTypes.bool,
  unique: PropTypes.bool,
  disabledTag: PropTypes.bool,
}

export default withStyles(style)(InputTag)
