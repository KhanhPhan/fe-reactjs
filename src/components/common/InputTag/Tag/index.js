import React from 'react'
import Chip from '@material-ui/core/Chip'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { tags } from './tags'

const tagStyle = (theme) => ({
  root: {
    height: 32,
    fontSize: 14,
    borderRadius: 5,
    border: `1px solid ${theme.color.darkGray}`,
    backgroundColor: 'white',
    margin: theme.spacing(0.5),
  },
})

const Tag = ({
  classes,
  size,
  variant,
  tag,
  fixedWidth,
  style,
  label,
  ...props
}) => (
  <Chip
    classes={classes}
    style={{
      color: tags[tag]?.color,
      backgroundColor: tags[tag]?.bgColor,
      minWidth: fixedWidth && 105,
      ...style,
    }}
    label={tags[tag]?.label || label}
    variant={variant}
    size={size}
    deleteIcon={(
      <img
        src="/icons/close.svg"
        style={{
          width: 10,
          paddingLeft: 3,
          paddingRight: 3,
        }}
        alt="close"
      />
    )}
    {...props}
  />
)

Tag.defaultProps = {
  size: 'medium',
  variant: 'default',
  tag: '',
  fixedWidth: false,
  style: {},
  label: '',
}

Tag.propTypes = {
  classes: PropTypes.shape().isRequired,
  size: PropTypes.string,
  variant: PropTypes.string,
  tag: PropTypes.string,
  fixedWidth: PropTypes.bool,
  style: PropTypes.shape(),
  label: PropTypes.string,
}

export default withStyles(tagStyle)(Tag)
