import { colors } from 'theme'

export const tags = {
  published: {
    label: 'Published',
    color: colors.primaryBlue,
    bgColor: colors.secondaryBlue3,
  },
  processing: {
    label: 'Processing',
    color: colors.primaryBlue,
    bgColor: colors.secondaryBlue3,
  },
  completed: {
    label: 'Completed',
    color: colors.viridianGreen1,
    bgColor: colors.viridianGreen2,
  },
  on_hold: {
    label: 'On Hold',
    color: colors.apricotOrange1,
    bgColor: colors.apricotOrange2,
  },
  pending: {
    label: 'Pending',
    color: colors.starYellow,
    bgColor: colors.mustardYellow2,
  },
  cancelled: {
    label: 'Cancelled',
    color: colors.rubyRed1,
    bgColor: colors.rubyRed2,
  },
  failed: {
    label: 'Failed',
    color: colors.white,
    bgColor: colors.rubyRed1,
  },
  refunded: {
    label: 'Refunded',
    color: colors.textGray2,
    bgColor: colors.disableGray2,
  },
  draft: {
    label: 'Draft',
    color: '#E0982F',
    bgColor: colors.apricotOrange2,
  },

  future: {
    label: 'Future',
  },
  trash: {
    label: 'Trash',
  },
}
