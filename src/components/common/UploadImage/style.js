const style = (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: 278,
    height: 158,
    padding: theme.spacing(2),
    border: `1px dashed ${theme.color.primaryBlue}`,
    borderRadius: theme.spacing(1),
    '&>*:not(:last-child)': {
      textAlign: 'center',
      marginBottom: theme.spacing(1.5),
    },
    '&.loading': {
      background: theme.color.backgroundBlue,
    },
  },
  icon: {
    height: theme.spacing(3),
  },
  fileInput: {
    display: 'none',
  },
  uploadButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    background: theme.color.backgroundBlue,
    color: theme.color.primaryBlue,
    padding: theme.spacing(0.5, 2),
    borderRadius: theme.spacing(0.5),
    '& svg': {
      marginRight: theme.spacing(1),
    },
  },
})

export default style
