import React from 'react'
import PropTypes from 'prop-types'
import { CloudUpload } from '@material-ui/icons'
import { CircularProgress, withStyles } from '@material-ui/core'
import clsx from 'clsx'
import style from './style'

const UploadImage = ({
  classes,
  icon, title, uploadTitle, handleUpload,
  loading,
}) => (
  <div className={clsx(classes.container, { loading })}>
    {
      loading ? <CircularProgress size={25} />
        : (
          <>
            {icon && (
            <img
              src={icon}
              alt="upload"
              className={classes.icon}
            />
            )}
            {title}
            <label
              className={classes.uploadLabel}
              htmlFor="file-upload"
            >
              <div className={classes.uploadButton}>
                <CloudUpload />
                {uploadTitle}
              </div>
              <input
                className={classes.fileInput}
                id="file-upload"
                type="file"
                accept="image/*"
                onChange={handleUpload}
              />
            </label>
          </>
        )
    }
  </div>
)

UploadImage.defaultProps = {
  icon: null,
  title: '',
  uploadTitle: 'Upload',
  loading: false,
}

UploadImage.propTypes = {
  classes: PropTypes.shape().isRequired,
  icon: PropTypes.string,
  title: PropTypes.string,
  uploadTitle: PropTypes.string,
  handleUpload: PropTypes.func.isRequired,
  loading: PropTypes.bool,
}

export default withStyles(style)(UploadImage)
