import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { withStyles, Tooltip } from '@material-ui/core'

const style = {
  divEl: {
    width: 'auto',
    display: 'inline-block',
    visibility: 'hidden',
    position: 'fixed',
    overflow: 'auto',
  },
  tooltip: {
    background: theme.color.textGray3,
    color: theme.color.white,
    fontSize: 14,
    fontWeight: 300,
    padding: 16,
    borderRadius: 8,
  },
}
const TooltipWrapper = ({
  classes, value, children, offset,
}) => {
  const [title, setTitle] = useState('')
  useEffect(() => {
    const divEl = document.getElementById('divEl')
    const wrapperEl = document.getElementById('wrapper')
    divEl.innerText = value

    if (divEl.clientWidth > wrapperEl.clientWidth - offset) {
      setTitle(value)
    } else {
      setTitle('')
    }
  }, [value])

  return (
    <>
      <Tooltip
        title={title}
        arrow
        classes={{
          tooltip: classes.tooltip,
        }}
      >
        <div id="wrapper">
          {children}
        </div>
      </Tooltip>
      <div id="divEl" className={classes.divEl} />
    </>
  )
}

TooltipWrapper.defaultProps = {
  offset: 0,
  value: '',
}

TooltipWrapper.propTypes = {
  classes: PropTypes.shape().isRequired,
  children: PropTypes.node.isRequired,
  value: PropTypes.string,
  offset: PropTypes.number,
}

export default withStyles(style)(TooltipWrapper)
