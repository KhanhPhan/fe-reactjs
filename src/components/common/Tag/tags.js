import { colors } from 'theme'

export const tags = {
  published: {
    label: 'Published',
    color: colors.primaryBlue,
    bgColor: colors.secondaryBlue3,
  },
  processing: {
    label: 'Processing',
    color: colors.primaryBlue,
    bgColor: colors.secondaryBlue3,
  },
  completed: {
    label: 'Completed',
    color: colors.viridianGreen1,
    bgColor: colors.viridianGreen2,
  },
  on_hold: {
    label: 'On Hold',
    color: colors.apricotOrange1,
    bgColor: colors.apricotOrange2,
  },
  pending: {
    label: 'Pending',
    color: colors.starYellow,
    bgColor: colors.mustardYellow2,
  },
  cancelled: {
    label: 'Cancelled',
    color: colors.rubyRed1,
    bgColor: colors.rubyRed2,
  },
  failed: {
    label: 'Failed',
    color: colors.white,
    bgColor: colors.rubyRed1,
  },
  refunded: {
    label: 'Refunded',
    color: colors.textGray2,
    bgColor: colors.disableGray2,
  },
  draft: {
    label: 'Draft',
    color: '#E0982F',
    bgColor: colors.apricotOrange2,
  },
  future: {
    label: 'Future',
  },
  private: {
    label: 'Private',
  },
  trash: {
    label: 'Trash',
  },
  incomplete: {
    label: 'Incomplete',
    color: colors.rubyRed1,
    bgColor: colors.rubyRed2,
  },
  synced: {
    label: 'Synced',
    color: colors.viridianGreen1,
    bgColor: colors.viridianGreen2,
  },
  importing: {
    label: 'Importing',
    color: colors.starYellow,
    bgColor: colors.mustardYellow2,
  },
  shipping_prepare: {
    label: 'Preparing',
    color: '#E0982F',
    bgColor: colors.apricotOrange2,
  },
  shipping_picked: {
    label: 'Picked Up',
    color: colors.viridianGreen1,
    bgColor: colors.viridianGreen2,
  },
  shipping_cancelled: {
    label: 'Cancelled',
    color: colors.rubyRed1,
    bgColor: colors.rubyRed2,
  },
  comment_pending: {
    label: 'Pending',
    color: 'white',
    bgColor: colors.apricotOrange1,
  },
  comment_approved: {
    label: 'Approved',
    color: colors.viridianGreen1,
    bgColor: colors.viridianGreen2,
  },
  in_transit: {
    label: 'In-Transit',
    color: colors.primaryBlue,
    bgColor: colors.secondaryBlue3,
  },
  partial_check_in: {
    label: 'Partial Check-in',
    color: colors.apricotOrange1,
    bgColor: colors.apricotOrange2,
  },
  unpaid: {
    label: 'Unpaid',
    color: colors.apricotOrange1,
    bgColor: colors.apricotOrange2,
  },
  paid: {
    label: 'Paid',
    color: colors.viridianGreen1,
    bgColor: colors.viridianGreen2,
  },
}
