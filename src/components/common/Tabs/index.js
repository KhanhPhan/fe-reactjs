/* eslint-disable react/no-array-index-key */
import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import { withStyles } from '@material-ui/core'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Skeleton from '@material-ui/lab/Skeleton'
import { FiberManualRecord } from '@material-ui/icons'
import SimpleTabs from './SimpleTabs'

const style = (theme) => ({
  subLabel: {
    fontSize: 28,
    fontWeight: 400,
    color: theme.color.textGray1,
  },
  label: {
    display: 'flex',
    alignItems: 'center',
  },
  title: {
    color: theme.color.textGray2,
    fontSize: 16,
    fontWeight: 'normal',
    textTransform: 'none',
  },
  tab: {
    minWidth: 0,
    padding: theme.spacing(0, 2),
    marginRight: theme.spacing(1),
    borderRadius: 3,
  },
  subtitleLoader: {
    minWidth: 60,
    fontSize: 28,
  },
  titleLoader: {
    minWidth: 60,
    fontSize: 16,
  },
  // TODO: improve because opacity is changed when button is seleted
  divBefore: {
    overflow: 'visible',
    '&::before': {
      content: '""',
      position: 'absolute',
      left: -4,
      width: 1,
      height: `calc(100% - ${theme.spacing(2)}px)`,
      backgroundColor: theme.color.grayBorder,
    },
  },
  redDot: {
    color: 'red',
    marginLeft: 8,
    fontSize: 14,
  },
  number: {
    color: theme.color.textGray2,
    fontSize: 12,
    fontWeight: 500,
    width: 16,
    height: 16,
    lineHeight: '16px',
    borderRadius: 8,
    border: `1px solid ${theme.color.textGray2}`,
    marginRight: 8,
  },
  fullWidth: {
    width: '100%',
    justifyContent: 'center',
  },
  fullWidthTab: {
    flex: 1,
  },
})

const StyledTabs = withStyles(() => ({
  root: {
  },
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    height: '3px !important',
    '& > span': {
      maxWidth: 110,
      minWidth: 60,
      width: '100%',
      backgroundColor: '#3B3BC7',
    },
  },
}))((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />)

const TabsTheme = ({
  classes,
  handleChangeTab,
  tabs,
  children,
  preview,
  loaderCount,
  value,
  dividerBefore,
  withNumber,
  fullWidth,
  ...props
}) => {
  const renderTabs = () => tabs.map((tab, index) => (
    <Tab
      key={`${index}${tab.label}`}
      className={clsx(
        classes.tab,
        {
          [classes.divBefore]: dividerBefore.includes(index),
          [classes.fullWidthTab]: fullWidth,
        },
      )}
      style={tab.styles || {}}
      value={tab.value}
      label={(
        <>
          <span className={classes.subLabel}>
            {tab.subLabel || ''}
          </span>
          <div className={classes.label}>
            {withNumber && (
              <span className={classes.number}>
                {index + 1}
              </span>
            )}
            <span className={classes.title}>
              {tab.label || ''}
            </span>
            {tab.hasNoti && <FiberManualRecord className={classes.redDot} />}
          </div>
        </>
      )}
    />
  ))

  const renderTabsLoader = () => [...Array(loaderCount).keys()].map((_, index) => (
    <Tab
      key={index}
      label={(
        <>
          <Skeleton className={classes.subtitleLoader} />
          <Skeleton className={classes.titleLoader} />
        </>
      )}
    />
  ))

  return (
    <>
      <StyledTabs
        classes={{
          root: clsx({ [classes.fullWidth]: fullWidth }),
        }}
        value={value}
        onChange={(_, v) => handleChangeTab(v)}
        aria-label="tabs"
        indicatorColor="primary"
        variant="scrollable"
        scrollButtons="auto"
        {...props}
      >
        {
          preview ? renderTabsLoader() : renderTabs()
        }
      </StyledTabs>
      {children}
    </>
  )
}

TabsTheme.defaultProps = {
  tabs: [],
  children: '',
  preview: false,
  handleChangeTab: () => {},
  loaderCount: 7,
  dividerBefore: [],
  withNumber: false,
  fullWidth: false,
}

TabsTheme.propTypes = {
  classes: PropTypes.shape().isRequired,
  children: PropTypes.node,
  tabs: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.array,
  ]),
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
  handleChangeTab: PropTypes.func,
  preview: PropTypes.bool,
  loaderCount: PropTypes.number,
  dividerBefore: PropTypes.arrayOf(PropTypes.number),
  withNumber: PropTypes.bool,
  fullWidth: PropTypes.bool,
}

TabsTheme.Simple = SimpleTabs

export default withStyles(style)(TabsTheme)
