import React from 'react'
import PropTypes from 'prop-types'
import { withStyles, Grid } from '@material-ui/core'
import clsx from 'clsx'
import Icon from 'components/common/Icon'
import { colors } from 'theme'
import Button from '../Button'

const style = (theme) => ({
  root: {
    fontWeight: 400,
  },
  disabled: {
    background: theme.color.disableGray2,
    color: `${theme.color.black} !important`,
    border: '1px solid transparent',
  },
})
const SelectButton = ({
  classes,
  buttons,
  onChange,
  value,
  buttonClassName,
  disabled,
  startIconClassName,
  className,
}) => (
  <Grid container spacing={1} className={className}>
    {buttons.map((button) => (
      <Grid item xs={6}>
        <Button
          className={
        clsx(
          classes.root,
          value !== button.value && classes.disabled,
          buttonClassName,
        )
      }
          key={button.value}
          label={button.label}
          color="secondary"
          variant="outlined"
          startIcon={(
        button.icon
      && (
      <Icon
        name={button.icon}
        size="small"
        color={value === button.value
          ? colors.primaryBlue
          : colors.black}
        className={startIconClassName}
      />
      ))}
          onClick={() => !disabled && onChange(button.value)}
        />
      </Grid>
    ))}
  </Grid>
)
SelectButton.defaultProps = {
  buttons: [],
  onChange: () => {},
  buttonClassName: '',
  disabled: false,
  startIconClassName: '',
  className: '',
}

SelectButton.propTypes = {
  classes: PropTypes.shape().isRequired,
  buttons: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.array,
  ]),
  onChange: PropTypes.func,
  buttonClassName: PropTypes.string,
  disabled: PropTypes.bool,
  startIconClassName: PropTypes.string,
  value: PropTypes.string.isRequired,
  className: PropTypes.string,
}

export default withStyles(style)(SelectButton)
