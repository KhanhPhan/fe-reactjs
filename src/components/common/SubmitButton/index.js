import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'
import Button from 'components/common/Button'

const SubmitButton = ({
  children, onValidationFail, submitWhen, ...props
}) => {
  const { handleSubmit, validateForm } = useFormikContext()
  const doSummit = useCallback(async () => {
    const errors = await validateForm()
    if (Object.keys(errors).length > 0) {
      onValidationFail(errors)
      // TODO: handle check validate 2 times
      handleSubmit()
    } else if (submitWhen()) handleSubmit()
  }, [handleSubmit, validateForm])
  return (<Button {...props} onClick={doSummit}>{children}</Button>)
}

export const generateValidateFormErrorMessage = (errors) => {
  const errorMessages = Object.values(errors).filter((msg) => !!msg)
  return errorMessages.join(',\n')
}

SubmitButton.defaultProps = {
  children: '',
  onValidationFail: () => {},
  submitWhen: () => true,
}

SubmitButton.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string,
  ]),
  onValidationFail: PropTypes.func,
  submitWhen: PropTypes.func,
}

export default SubmitButton
