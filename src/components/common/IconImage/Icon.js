import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'
import style from './style'

const IconImage = ({
  classes, src, size, className,
}) => (
  <img className={clsx(classes.icon, size, className)} src={src} alt="img" />
)

IconImage.defaultProps = {
  className: '',
  size: 'medium',
}

IconImage.propTypes = {

  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  src: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
}

export default withStyles(style)(IconImage)
