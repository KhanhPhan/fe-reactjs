const style = (theme) => ({
  icon: {
    '&.small': {
      height: theme.spacing(2),
      width: theme.spacing(2),
    },
    '&.medium': {
      height: 30,
      width: 30,
    },
    '&.large': {
      height: theme.spacing(5),
      width: theme.spacing(5),
    },
  },
})

export default style
