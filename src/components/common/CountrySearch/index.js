import React, { useEffect, useState } from 'react'
import { withStyles } from '@material-ui/core'
import { useFieldValue } from 'utils/formik/hooks'
import SearchBox from 'components/common/AsyncSelect/SearchBox'
import countryApi from 'services/apis/country'
import style from '../../../pages/Customer/CustomerList/Header/FilterForm/style'

const CountrySearch = () => {
  const allCountries = countryApi.getList().map((item) => ({
    id: item.code,
    value: item.code,
    label: item.name,
  }))
  const [country, setCountry] = useFieldValue('country')
  const [selectedCountries, setSelectedCountries] = useState([])

  useEffect(() => {
    if (country && country.length !== selectedCountries.length) {
      setSelectedCountries(allCountries.filter((x) => country.find((y) => y === x.value)))
    }
  }, [country])

  const onCountrySelect = (sltItems) => {
    setSelectedCountries(sltItems)
    if (!sltItems) {
      setCountry([])
      return
    }
    const countryCodes = []
    sltItems.forEach((p) => {
      countryCodes.push(p.value)
    })
    setCountry(countryCodes)
  }

  return (
    <SearchBox
      value={selectedCountries}
      options={allCountries?.filter((x) => !selectedCountries.find((y) => x.value === y.value))}
      handleFilterOption={
        (res) => res.filter((x) => !selectedCountries.find((y) => y.value === x.value))
      }
      inputProps={{
        placeholder: 'Select Country',
        autoComplete: 'off',
      }}
      multiple
      popupIcon
      disableClearable
      onSearchBoxChange={(object, value) => {
        onCountrySelect(value)
      }}
    />
  )
}

CountrySearch.defaultProps = {}

CountrySearch.propTypes = {}

export default withStyles(style)(CountrySearch)
