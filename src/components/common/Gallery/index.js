/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect, useCallback } from 'react'
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import ArrowNextIcon from '@material-ui/icons/ArrowForward'
import GallerySelect from 'components/common/Gallery/GallerySelect'
import GalleryPreview from 'components/common/Gallery/GalleryPreview'
import Button from 'components/common/Button'
import UploadFiles from 'components/common/Gallery/UploadFiles'
import { GALLERY_STEP } from 'constants/product'
import { MEDIA_FILE_TYPE, MEDIA_FOLDER_STATIC, FILE_INFO_MODE } from 'constants/media'
import { useMedia } from 'redux/hooks/media'
import MediaDialogLayout from './MediaDialogLayout'
import style from './style'

const Gallery = ({
  classes,
  images,
  openGallery,
  setOpenGallery,
  handleSave,
  gallerySelectFile,
  setGallerySelectFile,
  galleryStep,
  setGalleryStep,
  multiple,
  showTypes,
  selectTypes,
  isProductMode,
  removeMedia,
  noUploadVideo,
  acceptTypes,
  enabledEditMetadataIds,
}) => {
  const [fileSelected, setFileSelected] = useState(images)
  const [folderSelectedId, setFolderSelectedId] = useState(MEDIA_FOLDER_STATIC.ALL_FILE)

  const { mediaFile, loading, actions } = useMedia()
  useEffect(() => {
    setFileSelected(images)
  }, [images])

  const handleOpenUploadFile = () => {
    setGalleryStep(GALLERY_STEP.UPLOAD_FILE)
  }

  const handleAddMoreFile = useCallback(() => setGalleryStep(GALLERY_STEP.SELECT), [galleryStep])
  const didFinishUploadFiles = useCallback(() => {
    actions.getListFolder()
    actions.setListFileFolderData({
      total_pages: 0,
      page: 1,
    })
    const params = {
      folder_id: folderSelectedId,
      limit: mediaFile.limit,
      page: 1,
    }
    actions.getListFile(params)
    setGalleryStep(GALLERY_STEP.SELECT)
  }, [mediaFile, folderSelectedId])

  const didUpdatedFileMetadata = (filesSelect) => {
    const newFilesSelect = filesSelect.map((file) => {
      const isNumeric = (value) => /^-?\d+$/.test(value)
      // check if file id belong to file id woo or not
      const id = isNumeric(file.id) ? file.id : undefined
      const title = file.title ? file.title : (file.title === '' ? file.title : file.name)
      return ({
        id,
        name: title,
        src: file.src,
        thumbnail: file.public_url || file.src,
        alt: file.alternative_text,
        contentType: file.content_type,
        id_select: file.id,
      })
    })

    handleSave(newFilesSelect)
  }

  const renderFooter = useCallback(() => (
    <div className={classes.footer}>
      {
        galleryStep === GALLERY_STEP.UPLOAD_FILE && (
          <Button
            rounded
            color="secondary"
            label="Back"
            loading={loading.uploadFile}
            startIcon={<ArrowBackIcon />}
            onClick={() => setGalleryStep(GALLERY_STEP.SELECT)}
          />
        )
      }

      {
        galleryStep === GALLERY_STEP.SELECT ? (
          <Button
            rounded
            label="Upload Files"
            onClick={handleOpenUploadFile}
          />
        ) : <div />
      }

      {galleryStep === GALLERY_STEP.SELECT && (
        <div>
          <Button
            rounded
            label="Next"
            disabled={fileSelected.filter((f) => Object.keys(f).length !== 0).length === 0}
            onClick={() => {
              setFolderSelectedId(MEDIA_FOLDER_STATIC.ALL_FILE)
              setGalleryStep(GALLERY_STEP.PREVIEW)
            }}
            endIcon={<ArrowNextIcon />}
          />
        </div>
      )}

      {
        galleryStep === GALLERY_STEP.PREVIEW && (
          <Button
            rounded
            label="Save"
            type="submit"
            form="editFileMetadata"
            loading={loading.updateMetadata}
          />
        )
      }
    </div>
  ), [galleryStep, fileSelected, loading])

  return (
    <MediaDialogLayout
      title="GALLERY"
      renderFooter={renderFooter}
      maxWidth="xl"
      open={openGallery}
      onClose={() => {
        setOpenGallery(false)
        setFolderSelectedId(MEDIA_FOLDER_STATIC.ALL_FILE)
        setFileSelected(images)
      }}
    >
      <div className={classes.galleryContainer}>
        {
          galleryStep === GALLERY_STEP.SELECT && (
            <GallerySelect
              fileSelected={fileSelected}
              setFileSelected={setFileSelected}
              multiFileSelect={multiple}
              showTypes={showTypes}
              selectTypes={selectTypes}
              folderSelectedId={folderSelectedId}
              setFolderSelectedId={setFolderSelectedId}
            />
          )
        }
        {
          galleryStep === GALLERY_STEP.PREVIEW && (
            <GalleryPreview
              fileSelected={fileSelected}
              setFileSelected={setFileSelected}
              gallerySelectFile={gallerySelectFile}
              setGallerySelectFile={setGallerySelectFile}
              addMoreFile={handleAddMoreFile}
              multiFileSelect={multiple}
              isProductMode={isProductMode}
              removeMedia={removeMedia}
              didUpdatedFileMetadata={didUpdatedFileMetadata}
              enabledEditMetadataIds={enabledEditMetadataIds}
              mode={FILE_INFO_MODE.GALLERY_DIALOG}
            />
          )
        }
        {
          galleryStep === GALLERY_STEP.UPLOAD_FILE && (
            <UploadFiles
              noUploadVideo={noUploadVideo}
              didFinishUploadFiles={didFinishUploadFiles}
              acceptTypes={acceptTypes}
              uploadMultiFile={multiple}
              folderSelectedId={folderSelectedId}
            />
          )
        }
      </div>
    </MediaDialogLayout>
  )
}

Gallery.defaultProps = {
  images: [],
  handleSave: () => { },
  multiple: true,
  showTypes: [MEDIA_FILE_TYPE.All],
  selectTypes: [MEDIA_FILE_TYPE.IMAGE],
  isProductMode: true,
  removeMedia: false,
  noUploadVideo: false,
  acceptTypes: '.jpg,.png,.jpeg,.mp4,.mov',
  enabledEditMetadataIds: [],
}

Gallery.propTypes = {
  classes: PropTypes.shape().isRequired,
  images: PropTypes.arrayOf(PropTypes.shape()),
  handleSave: PropTypes.func,
  openGallery: PropTypes.bool.isRequired,
  setOpenGallery: PropTypes.func.isRequired,
  gallerySelectFile: PropTypes.number.isRequired,
  setGallerySelectFile: PropTypes.func.isRequired,
  galleryStep: PropTypes.number.isRequired,
  setGalleryStep: PropTypes.func.isRequired,
  multiple: PropTypes.bool,
  showTypes: PropTypes.arrayOf(PropTypes.string),
  selectTypes: PropTypes.arrayOf(PropTypes.string),
  isProductMode: PropTypes.bool,
  removeMedia: PropTypes.bool,
  noUploadVideo: PropTypes.bool,
  acceptTypes: PropTypes.string,
  enabledEditMetadataIds: PropTypes.oneOfType([PropTypes.array, PropTypes.shape]),

}

export default React.memo(withStyles(style)(Gallery))
