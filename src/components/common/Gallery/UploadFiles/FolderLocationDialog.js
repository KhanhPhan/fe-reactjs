import React, { useCallback, useRef } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'
import FormDialog from 'components/common/Dialog/FormDialog'
import Button from 'components/common/Button'
import { useMedia } from 'redux/hooks/media'
import FolderLocation from 'components/common/Gallery/FolderLocation'

const style = (theme) => ({
  dialogContainer: {
    padding: 20,
    color: '#2E2E2E',
    minHeight: 'calc(40vh - 200px)',
    '&.folderEdit': {
      minHeight: 'fit-content',
    },
  },
  folderTitle: {
    fontSize: 14,
    marginBottom: 10,
  },
  folderContainer: {
    border: '1px solid #B4BFD9',
    borderRadius: '5px',
    background: '#EDF1FB',
    maxHeight: 300,
    overflowY: 'auto',
  },
  footerContainer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-end',
    '& button:not(:last-child)': {
      marginRight: theme.spacing(2),
    },
  },
})

const FolderLocationDialog = (props) => {
  const {
    classes,
    show,
    setShow,
    setFolderSelected,
    folderSelected,
  } = props
  const { mediaFolder } = useMedia()
  const folderRef = useRef(null)
  const handleSave = () => {
    if (!folderRef.current) {
      setFolderSelected(folderSelected)
    } else {
      setFolderSelected(folderRef.current)
    }
    setShow(false)
  }
  const onFolderSelected = useCallback((f) => {
    folderRef.current = f
  }, [])

  const renderFooter = () => (
    <div className={classes.footerContainer}>
      <Button
        className={classes.addButton}
        color="secondary"
        rounded
        label="Cancel"
        onClick={() => setShow(false)}
      />
      <Button
        className={classes.addButton}
        rounded
        label="Save"
        onClick={handleSave}
      />
    </div>
  )

  return (
    <FormDialog
      open={show}
      maxWidth="xs"
      title="CHANGE UPLOAD DESTINATION"
      footer={renderFooter}
      onClose={() => setShow(false)}
      disableBackdropClick={false}
    >
      <div className={classes.dialogContainer}>
        <div className={classes.folderTitle}>Folders</div>
        <div className={clsx(classes.folderContainer, 'customized-scrollbar')}>
          <FolderLocation
            folders={mediaFolder.list}
            onFolderSelected={onFolderSelected}
            mFolderSelected={folderSelected}
          />
        </div>
      </div>
    </FormDialog>
  )
}

FolderLocationDialog.defaultProps = {
  folderSelected: null,
}

FolderLocationDialog.propTypes = {
  classes: PropTypes.shape().isRequired,
  show: PropTypes.bool.isRequired,
  setShow: PropTypes.func.isRequired,
  setFolderSelected: PropTypes.func.isRequired,
  folderSelected: PropTypes.oneOfType([PropTypes.object, PropTypes.shape]),

}

export default withStyles(style)(FolderLocationDialog)
