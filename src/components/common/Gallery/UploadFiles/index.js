/* eslint-disable no-async-promise-executor */
/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  useMediaQuery,
  useTheme,
  withStyles,
} from '@material-ui/core'
import Card from '@material-ui/core/Card'
import imageCompression from 'browser-image-compression'
import axios from 'axios'
import clsx from 'clsx'
import { useMedia } from 'redux/hooks/media'
import Button from 'components/common/Button'
import Loading3Dot from 'components/common/Loading3Dot'
import mediaApis from 'services/apis/media'
import { showDialog } from 'components/common/Dialog'
import { dialogParams } from 'components/common/Dialog/CustomDialog'
import { MEDIA_FOLDER_STATIC } from 'constants/media'
import { getFolderId } from 'utils/helpers/media'
import FolderLocationDialog from './FolderLocationDialog'
import style from './style'
import { TextBlockToolTip } from '../../TextBlock'

const UploadFile = (props) => {
  const {
    classes,
    noUploadVideo,
    uploadMultiFile,
    acceptTypes,
    folderSelectedId,
    didFinishUploadFiles,
  } = props
  const { actions, mediaFolder } = useMedia()
  const [uploading, setUploading] = useState(0)
  const [addFileError, setAddFileError] = useState('')
  const [folderSelected, setFolderSelected] = useState(
    { id: MEDIA_FOLDER_STATIC.UNSORTED, name: 'Media Library', paths: [] },
  )
  const [showFolderLocation, setShowFolderLocation] = useState(false)
  const theme = useTheme()
  const isTablet = useMediaQuery(theme.breakpoints.down(1024))

  useEffect(() => {
    if (folderSelectedId
      && folderSelectedId !== MEDIA_FOLDER_STATIC.ALL_FILE
      && folderSelectedId !== MEDIA_FOLDER_STATIC.UNSORTED) {
      setFolderSelected(mediaFolder.list.find((folder) => folder.id === folderSelectedId))
    }
  }, [folderSelectedId])

  const createFileMedia = ({ file }) => new Promise(async (resolve, reject) => {
    const params = {
      parent_id: getFolderId(folderSelected.id),
      name: file.name,
      file_type: file?.type.split('/')[1],
      content_type: file.type,
      width: 100,
      height: 100,
      size: file.size,
      alternative_text: '',
      title: '',
      caption: '',
      description: '',
      copy_link: null,
      tags: [],
    }
    const formData = new FormData()
    formData.append(file.name, file, file.name)

    try {
      const fileResult = await mediaApis.createFile(params)
      // TEST: Test localhost
      // 1. Open https://cors-anywhere.herokuapp.com/corsdemo in browser
      // 2. Un comment block code: await axios.put(`https://cors-anywhere.....
      // 3. Comment block code: await axios.put(fileResult.resumable...
      // await axios.put(
      //   `https://cors-anywhere.herokuapp.com/${fileResult.resumable_upload_session}`, file,
      //   {
      //     headers: {
      //       'Content-Type': file.type || 'application/octet-stream',
      //       'Access-Control-Allow-Origin': '*',
      //       'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      //       'X-Upload-Content-Type': file.type,
      //       'X-Upload-Content-Length': file.size,
      //     },
      //   },
      // )

      await axios.put(fileResult.resumable_upload_session, file,
        {
          headers: {
            'Content-Type': file.type || 'application/octet-stream',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
            'X-Upload-Content-Type': file.type,
            'X-Upload-Content-Length': file.size,
          },
        })
      const fileInfo = await mediaApis.notifyUploaded({ file_id: fileResult._id })
      resolve(fileInfo)
    } catch (error) {
      reject(error)
      actions.setListFileFolderData({ loading: { uploadFile: false } })
    }
  })

  const handleUploadFiles = async (files) => {
    if (files && files.length > 0) {
      const options = {
        maxSizeMB: 1,
        useWebWorker: true,
      }
      actions.setListFileFolderData({ loading: { uploadFile: true } })
      try {
        const uploadedFiles = await Promise.all(Array.from(files).map((async (file) => {
          const isImageFile = file.type.split('/')[0] === 'image'
          if (isImageFile && file.size / 1024 / 1024 > 1) {
            const filesCompressed = await imageCompression(file, options)
            return createFileMedia({ file: filesCompressed })
          }
          return createFileMedia({ file })
        })))
        actions.setListFileFolderData({ loading: { uploadFile: false } })
        setUploading(0)
        didFinishUploadFiles(
          uploadedFiles.map(
            (e) => ({ ...e, ...{ src: e.public_url } }),
          ), getFolderId(folderSelected.id),
        )
      } catch (error) {
        actions.setListFileFolderData({ loading: { uploadFile: false } })
        setUploading(0)
        showDialog(dialogParams.error(error))
      }
    }
  }

  const onFileLoad = (files) => {
    setUploading(files.length)
    const isImageFiles = Array.from(files).some((e) => e.type.split('/')[0] === 'image')
    const isVideoFiles = Array.from(files).some((e) => e.type.split('/')[0] === 'video')
    const isImageFileType = Array.from(files)
      .some((e) => e.type.split('/')[1] === 'jpg'
        || e.type.split('/')[1] === 'png' || e.type.split('/')[1] === 'jpeg')
    const isVideoFileType = Array.from(files)
      .some((e) => e.type.split('/')[1] === 'mp4'
        || e.type.split('/')[1] === 'quicktime')
    const isFileAccess = Array.from(files)
      .some((e) => e.type.split('/')[0] === 'video'
        || e.type.split('/')[0] === 'image')
    const fileLarger10MB = Array.from(files).find((e) => (e.size / 1024 / 1024) > 10)
    let inValidVideo = false
    if (acceptTypes.indexOf('mp4') === -1 && acceptTypes.indexOf('mov') === -1) {
      inValidVideo = true
    }
    let inValidImage = false
    if (acceptTypes.indexOf('jpg') === -1
      && acceptTypes.indexOf('png') === -1
      && acceptTypes.indexOf('jpeg') === -1) {
      inValidImage = true
    }
    if (isVideoFiles && (noUploadVideo || inValidVideo)) {
      setAddFileError('Sorry, this file type is not supported.')
      setUploading(0)
    } else if (isImageFiles && inValidImage) {
      setAddFileError('Sorry, this file type is not supported.')
      setUploading(0)
    } else if (!isFileAccess) {
      setAddFileError('Sorry, this file type is not supported.')
      setUploading(0)
    } else if (isImageFiles && !isImageFileType) {
      setAddFileError('For image formats, we only support JPG and PNG for now')
      setUploading(0)
    } else if (isVideoFiles && !isVideoFileType) {
      setAddFileError('For video formats, we only support MP4 and MOV for now')
      setUploading(0)
    } else if (fileLarger10MB) {
      setUploading(0)
      setAddFileError('Your file is bigger than 10MB')
    } else {
      handleUploadFiles(files, isImageFiles)
    }
  }

  const onDrop = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (!uploadMultiFile && (e.dataTransfer.files || []).length > 1) {
      setAddFileError('You can upload at most 1 file')
    } else {
      onFileLoad(e.dataTransfer.files)
    }
  }
  const onDragOver = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }

  return (
    <Card
      className={clsx(classes.uploadContainer, 'customized-scrollbar')}
      onDrop={onDrop}
      onDragOver={onDragOver}
    >
      <div className={classes.headerContainer}>
        <div className={classes.headerContent}>
          <span className={classes.headerTitle}>Upload Destination</span>
          <TextBlockToolTip
            title={isTablet
              ? (
                <span>
                  {`Media Library ${
                    folderSelected?.paths.length > 0 ? `> ${
                      folderSelected?.paths.join(' > ')}` : ''}`}
                </span>
              )
              : ''}
          >
            <div data-tooltip className={classes.uploadTitle}>
              {`Media Library ${folderSelected?.paths.length > 0
                ? `> ${folderSelected?.paths.join(' > ')}`
                : ''}`}
            </div>
          </TextBlockToolTip>
        </div>
        <div style={{ minWidth: 212 }}>
          <Button
            label="Change Upload Location"
            onClick={() => setShowFolderLocation(true)}
            disabled={uploading !== 0}
            rounded
          />
        </div>

      </div>
      <div className={classes.uploadContent}>
        {uploading === 0 ? (
          <>
            {!addFileError ? (
              <>
                <div className={classes.subTitle}>Drop files here to upload</div>
                <div className={classes.smallText}>or</div>
                <label
                  className={classes.button}
                  htmlFor="file-upload"
                >
                  {`Select ${uploadMultiFile ? 'Files' : 'File'}`}
                  <input
                    className={classes.input}
                    id="file-upload"
                    type="file"
                    name="myFile"
                    accept={acceptTypes}
                    multiple={uploadMultiFile}
                    onChange={(e) => onFileLoad(e.target.files)}
                  />
                </label>
                <div className={classes.smallText}>Maximum size: 10 MB</div>
              </>
            ) : (
              <div className={classes.errorOverlay}>
                <img className={classes.errorImageIcon} alt="warning" src="/icons/warning.svg" />
                <h3 className={classes.errorTitle}>ERROR</h3>
                <p className={classes.errorDescription}>{addFileError}</p>
                <Button
                  variant="contained"
                  rounded
                  className={classes.errorOkButton}
                  onClick={() => setAddFileError('')}
                >
                  Ok
                </Button>
              </div>
            )}
          </>
        ) : (
          <>
            <div className={classes.subTitle}>
              {uploading === 1 ? 'Uploading 1 File' : `Uploading ${uploading} Files`}
            </div>
            <div className={classes.loading}>
              <Loading3Dot />
            </div>
            {/* TODO: Migrate API feature */}
            {/* <Button
              className={classes.cancelButton}
              variant="contained"
              color="secondary"
              rounded
              label="Cancel"
            /> */}
          </>
        )}
      </div>
      <FolderLocationDialog
        show={showFolderLocation}
        setShow={setShowFolderLocation}
        folderSelected={folderSelected}
        setFolderSelected={setFolderSelected}
      />
    </Card>
  )
}
UploadFile.propTypes = {
  classes: PropTypes.shape().isRequired,
  noUploadVideo: PropTypes.bool,
  uploadMultiFile: PropTypes.bool,
  acceptTypes: PropTypes.string,
  folderSelectedId: PropTypes.string,
  didFinishUploadFiles: PropTypes.func,
}

UploadFile.defaultProps = {
  noUploadVideo: false,
  uploadMultiFile: true,
  acceptTypes: '.jpg,.png,.jpeg,.mp4,.mov',
  didFinishUploadFiles: () => { },
  folderSelectedId: null,
}

export default withStyles(style)(UploadFile)
