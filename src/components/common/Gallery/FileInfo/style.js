const style = (theme) => ({
  container: {
    [theme.breakpoints.up('lg')]: {
      borderTopWidth: 1,
      borderTopStyle: 'solid',
      borderTopColor: '#EAEAEA',
    },
  },
  galleryInfoLabel: {
    fontWeight: 500,
    fontSize: 14,
    color: '#3B4757',
    marginTop: 12,
  },
  galleryInfoValue: {
    fontSize: 14,
    color: '#3B4757',
    marginTop: 8,
  },

  shortText: {
    display: 'inline-block',
    fontSize: 14,
    color: '#3B4757',
    marginTop: 8,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  itemContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  infoContainer: {
    [theme.breakpoints.down('md')]: {
      height: '100%',
      overflowY: 'auto',
    },
    [theme.breakpoints.up('lg')]: {
      borderBottomWidth: 1,
      borderBottomStyle: 'solid',
      borderBottomColor: '#EAEAEA',
      padding: 20,
      flex: 0.75,
      overflow: 'hidden',

    },
  },
  metadataGrid: {
    [theme.breakpoints.down('md')]: {
      height: '100%',
      borderLeftWidth: 1,
      borderLeftStyle: 'solid',
      borderLeftColor: '#EAEAEA',
      paddingLeft: 20,
      paddingTop: 20,
    },
    [theme.breakpoints.up('lg')]: {
      flex: 1,
      overflow: 'hidden',
      // '&.galleryLayout': {
      //   height: 'calc(60vh - 300px)',
      // },
      // height: 'calc(70vh - 320px)',
    },
  },
  metadataContainer: {
    [theme.breakpoints.down('md')]: {
      height: '100%',
      overflowY: 'auto',
      paddingRight: 20,
    },
    [theme.breakpoints.up('lg')]: {
      overflowY: 'auto',
      height: '100%',
      padding: 20,
      boxSizing: 'border-box',
    },
  },
  formLabel: {
    textAlign: 'left',
    fontSize: 14,
    letterSpacing: 0,
    color: theme.color.black,
    opacity: 1,
    position: 'relative',
    zIndex: 2,
    marginBottom: theme.spacing(1),
  },
})

export default style
