/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { withStyles, FormLabel, Grid } from '@material-ui/core'
import { Form, Formik } from 'formik'
import clsx from 'clsx'
import { getGalleryFileInfo } from 'utils/helpers/media'
import { Field } from 'components/Formik'
import InputTag from 'components/common/InputTag'
import mediaApis from 'services/apis/media'
import { showDialog } from 'components/common/Dialog'
import { dialogParams } from 'components/common/Dialog/CustomDialog'
import { useMedia } from 'redux/hooks/media'
import TextInput from 'components/common/TextInput'
import { FILE_INFO_MODE } from 'constants/media'
import style from './style'

const FileInfo = (props) => {
  const {
    classes,
    file,
    dimensions,
    renderSetProductButton,
    renderReplaceFileButton,
    didUpdatedFileMetadata,
    listFileSelected,
    enabledEditMetadataIds,
    mode,
  } = props
  const {
    name,
    fileType,
    uploadedOn,
    fileSize,
    id,
  } = getGalleryFileInfo(file)
  const [fileMetadata, setFileMetadata] = useState({})
  const {
    mediaFile,
    mediaFolder,
    actions,
  } = useMedia()
  useEffect(() => {
    if (listFileSelected) {
      const listFileObj = listFileSelected
        .reduce((obj, _file) => ({
          ...obj,
          [_file.id_select || _file.id]:
            {
              ..._file,
              ...{ title: _file.title || _file.title === '' ? _file.title : _file.name },
            },
        }), {})
      setFileMetadata(listFileObj)
    }
  }, [listFileSelected])

  const onKeyDown = (keyEvent) => {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault()
    }
  }

  const isReadOnlyMetadata = (val) => !enabledEditMetadataIds.includes(val)

  const handleUpdateMetadata = async (values) => {
    const fileIds = Object.keys(values)
    actions.setListFileFolderData({
      files: mediaFile.list,
      folders: mediaFolder.list,
      loading: {
        updateMetadata: true,
      },
    })

    try {
      const metadataFiles = await Promise.all(Array.from(fileIds)
        .map((async (fileId) => mediaApis
          .updateFileMetadata({ ...{ file_id: fileId }, ...values[fileId] }))))

      actions.setListFileFolderData({
        loading: {
          updateMetadata: false,
        },
      })
      didUpdatedFileMetadata(metadataFiles)
    } catch (err) {
      showDialog({
        ...dialogParams.error(),
        ...{ description: `${Object.keys(err)[0]} : ${err[Object.keys(err)[0]]}` },
      })
      actions.setListFileFolderData({
        loading: {
          updateMetadata: false,
        },
      })
    }
  }

  const onSubmitForm = (values) => {
    if (mode === FILE_INFO_MODE.GALLERY_DIALOG) {
      const files = Object.values(values)
        .map((e) => ({ ...e, ...{ id: e.id_select || e._id || e.id } }))
      const newFilesSelected = [...listFileSelected]
      files.forEach((newFile) => {
        const fileIdx = newFilesSelected
          .findIndex((fileSelect) => (fileSelect.id_select || fileSelect.id) === newFile.id)
        newFilesSelected[fileIdx] = {
          ...newFile,
          ...{ id: newFile.id, src: newFile.src || newFile.public_url },
        }
      })

      return didUpdatedFileMetadata(newFilesSelected)
    }
    return handleUpdateMetadata(values)
  }

  const renderInfo = () => (
    <Grid container>
      <Grid item sm={mode === FILE_INFO_MODE.GALLERY_DIALOG ? 12 : 6} md={6}>
        <div className={classes.itemContainer}>
          <span className={classes.galleryInfoLabel}>File Name:</span>
          <span className={classes.shortText}>
            {mode === FILE_INFO_MODE.GALLERY_DIALOG ? 'No data' : (name || 'No data')}
          </span>
        </div>
        <div className={classes.itemContainer}>
          <span className={classes.galleryInfoLabel}>Uploaded On:</span>
          <span className={classes.galleryInfoValue}>{uploadedOn}</span>
        </div>
        <div className={classes.itemContainer}>
          <span className={classes.galleryInfoLabel}>Dimensions:</span>
          <span className={classes.galleryInfoValue}>{dimensions}</span>
        </div>

      </Grid>
      <Grid
        item
        sm={mode === FILE_INFO_MODE.GALLERY_DIALOG ? 12 : 6}
        md={6}
        style={{ paddingLeft: 10 }}
      >
        <div className={classes.itemContainer}>
          <span className={classes.galleryInfoLabel}>File Type:</span>
          <span className={classes.galleryInfoValue}>{fileType}</span>
        </div>
        <div className={classes.itemContainer}>
          <span className={classes.galleryInfoLabel}>File Size:</span>
          <span className={classes.galleryInfoValue}>{fileSize}</span>
        </div>
      </Grid>
      {renderSetProductButton()}
      {renderReplaceFileButton()}
    </Grid>
  )

  const renderMetadataForm = () => (
    <div className={clsx(classes.metadataContainer, 'customized-scrollbar')}>
      <Formik
        initialValues={fileMetadata}
        enableReinitialize
        onSubmit={onSubmitForm}
      >
        {({ values, setFieldValue }) => (
          <Form id="editFileMetadata" onKeyDown={onKeyDown}>
            <Field.Input
              name={`${id}.alternative_text`}
              legend="Alternative Text"
              inputProps={{
                disabled: isReadOnlyMetadata('alternative_text'),
              }}
            />
            <br />
            {FILE_INFO_MODE.MEDIA_LIBRARY ? (
              <TextInput
                name={`${id}.title`}
                legend="Title"
                value={values[id]?.title}
                onChange={(e) => {
                  const textVal = e.target.value
                  if (textVal === '') {
                    return setFieldValue(`${id}.title`, fileMetadata[id]?.title)
                  }
                  return setFieldValue(`${id}.title`, textVal)
                }}
                inputProps={{
                  disabled: isReadOnlyMetadata('title'),
                }}
              />
            ) : (
              <Field.Input
                name={`${id}.title`}
                legend="Title"
                inputProps={{
                  disabled: isReadOnlyMetadata('title'),
                }}
              />
            )}
            <br />
            <Field.Input
              name={`${id}.caption`}
              legend="Caption"
              multiline
              rows={4}
              inputProps={{
                disabled: isReadOnlyMetadata('caption'),
              }}
            />
            <br />
            <Field.Input
              name={`${id}.description`}
              legend="Description"
              multiline
              rows={4}
              inputProps={{
                disabled: isReadOnlyMetadata('description'),
              }}
            />
            <br />
            <TextInput
              name={`${id}.copy_link`}
              legend="Copy link"
              value={file.src}
              inputProps={{
                disabled: true,
              }}
            />
            <br />
            <FormLabel
              component="legend"
              className={classes.formLabel}
            >
              Tags
            </FormLabel>
            <InputTag
              tags={values[id]?.tags || []}
              onTagsChange={(tags) => {
                setFieldValue(`${id}.tags`, Array.from(new Set(tags)))
              }}
              disabledInput={isReadOnlyMetadata('tags')}
              disabledTag={isReadOnlyMetadata('tags')}
            />
          </Form>
        )}
      </Formik>
    </div>
  )

  return (
    <>
      <Grid
        item
        lg={12}
        sm={mode === FILE_INFO_MODE.GALLERY_DIALOG ? 5 : 6}
        md={mode === FILE_INFO_MODE.GALLERY_DIALOG ? 5 : 6}
        className={clsx(classes.infoContainer, 'customized-scrollbar')}
      >
        {renderInfo()}
      </Grid>
      <Grid
        item
        lg={12}
        sm={mode === FILE_INFO_MODE.GALLERY_DIALOG ? 7 : 6}
        md={mode === FILE_INFO_MODE.GALLERY_DIALOG ? 7 : 6}
        className={clsx(
          classes.metadataGrid, mode === FILE_INFO_MODE.GALLERY_DIALOG && 'galleryLayout',
        )}
      >
        {renderMetadataForm()}
      </Grid>
    </>
  )
}

FileInfo.defaultProps = {
  renderSetProductButton: () => { },
  renderReplaceFileButton: () => { },
  didUpdatedFileMetadata: () => { },
  enabledEditMetadataIds: [],
}

FileInfo.propTypes = {
  classes: PropTypes.shape().isRequired,
  file: PropTypes.oneOfType([PropTypes.object, PropTypes.shape()]).isRequired,
  listFileSelected: PropTypes.oneOfType([PropTypes.array, PropTypes.shape()]).isRequired,
  dimensions: PropTypes.string.isRequired,
  renderSetProductButton: PropTypes.func,
  didUpdatedFileMetadata: PropTypes.func,
  enabledEditMetadataIds: PropTypes.oneOfType([PropTypes.array, PropTypes.shape()]),
  renderReplaceFileButton: PropTypes.func,
  mode: PropTypes.string.isRequired,
}

export default withStyles(style)(FileInfo)
