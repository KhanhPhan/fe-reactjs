import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'
import Tree from 'components/common/Tree'
import { MEDIA_FOLDER_STATIC } from 'constants/media'
import { TextBlockToolTip } from 'components/common/TextBlock'

const styles = () => ({
  title: {
    fontSize: 18,
  },
  expandIcon: {
    marginRight: 6,
    transition: 'transform linear 0.1s',
    transform: 'rotate(0deg)',
    '&.isOpen': {
      transform: 'rotate(90deg)',
    },
  },
  folderContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8,
    '&.selected': {
      background: '#B6BFD7',
    },
  },
  folderItem: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    maxWidth: '95%',
  },
  folderName: {
    fontSize: 14,
    marginLeft: 12,
    color: '#2E2E2E',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  folderTotal: {
    display: 'block',
    padding: 4,
    width: 18,
    textAlign: 'center',
    background: '#EDF1FB',
    color: '#B5BFDA',
    fontSize: 12,
    borderRadius: 4,
    marginLeft: 12,
  },
  divider: {
    background: '#EAEAEA',
    height: 1,
    width: '100%',
    paddingLeft: '40px',
    marginLeft: '-20px',
    marginTop: 20,
    marginBottom: 20,
  },
})

const FolderLocation = ({
  classes,
  folders,
  onFolderSelected,
  mFolderSelected,
}) => {
  const [folderSelected, setFolderSelected] = useState(mFolderSelected || {
    id: MEDIA_FOLDER_STATIC.UNSORTED, name: 'Media Library', paths: [], openIds: [],
  })

  const staticFolders = [
    { id: MEDIA_FOLDER_STATIC.UNSORTED, name: 'Media Library', paths: [] },
  ]

  const handleFolderSelected = (f) => {
    setFolderSelected(f)
    onFolderSelected(f)
  }

  const renderFolder = (f, dynamicFolder, isOpen = false, onToggle = () => { }) => (
    <div
      key={f.id}
      style={{ paddingLeft: `${(f.level) * 24}px` }}
      className={clsx(classes.folderContainer,
        f.id === folderSelected.id && 'selected')}
    >
      <div
        className={classes.folderItem}
        onClick={() => handleFolderSelected(f)}
        role="presentation"
      >
        {f.droppable && dynamicFolder ? (
          <img
            className={clsx(classes.expandIcon, isOpen && 'isOpen')}
            src="/icons/arrow_right_folder.svg"
            alt="arrow_right_folder"
            onClick={(e) => {
              onToggle()
              e.stopPropagation()
            }}
            role="presentation"
          />
        ) : <div style={{ width: 3, marginRight: 6 }} />}
        <img
          src={`/icons/folder_item${f.id === folderSelected.id ? '_selected' : '_location'}.svg`}
          alt="folder_item"
        />

        <TextBlockToolTip
          title={(
            <span>
              {f.name}
            </span>
          )}
        >
          <div
            data-tooltip
            className={classes.folderName}
          >
            {f.name}
          </div>
        </TextBlockToolTip>
      </div>
    </div>
  )

  return (
    <>
      {staticFolders.map((f) => renderFolder(f, false))}
      <div>
        <Tree
          folders={folders}
          render={(node, { isOpen, onToggle }) => renderFolder(node, true, isOpen, onToggle)}
          initialOpenIds={folderSelected.openIds}
        />
      </div>
    </>
  )
}

FolderLocation.defaultProps = {
  mFolderSelected: null,
}
FolderLocation.propTypes = {
  classes: PropTypes.shape().isRequired,
  onFolderSelected: PropTypes.func.isRequired,
  folders: PropTypes.oneOfType([PropTypes.array, PropTypes.shape]).isRequired,
  mFolderSelected: PropTypes.oneOfType([PropTypes.object, PropTypes.shape]),
}

export default withStyles(styles)(FolderLocation)
