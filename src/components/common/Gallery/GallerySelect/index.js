import React, {
  useState,
  useCallback,
  useMemo,
  useEffect,
} from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import Files from 'pages/NewMediaLibrary/Files'
import Folders from 'pages/NewMediaLibrary/Folders'
import { useMedia } from 'redux/hooks/media'
import {
  MEDIA_FILE_TYPE,
  MEDIA_FOLDER_STATIC,
  PARENT_MODE,
  MEDIA_FILE_MODE,
  INITIAL_FILTERS_DATA,
} from 'constants/media'
import style from './style'
import { TextBlockToolTip } from '../../TextBlock'

const GallerySelect = ({
  classes,
  fileSelected,
  setFileSelected,
  multiFileSelect,
  showTypes,
  selectTypes,
  folderSelectedId,
  setFolderSelectedId,
}) => {
  const {
    mediaFolder,
  } = useMedia()

  const [filters, setFilters] = useState(INITIAL_FILTERS_DATA)
  const [fileMode, setFileMode] = useState(MEDIA_FILE_MODE.GRID_VIEW)

  const showVideo = showTypes.some((e) => e === MEDIA_FILE_TYPE.VIDEO)
  const showImage = showTypes.some((e) => e === MEDIA_FILE_TYPE.IMAGE)

  useEffect(() => {
    if (showVideo) {
      setFilters({ ...filters, ...{ content_type: 'video' } })
    }
    if (showImage) {
      setFilters({ ...filters, ...{ content_type: 'image' } })
    }
  }, [])

  const folderSelected = useMemo(() => {
    const newFolder = mediaFolder.list.find((f) => f.id === folderSelectedId)
    if (folderSelectedId === MEDIA_FOLDER_STATIC.ALL_FILE) {
      return {
        id: MEDIA_FOLDER_STATIC.ALL_FILE,
        name: MEDIA_FOLDER_STATIC.ALL_FILE,
        paths: ['All Files'],
      }
    }
    if (folderSelectedId === MEDIA_FOLDER_STATIC.UNSORTED) {
      return {
        id: MEDIA_FOLDER_STATIC.UNSORTED,
        name: MEDIA_FOLDER_STATIC.UNSORTED,
        paths: ['Unsorted Files'],
      }
    }
    return newFolder
  }, [mediaFolder, folderSelectedId])

  const handleSelected = useCallback((folder) => setFolderSelectedId(folder.id), [])
  return (
    <div className={classes.container}>
      <div className={classes.selectedLabel}>
        {`Choose or Upload File(s) | ${
          fileSelected.filter((f) => Object.keys(f).length !== 0).length} Selected`}
      </div>
      <div className={classes.header}>
        <div className={classes.label}>
          <span className="title">
            <TextBlockToolTip maxLine={1}>
              {`Media Library ${
                folderSelected?.paths.length > 0 ? `> ${folderSelected.paths.join(' > ')}` : ''}`}
            </TextBlockToolTip>
          </span>
        </div>
      </div>

      <Grid container spacing={2} style={{ flex: 1, overflow: 'hidden' }}>
        <Grid item lg={2} md={4} sm={4} xs={4} style={{ height: '100%' }}>
          <Folders
            folderSelectedId={folderSelectedId}
            onSelectedFolder={handleSelected}
            parentMode={[PARENT_MODE.GALLERY]}
          />
        </Grid>
        <Grid item lg={10} md={8} sm={8} xs={8} className={classes.fileInfo}>
          <Files
            folderSelectedId={folderSelectedId}
            onSelectedFolder={handleSelected}
            fileSelected={fileSelected}
            setFileSelected={setFileSelected}
            multiFileSelect={multiFileSelect}
            show={showTypes}
            select={selectTypes}
            parentMode={[PARENT_MODE.GALLERY]}
            filters={filters}
            setFilters={setFilters}
            fileMode={fileMode}
            setFileMode={setFileMode}
          />
        </Grid>
      </Grid>
    </div>
  )
}

GallerySelect.defaultProps = {
  multiFileSelect: true,
  showTypes: [MEDIA_FILE_TYPE.All],
  selectTypes: [MEDIA_FILE_TYPE.IMAGE],
  folderSelectedId: '',
  setFolderSelectedId: () => { },
}

GallerySelect.propTypes = {
  classes: PropTypes.shape().isRequired,
  fileSelected: PropTypes.oneOfType([PropTypes.array, PropTypes.shape()]).isRequired,
  setFileSelected: PropTypes.func.isRequired,
  multiFileSelect: PropTypes.bool,
  showTypes: PropTypes.arrayOf(PropTypes.string),
  selectTypes: PropTypes.arrayOf(PropTypes.string),
  folderSelectedId: PropTypes.string,
  setFolderSelectedId: PropTypes.func,
}

export default withStyles(style)(GallerySelect)
