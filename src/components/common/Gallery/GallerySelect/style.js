import { vh } from 'theme/common'

const style = () => ({
  container: {
    padding: '8px 24px',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  containerLoading: {
    height: 'calc(95vh - 200px)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerLoadMore: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 5,
  },
  selectedLabel: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    background: '#EDF1FB',
    color: '#3B4757',
    fontSize: 14,
    fontWeight: 500,
  },
  header: {
    marginBottom: 20,
  },
  label: {
    height: 35,
    paddingLeft: 20,
    marginTop: 20,
    borderRadius: 3,
    display: 'flex',
    alignItems: 'center',
    border: '1px solid #B4BFD9',
    flex: 1,
    '& .title': {
      marginLeft: 10,
      fontSize: 14,
    },
  },
  fileInfo: {
    height: '100%',
    [vh.down(500)]: {
      overflowX: 'hidden',
      overflowY: 'auto',
      '&::-webkit-scrollbar': {
        width: 5,
        height: 5,
      },
      '&::-webkit-scrollbar-track': {
        borderRadius: 10,
        backgroundColor: 'transparent',
      },
      '&::-webkit-scrollbar-thumb': {
        borderRadius: 10,
        backgroundColor: '#b4bfd9',
      },
    },
  },
})

export default style
