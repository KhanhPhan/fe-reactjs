/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Grid, withStyles } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Close'
import clsx from 'clsx'
import ReactPlayer from 'react-player'
import Button from 'components/common/Button'
import useDimensions from 'utils/hooks/useDimensions'
import FileInfo from 'components/common/Gallery/FileInfo'
import { isFileImage } from 'utils/helpers/media'
import VideoPlayer from 'components/common/Gallery/VideoPlayer'
import { useMedia } from 'redux/hooks/media'
import { FILE_INFO_MODE } from 'constants/media'
import style from './style'

const GalleryPreview = (props) => {
  const {
    classes,
    multiFileSelect,
    fileSelected,
    gallerySelectFile,
    setGallerySelectFile,
    setFileSelected,
    addMoreFile,
    isProductMode,
    removeMedia,
    didUpdatedFileMetadata,
    mode,
    enabledEditMetadataIds,
  } = props
  const filePreview = fileSelected[gallerySelectFile] || {}
  const dimensions = useDimensions(filePreview)
  const [videoDimensions, setVideoDimensions] = useState('')
  const { actions } = useMedia()
  useEffect(() => {
    if (multiFileSelect && fileSelected.length === 0) {
      setGallerySelectFile(0)
    }
  }, [fileSelected, multiFileSelect])

  const onReadyPlayVideo = () => {
    if (filePreview
      && filePreview.src
      && !isFileImage(filePreview)
      && document.getElementById('video-file').firstChild) {
      const currentVideo = document.getElementById('video-file').firstChild
      setVideoDimensions(`${currentVideo.videoWidth} x ${currentVideo.videoHeight} pixels`)
    }
  }

  const handleSetProductImage = () => {
    if (gallerySelectFile === -1) return
    const newFileSelected = [filePreview]
      .concat([...fileSelected.slice(0, gallerySelectFile), ...fileSelected
        .slice(gallerySelectFile + 1, fileSelected.length)])
    setFileSelected(newFileSelected)
    setGallerySelectFile(0)
  }

  const handleRemoveFile = (file, event) => {
    event.stopPropagation()
    const newFileSelected = fileSelected.filter((item) => item.src !== file.src)
    setGallerySelectFile(newFileSelected.length === 0 ? -1 : 0)
    setFileSelected(newFileSelected)
    if (mode === FILE_INFO_MODE.MEDIA_LIBRARY) {
      actions.deleteFile({
        fileId: file._id,
      })
    }
  }

  const renderGallerySelect = () => (
    <div className={clsx(classes.galleryLeftContent, 'customized-scrollbar')}>
      {fileSelected.map((file, index) => (
        <div
          key={file.src}
          role="presentation"
          className={clsx(
            classes.galleryFileSelectContainer, gallerySelectFile === index && 'selected',
          )}
          onClick={() => multiFileSelect && setGallerySelectFile(index)}
        >
          {isFileImage(file) ? (
            <img
              className={classes.galleryFileSelect}
              role="presentation"
              alt={`file-${index}`}
              src={file.src || '/icons/no_image.svg'}
            />
          ) : (
            <div
              className={classes.videoContainer}
              role="presentation"
            >
              <ReactPlayer
                className={classes.video}
                url={file.src}
                playing={false}
              />
            </div>
          )}
          {multiFileSelect && (
            <div
              className={classes.removeIconContainer}
              role="presentation"
              onClick={(e) => handleRemoveFile(file, e)}
            >
              <RemoveIcon className={classes.removeIcon} />
            </div>
          )}
          {(isProductMode && index === 0) && <div className={classes.mainFile}>P</div>}
        </div>
      ))}
      {
        multiFileSelect
        && (
          <div
            role="presentation"
            className={classes.galleryAddIconContainer}
            onClick={addMoreFile}
          >
            <AddIcon className={classes.addIcon} fontSize="large" />
          </div>
        )
      }

    </div>
  )
  const renderGalleryContent = () => (
    <div className={classes.galleryPreviewFileContainer}>
      {gallerySelectFile !== -1 && (
        <>
          {isFileImage(filePreview) ? (
            <img
              id="main-image"
              width={266}
              height={200}
              className={classes.galleryPreviewFile}
              src={filePreview?.src || '/icons/no_image.svg'}
              alt={filePreview.name}
            />
          ) : (
            <VideoPlayer
              url={filePreview.src}
              onReadyPlayVideo={onReadyPlayVideo}
            />
          )}
        </>
      )}
    </div>
  )

  const dimensionsVal = filePreview.src ? (isFileImage(filePreview)
    ? dimensions
    : videoDimensions) : 'No data'

  const renderGalleryInfo = () => (
    <FileInfo
      dimensions={dimensionsVal}
      file={filePreview}
      enabledEditMetadataIds={enabledEditMetadataIds}
      mode={mode}
      renderSetProductButton={() => {
        if (isProductMode) {
          return (
            <Button
              className={classes.setProductButton}
              label="Set Product Image"
              color="secondary"
              fullWidth={false}
              onClick={handleSetProductImage}
            />
          )
        }
        if (removeMedia) {
          return (
            <Button
              className={classes.setProductButton}
              label="Remove"
              color="secondary"
              fullWidth={false}
              // onClick={handleRemoveMedia}
              onClick={(e) => handleRemoveFile(filePreview, e)}
            />
          )
        }
        return null
      }}
      renderReplaceFileButton={() => !multiFileSelect && (
        <Button
          className={classes.setProductButton}
          label="Replace Image"
          color="secondary"
          fullWidth={false}
          onClick={addMoreFile}
        />
      )}
      listFileSelected={fileSelected
        .map((file) => ({
          ...file,
          ...{
            id: file._id
          || file.id,
            alternative_text: file.alt
          || file.alternative_text,
          },
        }))}
      didUpdatedFileMetadata={didUpdatedFileMetadata}
    />
  )

  return (
    <Grid container style={{ height: '100%' }}>
      <Grid item sm={3} md={3} lg={2} style={{ height: '100%' }}>
        {renderGallerySelect()}
      </Grid>
      <Grid
        item
        sm={9}
        md={9}
        lg={10}
        className={classes.galleryContentContainerGrid}
      >
        <Grid container style={{ height: '100%' }}>
          <Grid
            item
            lg={9}
            sm={12}
            className={classes.galleryContentGrid}
          >
            {renderGalleryContent()}
          </Grid>
          <Grid
            item
            lg={3}
            sm={12}
            className={classes.galleryInfoGrid}
          >
            <Grid
              container
              className={classes.galleryInfoContainerGrid}
            >
              {renderGalleryInfo()}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

GalleryPreview.defaultProps = {
  multiFileSelect: true,
  isProductMode: true,
  removeMedia: true,
  didUpdatedFileMetadata: () => { },
  enabledEditMetadataIds: [],
}

GalleryPreview.propTypes = {
  classes: PropTypes.shape().isRequired,
  fileSelected: PropTypes.oneOfType([PropTypes.array, PropTypes.shape()]).isRequired,
  multiFileSelect: PropTypes.bool,
  gallerySelectFile: PropTypes.number.isRequired,
  setGallerySelectFile: PropTypes.func.isRequired,
  setFileSelected: PropTypes.func.isRequired,
  addMoreFile: PropTypes.func.isRequired,
  isProductMode: PropTypes.bool,
  removeMedia: PropTypes.bool,
  didUpdatedFileMetadata: PropTypes.func,
  mode: PropTypes.string.isRequired,
  enabledEditMetadataIds: PropTypes.oneOfType([PropTypes.array, PropTypes.shape]),
}

export default withStyles(style)(GalleryPreview)
