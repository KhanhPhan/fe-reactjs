const style = (theme) => ({
  container: {
    display: 'flex',
    flex: 1,
  },
  content: {
    display: 'flex',
    flex: 1,
    height: 'calc(95vh - 165px)',

    borderTopWidth: 1,
    borderTopStyle: 'solid',
    borderTopColor: '#EAEAEA',
  },
  galleryLeftContent: {
    padding: 10,
    overflowY: 'auto',
    height: '100%',
    background: '#F8F9FF',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 20,
    borderRightWidth: 1,
    borderRightStyle: 'solid',
    borderRightColor: '#EAEAEA',
    boxSizing: 'border-box',
    overflowX: 'hidden',
  },
  galleryFileSelectContainer: {
    border: '1px solid #B4BFD9',
    borderRadius: 3,
    marginTop: 16,
    cursor: 'pointer',
    width: 120,
    height: '0',
    paddingTop: 120,
    position: 'relative',
    '&.selected': {
      borderRadius: 3,
      border: `2px solid ${theme.color.primaryBlue}`,
    },
  },
  galleryFileSelect: {
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    position: 'absolute',
    verticalAlign: 'top',
    objectFit: 'cover',
  },
  mainFile: {
    fontSize: 14,
    width: 18,
    height: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 5,
    left: 5,
    background: theme.color.primaryBlue,
    color: 'white',
    borderRadius: 3,
  },
  galleryAddIconContainer: {
    marginTop: 20,
    borderRadius: '50%',
    padding: 10,
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#B4BFD9',
  },
  addIcon: {
    color: 'white',
  },
  galleryMainContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 450,
    paddingLeft: 20,
    paddingRight: 20,
    [theme.breakpoints.up('md')]: {
      height: '100%',
    },
    [theme.breakpoints.only('md')]: {
      height: 500,
    },
  },
  removeIconContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 5,
    right: 5,
    borderRadius: '50%',
    width: 25,
    height: 25,
    background: '#fff',
    boxShadow: '0px 2px 6px #2933C51A',
  },
  removeIcon: {
    color: '#414141',
    fontSize: 18,
    fontWeight: 500,
  },
  galleryContentContainerGrid: {
    [theme.breakpoints.down('md')]: {
      height: '100%',
    },
    [theme.breakpoints.up('lg')]: {
      height: '100%',
    },
  },
  galleryContentGrid: {
    [theme.breakpoints.down('md')]: {
      height: '50%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    [theme.breakpoints.up('lg')]: {
      height: '100%',
    },
  },

  galleryInfoGrid: {
    [theme.breakpoints.down('md')]: {
      height: '50%',
    },
    [theme.breakpoints.up('lg')]: {
      height: '100%',
    },
  },
  galleryInfoContainerGrid: {
    [theme.breakpoints.down('md')]: {
      height: '100%',
      borderTopWidth: 1,
      borderTopStyle: 'solid',
      borderTopColor: '#EAEAEA',
      paddingLeft: 20,
      paddingRight: 20,
    },
    [theme.breakpoints.up('lg')]: {
      height: '100%',
      borderLeftWidth: 1,
      borderLeftStyle: 'solid',
      borderLeftColor: '#EAEAEA',
      flexDirection: 'column',
    },
  },
  galleryPreviewFileContainer: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  galleryPreviewFile: {
    maxWidth: '100%',
    height: 'auto',
    objectFit: 'contain',
    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      height: 200,
    },
    '@media screen and (max-height: 725px)': {
      height: 140,
    },
    '@media screen and (max-height: 600px)': {
      height: 100,
    },
    '@media screen and (max-height: 500px)': {
      height: 70,
    },
  },

  videoContainer: {
    position: 'absolute',
    borderRadius: 3,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  video: {
    width: '100% !important',
    height: '100% !important',
    objectFit: 'contain',
  },
  galleryInfoContainer: {
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      justifyContent: 'center',
    },
    [theme.breakpoints.up('md')]: {
      borderLeftWidth: 1,
      borderLeftStyle: 'solid',
      borderLeftColor: '#EAEAEA',
    },
  },

  galleryInfoLabel: {
    fontWeight: 500,
    fontSize: 14,
    color: '#3B4757',
    marginTop: 20,
  },
  galleryInfoValue: {
    fontSize: 14,
    color: '#3B4757',
    marginTop: 10,
  },
  setProductButton: {
    marginTop: 20,
    marginBottom: 20,
  },

})

export default style
