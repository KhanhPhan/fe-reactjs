const style = () => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
  },
  galleryContainer: {
    height: '100%',
  },
  fileDetailContainer: {
    overflowY: 'hidden',
  },
  mainFile: {
    fontSize: 14,
    width: 18,
    height: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 5,
    left: 5,
    background: theme.color.primaryBlue,
    color: 'white',
    borderRadius: 3,
  },
  gallerySelectIcon: {

  },
  selectFileButton: {
    width: 150,
    marginTop: 20,
  },
  footer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
  },
})

export default style
