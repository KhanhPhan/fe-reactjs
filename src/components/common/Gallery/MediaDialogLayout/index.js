import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import MuiDialogActions from '@material-ui/core/DialogActions'
import MuiDialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import PropTypes from 'prop-types'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import CloseIcon from '@material-ui/icons/Close'

const titleStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1, 2),
    borderBottom: '1px solid #EAEAEA',
  },
  closeButton: {
    position: 'absolute',
    top: 18,
    right: 14,
    color: theme.color.darkGray,
  },

  header: {
    display: 'flex',
    alignItems: 'center',
    minHeight: 44,
  },
  titleText: {
    fontSize: 16,
    color: theme.color.primaryBlue,
    marginRight: theme.spacing(3),
    fontWeight: 500,
  },
}))

const useStyle = makeStyles(() => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  paperScrollPaper: {
    height: 'calc(100% - 150px)',
  },
  paperWidthXl: {
    maxWidth: '1600px',
  },
}))

const DialogContent = withStyles(() => ({
  root: {
    overflowY: 'hidden',
    borderTopWidth: 0,
    padding: 0,
    height: '100%',
    '&.showBorderTop': {
      borderTopWidth: 1,
    },
  },
}))(MuiDialogContent)

const DialogActions = withStyles(() => ({
  root: {
    margin: 0,
    justifyContent: 'unset',
    minHeight: 61,
    padding: '8xp 16px',
    borderTop: '1px solid #EAEAEA',
  },
}))(MuiDialogActions)

const MediaDialogLayout = ({
  open,
  maxWidth,
  onClose,
  title,
  children,
  disableXClose,
  disableBackdropClick,
  renderFooter,
}) => {
  const titleClasses = titleStyles()
  const dialogClasses = useStyle()

  return (
    <Dialog
      classes={{
        root: dialogClasses.root,
        paperScrollPaper: dialogClasses.paperScrollPaper,
        paperWidthXl: dialogClasses.paperWidthXl,
      }}
      fullWidth
      maxWidth={maxWidth}
      open={open}
      onClose={onClose}
      disableBackdropClick={disableBackdropClick}
    >
      <DialogTitle
        disableTypography
        className={`${titleClasses.root} ${titleClasses.header}`}
      >
        <Typography variant="h6" className={titleClasses.titleText}>{title}</Typography>
        {onClose && !disableXClose ? (
          <IconButton
            aria-label="close"
            size="small"
            className={titleClasses.closeButton}
            onClick={onClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        ) : ''}
      </DialogTitle>
      <DialogContent>
        {children}
      </DialogContent>
      {renderFooter() && (
        <DialogActions>
          {renderFooter()}
        </DialogActions>
      )}
    </Dialog>
  )
}

MediaDialogLayout.defaultProps = {
  onClose: () => { },
  renderFooter: () => { },
  title: null,
  children: '',
  disableXClose: false,
  disableBackdropClick: true,
  maxWidth: 'lg',
}

MediaDialogLayout.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  maxWidth: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  renderFooter: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node,
  ]),
  disableXClose: PropTypes.bool,
  disableBackdropClick: PropTypes.bool,
}

export default MediaDialogLayout
