import React, { useState, useRef } from 'react'
import PropTypes from 'prop-types'
import {
  withStyles,
} from '@material-ui/core'
import ReactPlayer from 'react-player'
import style from './style'

const VideoPlayer = (props) => {
  const { classes, url, onReadyPlayVideo } = props
  const playerRef = useRef(null)
  const [played, setPlayed] = useState(0)
  const [playing, setPlaying] = useState(false)

  const handleSeekChange = (e) => {
    e.stopPropagation()
    setPlayed(parseFloat(e.target.value))
  }

  const handleSeekMouseUp = (e) => {
    e.stopPropagation()
    playerRef.current.seekTo(parseFloat(e.target.value))
  }
  return (
    <div
      className={classes.container}

    >
      <div
        role="presentation"
        className={classes.videoContainer}
        onClick={() => setPlaying(!playing)}
      >
        <ReactPlayer
          id="video-file"
          className={classes.video}
          ref={playerRef}
          url={url}
          playing={playing}
          onProgress={(value) => setPlayed(value.played)}
          onReady={onReadyPlayVideo}
        />
      </div>
      {!playing && (
      <img
        className={classes.videoImage}
        role="presentation"
        alt="img-mediaFolder"
        src="/img/play-video.svg"
        onClick={() => setPlaying(!playing)}
      />
      )}
      <div className={classes.sliderContainer}>
        <input
          className={classes.slider}
          type="range"
          min={0}
          max={0.999999}
          step="any"
          value={played}
          onChange={handleSeekChange}
          onMouseUp={handleSeekMouseUp}
        />
      </div>

    </div>
  )
}

VideoPlayer.propTypes = {
  classes: PropTypes.shape().isRequired,
  url: PropTypes.string.isRequired,
  onReadyPlayVideo: PropTypes.func.isRequired,
}

export default withStyles(style)(VideoPlayer)
