const style = (theme) => ({
  video: {
    width: '100% !important',
    height: '100% !important',
    [theme.breakpoints.down(720)]: {
      width: '250px !important',
    },
    [theme.breakpoints.down(540)]: {
      width: '150px !important',
    },
  },
  container: {
    height: '100% !important',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  videoContainer: {
    width: '100% !important',
    height: '94% !important',
    cursor: 'pointer',
  },
  sliderContainer: {
    width: '80%',
    padding: '12px 18px',
    background: '#2E2E2E',
    borderRadius: 20,
    display: 'flex',
    alignItems: 'center',
    position: 'absolute',
    opacity: 0.8,
    bottom: 20,

  },
  slider: {
    width: '100%',
    height: 2,
    cursor: 'pointer',
    WebkitAppearance: 'none',
    '&::-webkit-slider-runnable-track': {
      height: 2,
      backgroundColor: 'white',
    },
    '&::-webkit-slider-thumb': {
      WebkitAppearance: 'none',
      width: 20,
      height: 20,
      borderRadius: 10,
      border: '1px solid #AAAAAA',
      background: 'white',
      cursor: 'pointer',
      color: 'white !important',
      marginTop: '-9px',
    },
  },
  videoImage: {
    height: '20%',
    position: 'absolute',
    cursor: 'pointer',
    zIndex: 2,
  },
})

export default style
