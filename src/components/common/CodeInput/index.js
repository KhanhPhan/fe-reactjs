import React from 'react'
import PropTypes from 'prop-types'
import ReactCodeInput from 'react-code-input'
import { FormHelperText, withStyles } from '@material-ui/core'
import style from './style'

const CodeInput = ({
  fields,
  value, onChange,
  error, helperText,
}) => (
  <>
    <ReactCodeInput
      type="text"
      value={value}
      onChange={onChange}
      fields={fields}
    />
    {error && <FormHelperText error>{helperText}</FormHelperText>}
  </>
)

CodeInput.defaultProps = {
  fields: 6,
  error: false,
  helperText: '',
  value: '',
}

CodeInput.propTypes = {
  fields: PropTypes.number,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  error: PropTypes.bool,
  helperText: PropTypes.string,
}

export default withStyles(style)(CodeInput)
