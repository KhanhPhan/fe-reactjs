const style = () => ({
  switchContainer: {
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 21,
    display: 'inline-flex',
  },
  tab: {
    fontSize: 14,
    color: theme.color.primaryBlue,
    padding: '8px 13px',
    cursor: 'pointer',
    '&:focus': {
      outline: 'unset',
    },
    '&.active': {
      color: theme.color.white,
      backgroundColor: theme.color.primaryBlue,
      borderRadius: 21,
    },
  },
})

export default style
