/* eslint-disable react/no-array-index-key */
import React, {
  useState, useEffect, useRef, useMemo,
} from 'react'
import PropTypes from 'prop-types'
import {
  withStyles, Grid, Popover, IconButton,
} from '@material-ui/core'
import { KeyboardArrowDown } from '@material-ui/icons'
import ArrowDropUp from '@material-ui/icons/KeyboardArrowUp'
import clsx from 'clsx'
import TextInput from 'components/common/TextInput'
import Button from 'components/common/Button'
import GridItem from 'components/common/GridItem'
import Icon from 'components/common/Icon'
import { isNumeric } from 'utils/utility'
import useOnClickOutside from 'utils/hooks/useOnClickOutside'
import { colors } from 'theme'
import style from './style'

const RangeInput = ({
  classes,
  label,
  placeholder,
  fromValue,
  toValue,
  transformText,
  unit,
  disabled,
  handleChange,
  noBorderRight,
  noBorderLeft,
  onClear,
  stylePopper,
  optionsUnit,
  valueUnit,
  onSelectUnit,
}) => {
  const [minErrMess, setMinErrMess] = useState('')
  const [maxErrMess, setMaxErrMess] = useState('')
  const [showUnit, setAnchorElUnit] = useState(null)
  const [errUnit, setErrUnit] = useState('')
  const valueBoxRef = useRef()
  const valueUnitRef = useRef()
  const refWrapUnit = useRef()
  const clearErrMess = () => {
    setMinErrMess('')
    setMaxErrMess('')
    setErrUnit('')
  }

  const [tempValue, setTempValue] = useState({
    fromVal: fromValue,
    toVal: toValue,
  })

  const [anchorEl, setAnchorEl] = useState(null)

  const handleFocus = (e) => {
    e.preventDefault()
    e.target.blur()

    if (!anchorEl) {
      setAnchorEl(e.target.offsetParent)
    }
  }

  const handleCancel = () => {
    setAnchorEl(null)

    const newTemp = { ...tempValue, fromVal: fromValue, toVal: toValue }
    clearErrMess()
    setTempValue(newTemp)
  }

  const handleApply = () => {
    if (tempValue.fromVal === '' && tempValue.toVal === '') {
      handleChange('', '')
      handleCancel()
      clearErrMess()
    }
    clearErrMess()
    let err = false
    if (+tempValue.fromVal < 0) {
      setMinErrMess('Value must be greater than or equal to 0')
      err = true
    }
    if (+tempValue.toVal < 0) {
      setMaxErrMess('Value must be greater than or equal to 0')
      err = true
    }
    if (isNumeric(tempValue.fromVal)
    && isNumeric(tempValue.toVal)
    && +tempValue.fromVal > +tempValue.toVal) {
      setMaxErrMess('Max value must be greater than or equal to min value')
      err = true
    }
    if (!valueUnit && optionsUnit.length) {
      setErrUnit('Please choose unit')
      err = true
    }
    if (!err && isNumeric(tempValue.fromVal) && !isNumeric(tempValue.toVal)) {
      handleChange(+tempValue.fromVal)
      handleCancel()
      clearErrMess()
    } else if (!err && !isNumeric(tempValue.fromVal) && isNumeric(tempValue.toVal)) {
      handleChange(null, +tempValue.toVal)
      handleCancel()
      clearErrMess()
    } else if (!err && isNumeric(tempValue.fromVal) && isNumeric(tempValue.toVal)) {
      handleChange(+tempValue.fromVal, +tempValue.toVal)
      handleCancel()
      clearErrMess()
    }
  }

  useEffect(() => {
    setTempValue({
      fromVal: fromValue,
      toVal: toValue,
    })
  }, [fromValue, toValue])

  const showValue = useMemo(() => isNumeric(fromValue) || isNumeric(toValue), [fromValue, toValue])

  useOnClickOutside(refWrapUnit, () => setAnchorElUnit(false))

  return (
    <div className={classes.rangeInputContainer}>
      <TextInput
        checkRemoveCursor
        label={label}
        variant="outlined"
        size="small"
        placeholder={placeholder}
        className={clsx(
          classes.rangeDisplayInput, noBorderLeft
          && 'noBorderLeft', noBorderRight && 'noBorderRight',
        )}
        inputRef={valueBoxRef}
        InputProps={{
          onFocus: handleFocus,
          value: showValue ? transformText(fromValue, toValue, unit) : '',
          endAdornment: anchorEl
            ? <ArrowDropUp color="primary" />
            : onClear && (isNumeric(fromValue) || isNumeric(toValue))
              ? (
                <IconButton className={classes.buttonClear} size="small" onClick={onClear}>
                  <Icon
                    name="close"
                    color={colors.primaryBlue}
                    size={10}
                  />
                </IconButton>
              )
              : (
                <IconButton
                  size="small"
                  disabled={disabled}
                  onClick={() => valueBoxRef.current.focus()}
                >
                  <KeyboardArrowDown
                    color={disabled ? 'disabled' : 'primary'}
                  />
                </IconButton>
              ),
        }}
        disabled={disabled}
      />

      <Popover
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleCancel}
        style={{ ...stylePopper }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        classes={{ paper: classes.paper }}
      >
        <GridItem className={classes.popup}>
          <div className={classes.inputContainer}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextInput
                  error={!!minErrMess}
                  helperText={minErrMess}
                  autoFocus
                  endDotDotDot
                  InputProps={{
                    inputProps: {
                      min: 0.0,
                      max: tempValue.toVal > 0 ? tempValue.toVal : null,
                    },
                    type: 'number',
                    value: tempValue.fromVal,
                    onChange: (e) => setTempValue({ ...tempValue, fromVal: e.target.value }),
                  }}
                  leftItem={<div style={{ width: 30 }}>Min</div>}
                />
              </Grid>
              <Grid item xs={12}>
                <TextInput
                  error={!!maxErrMess}
                  helperText={maxErrMess}
                  InputProps={{
                    inputProps: {
                      min: tempValue.fromVal ? tempValue.fromVal : null,
                    },
                    type: 'number',
                    value: tempValue.toVal,
                    min: tempValue.fromVal || 0,
                    onChange: (e) => setTempValue({ ...tempValue, toVal: e.target.value }),
                  }}
                  leftItem={<div style={{ width: 30 }}>Max</div>}
                />
              </Grid>
            </Grid>
            {
              optionsUnit.length
                ? (
                  <div className={classes.wrapUnit} ref={refWrapUnit}>
                    <TextInput
                      label={label}
                      error={!!errUnit}
                      helperText={errUnit}
                      variant="outlined"
                      size="small"
                      endDotDotDot
                      placeholder="select unit"
                      className={clsx(
                        classes.rangeDisplayInput, noBorderLeft
                        && 'noBorderLeft', noBorderRight && 'noBorderRight',
                      )}
                      inputRef={valueUnitRef}
                      InputProps={{
                        onClick: () => setAnchorElUnit(!showUnit),
                        value: (() => {
                          if (!valueUnit) {
                            return 'Unit'
                          }
                          const index = optionsUnit.findIndex((item) => item.value === valueUnit)
                          return optionsUnit[index].label
                        })(),
                        endAdornment: showUnit
                          ? <ArrowDropUp color="primary" />
                          : (
                            <IconButton
                              size="small"
                              disabled={disabled}
                              onClick={() => valueUnitRef.current.focus()}
                            >
                              <KeyboardArrowDown
                                color={disabled ? 'disabled' : 'primary'}
                              />
                            </IconButton>
                          ),
                      }}
                    />
                    {
                showUnit ? (
                  <ul className={classes.wrapUnitList}>
                    {
                      optionsUnit.map((item, index) => (
                        <li
                          role="presentation"
                          key={index}
                          onClick={() => {
                            setAnchorElUnit(!showUnit)
                            onSelectUnit(item)
                          }}
                          style={item.value === valueUnit
                            ? {
                              opacity: 1,
                              background: '#d8e3fd 0% 0% no-repeat padding-box',
                            } : {}}
                        >
                          {item.label}
                        </li>
                      ))
                    }
                  </ul>
                ) : null
              }
                  </div>
                ) : null
            }

          </div>
          <div className={classes.buttonContainer}>
            <Button
              onClick={handleApply}
              disabled={tempValue.fromVal === ''
              && tempValue.toVal === ''
              && fromValue === ''
              && toValue === ''}
            >
              Apply
            </Button>
            <Button
              color="secondary"
              onClick={handleCancel}
            >
              Cancel
            </Button>
          </div>
        </GridItem>
      </Popover>
    </div>
  )
}

RangeInput.defaultProps = {
  label: '',
  placeholder: '',
  fromValue: '',
  toValue: '',
  transformText: (fromValue, toValue, unit) => `${
    isNumeric(fromValue) ? fromValue : '-∞'}-${
    isNumeric(toValue) ? toValue : '∞'} ${unit ? `(${unit})` : ''}`,
  disabled: false,
  handleChange: () => { },
  noBorderRight: false,
  noBorderLeft: false,
  onClear: null,
  unit: null,
  stylePopper: {},
  optionsUnit: [],
  valueUnit: '',
  onSelectUnit: () => null,
}

RangeInput.propTypes = {
  classes: PropTypes.shape().isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  fromValue: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.oneOf([null]),
  ]),
  toValue: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.oneOf([null]),
  ]),
  transformText: PropTypes.func,
  disabled: PropTypes.bool,
  handleChange: PropTypes.func,
  noBorderRight: PropTypes.bool,
  noBorderLeft: PropTypes.bool,
  onClear: PropTypes.func,
  unit: PropTypes.string,
  stylePopper: PropTypes.shape(),
  // eslint-disable-next-line react/forbid-prop-types
  optionsUnit: PropTypes.array,
  valueUnit: PropTypes.string,
  onSelectUnit: PropTypes.func,
}

export default withStyles(style)(RangeInput)
