const style = (theme) => ({
  rangeInputContainer: {
    width: 160,
    '& > div:first-child': {
      paddingTop: 0,
    },
    '& .MuiFormControl-root.MuiTextField-root': {
      '& .MuiInputBase-root.MuiOutlinedInput-root.MuiInputBase-formControl': {
        '& fieldset.MuiOutlinedInput-notchedOutline': {
          borderColor: 'rgba(0, 0, 0, 0.23)',
          borderWidth: '1px',
        },
      },
      '&[aria-describedby]': {
        '& .MuiInputBase-root.MuiOutlinedInput-root.MuiInputBase-formControl': {
          '& fieldset.MuiOutlinedInput-notchedOutline': {
            borderColor: '#2933C5',
            borderWidth: '2px',
          },
        },
      },
    },
    '& input.MuiInputBase-input': {
      paddingRight: 0,
    },
    '& [aria-describedby]': {
      '& .MuiSvgIcon-root': {
        padding: 3,
      },
    },
  },
  rangeDisplayInput: {
    '& input': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    '&.noBorderLeft': {
      '& .MuiOutlinedInput-root': {
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
      },
    },
    '&.noBorderRight': {
      '& .MuiOutlinedInput-root': {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
      },
    },
    '& input:placeholder-shown::placeholder': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  },
  buttonClear: {
    padding: 10,
  },
  popup: {
    marginBottom: theme.spacing(0),
    padding: theme.spacing(0, 2, 2, 2),
    minWidth: 250,
    width: 250,
    fontSize: 14,
  },
  buttonContainer: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    '& button.MuiButtonBase-root': {
      minWidth: 100,
    },
  },
  inputContainer: {
    paddingTop: theme.spacing(2),
    display: 'flex',
  },
  wrapTab: {
    padding: 0,
    minWidth: 100,
  },
  itemTab: {
    listStyle: 'none',
    font: 'normal normal normal 14px/17px Rubik',
    letterSpacing: 0,
    color: '#292929',
    borderRadius: 5,
    height: 32,
    display: 'flex',
    alignItems: 'center',
    padding: '0 5px',
    cursor: 'pointer',
    '&:hover': {
      background: '#EDF1FB 0% 0% no-repeat padding-box',
    },
  },
  wrapUnit: {
    position: 'relative',
    width: 100,
    zIndex: 50,
    marginLeft: 3,
    fontSize: 14,
  },
  wrapUnitList: {
    boxShadow: '0px 2px 10px #d6def2',
    borderRadius: '5px',
    padding: '10px',
    position: 'absolute',
    top: 24,
    maxWidth: 70,
    zIndex: 50,
    width: '100%',
    boxSizing: 'border-box',
    background: '#ffffff',
    '& li': {
      background: '#EDF1FB 0% 0% no-repeat padding-box',
      borderRadius: '5px',
      height: '32px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: '3px',
      cursor: 'pointer',
      opacity: 0.8,
      '&:hover': {
        opacity: 1,
        background: '#d8e3fd 0% 0% no-repeat padding-box',
      },
    },
  },
  paper: {
    boxShadow: ' 0px 2px 10px #d6def2',
    borderRadius: 5,
    marginTop: 10,
  },
})

export default style
