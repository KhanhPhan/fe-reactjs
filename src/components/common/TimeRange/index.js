import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import { Container } from '@material-ui/core'
import { DateRangePicker } from 'react-date-range'
import 'react-date-range/dist/styles.css'
import 'react-date-range/dist/theme/default.css'
import Popover from '@material-ui/core/Popover'
import InputAdornment from '@material-ui/core/InputAdornment'
import ArrowDropUp from '@material-ui/icons/KeyboardArrowUp'
import { KeyboardArrowDown } from '@material-ui/icons'
import IconButton from '@material-ui/core/IconButton'

import Paper from '@material-ui/core/Paper'
import {
  isSameDay,
  addDays,
  endOfDay,
  startOfDay,
  startOfMonth,
  endOfMonth,
  addMonths,
  format,
  isAfter,
  isBefore,
} from 'date-fns'
import moment from 'moment'
import Icon from 'components/common/Icon'
import TextInput from 'components/common/TextInput'
import Button from 'components/common/Button'
import { useWindowWidth } from 'utils/hooks/useSize'
import { DAY_PICKER_DESKTOP_PROPS, DAY_PICKER_TABLET_PROPS, isTableMode } from 'utils/layout'
import { colors } from 'theme'
import { CustomStaticRangeLabelContent } from './elements'
import style from './style'

const staticRangeHandler = {
  range: {},
  isSelected(range) {
    const definedRange = this.range()
    return (
      isSameDay(range.startDate, definedRange.startDate)
      && isSameDay(range.endDate, definedRange.endDate)
    )
  },
  hasCustomRendering: true,
}

const TimeRange = (props) => {
  const {
    classes,
    labelText,
    placeholder,
    value,
    className,
    onApply,
    onClear,
    leftItem,
    disableFuture,
    maxDate,
  } = props

  // INIT DATE RANGE VALUE

  const today = moment().format('MM/DD/YYYY')
  const defineds = {
    theFirstestDay: startOfDay(new Date(0)),
    startOfToday: startOfDay(new Date(today)),
    endOfToday: endOfDay(new Date(today)),
    startOfYesterday: startOfDay(addDays(new Date(today), -1)),
    endOfYesterday: endOfDay(addDays(new Date(today), -1)),
    startOf3Days: startOfDay(addDays(new Date(today), -2)),
    endOf3Days: startOfDay(addDays(new Date(today), -1)),
    startOf7Days: startOfDay(addDays(new Date(today), -7)),
    endOf7Days: startOfDay(addDays(new Date(today), -1)),
    startOf30Days: startOfDay(addDays(new Date(today), -30)),
    endOf30Days: startOfDay(addDays(new Date(today), -1)),
    startOfMonth: startOfMonth(new Date(today)),
    endOfMonth: endOfMonth(new Date(today)),
    startOfLastMonth: startOfMonth(addMonths(new Date(today), -1)),
    endOfLastMonth: endOfMonth(addMonths(new Date(today), -1)),
  }

  const createStaticRanges = (ranges) => ranges.map(
    (range) => ({ ...staticRangeHandler, ...range }),
  )

  const staticRanges = createStaticRanges([
    {
      label: 'All Time',
      labelKey: 'ALL',
      range: () => ({
        startDate: null,
        endDate: null,
      }),
    },
    {
      label: 'Today',
      range: () => ({
        startDate: defineds.startOfToday,
        endDate: defineds.endOfToday,
      }),
    },
    {
      label: 'Yesterday',
      range: () => ({
        startDate: defineds.startOfYesterday,
        endDate: defineds.endOfYesterday,
      }),
    },

    {
      label: 'Last 7 days',
      range: () => ({
        startDate: defineds.startOf7Days,
        endDate: defineds.endOf7Days,
      }),
    },
    {
      label: 'Last 30 days',
      range: () => ({
        startDate: defineds.startOf30Days,
        endDate: defineds.endOf30Days,
      }),
    },
    {
      label: 'This Month',
      range: () => ({
        startDate: defineds.startOfMonth,
        endDate: defineds.endOfMonth,
      }),
    },
    {
      label: 'Last Month',
      range: () => ({
        startDate: defineds.startOfLastMonth,
        endDate: defineds.endOfLastMonth,
      }),
    },
    {
      label: 'Custom',
      range: () => ({
        startDate: defineds.startOf3Days,
        endDate: defineds.endOfToday,
      }),
    },
  ])

  // END OF INIT DATE RANGE VALUE

  const defaultRange = {
    startDate: null,
    endDate: null,
    key: 'selection',
  }

  const [dataRange, setDataRange] = useState([defaultRange])
  const [tempRange, setTempRange] = useState({
    startDate: '',
    endDate: '',
  })
  const [disablePreview, setDisabledPreview] = useState(false)
  const [anchorEl, setAnchorEl] = useState(null)

  const handleOpen = (event) => {
    if (!anchorEl) {
      setAnchorEl(event.currentTarget)
    }
  }

  const handleClose = () => {
    setDataRange([
      {
        ...defaultRange,
        startDate: value.startDate ? new Date(value.startDate) : null,
        endDate: value.endDate ? new Date(value.endDate) : null,
      },
    ])
    setAnchorEl(null)
  }

  const handleChange = (item) => {
    setDataRange([item.selection])
  }

  const handleInputChange = (e) => {
    setTempRange({
      ...tempRange,
      [e.target.name]: e.target.value,
    })
  }

  const handleInputBlur = (e) => {
    const { target } = e
    if (!target.value
      || target.value.length < 8
      || target.value.length > 10
      || !moment(target.value, 'MM/DD/YYYY', true).isValid()
    ) {
      setDataRange([
        {
          ...dataRange[0],
          [target.name]: null,
        },
      ])
    } else if (target.name === 'endDate'
      && (!dataRange[0].startDate || isAfter(dataRange[0].startDate, new Date(target.value)))
    ) {
      setDataRange([
        {
          ...dataRange[0],
          startDate: new Date(target.value),
          endDate: new Date(target.value),
        },
      ])
    } else if (target.name === 'startDate' && !target.value && dataRange[0].endDate) {
      setDataRange([
        {
          ...dataRange[0],
          startDate: null,
          endDate: null,
        },
      ])
    } else if (target.name === 'startDate'
      && (dataRange[0].startDate && isBefore(dataRange[0].endDate, new Date(target.value)))
    ) {
      setDataRange([
        {
          ...dataRange[0],
          startDate: new Date(target.value),
          endDate: new Date(target.value),
        },
      ])
    } else {
      setDataRange([
        {
          ...dataRange[0],
          [target.name]: new Date(target.value),
        },
      ])
    }
  }

  const handleKeyDown = (e) => {
    if (e.keyCode === 13) {
      handleInputBlur(e)
    }
  }

  const handleApply = () => {
    onApply({
      startDate: dataRange[0].startDate ? format(dataRange[0].startDate, 'MM/dd/yyyy') : null,
      endDate: dataRange[0].endDate ? format(dataRange[0].endDate, 'MM/dd/yyyy') : null,
    })
    setAnchorEl(null)
  }

  useEffect(() => {
    setDataRange([
      {
        ...defaultRange,
        startDate: value.startDate ? new Date(value.startDate) : null,
        endDate: value.endDate ? new Date(value.endDate) : null,
      },
    ])
  }, [value]) // eslint-disable-line

  useEffect(() => {
    setTempRange({
      startDate: dataRange[0].startDate ? format(dataRange[0].startDate, 'MM/dd/yyyy') : '',
      endDate: dataRange[0].endDate ? format(dataRange[0].endDate, 'MM/dd/yyyy') : '',
    })
  }, [dataRange, setTempRange])

  const { startDate, endDate } = dataRange[0]

  const renderDisplayedValue = () => {
    if (!placeholder && !startDate && !endDate) {
      return staticRanges[0].label
    }

    const existedLabel = staticRanges.find((s) => (
      isSameDay(startDate, s.range().startDate)
      && isSameDay(endDate, s.range().endDate)
    ))

    if (existedLabel && existedLabel.label !== 'Custom') {
      return existedLabel.label
    }

    if (startDate || endDate) {
      return `${startDate
        ? format(startDate, 'MM/dd/yyyy') : '--/--/--'} - ${endDate ? format(endDate, 'MM/dd/yyyy')
        : '--/--/--'}`
    }

    return ''
  }

  const { width } = useWindowWidth()
  return (
    <Container className={`${classes.container}${className ? ` ${className}` : ''}`}>
      {leftItem && <div className={classes.leftItem}>{leftItem}</div>}
      <TextInput
        checkRemoveCursor
        label={labelText}
        className={anchorEl ? classes.inputOpen : ''}
        InputProps={{
          inputProps: {
            value: renderDisplayedValue(),
            readOnly: true,
            placeholder: placeholder || '',
          },
          classes: {
            adornedEnd: classes.adornedEnd,
          },
          endAdornment: anchorEl
            ? (
              <InputAdornment position="end" className={classes.inputAdornment}>
                <ArrowDropUp color="primary" />
              </InputAdornment>
            )
            : (onClear && (value.startDate || value.endDate))
              ? (
                <IconButton
                  size="small"
                  onClick={(e) => {
                    e.stopPropagation()
                    onClear()
                  }}
                  className={classes.buttonClear}
                >
                  <Icon
                    name="close"
                    color={colors.primaryBlue}
                    size={10}
                  />
                </IconButton>
              )
              : (
                <InputAdornment position="end" className={classes.inputAdornment}>
                  <KeyboardArrowDown color="primary" />
                </InputAdornment>
              ),
        }}
        onClick={handleOpen}
      />

      <Popover
        className={classes.popover}
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <Paper elevation={0} className={classes.paper}>
          <DateRangePicker
            className={`${classes.customDateRangePicker + (
              disablePreview ? ' disablePreview' : ''
            )}`}
            onChange={(item) => handleChange(item)}
            showMonthAndYearPickers={false}
            moveRangeOnFirstSelection={false}
            editableDateInputs
            weekdayDisplayFormat="EEEEEE"
            monthDisplayFormat="MMMM yyyy"
            startDatePlaceholder="MM/DD/YYYY"
            endDatePlaceholder="MM/DD/YYYY"
            rangeColors={[]}
            inputRanges={[]}
            staticRanges={staticRanges}
            fixedHeight
            ranges={dataRange}
            shownDate={defineds.startOfToday}
            maxDate={disableFuture && maxDate
              ? maxDate
              : disableFuture
                ? defineds.startOfToday
                : undefined}
            renderStaticRangeLabel={(definedRange) => (
              <CustomStaticRangeLabelContent
                {...definedRange}
                staticRanges={staticRanges}
                dataRange={dataRange}
                setDisabledPreview={setDisabledPreview}
              />
            )}
            {...(isTableMode(width) ? DAY_PICKER_TABLET_PROPS : DAY_PICKER_DESKTOP_PROPS)}
          />

          <div className={classes.footer}>
            <div className={classes.fieldGroup}>
              <div className={classes.fieldControl}>
                <span className={classes.fieldLabel}>From</span>
                <TextInput
                  name="startDate"
                  placeholder="MM/DD/YYYY"
                  value={tempRange.startDate || ''}
                  onChange={(e) => handleInputChange(e)}
                  onBlur={(e) => handleInputBlur(e)}
                  onKeyDown={(e) => handleKeyDown(e)}
                />
              </div>
              <div className={classes.fieldControl}>
                <span className={classes.fieldLabel}>To</span>
                <TextInput
                  name="endDate"
                  placeholder="MM/DD/YYYY"
                  value={tempRange.endDate || ''}
                  onChange={(e) => handleInputChange(e)}
                  onBlur={(e) => handleInputBlur(e)}
                  onKeyDown={(e) => handleKeyDown(e)}
                />
              </div>
            </div>
            <div className={classes.applyBtnWrapper}>
              <Button
                className={classes.applyBtn}
                onClick={() => handleApply()}
                label="Apply Filter"
              />
            </div>
          </div>
        </Paper>
      </Popover>
    </Container>
  )
}

TimeRange.defaultProps = {
  value: {
    startDate: null,
    endDate: null,
  },
  labelText: '',
  onApply: () => {},
  onChange: () => {},
  placeholder: '',
  className: '',
  onClear: null,
  leftItem: null,
  disableFuture: true,
  maxDate: undefined,
}

TimeRange.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  value: PropTypes.shape(),
  labelText: PropTypes.node,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onApply: PropTypes.func,
  onClear: PropTypes.func,
  leftItem: PropTypes.node,
  disableFuture: PropTypes.bool,
  maxDate: PropTypes.string,
}

export default withStyles(style)(TimeRange)
