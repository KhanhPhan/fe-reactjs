import React from 'react'
import {
  isSameDay,
} from 'date-fns'
import PropTypes from 'prop-types'

export const CustomStaticRangeLabelContent = ({
  staticRanges, dataRange, setDisabledPreview, ...definedRange
}) => {
  const start = dataRange[0].startDate
  const end = dataRange[0].endDate

  const isSelectAll = definedRange.labelKey === 'ALL' && start === null && end === null
  let isRangeExisted = staticRanges.filter(
    (s) => (
      (isSameDay(s.range().startDate, start) || s.range().startDate === start)
      && (isSameDay(s.range().endDate, end) || s.range().endDate === end)
    ),
  )

  isRangeExisted = isRangeExisted.length

  const customSelected = isSelectAll || (definedRange.label === 'Custom' && !isRangeExisted)

  return (
    // eslint-disable-next-line
    <div
      role="button"
      onMouseOver={() => setDisabledPreview(true)}
      onMouseLeave={() => setDisabledPreview(false)}
      className={customSelected ? 'customSelected' : ''}
    >
      {definedRange.label}
    </div>
  )
}

CustomStaticRangeLabelContent.propTypes = {
  dataRange: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  staticRanges: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  setDisabledPreview: PropTypes.func.isRequired,
}
