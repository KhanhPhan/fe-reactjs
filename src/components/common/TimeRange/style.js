/* eslint-disable max-len */

const style = (theme) => ({
  container: {
    display: 'flex',
    padding: 0,
    '&>div': {
      paddingTop: 0,
    },
    '& input.MuiInputBase-input': {
      paddingRight: 0,
    },
  },
  leftItem: {
    marginRight: theme.spacing(1),
  },
  formControl: {
    margin: 0,
    width: '100%',
    position: 'relative',
    verticalAlign: 'unset',
    '& input': {
      paddingRight: 20,
    },
    '& svg': {
      top: 'auto',
      bottom: 4,
      right: 24,
    },
  },
  customDateRangePicker: {
    '& .rdrDateDisplayWrapper': {
      display: 'none !important',
    },
    '& .rdrMonth': {
      padding: '0 20px 20px',
    },
    '& .rdrMonthName': {
      fontSize: 14,
      color: theme.color.primaryBlue,
      textAlign: 'center',
      fontWeight: 'bold',
      padding: '19px 10px 15px',
    },
    '& .rdrWeekDay': {
      fontSize: 13,
      color: theme.color.darkGray,
      textTransform: 'uppercase',
      borderBottomWidth: 1,
      borderBottomColor: '#EAF0F4',
      borderBottomStyle: 'solid',
    },
    '& .rdrDayNumber': {
      zIndex: 9,
      color: theme.color.textGray2,
      fontWeight: 500,
    },
    '& .rdrDayNumber span': {
    },
    '& .rdrDayPassive .rdrDayNumber span': {
      color: '#C2C2C2',
    },
    '& .rdrDay:not(.rdrDayPassive) .rdrInRange ~ .rdrDayNumber span, & .rdrDay:not(.rdrDayPassive) .rdrStartEdge ~ .rdrDayNumber span, & .rdrDay:not(.rdrDayPassive) .rdrEndEdge ~ .rdrDayNumber span, & .rdrDay:not(.rdrDayPassive) .rdrSelected ~ .rdrDayNumber span': {
      color: theme.color.primaryBlue,
    },
    '& .rdrSelected, & .rdrInRange, & .rdrStartEdge, & .rdrEndEdge': {
      background: theme.color.secondaryBlue3,
    },
    '& .rdrStartEdge, & .rdrEndEdge': {
      background: 'transparent',
      zIndex: 6,

      '&:before': {
        content: '""',
        position: 'absolute',
        top: '0',
        left: '0',
        display: 'inline-block',
        width: '50%',
        height: '100%',
        background: theme.color.secondaryBlue3,
      },

      '&:after': {
        content: '""',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        display: 'inline-block',
        width: 25,
        height: 25,
        background: '#91B1EA',
        borderRadius: '50%',
      },

      '& ~.rdrDayNumber span': {
        color: `${theme.color.primaryBlue} !important`,
      },

    },
    '& .rdrStartEdge': {
      '&:before': {
        left: '50% !important',
      },

      '&.rdrEndEdge': {
        '&:before': {
          display: 'none',
        },
      },
    },
    '& .rdrDayStartOfWeek .rdrEndEdge:before': {
      borderTopLeftRadius: ' 1.042em',
      borderBottomLeftRadius: ' 1.042em',
    },
    '& .rdrDayEndOfWeek .rdrStartEdge:before': {
      borderTopRightRadius: '1.042em',
      borderBottomRightRadius: ' 1.042em',
    },
    '& .rdrDayStartOfWeek .rdrDayEndPreview:before': {
      borderTopLeftRadius: ' 1.042em',
      borderBottomLeftRadius: ' 1.042em',
    },
    '& .rdrDayEndOfWeek .rdrDayStartPreview:before': {
      borderTopRightRadius: '1.042em',
      borderBottomRightRadius: ' 1.042em',
    },

    '& .rdrDayInPreview': {
      border: 'none',
      background: theme.color.secondaryBlue3,
      zIndex: 3,
    },
    '& .rdrDayStartPreview, .rdrDayEndPreview': {
      border: 'none',

      '&:before': {
        content: '""',
        position: 'absolute',
        top: '0',
        left: '0',
        display: 'inline-block',
        width: '50%',
        height: '100%',
        background: theme.color.secondaryBlue3,
      },

      '&:after': {
        content: '""',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        display: 'inline-block',
        width: 25,
        height: 25,
        background: '#91B1EA',
        borderRadius: '50%',
      },

      '& ~.rdrDayNumber span': {
        color: `${theme.color.primaryBlue} !important`,
      },
    },
    '& .rdrDayStartPreview': {
      '&:before': {
        left: '50% !important',
      },

      '& .rdrDayNumber span': {
        color: `${theme.color.primaryBlue} !important`,
      },

      '&.rdrDayEndPreview': {
        '&:before': {
          display: 'none',
        },
      },
    },
    '& .rdrDayToday:not(.rdrDayPassive) .rdrInRange ~ .rdrDayNumber span:after, & .rdrDayToday:not(.rdrDayPassive) .rdrStartEdge ~ .rdrDayNumber span:after, & .rdrDayToday:not(.rdrDayPassive) .rdrEndEdge ~ .rdrDayNumber span:after, & .rdrDayToday:not(.rdrDayPassive) .rdrSelected ~ .rdrDayNumber span:after': {
      background: theme.color.primaryBlue,
    },
    '& .rdrDayToday .rdrDayNumber span:after': {
      background: theme.color.textGray,
    },
    '& .rdrMonthAndYearPickers': {
      display: 'none',
    },

    '& .rdrCalendarWrapper': {
      position: 'relative',
      padding: '0 10px',
    },
    '& .rdrMonthAndYearWrapper': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      padding: '0 26px',
    },
    '& .rdrNextPrevButton': {
      position: 'relative',
      borderRadius: '50%',
      background: 'transparent',
      transition: 'all .3s ease-in-out',
      '&:before': {
        content: '""',
        display: 'inline-block',
        width: 8,
        height: 2,
        background: theme.color.primaryBlue,
        transform: 'translateX(-50%) rotate(40deg)',
        position: 'absolute',
        top: 9,
        left: '50%',
      },
      '&:after': {
        content: '""',
        display: 'inline-block',
        width: 8,
        height: 2,
        background: theme.color.primaryBlue,
        transform: 'translateX(-50%) rotate(-40deg)',
        position: 'absolute',
        bottom: 9,
        left: '50%',
      },
      '& i': {
        display: 'none',
      },
      '&:hover': {
        background: '#EFF2F7',
      },
      '&.rdrPprevButton': {
        '&:before': {
          transform: 'translateX(-50%) rotate(-40deg)',
        },
        '&:after': {
          transform: 'translateX(-50%) rotate(40deg)',
        },
      },
    },
    '& .rdrStaticRange': {
      border: 'none',
      color: '#3B4757',
      borderRadius: 3,
      margin: '3px 0',
      position: 'relative',

      '&:nth-child(1), &:nth-child(3), &:nth-child(7)': {
        marginBottom: 20,

        '&:after': {
          content: '""',
          display: 'block',
          position: 'absolute',
          bottom: -10,
          left: 5,
          right: 5,
          height: 1,
          background: '#eee',
        },
      },
    },
    '& .rdrStaticRangeLabel': {
      padding: 0,
      fontSize: 14,
      borderRadius: 3,
      overflow: 'hidden',

      '& > div': {
        padding: '10px 15px',
      },

    },
    '& .rdrStaticRange:hover .rdrStaticRangeLabel, & .rdrStaticRange:focus .rdrStaticRangeLabel': {
      background: theme.color.secondaryBlue3,
    },
    '& .rdrDefinedRangesWrapper .rdrStaticRangeSelected': {
      color: theme.color.darkGray,
      fontWeight: 400,
      borderRight: '1px solid #eee',

      '& .rdrStaticRangeLabel': {
        background: theme.color.secondaryBlue3,
      },
    },
    '& .rdrDefinedRangesWrapper': {
      padding: '10px 5px',
      width: '180px !important',
    },
    '&.disablePreview': {
      '& .rdrDayStartPreview, .rdrDayEndPreview, .rdrDayInPreview ': {
        display: 'none',
      },
    },

    '& .customSelected': {
      background: theme.color.secondaryBlue3,
      color: theme.color.darkGray,
    },
    '& .rdrInputRanges': {
      display: 'none',
    },
    '& .rdrDayDisabled': {
      background: 'transparent',
      '& .rdrDayNumber': {
        background: 'transparent',
      },
      '& .rdrDayNumber span': {
        background: 'transparent',
        color: '#aeb9bf !important',
      },
      '& .rdrInRange': {
        background: 'transparent',
      },
    },

    '@media not screen and (min-width: 1024px)': {
      '& .rdrMonthAndYearWrapper': {
        position: 'relative',
      },

      '& .rdrNextPrevButton': {
        margin: 0,
      },

      '& .rdrMonthAndYearPickers': {
        display: 'inherit',
        fontSize: 16,
        fontWeight: 500,
        color: theme.color.primaryBlue,
      },
      '& .rdrDay': {
        height: '2.5em',
        lineHeight: '2.5em',
      },
      '& .rdrMonth': {
        width: '25em',
      },
    },

    [theme.breakpoints.down('xs')]: {
      '&.rdrDateRangePickerWrapper': {
        display: 'block',
      },

      '& .rdrDefinedRangesWrapper': {
        width: '100% !important',
        borderRight: 'none',
        boxSizing: 'border-box',
      },

      '& .rdrStaticRanges': {
        display: 'block',
      },

      '& .rdrStaticRange': {
        display: 'inline-block',
        margin: '3px !important',
        border: '1px solid #eee',
        borderRadius: 5,

        '&:after': {
          display: 'none !important',
        },

        '& .rdrStaticRangeLabel > div': {
          padding: 8,
        },
      },

      '& .rdrCalendarWrapper': {
        width: '100%',
      },

      '& .rdrMonths': {
        alignItems: 'center',
      },

      '& .rdrMonth': {
        width: '100%',
        boxSizing: 'border-box',
      },

    },
  },
  // custom picker
  popover: {
    marginTop: 10,
    '& .MuiPaper-rounded': {
      borderRadius: 5,
    },
    '& .MuiPaper-elevation8': {
      boxShadow: '0px 2px 10px #D6DEF2',
      overflowX: 'unset',
      overflowY: 'unset',
    },

  },
  paper: {
    position: 'relative',
    overflow: 'hidden',
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 191,
    right: 0,
    padding: '0 10px 13px',

    [theme.breakpoints.down('xs')]: {
      position: 'relative',
      left: 'auto',
    },
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    '@media not screen and (min-width: 1024px)': {
      display: 'block',
      padding: '0 38px',
    },
  },
  fieldControl: {
    display: 'inline-flex',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 38px',

    '@media not screen and (min-width: 1024px)': {
      width: '100%',
      padding: 0,
      display: 'flex',

      '&:first-child': {
        marginBottom: 10,
      },
    },
  },
  fieldLabel: {
    color: theme.color.textGray1,
    fontSize: 14,
    minWidth: 50,

    '&+div': {
      paddingTop: 0,
    },
  },
  applyBtnWrapper: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: 20,

    '@media not screen and (min-width: 1024px)': {
      marginTop: 10,
      padding: '0 38px',
    },
  },
  applyBtn: {
    color: theme.color.white,

    '@media not screen and (min-width: 1024px)': {
      width: '100%',
    },
  },
  adornedEnd: {
    paddingRight: 7,
    '& fieldset.MuiOutlinedInput-notchedOutline': {
      borderColor: 'rgba(0, 0, 0, 0.23)!important',
      borderWidth: '1px!important',
    },
  },
  inputOpen: {
    '& fieldset.MuiOutlinedInput-notchedOutline': {
      borderColor: '#2933C5!important',
      borderWidth: '2px!important',
    },
  },
  inputAdornment: {
    padding: 3,
  },
  buttonClear: {
    padding: 10,
  },
})

export default style
