import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { withStyles, InputAdornment } from '@material-ui/core'
import isEmpty from 'lodash/isEmpty'
import style from './style'
import TextEditor from '../TextEditor'
import TextInput from '../TextInput'

const ShortTextEditor = ({
  classes, value, placeholder, ...props
}) => {
  const [isCollapse, setIsCollapse] = useState(false)

  useEffect(() => {
    if (!isEmpty(value) && !isCollapse) {
      setIsCollapse(true)
    }
  }, [value])

  // TODO: handle animation
  return (
    <>
      {!isCollapse && (
      <TextInput
        placeholder={placeholder}
        disabled
        InputProps={{
          endAdornment: (
            <InputAdornment position="end" className={classes.inputAdornment}>
              <img src="/icons/more_blue.svg" alt="more" />
            </InputAdornment>
          ),
        }}
        onClick={() => {
          setIsCollapse(true)
        }}
      />
      )}
      {isCollapse && <TextEditor value={value} {...props} />}
    </>
  )
}

ShortTextEditor.defaultProps = {
  ...TextEditor.defaultProps,
}

ShortTextEditor.propTypes = {
  ...TextEditor.propTypes,
  classes: PropTypes.shape().isRequired,
}

export default withStyles(style)(ShortTextEditor)
