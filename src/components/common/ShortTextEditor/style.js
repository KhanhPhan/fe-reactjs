const style = () => ({
  container: {
    transition: 'height 0.15s',
  },
  collapse: {
    // height: 'auto',
  },
  inputAdornment: {
    height: '100%',
    marginRight: 15,
  },
})

export default style
