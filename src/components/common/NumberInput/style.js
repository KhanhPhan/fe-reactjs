const style = () => ({
  NumberInputNoArrow: {
    '& input::-webkit-outer-spin-button, input::-webkit-inner-spin-button': {
      WebkitAppearance: 'none',
      margin: 0,
    },
    '& input': {
      '-moz-appearance': 'textfield',
    },
  },
})

export default style
