import React, { useMemo } from 'react'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import TextInput from '../TextInput'

import style from './style'

const DEFAULT_BLOCKED_CHARACTORS = ['e', 'E', '-', '+']

// TODO: solve >> cannot validate if users paste -- ++ +- ... into field
const NumberInput = ({
  classes,
  className,
  onOverMaxSafeInteger,
  disableArrowUI,
  disableValueMutationByArrowKeys,
  disableScrollAction,
  min,
  max,
  blockedCharacters: blockedCharsProps,
  inputProps,
  ...props
}) => {
  const blockedChars = useMemo(() => {
    if (Array.isArray(blockedCharsProps)) return blockedCharsProps

    if (blockedCharsProps instanceof Object) {
      return DEFAULT_BLOCKED_CHARACTORS
        .concat(blockedCharsProps.and || [])
        .filter((c) => !(blockedCharsProps.not || []).includes(c))
    }

    return [blockedCharsProps]
  }, [blockedCharsProps])

  return (
    <TextInput
      className={clsx(className, {
        [classes.NumberInputNoArrow]: disableArrowUI,
      })}
      {...props}
      inputProps={{
        onKeyDown: (e) => {
          if (disableValueMutationByArrowKeys && (e.key === 'ArrowUp' || e.key === 'ArrowDown')) {
            e.preventDefault()
          }
          if (blockedChars.length && blockedChars.includes(e.key)) {
            e.preventDefault()
          }
        },
        ...(disableScrollAction && { onWheel: (e) => e.currentTarget.blur() }),
        ...(min && { min }),
        ...(max && { max }),
        step: 'any',
        ...inputProps,
      }}
      type="number"
    />
  )
}

NumberInput.defaultProps = {
  className: '',
  onOverMaxSafeInteger: null,
  disableArrowUI: false,
  disableScrollAction: false,
  min: null,
  max: null,
  blockedCharacters: DEFAULT_BLOCKED_CHARACTORS,
  inputProps: null,
  disableValueMutationByArrowKeys: false,
}

NumberInput.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  onOverMaxSafeInteger: PropTypes.func,
  disableArrowUI: PropTypes.bool,
  disableScrollAction: PropTypes.bool,
  min: PropTypes.number,
  max: PropTypes.number,
  blockedCharacters: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.shape({
      and: PropTypes.arrayOf(PropTypes.string),
      not: PropTypes.arrayOf(PropTypes.string),
    }),
    PropTypes.string,
  ]),
  inputProps: PropTypes.shape(),
  disableValueMutationByArrowKeys: PropTypes.bool,
}

export default withStyles(style)(NumberInput)
