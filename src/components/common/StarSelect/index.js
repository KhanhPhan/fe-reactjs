import React from 'react'
import PropTypes from 'prop-types'
import {
  FormControl,
  FormHelperText,
  MenuItem,
  Select as MSelect,
  Typography,
  withStyles,
} from '@material-ui/core'
import { KeyboardArrowDown } from '@material-ui/icons'
import clsx from 'clsx'
import Star from 'components/common/Star'

const style = (theme) => ({
  container: {
    width: '100%',
  },
  input: {
    width: '100%',
    background: theme.color.secondaryBlue3,
    '&> .MuiMenu-paper': {
      marginTop: theme.spacing(1),
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      boxShadow: '0px 2px 10px #D6DEF2',
      borderRadius: 5,
    },
  },
  icon: {
    color: theme.color.primaryBlue,
  },
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    background: theme.color.secondaryBlue3,
    minHeight: 'unset',
    height: 16,
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    '&:focus': {
      background: theme.color.secondaryBlue3,
      border: 'none',
    },
  },
  legend: {
    marginBottom: 5,
  },
  item: {
    justifyContent: 'space-between',
    borderRadius: 3,
  },
  title: {
    borderTop: `1px solid ${theme.color.disableGray1}`,
    paddingTop: theme.spacing(1),
    paddingBottom: 1,
    marginTop: 5,
    '&.no0': {
      border: 'none',
      paddingTop: 1,
    },
  },
  subLabel: {
    color: theme.color.textGray2,
  },
  selectContainer: {
    display: 'flex',
    flex: 1,
  },
  leftItem: {
    marginRight: theme.spacing(1),
  },
  rightItem: {
    marginLeft: theme.spacing(1),
  },
  error: {
    marginRight: 3,
    marginTop: 4,
  },
})

const StarSelect = ({
  classes, value, options, className,
  legend, onChange, leftItem, rightItem,
  helperText, placeholder, ...props
}) => {
  const extraProps = {}

  if (placeholder) {
    extraProps.renderValue = (val) => {
      if (val === null) {
        return placeholder
      }
      return val
    }
  }

  return (
    <FormControl
      className={clsx(classes.container, className)}
      {...props}
    >
      {legend && <Typography variant="subtitle1" className={classes.legend}>{legend}</Typography>}
      <div className={classes.selectContainer}>
        {leftItem && <div className={classes.leftItem}>{leftItem}</div>}
        <MSelect
          {...props}
          className={classes.input}
          classes={{
            root: classes.root,
            iconOutlined: classes.icon,
          }}
          value={value}
          IconComponent={KeyboardArrowDown}
          variant="outlined"
          onChange={(e) => e && e.target && e.target.value && onChange(e.target.value)}
          {...extraProps}
        >
          {
            options.map((item) => (
              <MenuItem key={item.value} value={item.value} className={classes.item}>
                <Star
                  readOnly
                  value={item.value}
                />
              </MenuItem>
            ))
          }
        </MSelect>
        {rightItem && <div className={classes.rightItem}>{rightItem}</div>}
      </div>
      <FormHelperText className={classes.error}>{helperText}</FormHelperText>
    </FormControl>
  )
}

StarSelect.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  options: PropTypes.arrayOf(PropTypes.shape()),
  legend: PropTypes.node,
  onChange: PropTypes.func,
  leftItem: PropTypes.node,
  rightItem: PropTypes.node,
  helperText: PropTypes.string,
  placeholder: PropTypes.string,
}

StarSelect.defaultProps = {
  className: '',
  value: '',
  options: [],
  legend: '',
  onChange: () => {},
  leftItem: null,
  rightItem: null,
  helperText: '',
  placeholder: '',
}

export default withStyles(style)(StarSelect)
