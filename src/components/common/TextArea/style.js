const style = (theme) => ({
  textAreaContainer: {
    border: '1px solid #2959C5',
    borderRadius: 5,
    display: 'flex',
    backgroundColor: theme.color.secondaryBlue3,
    padding: 15,
    alignItems: 'flex-end',
    height: '100%',
    boxSizing: 'border-box',
  },
  textArea: {
    flex: 1,
    border: 'none',
    resize: 'none',
    backgroundColor: theme.color.secondaryBlue3,
    height: '100%',
    marginRight: 10,
    fontFamily: 'Rubik',
    '&:focus': {
      outline: 'none',
    },
    '&::placeholder': {
      fontFamily: 'Rubik',
      color: theme.color.textGray2,
      fontWeight: 300,
    },
  },
  submitButton: {
    background: theme.color.white,
    paddingLeft: 10,
    paddingRight: 10,
  },
})

export default style
