import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import Button from 'components/common/Button'
import style from './style'

const TextArea = ({
  classes, defaultValue, submitBtnText, placeholder, onSubmit, isCreateNoteLoading,
}) => {
  const [value, setValue] = useState(defaultValue)
  const handleSubmit = () => {
    setValue('')
    onSubmit(value)
  }
  return (
    <div className={classes.textAreaContainer}>
      <textarea
        className={classes.textArea}
        placeholder={placeholder}
        onChange={(e) => setValue(e.target.value)}
        value={value}
      />
      <Button
        color="custom"
        className={classes.submitButton}
        loading={isCreateNoteLoading}
        onClick={handleSubmit}
      >
        {submitBtnText}

      </Button>
    </div>
  )
}

TextArea.defaultProps = {
  defaultValue: '',
  submitBtnText: 'Post',
  placeholder: '',
  isCreateNoteLoading: false,
}

TextArea.propTypes = {
  classes: PropTypes.shape().isRequired,
  defaultValue: PropTypes.string,
  submitBtnText: PropTypes.string,
  placeholder: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  isCreateNoteLoading: PropTypes.bool,
}

export default withStyles(style)(TextArea)
