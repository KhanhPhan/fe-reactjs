import React from 'react'
import PropTypes from 'prop-types'
import PhoneInput from 'react-phone-input-2'
import { FormHelperText, FormLabel, withStyles } from '@material-ui/core'
import 'react-phone-input-2/lib/style.css'
import clsx from 'clsx'
import style from './style'
import Button from '../Button'
import Tooltip from '../Tooltip'

const PhoneNumber = ({
  classes, name, setFieldValue,
  value, country, onlyCountries,
  error, helperText,
  buttonLabel, onButtonClick, buttonLoading,
  legend, legendTooltip,
}) => {
  const onChange = (v) => {
    setFieldValue(name, v)
  }
  return (
    <>
      <div className={clsx(classes.container, { [classes.containerWithButton]: !!buttonLabel })}>
        {legend && (
          <FormLabel component="legend" className={classes.formLabel}>
            {legend}
            {legendTooltip
                && (
                  <Tooltip
                    className={classes.legendTooltip}
                    {...legendTooltip}
                  />
                )}
          </FormLabel>
        )}
        <PhoneInput
          country={country}
          value={value}
          onlyCountries={onlyCountries}
          onChange={onChange}
          disableDropdown
          inputClass={clsx(
            classes.input,
            { [classes.inputWithButton]: !!buttonLabel },
          )}
          placeholder="+84 123 456 789"
        />
        {
          buttonLabel && (
            <Button
              className={classes.button}
              loading={buttonLoading}
              onClick={onButtonClick}
            >
              {buttonLabel}
            </Button>
          )
        }
      </div>
      {error && <FormHelperText error className={classes.error}>{helperText}</FormHelperText>}
    </>
  )
}

PhoneNumber.defaultProps = {
  country: 'vn',
  onlyCountries: ['vn'],
  error: false,
  helperText: '',
  value: '',
  buttonLabel: '',
  onButtonClick: () => {},
  buttonLoading: false,
  legend: '',
  legendTooltip: null,
}

PhoneNumber.propTypes = {
  classes: PropTypes.shape().isRequired,
  name: PropTypes.string.isRequired,
  country: PropTypes.string,
  value: PropTypes.string,
  onlyCountries: PropTypes.arrayOf(PropTypes.string),
  setFieldValue: PropTypes.func.isRequired,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  buttonLabel: PropTypes.string,
  onButtonClick: PropTypes.func,
  buttonLoading: PropTypes.bool,
  legend: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  legendTooltip: PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.object,
  ]),
}

export default withStyles(style)(PhoneNumber)
