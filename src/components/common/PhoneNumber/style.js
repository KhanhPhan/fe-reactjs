const style = (theme) => ({
  input: {
    height: `${theme.spacing(4)}px !important`,
    width: '100% !important',
  },
  container: {
    marginBottom: `${theme.spacing(2)}px !important`,
  },
  containerWithButton: {
    display: 'flex',
  },
  inputWithButton: {
    flex: 1,
  },
  button: {
    width: 200,
    marginLeft: theme.spacing(1),
  },
  formLabel: {
    textAlign: 'left',
    fontSize: 14,
    letterSpacing: 0,
    color: theme.color.black,
    marginBottom: theme.spacing(0.5),
    opacity: 1,
    position: 'relative',
    zIndex: 2,
    display: '-webkit-box',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 1,
    paddingBottom: 2,
  },
  legendTooltip: {
    marginLeft: 8,
  },
  error: {
    margin: '-18px 3px 0 3px !important',
  },
})

export default style
