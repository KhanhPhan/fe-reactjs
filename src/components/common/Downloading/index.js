import React from 'react'
import PropTypes from 'prop-types'
import LinearProgress from '@material-ui/core/LinearProgress'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import { withStyles } from '@material-ui/core'
import style from './style'

const Downloading = ({ classes, display, loading }) => (
  <>
    {
      display && (
      <div className={classes.notificationContainer}>
        <div className={classes.notificationTrack}>
          <div
            role="presentation"
            className={classes.notification}
          >
            <div className={classes.meta}>
              <span className={classes.message}>Downloading</span>
            </div>
            <div className={classes.content}>
              <Box display="flex" width="100%" alignItems="center">
                <Box width="100%" mr={1}>
                  <LinearProgress variant="determinate" value={loading} />
                </Box>
                <Box minWidth={35}>
                  <Typography variant="body2" color="textSecondary">
                    {`${Math.round(
                      loading,
                    )}%`}
                  </Typography>
                </Box>
              </Box>
            </div>
          </div>
        </div>
      </div>
      )
    }
  </>
)

Downloading.propTypes = {
  classes: PropTypes.shape().isRequired,
  loading: PropTypes.number.isRequired,
  display: PropTypes.bool.isRequired,
}

export default withStyles(style)(Downloading)
