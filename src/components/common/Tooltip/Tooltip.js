import React from 'react'
import PropTypes from 'prop-types'
import Tooltip from '@material-ui/core/Tooltip'

const TooltipStyled = ({ className, children, ...props }) => {
  const isRefChildren = (component) => {
    const childrenComponent = component
    if (childrenComponent.ref !== null) return true
    const childrenType = childrenComponent
      .type?.$$typeof?.toString() || childrenComponent.type

    return childrenType in ['Symbol(react.forward_ref)', 'Symbol(react.memo)', 'Symbol(react.lazy)']
      || typeof childrenType === 'string'
  }

  const renderChildren = isRefChildren(children)
    ? children
    : <span>{children}</span>

  return (
    <Tooltip
      className={className}
      placement="right-start"
      {...props}
    >
      {renderChildren}
    </Tooltip>
  )
}

TooltipStyled.defaultProps = {
  className: '',
}

TooltipStyled.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
}

export default TooltipStyled
