/* eslint-disable max-len */
import { makeStyles } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles((theme) => ({
  container: {
    margin: 0,
    padding: theme.spacing(1),
  },
  item: {
    marginBottom: theme.spacing(1),
    '&:last-child': {
      marginBottom: 0,
    },
  },
}))

const TooltipPassword = () => {
  const classes = useStyles()
  return (
    <ol className={classes.container}>
      <li className={classes.item}>Your password has to be at least 8 characters long.</li>
      <li className={classes.item}>
        Must contains at least one lower case letter, one digit and one of these special characters  ~!@#$%^&amp;*()_+?
      </li>
      <li className={classes.item}>No white space.</li>
    </ol>
  )
}

export default TooltipPassword
