import React, {
  createElement, forwardRef, useEffect, useState, useRef,
} from 'react'
import PropTypes from 'prop-types'
import { Tooltip, withStyles } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { colors } from 'theme'
import textblockstyles from './style'

/**
 * base TextBlock
 */
const BaseTextBlock = forwardRef(({
  classes,
  className,
  wrapElement,
  style,
  maxLine,
  children,
  ...props
}, ref) => createElement(
  wrapElement,
  {
    ...props,
    ref,
    className: `${classes.TextBlockContainer} ${className}`,
    style: {
      ...(maxLine && { WebkitLineClamp: maxLine }),
      ...style,
    },
  },
  children,
))

BaseTextBlock.defaultProps = {
  className: '',
  wrapElement: 'div',
  style: {},
  maxLine: null,
  children: '',
}

BaseTextBlock.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  wrapElement: PropTypes.string,
  style: PropTypes.shape(),
  maxLine: PropTypes.number,
  children: PropTypes.node,
}

const TextBlock = withStyles(textblockstyles)(BaseTextBlock)

/**
 * TextBlockToolTip
 */

const useStyles = makeStyles({
  tooltip: {
    background: colors.textGray3,
    color: colors.white,
    fontSize: 14,
    fontWeight: 300,
    padding: 16,
    borderRadius: 8,
  },
})

const TextBlockToolTip = ({
  title,
  tooltipProps,
  refToolTip,
  ...props
}) => {
  const classes = useStyles()
  const refBlock = useRef(null)
  const [isToolTip, setIsToolTip] = useState(false)
  const checkOver = (el) => {
    const isOverFlow = el
      ? el.offsetHeight < el.scrollHeight || el.offsetWidth < el.scrollWidth
      : false
    setIsToolTip(isOverFlow)
  }
  useEffect(() => {
    let el = refBlock.current && refBlock.current.querySelector('[data-tooltip]')
    const elInput = refBlock.current.getElementsByTagName('input')
      && refBlock.current.getElementsByTagName('input').length
      && refBlock.current.getElementsByTagName('input')[0]
    if (elInput) {
      checkOver(elInput)
    } else {
      if (!el) {
        el = refBlock.current
      }
      checkOver(el)
    }
  }, [title])

  return (
    <>
      {
        isToolTip
          ? (
            <Tooltip
              {...tooltipProps}
              title={title}
              classes={{
                ...tooltipProps.classes,
                tooltip: classes.tooltip,
              }}
            >
              <TextBlock ref={refBlock} {...props} />
            </Tooltip>
          )
          : <TextBlock ref={refBlock} {...props} />
      }
    </>

  )
}

TextBlockToolTip.defaultProps = {
  title: '',
  tooltipProps: {},
  refToolTip: null,
}

TextBlockToolTip.propTypes = {
  title: PropTypes.node,
  tooltipProps: PropTypes.shape(),
  refToolTip: PropTypes.node,
}

export {
  TextBlockToolTip,
}
export default TextBlock
