/* eslint-disable no-nested-ternary */
import React, { useEffect, useState, useRef } from 'react'
import PropTypes from 'prop-types'
import {
  withStyles,
  Button,
} from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import { DEFAULT_PAGE_SIZE } from 'constants/settings'

import style from './style'

const Pagination = ({
  classes,
  onChangePage,
  page, perPage, total, loading,
}) => {
  const inputRef = useRef()
  const [tempPage, setTempPage] = useState(1)
  const numberOfPages = Math.ceil(total / perPage) || 1

  useEffect(() => {
    if (page !== tempPage) {
      setTempPage(page)
    }
  }, [page])

  const handleChange = (e) => {
    const value = e.target.value.replace(/[^0-9]/g, '')
    if (value > numberOfPages) {
      setTempPage(numberOfPages)
    } else {
      setTempPage(value)
    }
  }

  const handleBlur = () => {
    if (tempPage) {
      let acceptablePage = 1

      if (Number(tempPage) < 1) {
        acceptablePage = 1
      } else if (Number(tempPage) > numberOfPages) {
        acceptablePage = numberOfPages
      } else {
        acceptablePage = parseInt(tempPage, 10)
      }

      setTempPage(acceptablePage)
      if (acceptablePage !== page) onChangePage(acceptablePage)
    } else {
      setTempPage(page)
    }
  }

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      inputRef.current.blur()
    }
  }

  const handleNext = () => {
    if (page < numberOfPages) {
      onChangePage(page + 1)
    }
  }

  const handlePrev = () => {
    if (page > 1) {
      onChangePage(page - 1)
    }
  }
  return (
    <div className={classes.pagination}>
      <Button
        className={classes.btn}
        onClick={handlePrev}
        variant="text"
        disabled={loading || page === 1}
      >
        <ArrowBackIcon />
        Prev Page
      </Button>
      <div className={classes.inputContainer}>
        Page
        {' '}
        <input
          ref={inputRef}
          name="page"
          value={tempPage}
          onChange={handleChange}
          onBlur={handleBlur}
          onKeyDown={handleKeyDown}
          className={classes.input}
          disabled={loading}
        />
        {' '}
        of
        {' '}
        {numberOfPages}
      </div>
      <Button
        className={classes.btn}
        onClick={handleNext}
        variant="text"
        disabled={loading || page === numberOfPages}
      >
        Next Page
        <ArrowForwardIcon />
      </Button>
    </div>
  )
}

Pagination.defaultProps = {
  onChangePage: () => {},
  page: 1,
  perPage: DEFAULT_PAGE_SIZE,
  total: 0, // total items
  loading: false,
}

Pagination.propTypes = {
  classes: PropTypes.shape().isRequired,
  onChangePage: PropTypes.func,
  page: PropTypes.number,
  perPage: PropTypes.number,
  total: PropTypes.number,
  loading: PropTypes.bool,
}
export default withStyles(style)(Pagination)
