const style = (theme) => ({
  pagination: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    padding: theme.spacing(1.5, 0),
    fontSize: 14,
    color: theme.color.darkGray,
  },
  btn: {
    textTransform: 'none',
    display: 'inline-flex',
    alignItems: 'center',
    fontWeight: 400,

    '& svg': {
      fontSize: 18,
      margin: '0 3px 2px 3px',
    },
  },
  inputContainer: {
    margin: '0px 24px 2px 24px',
    '& input': {
      fontSize: 14,
    },
  },
  input: {
    border: `1px solid ${theme.color.secondaryBlue2}`,
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 5,
    display: 'inline-block',
    textAlign: 'center',
    width: theme.spacing(6),
    height: theme.spacing(4),
    boxSizing: 'border-box',
    transition: 'all .2s ease-in-out',

    '&:focus': {
      outline: 'none',
      borderColor: theme.color.primaryBlue,
    },
  },

})

export default style
