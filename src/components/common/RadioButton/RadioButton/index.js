import React from 'react'
import PropTypes from 'prop-types'
import {
  FormControlLabel, Radio, withStyles,
} from '@material-ui/core'

const style = (theme) => ({
  container: {
  },
  item: {
    display: 'flex',
    alignItems: 'flex-start',
    marginTop: theme.spacing(2),
    borderRadius: theme.spacing(1),
    marginLeft: 0,
  },
  radio: {
    margin: '-9px 0 -9px -9px',
  },
  label: {
  },
})

const RadioButton = ({
  classes, value, onChange, label, checked,
}) => (
  <FormControlLabel
    classes={{
      label: classes.label,
    }}
    className={classes.item}
    key={value}
    value={value}
    control={(
      <Radio
        color="primary"
        className={classes.radio}
        onChange={onChange}
        checked={checked}
      />
    )}
    label={label}
  />
)

RadioButton.defaultProps = {
  value: '',
  onChange: () => { },
  label: '',
  checked: false,
}

RadioButton.propTypes = {
  classes: PropTypes.shape().isRequired,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.string,
  checked: PropTypes.bool,
}

export default withStyles(style)(RadioButton)
