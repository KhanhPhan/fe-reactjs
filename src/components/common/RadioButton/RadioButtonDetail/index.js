import React from 'react'
import PropTypes from 'prop-types'
import {
  FormControl, FormControlLabel, Radio, RadioGroup, withStyles,
} from '@material-ui/core'
import clsx from 'clsx'

const style = (theme) => ({
  container: {
  },
  item: {
    display: 'flex',
    alignItems: 'flex-start',
    marginTop: theme.spacing(2),
    padding: theme.spacing(1, 1.5),
    borderRadius: theme.spacing(1),
    border: `1px solid ${theme.color.primaryBlue}`,
    marginLeft: 0,
  },
  radio: {
    margin: '-9px 0 -9px -9px',
  },
  label: {
    width: '100%',
  },
})

const RadioButtonDetail = ({
  classes, className, value, onChange, options, radioProps,
}) => (
  <FormControl className={clsx(classes.container, className)}>
    <RadioGroup
      aria-label="gender"
      name="gender1"
      value={value}
      onChange={(e) => e.target.value !== value && onChange(e.target.value)}
    >
      {
          options.map((x) => (
            <FormControlLabel
              classes={{
                label: classes.label,
              }}
              className={classes.item}
              key={x.value}
              value={x.value}
              control={(
                <Radio
                  color="primary"
                  className={classes.radio}
                  {...radioProps}
                />
              )}
              label={x.label}
            />
          ))
        }
    </RadioGroup>
  </FormControl>
)

RadioButtonDetail.defaultProps = {
  value: null,
  className: '',
  onChange: () => {},
  options: [],
  radioProps: {},
}

RadioButtonDetail.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  options: PropTypes.arrayOf(PropTypes.shape()),
  radioProps: PropTypes.shape(),
}

export default withStyles(style)(RadioButtonDetail)
