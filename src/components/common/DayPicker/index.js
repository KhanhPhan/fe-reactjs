/* eslint-disable max-len */
import React, {
  useState, useRef, useEffect,
} from 'react'
import moment from 'moment'
import Datetime from 'react-datetime'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import clsx from 'clsx'
import IconButton from '@material-ui/core/IconButton'
import { KeyboardArrowDown } from '@material-ui/icons'
import ArrowDropUp from '@material-ui/icons/KeyboardArrowUp'

import Icon from 'components/common/Icon'
import TextInput from 'components/common/TextInput'
import 'react-datetime/css/react-datetime.css'
import Button from 'components/common/Button'
import { getLocalTimeMoment } from 'utils/datetime'

const themeStyle = (theme) => ({
  dateTimeContainer: {
    '& .rdtPicker': {
      top: 60,
      right: 0,
      left: 0,
      padding: '10px',
      width: '100%',
      maxWidth: '300px',
      maxHeight: '318px',
      zIndex: '1299!important',
    },
    '& .rdtDay': {
      borderRadius: '50%',
      textAlign: 'center',
      width: 40,
      height: 35,
      fontSize: 13,
    },
    '& .rdtDays tr .dow': {
      color: theme.color.textGray2,
      fontWeight: 'normal',
      borderWidth: 0,
      textTransform: 'uppercase',
      fontSize: 13,
      paddingBottom: 15,
      letterSpacing: 0,
    },
    '& .rdtDay.rdtToday.rdtActive, .rdtDay.rdtActive, .rdtDay.rdtActive:hover': {
      backgroundColor: `${theme.color.secondaryBlue6} !important`,
      color: `${theme.color.primaryBlue} !important`,
      textDecoration: 'underline',
    },
    '& th.rdtSwitch': {
      minWidth: 50,
      color: `${theme.color.primaryBlue} !important`,
      fontSize: '16px',
      fontWeight: '500',
      transform: 'translateY(-6px)',
    },
    '& .rdtMonth': {
      width: 'calc(25% - 6px)',
    },
    '& .rdtYear': {
      width: 'calc(25% - 6px)',
    },
    '& .rdtPicker .dow, .rdtPicker th.rdtSwitch,.rdtPicker th.rdtNext, .rdtPicker th.rdtPrev, .rdtPicker .rdtTimeToggle': {
      color: '#4D4F5C',
    },
    '& .rdtPicker th.rdtNext, .rdtPicker th.rdtPrev': {
      borderRadius: '25%',
      color: `${theme.color.primaryBlue} !important`,
      paddingBottom: '10px',
    },
  },
  pickupInput: {
    '& .MuiOutlinedInput-input': {
      color: `${theme.color.primaryBlue} !important`,
      height: 35,
      padding: 0,
      fontSize: 14,
      paddingLeft: 10,
    },

  },
  bottomContainer: {
    marginTop: 30,
    display: 'flex',
    justifyContent: 'space-between',
  },
  leftItem: {
    marginRight: theme.spacing(1),
  },
  wrapper: {
    fontSize: 14,
    display: 'flex',
    '& .past': {
      '& .MuiOutlinedInput-root': {
        background: '#FFF6E9 0% 0% no-repeat padding-box',
      },
    },
    '& .future': {
      '& .MuiOutlinedInput-root': {
        background: '#E7F8F5 0% 0% no-repeat padding-box',
      },
    },
    '@media screen and (max-width: 1200px)': {
      '& .text-range': {
        display: 'none!important',
      },
    },
    '& #time-picker': {
      height: 32,
      width: '100%',
      display: 'flex',
      '& .rdt': {
        width: '100%',
      },
      '@media screen and (max-width: 1024px)': {
        width: '120px',
      },
      '& .MuiInputBase-root': {
        '@media screen and (max-width: 1024px)': {
          paddingRight: 5,
          '& input': {
            paddingRight: 0,
          },
        },
      },
    },
    '& .button-today': {
      marginRight: 20,
      marginLeft: 10,
      '@media screen and (max-width: 1024px)': {
        width: 65,
      },
    },
    '& .rangeDate.today-range': {
      opacity: 0,
    },
    '& .rangeDate': {
      width: 125,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FFF6E9',
      borderRadius: '17px',
      marginRight: 20,
      '& .clock-range': {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
      '& .text-range': {
        font: 'normal normal normal 14px/17px Rubik',
        marginLeft: '10',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '123px',
        borderRadius: '17px',
      },
      '&.past': {
        color: '#FFA92B',
        backgroundColor: '#FFF6E9',
      },
      '&.future': {
        color: '#1CB69A',
        backgroundColor: '#E7F8F5',
      },
      '& path.past': {
        fill: '#FFA92B',
      },
      '& path.future': {
        fill: '#1CB69A',
      },
    },
  },

  wrapNextDate: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexGrow: 1,
    '& .arrow-next': {
      width: '36px',
      height: '36px',
      borderRadius: '50%',
      background: '#EDF1FB 0% 0% no-repeat padding-box',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      cursor: 'pointer',
      '& img': {
        width: '70%',
      },
    },
    '& .right-arrow': {
      transform: 'rotateY(180deg)',
      marginRight: 5,
    },
  },
  buttonClear: {
    marginRight: 6,
  },
})

const DayPicker = ({
  classes,
  value: time,
  onChange: setTime,
  disabled,
  placeholder,
  className,
  leftItem,
  isTodayButton,
  isRangeTimeLabel,
  isNextDay,
  onClear,
  disablePastDate,
  disableFutureDate,
  disableToday,
  customDisable,
  isDropDownIcon,
  ...props
}) => {
  const [openPickup, setOpenPickup] = useState(false)
  const timePickerRef = useRef(null)
  const [isToday, setisToday] = useState(false)
  const [isPast, setisPast] = useState(false)
  useEffect(() => {
    function onOutsideClick(e) {
      if (
        timePickerRef && timePickerRef.current && !timePickerRef.current.contains(e.target)
        && (typeof e.target.className?.includes === 'function'
          && !e.target.className.includes('MuiDialog'))
          && (e.target.className && !e.target.className.includes('MuiButton'))
      ) {
        setOpenPickup(false)
      }
    }

    if (!disabled) window.addEventListener('mousedown', onOutsideClick)
    return () => {
      window.removeEventListener('mousedown', onOutsideClick)
    }
  }, [timePickerRef, disabled])
  useEffect(() => {
    const today = getLocalTimeMoment().format('MM/DD/YYYY')
    setisToday(today === time)
    setisPast(moment().diff(moment(time, 'MM/DD/YYYY'), 'days') > 0)
  }, [])

  useEffect(() => {
    const today = getLocalTimeMoment().format('MM/DD/YYYY')
    setisPast(moment().diff(moment(time, 'MM/DD/YYYY'), 'days') > 0)
    setisToday(today === time)
  }, [time])

  const handleChangeDate = (val) => {
    const convertVal = val.format('MM/DD/YYYY')
    const today = getLocalTimeMoment().format('MM/DD/YYYY')
    setisToday(today === convertVal)
    setTime(convertVal)
    setOpenPickup(false)
    setisPast(moment().diff(moment(convertVal, 'MM/DD/YYYY'), 'days') > 0)
  }
  const renderView = (_, renderDefault) => (
    <div className={classes.wrapper}>
      {renderDefault()}
    </div>
  )
  const yesterday = moment().subtract(1, 'day')
  const today = moment()

  const disableDate = (current) => {
    if (disableToday) {
      return current.isAfter(today)
    }
    if (disablePastDate) {
      return current.isAfter(yesterday)
    }
    if (disableFutureDate) {
      return current.isBefore(today)
    }
    return customDisable(current)
  }

  const onBacktoToday = () => {
    const todayTemp = getLocalTimeMoment().format('MM/DD/YYYY')
    setTime(todayTemp)
    setisToday(true)
    setisPast(false)
  }

  const onPreviousDate = () => {
    const todayTemp = getLocalTimeMoment().format('MM/DD/YYYY')
    const yesterdayTemp = moment(time, 'MM/DD/YYYY')
    yesterdayTemp.subtract(1, 'days')
    setTime(yesterdayTemp.format('MM/DD/YYYY'))
    setisToday(todayTemp === moment(yesterdayTemp).format('MM/DD/YYYY'))
  }
  const onNextDate = () => {
    const tomorrow = moment(time, 'MM/DD/YYYY')
    tomorrow.add(1, 'days')
    setTime(tomorrow.format('MM/DD/YYYY'))
  }

  return (
    <div className={clsx(classes.wrapper, 'wrap-daypicker')}>
      {leftItem && <div className={classes.leftItem}>{leftItem}</div>}
      <div
        className={isPast && !isToday && isRangeTimeLabel
          ? 'past'
          : !isPast && !isToday && isRangeTimeLabel ? 'future' : ''}
        id="time-picker"
        ref={timePickerRef}
      >
        <Datetime
          {...props}
          className={clsx(classes.dateTimeContainer, className)}
          timeFormat={false}
          value={time}
          onChange={handleChangeDate}
          open={openPickup}
          isValidDate={disableDate}
          renderView={(mode, renderDefault) => renderView(mode, renderDefault)}
          renderInput={(xProps) => (
            <TextInput
              placeholder={placeholder}
              {...xProps}
              className={clsx(classes.pickupInput, xProps?.className)}
              value={time}
              onClick={(e) => {
                e.stopPropagation()
                e.preventDefault()
                if (!disabled) {
                  setOpenPickup(!openPickup)
                }
              }}
              InputProps={{
                inputProps: { readOnly: true },
                endAdornment: openPickup && isDropDownIcon
                  ? <ArrowDropUp color="primary" style={{ pointerEvents: 'none' }} />
                  : onClear && time
                    ? (
                      <IconButton
                        size="small"
                        className={classes.buttonClear}
                        onClick={(e) => {
                          e.stopPropagation()
                          onClear()
                        }}
                      >
                        <Icon
                          name="close"
                          color={theme.color.primaryBlue}
                          size={10}
                        />
                      </IconButton>
                    )
                    : isDropDownIcon && <KeyboardArrowDown color={disabled ? 'disabled' : 'primary'} style={{ pointerEvents: 'none' }} />,
              }}
              disabled={disabled}
            />
          )}
        />
      </div>
      {isTodayButton && <Button className="button-today" onClick={() => onBacktoToday()} disabled={isToday}>Today</Button>}

      {
        isRangeTimeLabel
        && (
        <div className={`rangeDate ${isPast && !isToday
          ? 'past'
          : !isPast && !isToday ? 'future' : 'today-range'}`}
        >
          <div className="clock-range">
            <svg
              id="Group_11771"
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              viewBox="0 0 79.23 78.594"
            >
              <path
                id="Path_3418"
                d="M73.437,78.44H7a6.021,6.021,0,0,1-6-6V6A6.021,6.021,0,0,1,7,0H73.437a6.021,6.021,0,0,1,6,6V72.437A6.021,6.021,0,0,1,73.437,78.44Z"
                transform="translate(-0.21)"
                fill="transparent"
              />
              <rect
                id="Rectangle_2159"
                width="78.594"
                height="78.594"
                transform="translate(0)"
                fill="none"
              />
              <rect
                id="Rectangle_2160"
                width="78.594"
                height="78.594"
                transform="translate(0)"
                fill="none"
              />
              <g id="Group_7426" transform="translate(36.645 23.38)">
                <path
                  className={`${isPast ? 'past' : 'future'}`}
                  id="Path_3419"
                  d="M100.651,85.472a2.391,2.391,0,0,1-1.7-.7L91.066,76.88a1.621,1.621,0,0,1-.476-1.145V60.92a2.467,2.467,0,0,1,2.325-2.5,2.4,2.4,0,0,1,2.477,2.4v13.6l6.956,6.956a2.4,2.4,0,0,1-1.7,4.1Z"
                  transform="translate(-90.59 -58.417)"
                />
              </g>
              <path
                className={`${isPast ? 'past' : 'future'}`}
                id="Path_3420"
                d="M74.464,55.455a25.381,25.381,0,0,0-50.278,4.526l-1.657-1.541a2.4,2.4,0,1,0-3.27,3.518l6.547,6.087a1.766,1.766,0,0,0,2.5-.092l6.019-6.475a2.47,2.47,0,0,0,.112-3.3,2.4,2.4,0,0,0-3.562-.044l-1.885,2.029a20.575,20.575,0,1,1,9.117,17.289,2.4,2.4,0,0,0-2.677,3.99A25.382,25.382,0,0,0,74.464,55.455Z"
                transform="translate(-10.702 -20.98)"
              />
            </svg>
          </div>

          <div className="text-range">
            {isPast ? 'Past Date' : 'Future date'}
          </div>

        </div>
        )
      }
      {
        isNextDay
        && (
        <div className={classes.wrapNextDate}>
          <div onClick={() => onPreviousDate()} className="arrow-next" role="presentation">
            <img src="/icons/arrowLeft.svg" alt="" />
          </div>
          <div onClick={() => onNextDate()} className="arrow-next right-arrow" role="presentation">
            <img src="/icons/arrowLeft.svg" alt="" />
          </div>
        </div>
        )
      }

    </div>
  )
}

DayPicker.defaultProps = {
  disabled: false,
  placeholder: '',
  value: '',
  onChange: () => {},
  className: '',
  leftItem: null,
  isTodayButton: false,
  isRangeTimeLabel: false,
  isNextDay: false,
  onClear: null,
  disablePastDate: false,
  disableFutureDate: false,
  disableToday: false,
  customDisable: () => true,
  isDropDownIcon: true,
}
DayPicker.propTypes = {
  classes: PropTypes.shape().isRequired,
  disabled: PropTypes.bool,
  isTodayButton: PropTypes.bool,
  isRangeTimeLabel: PropTypes.bool,
  isNextDay: PropTypes.bool,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  className: PropTypes.string,
  leftItem: PropTypes.node,
  onClear: PropTypes.func,
  disablePastDate: PropTypes.bool,
  disableFutureDate: PropTypes.bool,
  disableToday: PropTypes.bool,
  customDisable: PropTypes.func,
  isDropDownIcon: PropTypes.bool,
}

export default withStyles(themeStyle)(DayPicker)
