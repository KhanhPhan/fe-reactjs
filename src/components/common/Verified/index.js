import React from 'react'
import PropTypes from 'prop-types'
import VerifiedImage from 'assets/images/icons/verified.svg'
import NotVerifiedImage from 'assets/images/icons/not-verified.svg'

const Verified = ({ notVerified }) => (
  <img
    style={{ marginBottom: -3 }}
    src={notVerified ? NotVerifiedImage : VerifiedImage}
    alt="verified"
  />
)

Verified.defaultProps = {
  notVerified: false,
}
Verified.propTypes = {
  notVerified: PropTypes.bool,
}

export default Verified
