/* eslint-disable guard-for-in */
import { useEffect } from 'react'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

const FormikErrorFocus = ({ callback }) => {
  const { isSubmitting, isValidating, errors } = useFormikContext()
  useEffect(() => {
    const keys = Object.keys(errors)
    if (keys.length > 0 && isSubmitting && !isValidating) {
      if (Array.isArray(errors[keys[0]])) {
        const arrErr = errors[keys[0]]
        const { length } = arrErr
        let isDone = false
        for (let i = 0; i < length; i += 1) {
          if (arrErr[i]) {
            // eslint-disable-next-line no-restricted-syntax
            for (const el in arrErr[i]) {
              const id = `${keys[0]}[${i}].${el}`
              const errorElement = document.querySelector(`[name="${id}"]`)
              if (errorElement) {
                setTimeout(() => {
                  errorElement.scrollIntoView({ behavior: 'smooth', block: 'center' })
                }, [500])
                isDone = true
                callback()
                break
              }
            }
            if (isDone) {
              break
            }
          }
        }
      } else {
        const errorElement = document.querySelector(`[name="${keys[0]}"]`)
        if (errorElement) {
          setTimeout(() => {
            errorElement.scrollIntoView({ behavior: 'smooth', block: 'center' })
          }, [500])
          callback()
        }
      }
    }
  }, [isSubmitting, isValidating, errors])
  return null
}
FormikErrorFocus.defaultProps = {
  callback: () => null,
}

FormikErrorFocus.propTypes = {
  callback: PropTypes.func,
}
export default FormikErrorFocus
