/* eslint-disable react/no-array-index-key */
/* eslint-disable no-nested-ternary */
import React, {
  useRef, useEffect, useState, useMemo, useCallback,
} from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import {
  Table as ThemeTable, TableHead, TableRow, TableCell,
  withStyles, TableBody,
  TableFooter, CircularProgress, TableContainer, Divider,
} from '@material-ui/core'
import {
  ExpandMore, ExpandLess, ArrowDropDown, KeyboardArrowDown,
} from '@material-ui/icons'
import debounce from 'lodash/debounce'
import { useLocation } from 'react-router-dom'
import Checkbox from 'components/common/Checkbox'
import Pagination from 'components/common/Pagination'
import Icon from 'components/common/Icon'
import Tooltip from 'components/common/Tooltip'

import { DEFAULT_PAGE_SIZE } from 'constants/settings'

import Skeleton from 'components/common/Skeleton'

import useFirstLoadingDetection from 'utils/hooks/useFirstLoadingDetection'
import { goToTop } from 'utils/scrollUp'
import style from './style'

const Table = ({
  classes, selected, className, noHightlight,
  columns, footerColumns, data: dataProps, renderExpandRow, footerData,
  onSelectAll,
  onSelectOne,
  onChangePage,
  page, perPage, total,
  loading, noDataStyle,
  striped,
  stickyHeader,
  sortBy, onSort,
  verticalAlignTop,
  noHrBorder,
  customColumns,
  isNotShowNoResult,
  getCellStyle,
  simple,
  headerClasses,
  noResultText,
  skeletonLoading,
  onRowClick,
  clickableRows,
  sections,
  isLoadingMore,
  showEndOfResults,
  isEndOfResults,
}) => {
  const firstLoading = useFirstLoadingDetection([loading])

  const data = (firstLoading && skeletonLoading)
    ? new Array(perPage || 10)
      .fill(columns.reduce((acc, cur) => Object.assign(acc, {
        [cur.id]: cur.skeleton || <Skeleton randomWidthPercent={[50, 100]} />,
      }), {}))
    : dataProps && dataProps.find((item) => Object.values(item).length > 0)
      ? dataProps
      : [{ id: '_noData' }]

  const visibleColumns = columns.filter((col) => col.hidden !== true)

  const containerRef = useRef(null)
  const { pathname, state } = useLocation()
  const bodyRef = useRef(null)
  const [showStickySeparator, setShowStickySeparator] = useState(false)

  const calculateWidth = () => {
    const containerWidth = containerRef?.current?.offsetWidth || 0
    let colsWidth = 0
    if (bodyRef?.current?.children?.[0]?.children) {
      const cols = Object.values(bodyRef?.current?.children?.[0]?.children)
      colsWidth = cols.reduce((acc, cur) => acc + cur.offsetWidth, 0)
    }

    if (containerWidth < colsWidth) {
      setShowStickySeparator(true)
    } else {
      setShowStickySeparator(false)
    }
  }
  const locationFrom = state?.from || ''

  useEffect(() => {
    if (!isLoadingMore
      && !locationFrom.includes('modal') && !new RegExp(/\/warehouses\//g).test(pathname)) {
      goToTop('content')
      goToTop('content-under-sticky')
      containerRef.current.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
    }
  }, [dataProps, locationFrom])

  useEffect(() => {
    calculateWidth()

    const resize = debounce(() => {
      calculateWidth()
    }, 200)

    window.addEventListener('resize', resize)

    return () => window.removeEventListener('resize', resize)
  }, [])

  const renderHeadCell = ({
    id, title, cellProps, cellHeaderProps, cellStyle,
    sticky, sortable, tooltip, noDisplay,
  }) => {
    switch (id) {
      case '_checkbox':
        return (
          <>
            {onSelectAll ? (
              <TableCell
                key={id}
                className={clsx(
                  classes.tableCell,
                  classes.tableHeadCell,
                  classes.checkboxCell,
                  { [classes.stickyCell]: sticky },
                )}
                style={{ ...cellStyle, ...(sticky || {}) }}
                {...(cellProps || {})}
                {...(cellHeaderProps || {})}
              >
                {(firstLoading && skeletonLoading)
                  ? (
                    <Skeleton
                      variant="rect"
                      width={23}
                      height={23}
                      className={classes.checkboxSkeleton}
                    />
                  )
                  : dataProps && dataProps.find((item) => Object.values(item).length > 0) ? (
                    <Checkbox
                      className={classes.checkbox}
                      checked={
                      selected.length === data.filter((x) => Object.keys(x).length > 0).length
                    }
                      onChange={onSelectAll}
                    />
                  ) : ''}
              </TableCell>
            ) : <div />}
          </>

        )

      case '_cat':
        return (
          <TableCell
            key={id}
            className={clsx(
              classes.tableCell,
              classes.tableHeadCell,
              classes.checkboxCell,
              { [classes.stickyCell]: sticky },
            )}
            style={{ ...cellStyle, ...(sticky || {}) }}
            {...(cellProps || {})}
            {...(cellHeaderProps || {})}
          >
            <div className={classes.xCatHeadCell}>
              {
              dataProps && dataProps.find((item) => Object.values(item).length > 0)
                ? (firstLoading && skeletonLoading
                  ? (
                    <Skeleton
                      variant="rect"
                      width={23}
                      height={23}
                      className={classes.checkboxSkeleton}
                      style={{ display: 'inline-block', marginRight: 32 }}
                    />
                  )
                  : (
                    <Checkbox
                      className={classes.checkbox}
                      checked={
                        selected.length === data.filter((x) => Object.keys(x).length > 0).length
                      }
                      onChange={onSelectAll}
                    />
                  )
                ) : ''
            }
              <span className={classes.xCatImageText}>Image</span>
              <span
                role="presentation"
                className={classes.sortLabel}
                onClick={() => {
                  const catId = 'name'
                  let desc = 0
                  if (!sortBy[catId]) {
                    desc = -1
                  } else if (sortBy[catId] === -1) {
                    desc = 1
                  }
                  onSort(catId, desc)
                }}
              >
                {title}
                {!sortBy.name
                  ? <span className={classes.sortableIcon} />
                  : (
                    <ArrowDropDown className={clsx(
                      classes.sortIcon,
                      { [classes.sortIconDesc]: sortBy.name === 1 },
                    )}
                    />
                  )}

              </span>
            </div>

          </TableCell>
        )

      default:
        return (
          noDisplay ? <></> : (
            <TableCell
              key={id}
              className={clsx(
                classes.tableCell,
                classes.tableHeadCell,
                {
                  [classes.stickyCell]: sticky,
                  [classes.nowrap]: tooltip,
                  [classes.simpleSticky]: simple,
                },
              )}
              style={{ ...cellStyle, ...(sticky || {}) }}
              {...(cellProps || {})}
              {...(cellHeaderProps || {})}
            >
              { sortable
                ? (
                  <span
                    role="presentation"
                    className={classes.sortLabel}
                    onClick={() => {
                      let desc = 0
                      if (!sortBy[id]) {
                        desc = -1
                      } else if (sortBy[id] === -1) {
                        desc = 1
                      }
                      onSort(id, desc)
                    }}
                  >
                    {title}
                    {!sortBy[id]
                      ? <span className={classes.sortableIcon} />
                      : (
                        <ArrowDropDown className={clsx(
                          classes.sortIcon,
                          { [classes.sortIconDesc]: sortBy[id] === 1 },
                        )}
                        />
                      )}
                  </span>
                )
                : title}
              {tooltip && (
              <Tooltip
                content={tooltip}
                placement="right"
                size={10}
                icon={<img src="/icons/question.svg" alt="info" />}
                className={classes.tooltip}
              />
              )}
            </TableCell>
          )
        )
    }
  }
  const renderCell = (column, item, index, getCellStyleProps) => {
    const customStyle = getCellStyleProps ? getCellStyleProps(item, column, index) : {}
    switch (column.id) {
      case '_checkbox':
        return (
          <TableCell
            key={`cell_${index}_${column.id}`}
            className={clsx(
              classes.tableCell,
              classes.checkboxCell,
              { [classes.stickyCell]: column.sticky },
            )}
            style={{ ...column.cellStyle, ...customStyle, ...(column.sticky || {}) }}
            {...(column.cellProps || {})}
          >
            {Object.keys(item).length > 0
              ? (
                <>
                  {(firstLoading && skeletonLoading)
                    ? (
                      <Skeleton
                        variant="rect"
                        width={23}
                        height={23}
                        className={classes.checkboxSkeleton}
                      />
                    )
                    : (
                      <Checkbox
                        className={classes.checkbox}
                        checked={selected.indexOf(item.id) !== -1}
                        onChange={(event) => onSelectOne(event, item.id)}
                      />
                    )}
                </>
              )
              : ''}
          </TableCell>
        )
      case '_empty':
        return (
          <TableCell
            key={`cell_${index}_${column.id}`}
            className={clsx(
              classes.tableCell,
              classes.emptyItem,
              { [classes.stickyCell]: column.sticky },
            )}
            style={{ ...column.cellStyle, ...customStyle, ...(column.sticky || {}) }}
            {...(column.cellProps || {})}
          >
          &nbsp;
          </TableCell>
        )
      case '_more':
        return (
          <TableCell
            key={`cell_${index}_${column.id}`}
            className={clsx(
              classes.tableCell,
              classes.moreItem,
              { [classes.stickyCell]: column.sticky },
            )}
            style={{ ...column.cellStyle, ...customStyle, ...(column.sticky || {}) }}
            {...(column.cellProps || {})}
            onClick={(event) => onSelectOne(event, item.id)}
          >
            {selected.indexOf(item.id) !== -1 ? <ExpandLess /> : <ExpandMore /> }
          </TableCell>
        )

      case '_cat':
        return (
          <TableCell
            key={`cell_${index}_${column.id}`}
            className={clsx(
              classes.tableCell,
              {
                [classes.stickyCell]: column.sticky,
                [classes.xCatDescendant]: item.level > 1,
              },
            )}
            style={{
              ...column.cellStyle,
              ...customStyle,
              ...(column.sticky || {}),
            }}
            {...(column.cellProps || {})}
          >
            <div
              className={classes.xCatCell}
              style={{
                paddingLeft: (item.level && item.level > 1) ? (item.level - 2) * 47 + 12 : 0,
              }}
            >
              {item.level > 1
              && <img className={classes.xCatEnterIcon} src="/icons/enter.svg" alt="" />}
              {(firstLoading && skeletonLoading)
                ? (
                  <Skeleton
                    variant="rect"
                    width={23}
                    height={23}
                    className={classes.checkboxSkeleton}
                    style={{ display: 'inline-block', marginRight: 32 }}
                  />
                )
                : (
                  <Checkbox
                    className={classes.checkbox}
                    checked={selected.indexOf(item.id) !== -1}
                    onChange={(event) => onSelectOne(event, item.id)}
                  />
                )}

              {column.prefix || ''}
              {(firstLoading && skeletonLoading)
                ? (
                  <>
                    <Skeleton variant="rect" width={60} height={60} />
                    <div style={{ display: 'flex', flexDirection: 'column', marginLeft: 20 }}>
                      <Skeleton randomWidth={[50, 300]} />
                      <div style={{
                        display: 'flex', width: 170, justifyContent: 'space-between', marginTop: 10,
                      }}
                      >
                        <Skeleton variant="rect" width={40} height={20} />
                        <Skeleton variant="rect" width={70} height={20} />
                        <Skeleton variant="rect" width={50} height={20} />
                      </div>
                    </div>
                  </>
                )
                : column.renderComponent(item, column, index)}
            </div>
          </TableCell>
        )

      default:
        if (column.renderComponent && !(firstLoading && skeletonLoading)) {
          return (
            <TableCell
              key={`cell_${index}_${column.id}`}
              className={clsx(
                classes.tableCell,
                { [classes.stickyCell]: column.sticky },
              )}
              style={{ ...column.cellStyle, ...customStyle, ...(column.sticky || {}) }}
              {...(column.cellProps || {})}
            >
              {column.prefix || ''}
              {column.renderComponent(item, column, index)}
            </TableCell>
          )
        }
        return (
          <TableCell
            key={`cell_${index}_${column.id}`}
            colSpan={column.colSpan}
            className={clsx(
              classes.tableCell,
              { [classes.stickyCell]: column.sticky },
            )}
            style={{ ...column.cellStyle, ...customStyle, ...(column.sticky || {}) }}
            {...(column.cellProps || {})}
          >
            {item[column.id] !== undefined ? column.prefix : ''}
            {item[column.id] !== undefined ? item[column.id] : ''}
          </TableCell>
        )
    }
  }

  const clickableRowIds = useMemo(() => {
    if (onRowClick) {
      if (clickableRows.length > 0) {
        return clickableRows
      }
      return data.map((item) => item.id)
    }
    return []
  }, [clickableRows, data, onRowClick])

  const isClickable = useCallback(
    (item) => clickableRowIds.includes(item.id),
    [clickableRowIds],
  )

  const renderRows = (items) => (
    <>
      {items.filter((i) => !i.hidden).map((item, index) => {
        if (renderExpandRow && selected.indexOf(item.id) !== -1) {
          return (
            <>
              <TableRow
                key={item.id}
                className={classes.tableRow}
                selected={!noHightlight && selected.indexOf(item.id) !== -1}
              >
                { customColumns(
                  visibleColumns, item, index,
                ).map((column) => renderCell(column, item, index, getCellStyle)) }
              </TableRow>
              {
                renderExpandRow(renderCell, item, index)
              }
            </>
          )
        }
        if (isNotShowNoResult) {
          return null
        }
        if (item.id === '_noData') {
          return (
            <TableRow key={item.id} className={classes.tableRow}>
              <TableCell
                key="noData"
                colSpan={visibleColumns.length}
                style={noDataStyle}
                className={classes.noDataCell}
              >
                {noResultText}
              </TableCell>
            </TableRow>
          )
        }
        return (
          <TableRow
            key={(!item.id || skeletonLoading)
              ? `row_${index}`
              : item.id}
            className={clsx(classes.tableRow, { [classes.clickable]: isClickable(item) })}
            selected={!noHightlight && selected.indexOf(item.id) !== -1}
            onClick={(e) => (isClickable(item) ? onRowClick(e, item) : {})}
          >
            {customColumns(
              visibleColumns, item, index,
            ).map((column) => renderCell(column, item, index, getCellStyle))}
          </TableRow>
        )
      })}

    </>
  )

  return (
    <>
      <div className={classes.tableOutermost}>
        {loading && !(firstLoading && skeletonLoading) && (
          <div className={(classes.loadingContainer)}>
            <CircularProgress size={30} />
          </div>
        )}

        <TableContainer
          id="table-container"
          className={clsx(classes.tableContainer, {
            [className]: className,
            [classes.noHrBorder]: noHrBorder,
            [classes.noBorderTop]: simple,
            [classes.stickySeparator]: showStickySeparator,
          })}
          ref={containerRef}
        >
          <ThemeTable
            className={clsx(classes.table, {
              [classes.striped]: striped,
              [classes.stickyHeader]: stickyHeader,
              [classes.verticalAlignTop]: verticalAlignTop,
            })}
          >
            {visibleColumns.some((col) => col.title)
          && (
            <TableHead>
              <TableRow
                key="header"
                className={clsx(classes.tableRow, headerClasses, {
                  [classes.simpleHeader]: simple,
                })}
              >
                {
                  visibleColumns.map((props) => renderHeadCell(props))
                }
              </TableRow>
            </TableHead>
          )}
            <TableBody ref={bodyRef}>
              {!firstLoading
                && sections.length !== 0
                && !data.find((item) => item.id === '_noData')
                && sections.map((section, sectionId) => {
                  const validatedItems = data.filter(section.condition)
                  return (
                    <>
                      <TableRow key={sectionId}>
                        <TableCell colSpan={visibleColumns.length} className={classes.tableCell}>
                          <div className={classes.sectionHeader}>
                            <Divider className={classes.sectionDivider} />
                            {section.isExpandable && (
                              <div
                                className={classes.sectionButton}
                                onClick={section.onChangeExpand}
                                role="presentation"
                              >
                                <span>{section.label}</span>
                                <KeyboardArrowDown
                                  color="primary"
                                  className={classes.sectionBtnArr}
                                  style={{
                                    transform: section.isExpand
                                      ? 'rotate(180deg)' : 'rotate(0)',
                                  }}
                                />
                              </div>
                            )}
                            {!section.isExpandable
                            && <span className={classes.sectionLabel}>{section.label}</span>}
                            <Divider className={classes.sectionDivider} />
                          </div>
                        </TableCell>
                      </TableRow>
                      {(!section.isExpandable || section.isExpand) && renderRows(validatedItems)}
                    </>
                  )
                })}
              {(!sections.length || data.find((item) => item.id === '_noData') || firstLoading)
              && renderRows(data)}
              {isLoadingMore && (
                <TableRow key="loading-more-row">
                  <TableCell
                    colSpan={visibleColumns.length}
                    className={clsx(classes.tableCell, classes.loadingMoreRow)}
                  >
                    <div className={classes.loadingMoreContainer}>
                      <div className={classes.loadingIcon}>
                        <Icon name="loader" size={21} />
                      </div>
                      Loading
                    </div>
                  </TableCell>
                </TableRow>
              )}
              {showEndOfResults
              && isEndOfResults && !data.find((item) => item.id === '_noData')
              && (
                <TableRow key="end-of-results-row">
                  <TableCell colSpan={visibleColumns.length} className={classes.tableCell}>
                    <div className={classes.sectionHeader}>
                      <span className={classes.sectionLabel}>End of results</span>
                    </div>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
            {
              footerData && (
                <TableFooter>
                  <TableRow key="footer" className={classes.tableRow}>
                    {footerColumns.map(
                      (column) => renderCell(column, footerData, data.length, getCellStyle),
                    )}
                  </TableRow>
                </TableFooter>
              )
            }
          </ThemeTable>
        </TableContainer>
      </div>
      {
        onChangePage && (
          <Pagination
            total={total}
            page={page}
            perPage={perPage}
            onChangePage={onChangePage}
            loading={loading}
          />
        )
      }
    </>
  )
}

Table.defaultProps = {
  className: '',
  selected: [],
  columns: [],
  renderExpandRow: null,
  footerData: null,
  footerColumns: [],
  data: [],
  onSelectAll: null,
  onSelectOne: () => {},
  onChangePage: null,
  noHightlight: false,
  noDataStyle: {},
  page: 0,
  perPage: DEFAULT_PAGE_SIZE,
  total: 0,
  loading: false,
  striped: false,
  stickyHeader: false,
  sortBy: {},
  onSort: () => {},
  verticalAlignTop: false,
  noHrBorder: false,
  customColumns: (c) => c,
  isNotShowNoResult: false,
  getCellStyle: () => {},
  simple: false,
  headerClasses: '',
  noResultText: 'No results found.',
  skeletonLoading: false,
  onRowClick: null,
  clickableRows: [],
  sections: [],
  isLoadingMore: false,
  showEndOfResults: false,
  isEndOfResults: false,
}

Table.propTypes = {
  classes: PropTypes.shape().isRequired,
  className: PropTypes.string,
  columns: PropTypes.arrayOf(PropTypes.shape()),
  renderExpandRow: PropTypes.func,
  footerData: PropTypes.shape(),
  footerColumns: PropTypes.arrayOf(PropTypes.shape()),
  data: PropTypes.arrayOf(PropTypes.shape()),
  selected: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.shape(),
    PropTypes.number,
  ])),
  onSelectAll: PropTypes.func,
  onSelectOne: PropTypes.func,
  onChangePage: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.oneOf([null]),
  ]),
  page: PropTypes.number,
  perPage: PropTypes.number,
  total: PropTypes.number,
  noHightlight: PropTypes.bool,
  loading: PropTypes.bool,
  noDataStyle: PropTypes.shape(),
  striped: PropTypes.bool,
  stickyHeader: PropTypes.bool,
  sortBy: PropTypes.shape(),
  onSort: PropTypes.func,
  verticalAlignTop: PropTypes.bool,
  noHrBorder: PropTypes.bool,
  customColumns: PropTypes.func,
  isNotShowNoResult: PropTypes.bool,
  getCellStyle: PropTypes.func,
  simple: PropTypes.bool,
  headerClasses: PropTypes.string,
  noResultText: PropTypes.oneOf([
    PropTypes.string, PropTypes.node,
  ]),
  skeletonLoading: PropTypes.bool,
  onRowClick: PropTypes.func,
  clickableRows: PropTypes.arrayOf(PropTypes.string),
  sections: PropTypes.arrayOf(PropTypes.shape()),
  isLoadingMore: PropTypes.bool,
  showEndOfResults: PropTypes.bool,
  isEndOfResults: PropTypes.bool,
}

export default withStyles(style)(Table)
