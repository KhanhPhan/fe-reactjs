const style = (theme) => ({
  tableOutermost: {
    position: 'relative',
    width: '100%',
  },
  tableContainer: {
    borderTop: `1px solid ${theme.color.disableGray1}`,
    borderBottom: `1px solid ${theme.color.disableGray1}`,
    boxSizing: 'border-box',
    position: 'relative',
    zIndex: 1,

    '&::-webkit-scrollbar': {
      width: 5,
      height: 5,
    },
    '&::-webkit-scrollbar-track': {
      borderRadius: 10,
      backgroundColor: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      borderRadius: 10,
      backgroundColor: theme.color.secondaryBlue2,
    },
  },
  noHrBorder: {
    border: 'none',
  },
  noBorderTop: {
    borderTop: 'none',
  },
  table: {
  },
  tableRow: {
  },
  tableCell: {
    borderBottom: `1px solid ${theme.color.disableGray1}`,
    fontWeight: 400,
    color: theme.color.darkGray,
    padding: theme.spacing(1),
    position: 'relative',

    '.MuiTableBody-root >.MuiTableRow-root:last-child &': {
      borderBottom: 'none',
    },
    '&:first-child': {
      paddingLeft: theme.spacing(2.5),
    },
    '&:last-child': {
      paddingRight: theme.spacing(2.5),
    },
  },
  tableHeadCell: {
    whiteSpace: 'nowrap',
    padding: theme.spacing(1),
    borderBottom: 'none',
    fontWeight: 500,
  },
  smallHeader: {
    fontSize: 12,
  },
  striped: {
    '& $tableRow:nth-child(even) $tableCell': {
      backgroundColor: theme.color.secondaryBlue9,
    },
    '& $tableCell': {
      borderBottom: 'none',
    },
  },
  checkboxCell: {
    width: 40,
    paddingRight: 0,
  },
  checkbox: {
    margin: 0,
    display: 'inline-block',
  },
  stickyHeader: {
    '& $tableHeadCell': {
      position: 'sticky',
      top: 0,
      zIndex: 2,
      backgroundColor: '#fff',
    },
  },
  stickyCell: {
    position: 'sticky',
    zIndex: 1,
    padding: theme.spacing(1.5, 2),
    backgroundColor: '#FFF',

    '&$tableHeadCell': {
      zIndex: 3,
    },
  },
  stickySeparator: {
    '& $stickyCell': {
      backgroundImage:
        `linear-gradient(to right, ${theme.color.grayBorder2}, ${theme.color.grayBorder2})`,
      backgroundSize: '1px 100%',
      backgroundRepeat: 'no-repeat',
    },
  },
  verticalAlignTop: {
    '& $tableCell': {
      verticalAlign: 'top',
    },
  },
  sortLabel: {
    display: 'inline-flex',
    alignItems: 'flex-start',
    cursor: 'pointer',
  },
  sortIcon: {
    transition: 'all .2s ease-in-out',
  },
  sortIconDesc: {
    transform: 'rotate(180deg)',
  },
  sortableIcon: {
    display: 'block',
    width: 18,
    height: 24,
    position: 'relative',
    opacity: 0.3,
    '&:before': {
      content: '""',
      width: 0,
      height: 0,
      borderBottom: '5px solid',
      borderLeft: '5px solid transparent',
      borderRight: '5px solid transparent',

      position: 'absolute',
      top: 'calc(50% - 7px)',
      right: 0,
    },
    '&:after': {
      content: '""',
      width: 0,
      height: 0,
      borderTop: '5px solid',
      borderLeft: '5px solid transparent',
      borderRight: '5px solid transparent',

      position: 'absolute',
      top: 'calc(50% + 1px)',
      right: 0,
    },
  },

  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end',
  },
  moreItem: {
    cursor: 'pointer',
    width: 24,
  },
  emptyItem: {
    borderBottom: 'none',
  },
  noDataCell: {
    height: 56,
    textAlign: 'center',
    verticalAlign: 'text-top',
    paddingTop: 30,
    opacity: 0.5,
    borderBottom: 'none',
  },
  loadingContainer: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 9,
    background: 'rgba(256, 256, 256, 0.7)',
  },
  tooltip: {
    marginLeft: 8,
  },
  nowrap: {
    whiteSpace: 'nowrap',
  },

  // start style for category list
  xCatHeadCell: {
    '& > label': {
      marginRight: theme.spacing(2),
    },
  },

  xCatImageText: {
    marginRight: theme.spacing(4.5),
  },

  xCatEnterIcon: {
    width: 16,
    height: 16,
    marginRight: 20,
  },

  xCatCell: {
    display: 'flex',
    alignItems: 'center',
    '& > label': {
      marginRight: theme.spacing(2),
      flex: '0 0 40px',
    },
  },

  xCatDescendant: {
    borderTop: `1px solid ${theme.color.disableGray1}`,
    // backgroundColor: theme.color.secondaryBlue9,
    '& ~ td': {
      borderTop: `1px solid ${theme.color.disableGray1}`,
      // backgroundColor: theme.color.secondaryBlue9,
    },
  },
  // end style for category list

  simpleHeader: {
    background: theme.color.secondaryBlue3,
    borderRadius: 5,
    '& > th': {
      '&:first-child': {
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
      },
      '&:last-child': {
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
      },
      padding: '6px 12px',
    },
  },
  simpleSticky: {
    background: 'inherit',
  },

  checkboxSkeleton: {
    transform: 'translate(4px, 4px)',
  },
  clickable: {
    cursor: 'pointer',
  },
  sectionHeader: {
    backgroundColor: theme.color.white,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sectionDivider: {
    flex: 1,
  },
  sectionLabel: {
    fontSize: 14,
    color: theme.color.midGray,
    marginRight: 16,
    marginLeft: 16,
  },
  loadingMoreRow: {
    textAlign: 'center',
    fontSize: 14,
    color: theme.color.midGray,
  },
  loadingMoreContainer: {
    display: 'inline-flex',
    alignItems: 'center',
  },
  loadingIcon: {
    marginRight: 10,
    animation: '$rotate 1.5s linear infinite',
  },
  '@keyframes rotate': {
    from: { transform: 'rotate(0deg)' },
    to: { transform: 'rotate(360deg)' },
  },
  endOfResults: {
    textAlign: 'center',
    fontSize: 14,
    color: theme.color.midGray,
  },
  sectionButton: {
    backgroundColor: theme.color.secondaryBlue3,
    borderRadius: 16,
    fontSize: 14,
    padding: '6px 20px',
    display: 'flex',
    alignItems: 'center',
    marginRight: 16,
    marginLeft: 16,
    '& > *:not(:last-child)': {
      marginRight: 5,
    },
    whiteSpace: 'nowrap',
    [theme.breakpoints.down(767)]: {
      margin: 'auto',
    },
    '&:hover': {
      cursor: 'pointer',
      boxShadow: '0px 2px 6px #2933C533',
    },
  },
  sectionBtnArr: {
    transition: 'transform 0.2s',
  },
})

export default style
