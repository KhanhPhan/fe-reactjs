/* eslint-disable react/prop-types */
import React from 'react'
import ReactDOM from 'react-dom'
import 'assets/styles/index.scss'
import { ThemeProvider } from '@material-ui/core/styles'
import { Provider } from 'react-redux'
import {
  BrowserRouter, Route, Switch,
} from 'react-router-dom'
import { store } from 'redux/store'
import theme from './theme'
import Routers from './routers'
import Layout from './layouts'

const App = () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Layout>
            {Routers.map((router) => (
              <Route
                key={router.path}
                exact={router.exact}
                path={router.path}
                component={(props) => (
                  <router.component {...props} {...props.match.params} />
                )}
              />
            ))}
          </Layout>
        </Switch>
      </BrowserRouter>
    </Provider>

  </ThemeProvider>
)

ReactDOM.render(<App />, document.getElementById('root'))
